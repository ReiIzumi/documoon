import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';
import useBaseUrl from '@docusaurus/useBaseUrl';
import Link from '@docusaurus/Link';
import Translate, {translate} from '@docusaurus/Translate';

const FeatureList = [
  {
    title: 'Blog "En la jaula de un Geek"',
    docLink: '/blog',
    imageUrl: 'img/projects/blog.jpg',
    description: (
      <Translate
        id="homepage.projects.blog.desc">
        En la jaula de un Geek es un blog de informática que inicié para documentar 
        todas las instalaciones y configuraciones que he ido haciendo en mi propia red 
        a lo largo de los años.
      </Translate>
    ),
  },
  {
    title: 'Sora Project',
    docLink: '/docs/sora/introduction',
    imageUrl: 'img/projects/sora.jpg',
    description: (
      <Translate
        id="homepage.projects.sora.desc">
        Tras un tiempo utilizando Kubernetes, descubres que necesitas más piezas y más automatización.
        Sora nace a partir de esa idea, unificar todos los sistemas en uno.
      </Translate>
    ),
  },
  {
    title: 'Proxmox',
    docLink: '/docs/it/proxmox/',
    imageUrl: 'img/projects/proxmox.png',
    description: (
      <Translate
        id="homepage.projects.proxmox.desc">
        ¡Larga vida a la Infrastructure as a Code (IaC)! Proxmox para virtualizar máquinas, 
        Terraform para gestionar esas máquinas y un poco de métricas al conjunto.
      </Translate>
    ),
  },
  {
    title: 'K8s Project',
    docLink: '/docs/k8s/introduction',
    imageUrl: 'img/projects/k8s.png',
    description: (
      <Translate
        id="homepage.projects.k8s.desc">
        Tras Docker y Docker Swarm, la siguiente vía es Kubernetes (K8s). Pods auto gestionados,
        caídas totales del clúster y preguntas sobre tu existencia. ¿Quién dijo falta de diversión?
      </Translate>
    ),
  },
  {
    title: 'Kirin',
    codeLink: 'https://gitlab.com/moon-kirin',
    imageUrl: 'img/projects/kirin.png',
    description: (
      <Translate
        id="homepage.projects.kirin.desc">
        Gestionar la economía no es sencillo. En este servicio se define anualmente varios 
        monederos para gastos y ahorros. Al introducir los gastos permite conocer cuánto dinero 
        queda en cada uno de ellos.
      </Translate>
    ),
  },
  {
    title: 'Toranku',
    codeLink: 'https://gitlab.com/moon-toranku',
    imageUrl: 'img/projects/toranku.png',
    description: (
      <Translate
        id="homepage.projects.toranku.desc">
        Varios servicios necesitan agrupar sus secciones según categorías, Toranku 
        es un gestor de categorías utilizando mediante una API REST. Cada servicio se 
        autentica por OIDC y dispone de sus propias categorías.
      </Translate>
    ),
  },
  {
    title: 'Shizen Warhammer',
    docLink: 'https://shizen.moon.cat/',
    codeLink: 'https://gitlab.com/ReiIzumi/shizen',
    imageUrl: 'img/projects/shizen.jpg',
    description: (
      <Translate
        id="homepage.projects.shizen.desc">
        Shizen es una web generadora de escenarios para Warhammer Fantasy que permite separar 
        el terreno como una cuadricula o utilizar la regla oficial de 1D6+4 elementos de 
        escenografía.
      </Translate>
    ),
  },
  {
    title: translate({
      id: 'homepage.projects.book',
      message: 'Libro'
    }),
    docLink: 'docs/general/book',
    imageUrl: 'img/projects/book.jpg',
    description: (
      <Translate
        id="homepage.projects.book.desc">
        "Aquellos destinos interconectados que nunca se hacen realidad" es el nombre de 
        mi segundo libro aun en creación. Cuatro protagonistas tendrán que encontrar su 
        sitio en el mundo...
      </Translate>
    ),
  },
];

function Feature({imageUrl, title, description, docLink, codeLink}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <div className="projectBox">
        <h3>{title}</h3>
        <p>{description}</p>
        <div className={styles.buttonsLine}>
        {docLink && (
          <div className={styles.buttons}>
          <Link
            className={clsx(
              'button button--outline button--secondary button--lg',
              styles.getStarted,
            )}
            to={docLink}>
            <Translate>Abrir</Translate>
          </Link>
          </div>
        )}
        {codeLink && (
          <div className={styles.buttons}>
          <Link
            className={clsx(
              'button button--outline button--secondary button--lg',
              styles.getStarted,
            )}
            to={codeLink}>
            <Translate>Código</Translate>
          </Link>
          </div>
        )}
        </div>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
