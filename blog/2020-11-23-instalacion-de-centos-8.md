---
title: Instalación de CentOS 8
authors: rei.izumi
tags: [CentOS]
---

Llevo tantos años siguiendo a SuSE, que ni siquiera me había planteado seriamente alguno de sus rivales, ni a nivel doméstico ni empresarial, SuSE tenía todo, una versión community y una de pago con soporte, seguía estando allá arriba junto a Red Hat.

Pero los tiempos cambian, unos se adaptan, y otros no.

En estos últimos años, he seguido utilizando OpenSuSE y viendo sus cambios de versión en solo 1 año vista, las actualizaciones que explotan destruyendo entornos de producción con pocos años, y teniendo que revisar todos los cambios de la siguiente versión antes de hacer el salto, debido a que podías llegar a perder un servidor sin darte cuenta por un motivo desconocido.

A todo esto, YaST cada vez era menos útil, debido a que los cambios tan rápidos hacían que YaST no llegara a actualizarse al mismo nivel.

Mientras, Red Hat junto a Fedora, y la versión CentOS de la comunidad han hecho unos grandes avances y cada vez han ido cogiendo más mercado, hasta el punto de luchar tu a tu contra Ubuntu en zonas domésticas, y quedarse el liderazgo en el ámbito empresarial.

Por todo esto, traigo una guía rápida para instalar CentOS.

<!-- truncate -->

## **Explicación**

Primero he de decir que las versiones oficiales son Red Hat (versión que normalmente se utiliza con suscripción) y Fedora, que cambia muy rápidamente de versión. Fedora puede ser muy buena para un desktop, pero no para un servidor donde lo más importante es la estabilidad, y Red Hat al requerir pagar, tampoco se convierte en una buena opción.

Por otro lado, tenemos CentOS, una versión community de Red Hat, muy similar a la original y con su misma estabilidad, siendo la mejor opción.

Esta es una mini-guía rápida de los pasos más importantes a seguir en la instalación para tener configurado:

- IP
- NTP
- Particiones básicas
- Sistema actualizado
- SSH con claves

A partir de aquí se puede empezar a instalar aquello que se requiera.

Podemos descargar las versiones de CentOS en la [página oficial](https://www.centos.org/centos-linux/), en el momento de escribir este artículo las versiones activas son la 7 y 8, pero debido a que la 7 terminará en 2024, prefiero apostar por la 8, que aun estará activa hasta 2029.

## **1-Instalación**

#### Language Support

Lo primero es aceptar el idioma por defecto: Inglés.

Es muy preferible mantener el idioma del sistema operativo en inglés, ya que así es más sencillo cuando tienes que buscar ejemplos o errores.

#### Keyboard

Por defecto el idioma se define en English (US), si nuestro teclado no coincide con este, añadimos el nuevo y borramos el viejo.

#### Network & Host Name

Antes de configurar cualquier servicio, hará falta configurar el nombre de máquina, IP, Gateway y DNS.

Se aplica el Host Name, nos confirmará que se ha aplicado en el Current host name.

De aquí se clica en Configure... para añadir el resto de los datos.

Vamos a la pestaña de IPv4 Settings, cambiamos el Method a Manual y rellenamos la Address (IP), Netmask si es necesario, Gateway, la lista de servidores (DNS servers) y el Search domains.

El Search domains se puede utilizar para definir el dominio de intranet y así poder hacer búsquedas únicamente por el nombre, el sistema añadirá el resto.

Una vez configurada, clicamos en Off para que este se inicie, si todo es correcto nos mostrará los datos e incluso podremos hacer ping hacia la nueva IP para confirmar.

#### Software Selection

Si la nueva máquina es un servidor, sin dudar la mejor opción es **Minimal Install** sin ningún añadido, incluso la opción Server incluye muchas aplicaciones que se pueden convertir en un problema para desinstalarlas si necesitas otras que no sean compatibles, es mejor instalar aquello que necesitemos después.

#### Time & Date

Lo primero es elegir nuestra zona horaria, podemos elegirla a mano o simplemente clicar en el mapa.

En caso de tener un servidor NTP local (algo muy aconsejable), clicamos en **Configure Network Time**, indicamos la URL, deseleccionamos el Use de la original y reiniciamos el servicio clicando dos veces en **Network Time**.

#### Installation Destination

Este es uno de los puntos donde más puede cambiar un sistema, lo primero será elegir el o los discos que utilizaremos y clicar en Storage Configuration: Custom, después de aceptar (Done), nos dará las opciones.

Lo primero es elegir el tipo de particiones entre LVM o Standard, en caso de utilizar un sistema físico, la opción de LVM puede ser la mejor opción ya que podremos fusionar la partición con futuros discos en caso de necesitarlos más adelante, si es una máquina virtual, siempre prefiero el uso de Standard ya que la máquina virtual ya tiene opciones para resolver problemas de espacio.

Sea la que sea, si clicamos en "Click here to create them automatically" después de elegir la opción deseada, nos creará las de por defecto y podemos modificar a partir de ello.

Las particiones que necesitamos son:

| Mount Point | Desired Capacity | File System |
| --- | --- | --- |
| /boot | 1024 MiB | ext4 |
| /boot/efi | 600 MiB | EFI System Partition |
| swap | 4-6 GiB RAM > 2 GiB 8-12 GiB RAM > 3 GiB 16 GiB RAM > 4 GiB 24 GiB RAM > 5 GiB 32 GiB RAM > 6 GiB | swap |
| / |  | xfs |

Los valores de tamaño son los recomendables, en caso de la swap, la recomendación depende según cuanta RAM tenga disponible. Debo recordar que la swap no es obligatoria, y en algunos casos será mejor no tenerla (por ejemplo, si el servidor será utilizado para Kubernetes).

Además de estas particiones, es posible que sea necesario crear otras (para /home si se utilizará como desktop, /opt o /var/lib según el tipo de aplicaciones que se vayan a desplegar).

Como último detalle, si hemos elegido más de un disco duro habrá que ajustar la opción en la sección de Device para que se cree la partición allá donde queremos.

#### Begin Installation

Al clicar en este botón, empezará a instalar y nos dará la opción de indicar el **Root Password**, y, en caso de que queremos, añadir un usuario (**User Creation**).

Al terminar clicamos en **Reboot** y ya estará instalado.

## **2-Configuración**

La instalación ha completado gran parte de las configuraciones típicas que vamos a necesitar para empezar,

#### SSH

Siempre se debe activar el banner en las conexiones por SSH hacia máquinas que son restringidas, esto es un tema legal y totalmente necesario, simplemente con avisar será necesario, aunque si es para una empresa sería mejor indicar más detalles según los temas legales que se tengan.

```
vi /etc/ssh/sshd_config
```

Descomentamos la línea de banner y añadimos una ruta.

```
Banner /etc/ssh/banner
```

Y creamos el fichero.

```
vi /etc/ssh/banner
```

Este es un ejemplo de banner:

```
========== ========== ==========
   Welcome to <serverName>
========== ========== ==========
You are accessing a private server
Unauthorized access is not allowed
```

Reiniciamos para aplicar los cambios.

```
systemctl restart sshd
```

Para poder acceder más fácilmente, añadimos las claves públicas que necesitemos, en mi caso las crearé con PuTTYgen, una aplicación para Windows que viene con el paquete de PuTTY, pero podemos utilizar cualquiera.

```
mkdir /root/.ssh
vi /root/.ssh/authorized_keys
```

#### Actualización

Con todo terminado, ya solo falta actualizar

```
dnf update -y && dnf autoremove -y && dnf clean all && reboot
```

Cuando termine, el sistema se reiniciará y ya estará listo para su uso.

## **Final**

El instalador de CentOS nos hace la vida realmente fácil, quitando del medio las configuraciones típicas, algo que se agradece.

En caso de provenir de SuSE, los cambios base no son tan grandes, cambiar zypper por dnf es muy rápido y ambos utilizan los ficheros RPM (que fue creado por Red Hat y SuSE absorbió).
