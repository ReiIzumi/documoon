---
title: WAS en OpenSuSE Leap 15
authors: rei.izumi
tags: [WAS, WebSphere Application Server]
---

WebSphere Application Server (WAS en adelante) es un servidor de aplicaciones Java desarrollado por IBM. Este compite contra Tomcat, JBOSS, Glassfish y otros, pero está diseñado exclusivamente para el uso empresarial permitiendo gestionar un cluster con facilidad.

Existen varias versiones de WAS, pero principalmente se dividen en la Liberty y la normal (Application Server o Application Server Network Deployment), aunque normalmente me dedico a explicar aplicaciones OpenSource o las versión gratuita de una aplicación, en este caso me decantaré por la versión no-Liberty de WAS, la cual es de pago, esta se orienta a PYMES o grandes empresas únicamente, pero debido a la carga de trabajo que requieran sus aplicaciones, WAS puede ser una gran mejora en comparación a sus rivales.

<!-- truncate -->

## **0- Previos**

WAS es una aplicación de pago, así que para su uso primero se deberá adquirir, ya sea mediante IBM o un partner de este, a partir de ello obtendremos los instalables. Además, este únciamente puede ser instalado en SuSE y Red Hat, aunque es recomendable primero mirar la licencia, al instalarlo sobre otro sistema (como OpenSuSE), se perdería la licencia. Los pasos para la instalación en SuSE 15 y OpenSuSE Leap 15 deberían ser exactamente iguales.

Debido a que existen 2 versiones (la normal y la Network Deployment que es la que tiene soporte para cluster), algunos puntos de este artículo pueden diferir ligeramente entre una y otra, en esos puntos indicaré para cual afecta, en cualquier caso, la Network Deployment tiene todo lo de la versión normal.

En un caso normal, WebSphere se encarga de las aplicaciones Java, pero es recomendable (mucho) disponer de un Apache o IBM HTTP Server (IHS en adelante) para hacer de frontal web, esto mitigará el trabajo de WebSphere ya que el frontal web se encargará de la gestión de cantidad de peticiones. Para sincronizar los proyectos Java entre WebSphere y IHS (o Apache, es totalmente compatible, aunque este artículo se escribe especificamente para IHS), se instala un Plugin, este es similar al uso de un proxy inverso, pero es propio del producto y WebSphere lo autogestiona. Toda la instalación se hará partiendo de que se desea instalar el pack entero, si únicamente se quiere WebSphere, solo será necesario obviar los puntos de IHS y Plugin.

WAS 9.0 funciona sobre IBM SDK Java7 o Java8, debido a que Java7 ya está marcado como deprecated a día de escribir este post, me decantaré por la opcíón de Java8.

Este es un software profesional, no está orientado ni a usuarios domésticos ni a pequeñas empresas (ni su precio tampoco), así que para gestionarlo normalmente se requiere a un especialista de WAS, esto pasa a ser obligatorio en caso de utilizar su versión de cluster, debido a esto, este artículo está más orientado a especialistas que ya conozcan mínimamente su uso y deseen conocer como instalar esta última versión. En la instalación explicaré como instalar WebSphere con IHS y SSL activo, y que WebSphere se conecte a un LDAP, lo cual suele ser los mínimos para funcionar, el despliegue, configuración y mantenimiento de aplicaciones se deberá conocer ya, ninguno de estos ha cambiado desde las versiones anteriores.

## **1- Preparar el entorno**

La instalación de WebSphere requiere de varias aplicaciones que utilizan entorno gráfico, así que nuestro OpenSuSE debe tener entorno gráfico instalado, cualquiera sirve para ello, si no lo vamos a utilizar para nada más, la mejor opción es XFCE debido a su poco consumo, esta puede ser instalada en la instalación de OpenSuSE, está en la lista de paquetes.

En mi caso, me conecto al OpenSuSE utilizando putty y xming, con X11 activado en el ssh server y en putty, lo cual me permite ejecutar aplicaciones UI remotamente, otra opción sería instalar directamente desde la pantalla del servidor.

Debido a que varias aplicaciones son algo antiguas, es necesario instalar varias librerías de 32 bits, en el caso de tener OpenSuSE Leap 15, en la lista de paquetes podemos elegir las librerías de 32 bits, esto instalará todas las librerías que necesitamos. Si tenemos un OpenSuSE anterior (42.3 o anterior) o no queremos instalar todo el pack de librerías, deberemos instalar todas aquellas que nos fallen en el proceso. Personalmente recomiendo instalar el pack.

El servidor de OpenSuSE estará conectado a internet, así que podrá actualizar por si mismo, de no ser así, habrá que descargar e instalar todos los fixpack de todas las aplicaciones manualmente. Esto es obligatorio para no tener problemas con la licencia y evitar la aparición de bugs.

## **2- Instalar WebSphere + IHS + Plugin**

Una vez hemos subido todos los ficheros al servidor, los descomprimimos. En mi caso estarán en /opt/soft.

```
unzip agent.installer.lnx.gtk.x86_64_1.8.5.zip -d installer
unzip WAS_ND_V9.0_MP_ML.zip -d was_nd
unzip sdk.repo.8030.java8.linux.zip -d java8
unzip was.repo.9000.ihs.zip -d ihs
unzip was.repo.9000.plugins.zip -d plugin
```

El primero de todos es el IBM Installation Manager, que será el encargado de la instalación, los siguientes son el WebSphere Application Server Network Deployment, IBM SDK Java8 para Linux, IHS y Plugin en orden, estos pueden variar según la versión que utilicemos. Todos ellos son los repositorios que indicaremos al Installation Manager.

Lo primero es instalar el Installation Manager.

```
cd installer
./install
```

La instalación no tiene ninguna complicación, es aceptar cada pantalla e indicar la carpeta de destino si queremos cambiarla, por defecto es: /opt/IBM/InstallationManager/eclipse

Al terminar se iniciará a sí mismo, si no es el caso o tenemos que volver a iniciarlo a posterior, se ejecuta manualmente con el siguiente comando:

```
/opt/IBM/InstallationManager/eclipse/launcher
```

Siempre que instalemos una nueva aplicación o actualicemos una existente desde el Installation Manager, debemos actualizarlo, siempre, esto nos evitará posibles problemas debido a que la aplicación o actualización sea más nueva que el Installation Manager y este no disponga de alguna orden necesaria para ella.

Activaremos por configuración que siempre revise si hay actualizaciones nuevas al clicar en Update, esto es solo necesario la primera vez. También aprovechamos para añadir los repositorios para instalar el software.

```
File > Preferences...
Pestaña Updates:
- Seleccionar: Search for Installation Manager updates 
Pestaña Repositories:
(Añadir cada repositorio)
- Add Repository...
	/opt/soft/was_nd/repository.config
	/opt/soft/java8/repository.config
	/opt/soft/ihs/repository.config
	/opt/soft/plugin/repository.config
```

Primero actualizamos clicando en _**Update**_, este se conectará a internet y si hay una nueva versión nos dirá de actualizarla, simplemente es aceptar todas las pantallas, al terminar se reiniciará.

Ahora empezamos la instalación, clicamos en _**Install**_ y nos mostrará todo el software de los repositorios, todos son opcionales, pero es obligatorio tener un Java7 o 8 entre los repositorios.

Nos mostrará WebSphere, HTTP Server y el Plugins, todos con el SDK Java8. Podemos empezar la instalación y actualizarlos después, pero para acelerar el proceso es mejor instalar directamente las versiones más nuevas. Clicamos en _**Check for Other Versions, Fixes and Extensions**_, esto se conectará a los repositorios de IBM para buscar actualizaciones, así que nos pedirá un usuario de IBM para conectar, cualquier usuario de IBM sirve para ello, aunque lo normal será utilizar el usuario que dispondremos tras haber comprado el software.

La búsqueda tardará un rato y finalmente volverá a mostrar las 3 aplicaciones y los diferentes Java, hay que marcar únicamente la versión más nueva de cada uno de ellos, siempre evitando tener duplicados. En el caso de Java, para cada software debemos indicar el Java que utilizará. Es muy importante que WebSphere, IHS y Plugins utilicen la misma versión, también lo es que los 3 utilicen la misma versión de Java.

Una vez seleccionados, seguimos adelante, si nos muestra más fixes a instalar, los aceptamos, también habrá que aceptar la licencia.

Todas las aplicaciones tienen una carpeta de datos compartido, la carpeta por defecto es: /opt/IBM/IMShared

Después nos preguntará por la versión (32 o 64 bits) y la carpeta para cada aplicación, recomiendo que todas las aplicaciones se instalen en 64 bits, las carpetas por defecto son:

- WebSphere: /opt/IBM/WebSphere/AppServer
- HTTP Server: /opt/IBM/HTTPServer
- Plugins: /opt/IBM/WebSphere/Plugins

Mantener siempre el inglés como único idioma, ya que las aplicaciones traducidas pueden suponer un gran problema a la hora de investigar los logs (que normalmente también son traducidos) o encontrar la configuración correcta en la administración de WebSphere.

La lista por defecto de Features ya es correcta, instalará todo menos las aplicaciones de ejemplo, que únicamente son utilizadas para confirmar que la instalación se ha realizado con éxito y borrarlas después.

En caso de tener IHS, nos preguntará por su puerto, por defecto es 80/TCP.

A partir de aquí, es instalar y esperar a que termine.

Al terminar nos permitirá abrir el _**Profile Management Tool**_ para definir la configuración de WebSphere, sin profiles definidos no podemos hacer nada con WAS, así que será el siguiente paso.

Una vez iniciado, clicamos en _**Create...**_ esto nos muestra la lista de opciones, según el tipo de WebSphere que tenemos tendremos unas u otras, pero me centraré únicamente en 2:

- Cell: Este es único para Node Deployment, es la opción típica para este ya que genera el Dmgr que será el servidor administrador de todos los demás y también quien tiene la consola administrativa, este se conecta al resto de servidores y nodos que tengamos, así tendremos un único punto de gestión que se encargará de los demás. También genera el Node y un server1.
- Application server: La versión básica, genera un servidor (server1) que tiene su propia consola administrativa, cada servidor requiere tener su propia consola para poder gestionar fácilmente sus aplicaciones.

Si no tenemos que elegir puertos específicos o cambiar los nombres de determinadas partes, la instalación **_Typical profile creation_** será suficiente.

Lo primero es habilitar la seguridad de WAS y definir el usuario administrador.

Antes de instalar, nos muestra los datos por defecto de puertos y nombres que ha seleccionado, si todo es correcto aceptamos.

Al terminar nos preguntará de ejecutar el _**Launch the First steps console**_, pero no es necesario. Si todo ha ido bien, tendremos el Dmgr01 y el AppSrv01 (este contiene el Node y server1).

Cerramos todo y aprovechamos para quitar los repositorios del Installation Manager, así podremos borrar los instalables (tanto los zip como los descomprimidos), ya que no los necesitaremos más.

## **3- Iniciar y servicio de autoarranque**

Ahora tendremos que iniciar el Dmgr o el servidor que tenga la consola administrativa, así como abrir el puerto para poder acceder.

Por defecto, el puerto de la consola administrativa en un Dmgr (y posiblemente también para un servidor único), es el 9043/TCP, así que lo abrimos.

firewall-cmd --zone=public --add-port=9043/tcp --permanent firewall-cmd --reload rcfirewalld restart

Ahora tendremos que iniciar el servidor, si hemos dejado las carpetas y nombres por defecto, tendremos 3 scripts:

- /opt/IBM/WebSphere/AppServer/profiles/Dmgr01/bin/startManager.sh
- /opt/IBM/WebSphere/AppServer/profiles/AppSrv01/bin/startNode.sh
- /opt/IBM/WebSphere/AppServer/profiles/AppSrv01/bin/startServer.sh server1

Cada uno de ellos tiene su versión contraria con stop, la diferencia es que en stop será obligatorio enviar los argumentos con el usuario y contraseña (-username \<user\> -password \<pwd\>).

Para facilitar esto, he creado unos scripts que se encargarán de todo que se puede descargar en: [was\_scripts](/files/blog/was_scripts.zip).

Los scripts se dividen en 2 partes:

- **/opt/scripts**

Estos son los scripts reales, hay uno para cada servicio y se pueden duplicar para cada servidor que tengamos. Cada uno se encarga de gestionar una única cosa (un nodo, un servidor, ...).

Lo primero es copiarlos en la carpeta correspondiente y darles permisos de ejecución.

```
chmod u+x /opt/scripts/*
```

Esta es la lista de scripts:

- wasDmgr.sh: se encarga únicamente del dmgr.
- wasNode.sh: se encarga del node agent.
- wasServer1.sh: inicia servidores de WebSphere, concretamente el server1, se deberá copiar para cada servidor que tengamos.
- httpServer.sh: se encarga del IHS.
- checkPort.sh: es una herramienta para los scripts de autoarranque, permite comprobar si un servicio está arrancado, no lo utilizan el resto de servicios de esta carpeta.

En cada caso, habrá que cambiar las variables en mayúsculas.

- **/etc/init.d**

Estos son scripts de autoarranque para Linux, básicamente se encargan de llamar a los scripts anteriores en el orden correcto.

Igual que antes, habrá que copiarlos en su carpeta y darles permisos de ejecución.

```
chmod u+x /etc/init.d/was* /etc/init.d/httpServer
```

Esta es la lista de scripts:

- wasDmgr: inicia únicamente el dmgr, debido a que, si el LDAP no está activo, el inicio podría fallar, antes de iniciar comprueba que tiene acceso al puerto del LDAP, de no ser así, fallará y no se iniciará.
- wasApp: Inicia el node agent y todos los servidores, al apagar lo hará en orden invertido. Igual que el anterior, comprueba que el LDAP esté iniciado, además, seguramente las aplicaciones que tengamos necesiten de conexión a base de datos, así que también se comprueba que se tenga acceso, si uno de estos falla, el script no permitirá iniciar. Además, se deja 20 segundos entre procesos para dar tiempo al sistema a acabar de iniciar o apagar, ya que los scripts de WebSphere suelen dar el ok antes de terminar completamente.
- httpServer: Igual que el wasDmgr pero para IHS, no requiere de nadie para funcionar, así que no hace comprobaciones.

Por ahora se puede dejar todos configurados y, una vez tengamos todos los servidores creados, añadirlos a la lista del wasApp.

Ninguno de estos scripts tiene definido el usuario y contraseña de administrador para apagar, debido a que si alguien malintencionado tuviera acceso a estos servidores o viera el comando de apagado mientras se ejecuta, podría obtener estos datos, así que la idea es definirlos en los ficheros de WebSphere y encriptar la contraseña con su propia herramienta.

El proceso es el mismo para el dmgr y las aplicaciones:

```
vi /opt/IBM/WebSphere/AppServer/profiles/Dmgr01/properties/soap.client.props
```

Cambiar los siguientes:

```
com.ibm.SOAP.securityEnabled=true
com.ibm.SOAP.loginUserid=<admin_user>
com.ibm.SOAP.loginPassword=<admin_password>
```

Y repetir lo mismo con el de aplicaciones.

```
vi /opt/IBM/WebSphere/AppServer/profiles/AppSrv01/properties/soap.client.props
```

Acabamos encriptando las contraseñas de los 2 ficheros.

```
/opt/IBM/WebSphere/AppServer/profiles/AppSrv01/bin/PropFilePasswordEncoder.sh /opt/IBM/WebSphere/AppServer/profiles/Dmgr01/properties/soap.client.props com.ibm.SOAP.loginPassword
/opt/IBM/WebSphere/AppServer/profiles/AppSrv01/bin/PropFilePasswordEncoder.sh /opt/IBM/WebSphere/AppServer/profiles/AppSrv01/properties/soap.client.props com.ibm.SOAP.loginPassword
```

Con esto ya podremos utilizar todos los scripts, ahora vamos a definir los servicios y crear los accesos directos.

```
insserv /etc/init.d/wasDmgr
ln -s /etc/init.d/wasDmgr /sbin/rcwasDmgr
insserv /etc/init.d/wasApp
ln -s /etc/init.d/wasApp /sbin/rcwasApp
insserv /etc/init.d/httpServer
ln -s /etc/init.d/httpServer /sbin/rchttpServer
```

Si reiniciamos el OpenSuSE, se inicializará todo y comprobaremos que realmente se ha iniciado todo.

## **4- Configuración de seguridad y LDAP**

A partir de aquí, todo se hará desde la consola administrativa de WebSphere, si el dmgr o servidor con la consola se ha iniciado correctamente, seguramente será accesible desde la siguiente URL:

[https://serverWebsphere.domain:9043/ibm/console](https://serverWebsphere.domain:9043/ibm/console)

Una vez se accede, revisaremos la seguridad:

- Security - Global security

Siempre debemos tener la seguridad activa (_**Enable administrative security**_) para poder controlar el acceso a gente no autorizada, además, muchas aplicaciones pueden tener su propia seguridad, para ello se debe activar la opción _**Enable application security**_.

Los usuarios por defecto provienen de un fichero de WAS, en toda PYME o superior, los usuarios provienen de, al menos, 1 LDAP, así que habrá que configurar todos ellos para que puedan acceder, aun así, es mejor mantener el usuario administrador (al menos uno) en el fichero por defecto, debido a que si el LDAP dejara de funcionar, no podriamos acceder a la consola administrativa.

En _**Available realm definitions**_, lo mantenemos en _**Federated repositories**_ y clicamos en _**Configure...**_

Aquí nos mostrará que está utilizando el fichero como único, creamos uno nuevo en _**Add repositories**_.

En la nueva pantalla elegimos _**New Repository... - LDAP repository**_.

Para cada LDAP debemos configurar:

- Repository identifier: Nombre que identificará el LDAP.
- Directory type: Tipo de LDAP, si no está en la lista, tendremos que configurar todo manualmente, lo cual no es nada aconsejable.
- Primary host name: nombre del host de LDAP.
- Port: puerto del LDAP
- Bind distinguished name: Usuario de bind.
- Bind password: Password del usuario de bind.

En condiciones normales el resto no será necesario modificarlo.

Al guardar volveremos a la pantalla inicial donde se seleccionará nuestro nuevo repositorio, añadimos el _**Base DN**_, aceptamos todas las pantallas y guardamos.

Después de reiniciar el Dmgr ya deberíamos ver la lista de usuarios del LDAP desde _**Users and Groups - Manage Users**_.

Para reiniciar podemos utilizar los scripts ya creados.

```
rcwasDmgr restart
rcwasApp restart
```

## **5- Definir y configurar servidores**

En caso de tener dmgr, podemos crear nuevos servidores desde la consola administrativa, si no, habrá que crearlos manualmente desde los scripts de WebSphere, esta es la opción con dmgr.

Ir a _**Servers - Server Types - WebSphere application server**_, podemos dejar la plantilla y puertos por defecto que genere a no ser que queramos algo especifico.

Según lo que tengan estos servidores, es posible que se tenga que aumentar la RAM que tendrán por defecto, para ello, acceder al nuevo servidor e ir a _**Java and Process Management > Process definition**_. Dentro de **_Java Virtual Machine_** tenemos el _**Initial heap size**_ donde definimos el mínimo de RAM que tendrá disponible, cada servidor puede tener un valor diferente.

Desde la pantalla inicial del servidor, podemos desplegar la lista de puertos que utiliza, tendremos que mirar qué puerto utilizar para HTTP y HTTPS, debido a que tenemos que abrir esos puertos y también definir su uso en la consola administrativa.

Este sería el proceso si nuestro servidor utilizara 9081/HTTP y 9444/HTTPS.

Primero los definimos en la consola administrativa desde _**Environment - Virtual hosts**_, en caso normal utilizamos el _**default\_host**_, así que accedemos a él y vamos a _**Host Aliases**_, aquí añadimos el puerto 9081 y 9444. Requerirá reiniciar todo.

```
rcwasDmgr restart
rcwasApp restart
```

Ahora abrimos los puertos en el firewall.

```
firewall-cmd --zone=public --add-port=9081/tcp --permanent
firewall-cmd --zone=public --add-port=9444/tcp --permanent
firewall-cmd --reload
rcfirewalld restart
```

Como hemos creado más servidores, copiamos el script /opt/scripts/wasServer1.sh para cada uno de ellos y los añadimos en el /etc/init.d/wasApp en el orden correcto

## **5- Configurar IHS y Plugin**

Aunque el IHS esté instalado, seguramente no estará configurado, podemos verlo en _**Servers - Server Types - Web servers**_. Si no está, lo añadimos con _**New...**_

Si no cambiamos el puerto y carpeta por defecto, únicamente habrá que definir el nombre y el tipo (IBM HTTP Server en este caso).

Al terminar y guardar, tendremos el servidor al que podremos iniciar y apagar directamente desde la consola, aun así, si el nodo estuviera apagado, no tendríamos acceso a él (igual que los servidores).

Aunque todo esté asociado y el Plugin ya se regenere según nuestras aplicaciones, el IHS no lo tiene activo por defecto, así que hay que añadirlo manualmente.

Primero miramos el path donde está el plugin para este, clicamos en _**Plug-in properties**_ dentro del _**HTTP Server**_ y nos dirá el path en _**Web server copy of Web server plug-in files**_.

Editamos el fichero del IHS.

```
vi /opt/IBM/HTTPServer/conf/httpd.conf
```

Y le indicamos la librería y la ruta que hemos copiado previamente.

```
LoadModule was_ap24_module "/opt/IBM/WebSphere/Plugins/bin/64bits/mod_was_ap24_http.so"
WebSpherePluginConfig "/opt/IBM/WebSphere/Plugins/config/webserver1/plugin-cfg.xml"
```

Y reiniciamos.

```
rchttpServer restart
```

Si no lo hemos hecho ya, habrá que abrir el puerto 80.

```
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --reload
rcfirewalld restart
```

Normalmente la instalación ya define el puerto 80 en el default\_hosts, pero si no es así, tendremos que añadirla como se ha hecho en la sección de servidores (_**Environment - Virtual hosts**_).

Ya debería mostrarnos la página del IHS:

[http://serverWebSphere.domain](http://serverWebSphere.domain)

## **6- IHS con SSL**

Si además queremos que el IHS tenga HTTPS, deberemos crearle los certificados, podemos utilizar unos autofirmados (aunque la web siempre mostrará que no son seguros ya que no los puede validar) o añadirle unos comprados.

Estos pasos son para generar unos autofirmados, pero se podría usar igual para añadir uno comprado.

Lo primero es ejecutar el ikeyman (requiere UI)

```
/opt/IBM/HTTPServer/bin/ikeyman
```

Desde él, crea un nuevo certificado de tipo CMS.

```
Filename: ihsserverkey.kdb
Location: /opt/IBM/HTTPServer/conf
```

Indicamos una contraseña y seleccionamos _**Stash password to a file**_.

Clicamos en _**New Self-Signed...**_ para generar un certificado autofirmado. Estos son unos pasos de ejemplo, pero se pueden adaptar según necesidades.

```
Key Label: IHS
Version: X509 V3
Key Size: 2048
Signature Algorithm SHA256WithRSA
Common Name: serverWebSphere.domain
Country or region: ES
Validity Period: 365 Days
```

Aceptar y cerrar (**_Key Database File - Close_**), ya podemos cerrar la aplicación.

Los siguiente es añadirlo a la configuración.

```
vi /opt/IBM/HTTPServer/conf/httpd.conf
```

Con lo siguiente:

```
LoadModule ibm_ssl_module modules/mod_ibm_ssl.so
Listen 443
<VirtualHost *:443>
	SSLEnable
	SSLClientAuth none
</VirtualHost>
KeyFile /opt/IBM/HTTPServer/conf/ihsserverkey.kdb
SSLDisable
```

Y reiniciamos.

```
rchttpServer restart
```

Como siempre, habrá que revisar que el puerto 443 está abierto en el firewall y que el default\_host de la consola administrativa lo tiene definido.

Si todo ha funcionado, podremos acceder ya:

[https://serverWebSphere.domain](https://serverWebSphere.domain)

## **Final**

A partir de aquí habría que empezar a desplegar cada aplicación en los servidores que correspondan, para cada uno también habrá que elegir si debe o no publicarse en el IHS y configurar todo aquello que cada aplicación necesite (bases de datos, configuraciones propias, seguridad, ...).

Como ya he indicado al principio, WebSphere es un servidor de aplicaciones Java profesional, así que su uso está destinado en PYMES o superiores, su precio no es asequible para inferiores a estos, pero en caso de necesitar sistemas de cluster o tener una gran carga de aplicaciones, un solo WebSphere es capaz de mover cientos de mini aplicaciones o varias de gran envergadura, otro tipo de servidores no podría con ello y nos obligaría a tener muchos servidores diferentes, así que elegir el servidor de aplicaciones Java correcto para una empresa, requerirá previamente de entender el consumo de las aplicaciones Java que va a requerir y su necesidad, o no, del uso de cluster.
