---
title: PostgreSQL en OpenSuSE Leap 15.1
authors: rei.izumi
tags: [PostgreSQL]
---


Últimamente PostgreSQL es la base de datos que más está siendo implementada, debido a su origen OpenSource y su potencial que le permite estar a la altura de las grandes bases de datos de pago.

También hay que sumar la cantidad de años que tiene, lo que la hace una base de datos sólida, y con más funcionalidad que MariaDB, si principal rival en el OpenSource.

Actualmente muchas aplicaciones aceptan (e incluso recomiendan) su uso, si a eso añadimos que también está ganando dentro del mundo cloud y contenedores, la convierte en una base de datos a tener en cuenta.

<!-- truncate -->

## **0- Previos**

A partir de la versión 10, PostgreSQL ya no está en los repositorios oficiales de OpenSuSE, pero tiene un repositorio oficial para esta distribución, debido a que la última versión implementa muchas nuevas funcionalidades, partiré de ella. Esto se debe tener en cuenta para próximas actualizaciones tanto de PostgreSQL como del propio OpenSuSE.

Todos los comandos se ejecutarán como root, aunque la gestión propia de PostgreSQL se hace con el usuario postgres, generado automáticamente por el instalador.

## **1- Instalación**

En la [página de descarga](https://www.postgresql.org/download/) tenemos la lista de sistemas operativos oficiales y su documentación, en mi caso utilizo un OpenSuSE así que requiero [esta documentación](https://www.postgresql.org/download/linux/suse/).

Para cada distribución tenemos varias opciones, en mi caso quiero añadir el repositorio para disponer de las últimas versiones, así que la documentación redirige a la [instalación con Zypper](https://zypp.postgresql.org/howtozypp/).

En el momento de redactar este artículo, la versión 13 está en pruebas, así que utilizaré la versión 12.

Añadimos el repositorio.

```
zypper addrepo https://download.postgresql.org/pub/repos/zypp/repo/pgdg-sles-12.repo
```

Actualizamos el índice de repositorios, al conectar por primera vez, nos preguntará si queremos confiar en este, así que utilizaremos la opción de **Trust always (a)** para aceptarla y que no nos vuelva a preguntar.

```
zypper ref
```

A partir de aquí ya podemos instalar como siempre, si buscáramos nos saldrían todas las versiones disponibles (incluyendo la del repositorio oficial), yo instalaré la última disponible.

```
zypper in postgresql12-server
```

Una vez instalada, habrá que inicializar, activar el autoarranque y abrir el puerto.

```
/usr/pgsql-12/bin/postgresql-12-setup initdb
systemctl enable postgresql-12
firewall-cmd --zone=public --add-port=5432/tcp --permanent
firewall-cmd --reload
```

Como suele pasar en estas bases de datos, por defecto no admiten conexiones remotas y se debe abrir por cada IP, rango, base de datos y/o usuario. Debido a que en muchos casos la seguridad procede de la propia red, en mi caso abriré todo hacia cualquier IP que esté dentro del rango 192.168.1.0/24.

Primero abrimos el puerto para conexiones remotas.

```
vi /var/lib/pgsql/12/data/postgresql.conf
```

Cambiando el siguiente valor.

```
listen_addresses = '*'
```

A partir de aquí hay que abrir los usuarios y bases de datos accesibles, ya que PostgreSQL permite un control diferente para cada una de ellas.

```
vi /var/lib/pgsql/12/data/pg_hba.conf
```

En mi caso abro todo hacia mi rango.

```
host all all 192.168.1.0/24 md5
```

También es posible abrir todo a todos los rangos.

```
host all all 0.0.0.0/0 md5
host all all ::/0 md5
```

Finalmente iniciamos PostgreSQL y ya lo tendremos funcionando.

```
systemctl start postgresql-12
```

El fichero de configuración tiene muchas opciones, desde la [documentación oficial](https://www.postgresql.org/docs/12/auth-pg-hba-conf.html) se puede encontrar

## **2- Mover carpeta de datos**

La instalación deja la carpeta de datos en un lugar predefinido, pero esta no tiene que estar en la partición donde se dispone de todo el espacio, así que antes de nada la moveré a un nuevo lugar.

Lo primero es saber dónde está la carpeta por defecto, similar a MariaDB, deberemos conectar al servicio y lanzar la consulta. Así que empezamos conectando, para ello lanzaré el comando con el usuario postgres.

```
su - postgres -c psql
```

Lanzamos la pregunta.

```
SHOW data_directory;
```

Con lo que obtendremos algo así.

```
    data_directory
------------------------
 /var/lib/pgsql/12/data
(1 row)
```

Salimos con el comando **\\q** y vamos a copiar la carpeta.

Antes de copiar la carpeta, se debe tener en cuenta que el fichero principal de configuración (**psotgresql.conf**) se seguirá leyendo de este directorio, el resto será leído de la carpeta nueva (por ejemplo, el pg\_hba.conf que contiene los accesos)

Paramos el servicio y sincronizamos la carpeta hacia su nuevo destino, manteniendo los permisos.

```
systemctl stop postgresql-12
rsync -av /var/lib/pgsql/12/data/ /opt/postgres-data
```

Ahora editamos el fichero original.

```
vi /var/lib/pgsql/12/data/postgresql.conf
```

Indicando la nueva carpeta.

```
data_directory = '/opt/postgres-data'
```

Y ya podemos volver a iniciar el servicio.

```
systemctl start postgresql-12
```

Si quisiéramos asegurarnos de que todo ha funcionado bien, simplemente tendríamos que lanzar los pasos iniciales para preguntar dónde está la carpeta.

## **3- Crear una base de datos**

La creación básica de bases de datos y usuarios es muy similar a la de MariaDB, aunque siempre hay que recordar que PostgreSQL es algo puñetero y debemos usar minúsculas en nombres de bases de datos, usuarios y campos, si utilizamos mayúsculas nos podemos encontrar con problemas, o que el propio sistema los cambie.

Empezamos conectado con el usuario de postgres.

```
su - postgres -c psql
```

En este ejemplo crearé una base de datos llamada '**database\_name**', con el usuario '**user\_name**', contraseña '**password**' y con todos los permisos a esa base de datos.

```
CREATE DATABASE database_name;
CREATE USER user_name WITH PASSWORD 'my_password';
GRANT ALL PRIVILEGES ON DATABASE database_name TO my_user_name;
```

Esta es una configuración básica, pero se puede modificar para aplicar los permisos que queramos.

## **4- Copias de seguridad notificadas con Nagios**

Siguiendo los pasos que ya expliqué para las [copias de seguridad con Nagios](/blog/2020/05/01/copias-seguridad-notificadas-nagios), utilizando el comando original de exportación (**pg\_dump**)**,** he preparado un script para que se pueda automatizar.

Se puede descargar desde **[aquí](/files/blog/backup_PostgreSQL.zip)**.

Estos son los campos que varían al original:

| Nombre | Descripción | Valor por defecto |
| --- | --- | --- |
| POSTGRESQL\_PORT | Puerto de PostgreSQL, únicamente utilizado para confirmar que esté activo | 5432 |
| POSTGRESQL\_USER | Usuario que de PostgreSQL para utilizar el comando de exportación | postgres |
| POSTGRESQL\_DATABASES | Lista de bases de datos, separadas por coma. |  |

Y los permisos del script:

| Ruta | Permisos |
| --- | --- |
| /opt/scripts/backup/backup\_PostgreSQL.sh | 700 |

## **Final**

En algunos puntos, PostgreSQL es algo diferente al resto, el problema de que todo debe estar en minúsculas choca con el mismo problema hacia mayúsculas de Oracle, y algunos comandos, como para listar o salir, son bastante peculiares, pero en general, es, posiblemente, la mejor opción entre las bases de datos OpenSource sin envidiar nada de las empresariales, y aunque los pasos para instalar también son algo diferentes a lo normal al no encontrarse en los repositorios locales, al final no se requiere mucho tiempo para tenerla lista y funcionando.
