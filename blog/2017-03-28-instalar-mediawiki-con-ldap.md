---
title: Mediawiki con LDAP
authors: rei.izumi
tags: [MediaWiki, LDAP]
---

Una de las herramientas web que más rápidamente se han extendido a nivel de usuario han sido las llamadas "_wiki_" gracias al proyecto [Wikipedia](https://es.wikipedia.org/), pero antiguamente ya existían otros proyectos web similares.

Estas herramientas permiten visualizar y buscar rápidamente entre los diferentes artículos que disponen, creando sistemas de categorías en árbol, así puedes, por ejemplo, documentar varios proyectos y, al realizar una búsqueda, utilizar el texto de cada documento como parte de la búsqueda. Esto también pueden hacerlo gestores documentales, pero requieren indexar sus documentos, la idea puede resultar similar, pero la base es totalmente diferente.

La otra gran ventaja es la capacidad de que una comunidad pueda crear artículos y editarlos para ir actualizando o añadiendo nuevos datos continuamente, esto también es una gran diferencia con lo que haría un blog.

<!-- truncate -->

## **0- Previos**

Me basaré en el proyecto [Mediawiki](https://www.mediawiki.org/) del que proviene la Wikipedia por su gran extensión y guías de usuario que podemos encontrar en cualquier lado, lo cual lo hace más fácil de utilizar y gestionar.

Mediawiki crece continuamente y ya no solo dispone de varias formas de instalar, sino que la cantidad de extensiones que tiene es enorme, además la gestión de algunos de los más modernos (como Lua) puede resultar casi imposible de utilizar si no se tienen unos altos conocimientos en este. Después de plantear diferentes opciones y probar estas extensiones tan complicadas, mi idea final ha sido descartarlas y no replicar el potencial de la Wikipedia, ya que el nivel de complejidad y aprendizaje necesario para utilizarlo sale de mis necesidades, así que en este artículo explicaré la forma básica de instalación junto a algunas extensiones básicas.

En todo momento utilizaré el usuario root y un entorno [LAMP](/blog/2017/01/22/instalar-lamp-opensuse), el cual daré por hecho que ya está listo para utilizar.

## **1- Instalar Mediawiki**

El primer paso es configurar la base de datos.

```
mysql -u root -p
```

Creamos la base de datos y el usuario con acceso a esta, ya que la base de datos está en local, no será necesario darle permisos para acceder de forma remota.

```
CREATE DATABASE wikiDB;
GRANT ALL ON wikiDB.* TO 'wikiUser'@localhost IDENTIFIED BY 'wikiPassword';
```

Descargamos la última versión en una carpeta temporal y lo movemos al destino final.

```
wget https://releases.wikimedia.org/mediawiki/1.28/mediawiki-1.28.0.tar.gz
tar zxf mediawiki-1.28.0.tar.gz
mv mediawiki-1.28.0 /srv/www/wiki
cd /srv/www
chown -R wwwrun:www wiki
```

Ya tenemos la base, es hora de publicarlo desde Apache, podemos crear un vhosts nuevo o partir de otro existente.

```
vi /etc/apache2/vhosts.d/005-wiki.conf
```

Configuramos el vhosts para la Wiki.

```
<VirtualHost *:80>
        ServerName wiki.domain.cat

        DocumentRoot /srv/www/wiki

        ErrorLog /var/log/apache2/wiki-error_log
        CustomLog /var/log/apache2/wiki-access_log combined

        HostnameLookups Off
        UseCanonicalName Off
        ServerSignature Off

        <Directory "/srv/www/wiki">
                Options FollowSymLinks
                AllowOverride All
                AddType text/plain .html .htm .shtml .php .phtml .php5
                Require all granted
        </Directory>
</VirtualHost>
```

En condiciones normales esto es suficiente para empezar a instalar Mediawiki, pero seguramente nos faltaran módulos de PHP por instalar, podemos confirmar lo que nos falte desde el instalador al que accederemos desde la URL de publicación.

En mi caso faltaban dos herramientas, estas no son obligatorias, pero sí recomendadas, estas son la normalización de Unicode, instalable en un simple paso.

```
zypper in php7-intl
```

Y la caché con [APCu](https://pecl.php.net/package/APCu), pero como utilizo PHP7, la antigua no me sirve, para ello se utiliza la versión nueva que requiere compilarla con los siguientes pasos.

```
zypper in php7-devel
wget https://pecl.php.net/get/apcu-5.1.8.tgz
tar -zxf apcu-5.1.8.tgz
cd apcu-5.1.8/
phpize
./configure --with-php-config=/usr/bin/php-config
make
make install

```

Una vez compilado añadimos esta extensión al php.ini configurado para Apache.

```
vi /etc/php7/apache2/php.ini
```

En el apartado de extensiones, añadimos la nueva.

```
extension = apcu.so
```

Después de reiniciar Apache, refrescamos la instalación de Mediawiki y seguimos revisando que estén todos los pasos necesarios.

La instalación es muy intuitiva, por defecto ya marca las mejores opciones y explica cualquier parte en la que tengamos dudas. Al final de las preguntas base podemos elegir si terminar ya o seguir respondiendo a dudas, en el siguiente paso me centraré en estas.

## **2- Configuración avanzada de la instalación**

La Wiki puede ser configurada acorde a diferentes permisos, según si será abierta (cualquiera puede modificar), necesita usuario (pero igualmente cualquiera puede modificar únicamente registrándose), requiere usuario que un administrador le proporcione o privada donde ni siquiera se podrá leer por los usuarios no registrados, este y el tipo de licencia son puntos muy importantes.

En caso de necesitar que la Wiki gestione e-mails deberemos definir cada uno de los puntos indicados.

Por defecto, Mediawiki dispone de varios themes y extensiones, el theme por defecto de Wikipedia es Vector siendo el de por defecto y más conocido, si no vamos a utilizar ninguno de los otros, los podemos quitar para evitar que un usuario cambie a uno de ellos y nuestros cambios en la CSS no se apliquen correctamente en esos.

Sobre las extensiones, lo más recomendable es revisar qué hace cada una de ellas y activarlas o no acorde a nuestras necesidades, aun así, esta es una lista de los más comunes de entre los que lleva por defecto:

- Cite
- CiteThisPage
- InputBox
- ParserFunctions
- PdfHandler
- SyntaxHighlight\_GeSHi
- WikiEditor

Habilitamos la subida de ficheros si es necesaria y cambiamos el nombre del logo para no modificar el original, ya que en una actualización podríamos reemplazar el nuestro por el de por defecto.

Una vez terminado subimos el fichero LocalSettings.php a la raíz del proyecto para poder acceder.

## **3- Configuración de extensiones**

Todas las configuraciones se añaden al fichero _LocalSettings.php_, así que las siguientes explicaciones utilizaran este fichero de base.

- ### **Logo**
    

Como se ha visto anteriormente, existe un path concreto para indicar el logo, pero es mejor cambiarlo por otro nombre para evitar problemas a futuro. Además de este path se pueden incluir otros tipos como el favicon o el favicon de midas para dispositivos Apple.

Todos los logos e imágenes estáticas que necesitemos que estén en todos los theme se incluyen en la carpeta _resources/assets_

```
$wgLogo = "$wgResourceBasePath/resources/assets/wiki-logo.png";
$wgFavicon = "$wgResourceBasePath/resources/assets/favicon.ico";
$wgAppleTouchIcon = "$wgResourceBasePath/resources/assets/wiki-logo_faviconApple.png";
```

- ### **Subir ficheros**
    

Si elegimos la opción de subir ficheros, se activará la lista de extensiones permitidas por defecto, pero esta quizás sea demasiado restrictiva, o quizás la queramos restringir más, en cualquier caso, la podemos modificar a nuestras necesidades.

```
$wgFileExtensions = array(
	'png', 'gif', 'jpg', 'jpeg', 'psd',
	'pdf', 'doc', 'docx', 'xls', 'xlsx',
	'zip', 'rar', 'txt' 
);
```

- ### **WikiEditor**
    

Mediawiki dispone de un editor por defecto, pero esta extensión viene dentro del instalable por su gran uso, en la instalación la incluimos, pero debe ser activada manualmente, las siguientes variables activan todas sus opciones.

```
$wgDefaultUserOptions['usebetatoolbar'] = 1;
$wgDefaultUserOptions['usebetatoolbar-cgd'] = 1;
$wgDefaultUserOptions['usenavigabletoc'] = 1;
$wgDefaultUserOptions['wikieditor-preview'] = 1;
$wgDefaultUserOptions['wikieditor-publish'] = 1;
```

- ### **LDAP**
    

Existe una [extensión para autenticar desde LDAP](https://www.mediawiki.org/wiki/Extension:LDAP_Authentication) aunque es algo vieja, pero se puede seguir configurando. Hay que tener en cuenta que es una de las partes más complejas y seguramente requerirá de mucha investigación ya que cada LDAP es diferente. La configuración que yo indico es para Oracle DSEE.

Esta extensión requiere el módulo de LDAP de PHP para funcionar, así que el primer paso es instalarlo y reiniciar.

```
zypper in php7-ldap
```

Para instalar una extensión como esta, debemos descargar la última versión, descomprimirla en la carpeta _extensions_ y lanzar el proceso de mantenimiento.

```
wget https://extdist.wmflabs.org/dist/extensions/LdapAuthentication-REL1_28-770c89e.tar.gz
tar -xzf LdapAuthentication-REL1_28-770c89e.tar.gz -C /srv/www/wiki/extensions
chown -R wwwrun:www /srv/www/wiki/extensions/LdapAuthentication
php /srv/www/wiki/maintenance/update.php
```

Esta extensión genera los datos del usuario la primera vez que se inicia sesión con su usuario y contraseña, pero con el sistema actual de gestión de usuarios, esto no funciona, así que se debe activar una nueva función para autocreación de usuarios a partir de extensiones.

```
$wgGroupPermissions['*']['autocreateaccount'] = true;
```

La siguiente configuración permite a los usuarios que pertenecen al grupo _cn=Wiki,ou=Groups,dc=domain,dc=cat_ acceder a la Wiki.

**Configuración para LDAP diferente a Active Directory**

Mientras el LDAP no sea un Active Directory, esta configuración debería ser iguao o muy similar. Esta concretamente es para un Oracle Directory Server (ODSEE).

```
require_once ('extensions/LdapAuthentication/LdapAuthentication.php');
require_once ('extensions/LdapAuthentication/LdapAutoAuthentication.php');
$wgAuth = new LdapAuthenticationPlugin();
$wgLDAPUseLocal = false;
$wgLDAPDomainNames = array('LDAP name');
$wgLDAPServerNames = array('LDAP name' => 'localhost');
$wgLDAPPort = array('LDAP name' => 389);
$wgLDAPEncryptionType   = array('LDAP name' => 'clear');

$wgLDAPSearchStrings    = array('LDAP name' => 'uid=USER-NAME,ou=People,dc=domain,dc=cat');
$wgLDAPBaseDNs          = array('LDAP name' => 'dc=domain,dc=cat');
$wgLDAPGroupBaseDNs     = array('LDAP name' => 'ou=Groups,dc=domain,dc=cat');
$wgLDAPGroupUseFullDN   = array('LDAP name' => true);
$wgLDAPGroupObjectclass = array('LDAP name' => 'groupOfUniqueNames');
$wgLDAPGroupAttribute   = array('LDAP name' => 'uniqueMember');
$wgLDAPGroupSearchNestedGroups = array('LDAP name' => false);
$wgLDAPGroupNameAttribute=array('LDAP name' => 'cn' );
$wgLDAPRequiredGroups   = array('LDAP name' => array('cn=Wiki,ou=Groups,dc=domain,dc=cat'));
```

**Configuración para Active Directory**

Active Directory es otro mundo dentro de los LDAP, para él se requiere cambiar unas cuantas cosas.

```
require_once ('extensions/LdapAuthentication/LdapAuthentication.php');
require_once ('extensions/LdapAuthentication/LdapAutoAuthentication.php');
$wgAuth = new LdapAuthenticationPlugin();
$wgLDAPUseLocal = false;
$wgLDAPDomainNames = array('LDAP name');
$wgLDAPServerNames = array('LDAP name' => 'localhost');
$wgLDAPPort = array('LDAP name' => 389);
$wgLDAPEncryptionType   = array('LDAP name' => 'clear');

$wgLDAPSearchStrings    = array('LDAP name' => 'domain\\USER-NAME');
$wgLDAPBaseDNs          = array('LDAP name' => 'dc=domain,dc=cat');
$wgLDAPSearchAttributes = array('LDAP name' => 'sAMAccountName');
$wgLDAPGroupUseFullDN   = array('LDAP name' => true);
$wgLDAPGroupObjectclass = array('LDAP name' => 'group');
$wgLDAPGroupAttribute   = array('LDAP name' => 'member');
$wgLDAPGroupNameAttribute=array('LDAP name' => 'cn' );
$wgLDAPActiveDirectory   =array('LDAP name' => true);

$wgLDAPRequiredGroups   = array('LDAP name' => array('cn=Wiki,ou=Groups,dc=domain,dc=cat'));
```

- ### **SyntaxHighlight\_GeSHi**
    

Esta [extensión](https://www.mediawiki.org/wiki/Extension:SyntaxHighlight) activada en la instalación permite escribir secciones en idiomas de programación concretos, tiene una infinidad de idiomas soportados que se pueden ver desde su web.

En este caso solo es necesario cargar su extensión como ya tendremos, pero es importante conocer el formato de escritura en un artículo Wiki, este es un ejemplo para escribir en PHP:

```
<syntaxhighlight lang="php">
$varName = 'varValue';
</syntaxhighlight>
```

- ### **URL amigables (SEO)**
    

Esto no es una extensión, por defecto la Mediawiki utiliza _index.php_ en su URL, como esto es poco amigable y no sigue los ideales de SEO, lo cambiaremos por _wiki_.

Primero hay que añadir un .htaccess en la raíz con los siguientes datos para permitir las redirecciones.

```
# Enable the rewrite engine
RewriteEngine On

# Short url for wiki pages
RewriteRule ^/?wiki(/.*)?$ %{DOCUMENT_ROOT}/index.php [L]

# Redirect / to Main Page
RewriteRule ^/*$ %{DOCUMENT_ROOT}/index.php [L]
```

Con ello modificamos las variables para indicar el nuevo nombre.

```
$wgScriptPath = "";
$wgArticlePath = '/wiki/$1';
```

## **4- Uso de Mediawiki**

- ### **Crear página nueva**
    

La típica duda cuando no se conoce el funcionamiento es cómo crear una página nueva, y es muy simple a la vez que no demasiado clara, en la búsqueda escribe el título que tendrá la nueva página, al no encontrará, mostrará la opción de crear la página nueva con ese nombre, así de simple.

- ### **CSS y JS**
    

Existen dos páginas, una para CSS y otra para JS, que permiten añadir estas de forma dinámica sin tener que modificar cada theme, se acceden modificando la URL hacia las siguientes:

- wiki/MediaWiki:Common.css
- wiki/MediaWiki:Common.js

Son tratadas de forma similar que otro artículo, así que pueden ser modificadas igual y disponen de historial

- ### **Sidebar**
    

El menú de la izquierda se llama Sidebar y puede ser modificado, hay que tener en cuenta que el nombre de este es visible para cualquier usuario sin iniciar sesión, así que, aunque no pueda acceder a los artículos, sí que podrá ver cada nombre que aparezca.

La página para acceder es:

- wiki/MediaWiki:Sidebar

Debido a que el formato es diferente al del resto de artículos de la Mediawiki, es importante revisar la [documentación oficial](https://www.mediawiki.org/wiki/Manual:Interface/Sidebar) para cualquier cambio que hagamos en este.

- ### **Templates**
    

El verdadero potencial de una Mediawiki proviene de los Templates, estas son páginas especiales donde se definen formatos que después pueden ser reutilizados en cualquier otro artículo pasándole parámetros. Los típicos menús laterales que tantas veces vemos en Wikipedia provienen de Templates que a su vez provienen de otras Template y estas de otras.

Para crear una nueva template introduciremos la siguiente URL:

- wiki/Template:NombreNuevaTemplate

Sigue los mismos pasos de cualquier otro artículo.

Una vez creada la utilizaremos en otros artículos con \{\{INombreNuevaTemplate\}\}.

Debido a la gran complejidad que puede tener una template, vuelvo a recomendar la [documentación oficial](https://www.mediawiki.org/wiki/Help:Templates), después de leer todo el artículo es recomendable ver cómo están diseñadas las Templates de otra Mediawiki como por ejemplo cualquiera de las Wikia, Wikipedia es muy mala opción de donde basarnos ya que todos sus contenidos requieren de Lua para funcionar.

- ### **Importar/Exportar página**
    

Para minimizar el impacto al hacer pruebas y debido a que instalar varias Mediawiki puede ser muy sencillo, la mejor opción es tener una solo para hacer pruebas, por ejemplo, para crear las nuevas Template, una vez funciona, solo tenemos que exportarla indicando que nos copie las templates utilizadas e importarla en la nueva Wiki, este proceso no copia las imágenes utilizadas, pero será fácil de subir manualmente.

Estas herramientas y otras muy útiles se encuentran desde las **Páginas especiales** del Sidebar.

## **Final**

Hasta la fecha, este ha sido, sin duda, el servicio más complicado de instalar y configurar (y sigo en ello) de todos. Inicialmente plantee tener exactamente la última versión con todo lo que pudiera igualar a la Wikipedia, pero Scribunto y su Lua fueron una parte de extrema complejidad, sin ello se pierde mucha libertad de configuración, pero también se simplifica enormemente el proceso.

Configurar una Mediawiki básica es muy sencillo y con estos pasos se puede tener lista en pocas horas sin conocer nada de ella, pero si la idea es crear tus propias plantillas de forma correcta, con funciones y una organización muy concreta, la mejor opción sería instalar una Mediawiki de pruebas y dedicarle días o semanas a practicar, gracias a la importación/exportación de páginas, una vez consigues la plantilla correcta puedes pasarla al entorno real fácilmente.

Aun así, incluso Wikipedia dispone de páginas específicamente para que los usuarios prueben antes de cambiar algo importante.
