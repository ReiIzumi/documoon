---
title: DHCP en OpenSuSE Leap 15.1
authors: rei.izumi
tags: [DHCP]
---

El servicio de DHCP quizás es uno de los más olvidados, en muy raro que no esté disponible, y al funcionar de forma transparente, nadie le hace caso, además, en un sistema doméstico o una PYME, normalmente es el router del proveedor de Internet quien se encarga de esto, así que está ahí, pero nadie le hace caso.

La realidad es que es uno de los servicios más importantes, y más ahora con tantos dispositivos conectados a la red y que no pueden ajustar una IP fija, sin DHCP, muchos dispositivos ni siquiera funcionarían.

Además de asignar una IP diferente a cada dispositivo, este se debe encargar de poder definir una IP fija a una Mac, de esta forma podremos asignar IPs que no cambiarán a esos dispositivos que no pueden contener estáticas o para evitarnos tener que poner fijas a dispositivos que se mueven fuera de la red, otra aún más importante es poder enviar las DNS que queremos utilizar, y este último es lo que provoca que valga la pena obviar el servicio original de un router y tener un servidor DHCP propio.

<!-- truncate -->

## **Explicación**

Hasta la fecha, tener un servidor DHCP era bastante innecesario debido a que los routers que ofrecen los proveedores de Internet lo hacen correctamente, normalmente estos no tenían la capacidad de asignar una IP fija, lo que puede ser un inconveniente si se quiere asignar IP estáticas para tener un control más exhaustivo, pero esto aun podía tener soluciones, e incluso los routers más nuevos tienen esto resuelto.

Entonces, ¿por qué complicarse la vida y crear un servicio propio? por las DNS.

Muchos proveedores parecen haber llegado a la conclusión de que "por nuestra seguridad", sus routers bloquean la DNS para obligarnos a utilizar la suya, si esto no nos convence, podemos cambiar la DNS en cada uno de nuestros dispositivos, manualmente, cosa que no es una solución viable.

Después de preguntar, lo que indican es que, debido a que existen ataques de redireccionamiento, ellos han decidido fijar nuestra DNS hacia la suya que está vigilada y protegida, estas palabras son muy bonitas, pero si se tiene un mínimo de conocimiento, no se sostienen por ningún lado.

Si tu PC tiene un problema de redireccionamiento, seguramente es porque algo ha modificado tu fichero de hosts (ya sea Windows, Unix o Mac), ya que este fichero va por encima de las DNS, el router no te ayudará en ello, la otra explicación es que el servidor DNS que estás utilizando sea dañino, pero esto simplemente se evita utilizando uno de confianza, que seguramente te dará más confianza que el del proveedor de Internet. Siendo así, ¿que explicación queda? Fácil, si el país decide bloquear una IP, difícilmente podrá evitar que tengas acceso si lo quieres, pero si evita la resolución de la IP, la primera entrada ya estará bloqueada, de la forma más rápida posible.

Y de esta manera llegamos al punto de que, si tu red tiene una DNS propia, el router no te permitirá asignarla por DHCP, ya que debes utilizar el del proveedor si o si. Para ello también tienen excusas, y es que algunos routers ya tienen un servicio DNS propio, extremadamente limitado, eso si.

## **0- Previos**

Los servidores DHCP actuales pueden configurar tanto para IPv4 como para IPv6 (o ambas al mismo tiempo), pero debido a que una red doméstica o PYME no es tan grande, en este artículo descartaré la IPv6 ya que la 4 me parece más simple de utilizar.

Todos los comandos se ejecutarán como root.

## **1- Instalación**

Empezamos instalando el servidor.

```
zypper in -y dhcp-server
```

Ahora vamos a configurar la network a la que estará asignado el servidor DHCP, únicamente dará IP a aquellas peticiones que vengan de esta red, con lo que podemos evitar resolver IP a redes secundarias o que ya dispongan de su propio DHCP. Para ver todas las redes que tenemos disponibles podemos usar el comando **ip a**.

Una vez sabemos a qué red asignar, editamos el fichero.

```
vi /etc/sysconfig/dhcpd
```

Y la asignamos, en mi caso es la eth0.

```
DHCPD_INTERFACE="eth0"
```

Lo siguiente será configurar la información que devolverá por DHCP (rango de IP, gateway y DNS) y también podemos asignar IP estáticas según la Mac.

Editamos el fichero.

```
vi /etc/dhcpd.conf
```

Primero empezamos definiendo la DNS a utilizar, ya sea la local o una a Internet.

```
option domain-name "domain.intranet";
option domain-name-servers 192.168.1.3;
```

Seguimos con los tiempos que durará esa asignación y el rango de la red. En este ejemplo asigno que afectará a la red 192.168.1.X/24, donde el router está en la 1 y se reserva las IP desde la 200 a la 249, así que si tenemos 50 dispositivos, el último no se le podrá asignar una IP.

```
default-lease-time 3600;
max-lease-time 7200;

authoritative;

subnet 192.168.1.0 netmask 255.255.255.0 {
	range 192.168.1.200 192.168.1.249;
	option routers 192.168.1.1;
	option broadcast-address 192.168.1.255;
}
```

Para cada dispositivo al que le queramos asignar una IP fija, lo añadimos junto a su Mac, la Mac suele estar en una etiqueta del dispositivo o en su configuración, y, en teoría, es única.

Cabe destacar que estos dispositivos no ocupan espacio en el rango definido anteriormente si la IP está fuera del rango (y debería estar fuera para evitar problemas).

```
host nombre_dispositivo {
	hardware ethernet 01:23:45:AB:FF:99;
	fixed-address 192.168.1.100;
}
```

Finalmente añadimos el servicio al autoarranque y lo iniciamos.

```
systemctl enable dhcpd
rcdhcpd start
```

## **Final**

En unos sencillos y rápidos pasos se puede tener listo el servicio de DHCP sin limitación alguna.

Ya que está, al asignar una IP fija por Mac, podemos bloquear la IP de cada pc, móvil, tablet y hasta de enchufes conectados a la wifi, así podremos rastrear cualquier petición de forma más eficiente y también nos permitirá que Nagios se encargue de avisarnos si alguno de esos dispositivos se ha apagado.
