---
title: SVN + LDAP en OpenSuSE Leap 42.2
authors: rei.izumi
tags: [SVN, WebSVN, LDAP]
---

Entre las herramientas obligatorias para poder programar se encuentra el repositorio de código, el lugar donde se almacena todo el código creando versiones y sincronizando entre las personas que contribuyen a este, sin él, no habría forma de trabajar eficazmente entre varias personas, e incluso sin equipo, permite tener siempre una versión estable subida mientras añadimos código de pruebas, si este no funciona, podemos volver atrás fácilmente.

Una de las formas más eficientes es instalar Subversion sobre un Apache que se encargará de la gestión y publicación de nuestro código.

<!-- truncate -->

## **0- Previos**

Se instalará Subversion (de ahora en adelante SVN) sobre un Apache, para ello hay que tener el [Apache](/blog/2017/01/22/instalar-lamp-opensuse) ya configurado, además si se quiere configurar en SSL se puede seguir [este post](/blog/2017/01/25/apache-con-ssl) al terminar esta instalación.

Para facilitar la lectura y tener una página de índice, instalaré WebSVN (requiere PHP), una página que utiliza la misma seguridad de SVN y puede mostrar todos los repositorios a los que se tiene acceso, esto permite tener una lista de los repositorios a los que tenemos acceso (con el nombre exacto para después construir fácilmente la URL de acceso real) y tener una forma web de revisar el código por si queremos revisar cualquier parte sin tener que encender nuestra herramienta de desarrollo.

La autenticación de usuarios se realizará contra el LDAP que configuré en [este post](/blog/2017/01/23/instalar-oracle-directory-server), pero en caso de tener otro seguramente cambiaría la URL y nada más, de tener un Active Directory quizás sea algo más complicado, ya que suele ser diferente al resto, pero encontraréis rápidamente los datos para Apache.

Además de la autenticación de usuarios, tendremos un fichero que indicará quienes tienen acceso a cada repositorio de LDAP, esto nos da más control sobre la visibilidad del código y de paso el usuario se evita tener tantos repositorios innecesarios que evitan que encuentre el suyo, quienes han trabajado con un CSV sin limitar me entenderán.

En todo momento se utilizará el usuario root.

## **1- Instalar Subversion y WebSVN**

Empezamos primero con instalar todo lo que vamos a necesitar y después las configuraremos todas de golpe para ahorrarnos faena.

Voy a dividir las rutas de publicación del WebSVN y las de SVN para evitar errores de publicar por donde no toca el SVN y de paso tener mejor control de los backups, ya que el WebSVN no cambia nunca y por tanto su backup no es importante, pero el del SVN sí lo es. Para ello la ruta del WebSVN será _/srv/www/svn_ y la del SVN _/srv/svn_. Este último contendrá también el fichero de acceso a repositorios ya que así estará dentro de la zona de backups.

Lo primero es instalar el Subversion para obtener los comandos de creación, también añadimos enscript, necesario para permitir que WebSVN muestre colores y así sea más legible el código.

```
zypper in subversion-server subversion-tools enscript
```

Creamos las carpetas, descargamos el [WebSVN](http://www.websvn.info/download/) dejándolo en el destino final y de paso dejamos su fichero configurado

```
mkdir /srv/svn
mkdir /srv/www
cd /srv/www
wget http://websvn.tigris.org/files/documents/1380/49057/websvn-2.3.3.zip
unzip websvn-2.3.3.zip
mv websvn-2.3.3 svn
rm websvn-2.3.3.zip
cd svn/include
cp distconfig.php config.php
vi config.php
```

Habilitamos el enscript para colorear el código, indicamos la ruta donde estarán los repositorios, la ruta del fichero de acceso a cada repositorio y, en el caso de que nuestro SVN sea privado y esté publicado a internet, quizás nos interese bloquear los robots.

```
$config->useEnscript();
$config->parentPath('/srv/svn');
$config->useAuthenticationFile('/srv/svn/authz');
$config->setBlockRobots();

```

Creamos un repositorio para utilizarlo en la verificación de acceso, nunca hay que olvidar cambiar los permisos o Apache no podrá crear ficheros en él.

```
mkdir /srv/svn/Test
svnadmin create /srv/svn/Test
chmod -R 770 /srv/svn/Test
chown -R wwwrun:www /srv/svn/Test
```

Para no tener que memorizar o apuntar el comando de creación de repositorios, he creado dos scripts, uno para [crear repositorios](/files/blog/svnCreateRepos.txt) crear y otro para [borrar repositorios](/files/blog/svnDeleteRepos.txt), por si fuera necesario alguna vez. Solo requieren modificar la primera variable con nuestra ruta para SVN.

## **2- Configurar Apache**

Con todas las herramientas listas, lo que falta es configurar Apache para publicar tanto los repositorios de SVN como el WebSVN.

Primero añadimos todos los módulos necesarios tanto para configurar el SVN en Apache cómo la autenticación por LDAP.

```
a2enmod dav
a2enmod dav_svn
a2enmod authz_svn
a2enmod mod_ldap
a2enmod mod_authnz_ldap
```

Ahora creamos un nuevo Virtual Host, este se encargará de publicar el WebSVN en la página inicial y los repositorios dentro de la carpeta SVN.

```
vi /etc/apache2/vhosts.d/001-svn.conf
```

Añadimos la información de las dos secciones indicando que se debe usar la autenticación de LDAP y la ruta al archivo de accesos para el SVN, recordad actualizar las rutas y usuarios para cada parte.

```
<VirtualHost *:80>
        ServerName svn.emmy.moon.cat

        DocumentRoot /srv/www/svn

        ErrorLog /var/log/apache2/svn-error_log
        CustomLog /var/log/apache2/svn-access_log combined

        HostnameLookups Off
        UseCanonicalName Off
        ServerSignature Off

        <Directory "/srv/www/svn">
                AuthType Basic
                AuthName "SVN"

                Require valid-user

                AuthBasicProvider ldap
                AuthLDAPBindDN "<ldapBindOrAdminUser>"
                AuthLDAPBindPassword "<password>"
                AuthLDAPURL "ldap://<ldapIP>:389/DC=moon,DC=cat?uid?sub?" NONE

                Options FollowSymLinks
                AllowOverride All
                Require all denied
        </Directory>
        <Location /svn>
                DAV svn
                SVNParentPath /srv/svn
                AuthType Basic
                AuthName "SVN"

                Require valid-user

                AuthBasicProvider ldap
                AuthLDAPBindDN "<ldapBindOrAdminUser>"
                AuthLDAPBindPassword "<password>;"
                AuthLDAPURL "ldap://<ldapIP:389/DC=moon,DC=cat?uid?sub?" NONE

                AuthzSVNAccessFile /opt/svn/authz
        </Location>
</VirtualHost>
```

Finalmente reiniciamos Apache.

```
rcapache restart
```

## **3- Configuración de acceso**

Por último, creamos el fichero con el acceso a cada repositorio, a grandes rasgos podemos configurar un grupo propio al que añadir usuarios y grupos o aplicarlos directamente en cada repositorio.

```
touch /srv/svn/authz
chown root:www /srv/svn/authz
chmod 640 /srv/svn/authz
vi /srv/svn/authz
```

Este es un ejemplo creando grupos y definiendo el repositorio que hemos creado previamente.

```
[groups]
developers = Inada,Hiramatsu,Kitamura

[Test:/]
@developers = rw
Daikawa = rw
Nakanishi = r
```

Para acceder al WebSVN utilizamos:

**http://svn.domain.com**

En cambio, nuestra herramienta de SVN utilizará la dirección real, en el caso del repositorio Test:

**http://svn.domain.com/svn/Test**

## **Final**

Con esto ya tenemos el SVN funcionando y conectado al LDAP con el resto de servicios, pero controlando quienes acceden a cada repositorio manualmente.

Cada vez que necesitemos un repositorio nuevo simplemente lo crearemos con el script e indicaremos quienes tendrán permiso
