---
title: ¡Métricas!
authors: rei.izumi
tags: [proxmox, prometheus, grafana]
---

Tras cambiar a Proxmox y jugar con Terraform para automatizar la creación de una plataforma Kubernetes (sí, la documentaré cuando la considere totalmente estable 😋), faltaba añadir las métricas.

En mi caso, las máquinas virtuales se utilizan casi en exclusiva para construir clústers de Kubernetes, y estos a su vez tienen métricas de cada nodo, mientras el Linux es monitorizado por [Nagios](/docs/rasp/config/nagios).

Y aun así, el tiempo me ha mostrado que no es suficiente, necesitas monitorizar también el host físico que gestiona a Proxmox.

Teniendo esto en cuenta, he creado una serie de [modificaciones para exportar métricas](/docs/it/proxmox/metrics) hacia un Prometheus existente, desplegado en el Kubernetes en mi caso, pero se puede ajustar hacia cualquier otro.

:::info
El objetivo es desplegar un clúster de Kubernetes auto-gestionado, que incorpore toda la plataforma y aplicaciones, con 1 comando. Está en camino ... 🥹
:::
