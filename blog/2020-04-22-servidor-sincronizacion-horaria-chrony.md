---
title: Sincronización horaria
authors: rei.izumi
tags: [NTP, Chrony]
---

Anteriormente publiqué una entrada de cómo configurar un [servidor de sincronización horaria utilizando NTP](/blog/2017/09/06/configurar-servidor-sincronizacion-horaria-ntp), debido a la importancia de que todos los servidores de una red tengan exactamente la misma hora para evitar problemas de sincronización.

Con el tiempo, ha surgido un nuevo proyecto Chrony, que se ha asentado como el nuevo estándar, por encima de NTP, debido a que varias ventajas.

A partir de OpenSuSE 15, NTP ha sido sustituido por Chrony, e incluso YaST ha sido modificado para incorporar a este, así que, aunque podemos seguir instalando el viejo NTP y borrar a Chrony, la mejor opción es dar el paso y cambiar hacia este.

<!-- truncate -->

## **0-Previos**

En mi caso dispongo de OpenSuSE Leap 15.1 que ya tiene el servicio chrony instalado, en caso de utilizar otra distribución que no lo tenga, habrá que instalarlo previamente, yo me saltaré esa parte.

Todos los comandos son con el usuario root.

## **1- Servidor**

Lo primero es configurar el servidor que sincronizará con servidores externos mientras toda nuestra red se sincronizará sobre este.

Toda la configuración de chrony se edita en el mismo fichero.

```
vi /etc/chrony.conf
```

En este fichero debemos indicar 3 apartados:

- Los servidores que utilizaremos para recuperar los datos (pool), en mi caso añadiré el pool de Europa.
- La red que podrá tener acceso a este sincronizador para recuperar la hora. En mi caso es la red 192.168.1.X.
- Activar el servicio como servidor, y no únicamente como cliente.

Es muy posible que ya nos encontremos con estos campos y únicamente tengamos que descomentar y editar según nuestras necesidades.

```
pool 1.europe.pool.ntp.org
pool 2.europe.pool.ntp.org
pool 3.europe.pool.ntp.org

allow 192.168.1.0/24

local stratum 10
```

Lo siguiente es abrir el puerto 123/UDP y reiniciar el servicio.

```
firewall-cmd --zone=public --add-port=123/udp --permanent
firewall-cmd --reload
rcchronyd restart
```

Esto es suficiente para que el servidor empiece a funcionar, así que los siguiente será conectar los clientes hacia él.

## **2- Cliente**

Aquí tenemos 2 opciones, o utilizar YaST para configurarlo (también está disponible en el momento de instalar OpenSuSE), o configurarlo manualmente. YaST no tiene mucho misterio, así que explicaré la versión manual.

Nuevamente editaremos el mismo fichero.

```
vi /etc/chrony.conf
```

Esta vez únicamente tenemos que indicar en el pool el nombre o IP del servidor que acabamos de configurar, si queremos tener otros por si acaso este fallara, podemos marcar el nuestro como preferido

```
pool ntp.domain.intranet iburst prefer
```

Después de reiniciar el servicio comenzará a sincronizar.

```
rcchronyd restart
```

En mi caso, el servicio ha tardado varios minutos en sincronizar, podemos mirar esta información con el servicio chronyc.

Resumiendo, estas son las que necesitaremos para confirmar que todo funciona:

```
chronyc tracking
```

Esta nos mostrará el **Leap status**, si es **Normal** es que ha sincronizado, en caso de fallar o que aún no esté sincronizado mostrará **Not synchronised**.

Este es un ejemplo:

```
Reference ID    : C0A80114 (ntp.moon.intranet)
Stratum         : 4
Ref time (UTC)  : Wed Apr 22 18:59:05 2020
System time     : 0.000020141 seconds fast of NTP time
Last offset     : +0.000021893 seconds
RMS offset      : 0.000026798 seconds
Frequency       : 23.604 ppm fast
Residual freq   : +0.203 ppm
Skew            : 0.752 ppm
Root delay      : 0.020367019 seconds
Root dispersion : 0.000975196 seconds
Update interval : 65.2 seconds
Leap status     : Normal
```

También podemos mostrar la lista de servidores a los que se ha conectado y si ha realizado la sincronización.

```
chronyc sources
```

Para nuestro caso, el único valor necesario será el **S**, donde indicará un **\*** en el último servidor que se ha utilizado para la sincronización.

En mi ejemplo:

```
210 Number of sources = 1
MS Name/IP address         Stratum Poll Reach LastRx Last sample
===============================================================================
^* ntp.moon.intranet       >     3   6   377    20    -16us[  -27us] +/- 11ms
```

En caso de mostrar un **?**, habría que revisar si el servicio está encendido y el puerto habilitado.

## **Final**

Con estos simples pasos tendremos las fechas de todos los servidores sincronizados y actualizados con el nuevo servicio.
