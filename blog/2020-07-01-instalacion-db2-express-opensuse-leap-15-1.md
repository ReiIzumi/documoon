---
title: DB2 Express en OpenSuSE Leap 15.1
authors: rei.izumi
tags: [DB2]
---

Solo unas pocas bases de datos han conseguido sobrevivir a lo largo de los años, y aun menos son las que compiten por usarse en grandes aplicaciones. Entre estas grandes bases de datos, posiblemente DB2 de IBM sea la más olvidada, aunque cuando compites contra Oracle, es difícil ganar, y Microsoft SQL Server tampoco se ha quedado corta en el mercado.

Aun con todo, DB2 ha seguido evolucionando para no quedarse atrás, y aunque IBM ha tenido ideas realmente malas en su evolución (como reemplazar la consola administrativa, ligera y funcional, por un eclipse imposible de instalar en el propio servidor y apenas sin nada), a seguido luchando hasta los tiempos actuales.

Con los años, he aprendido que DB2 está a la altura de sus rivales, y el único motivo por el que nunca ha despegado, es el tema de licenciamiento, IBM ofusca todo esto a límites, realmente, es más complejo entender o conseguir la licencia que quieres, que no instalar y utilizar esta base de datos.

Actualmente disponemos de la versión de Community Edition, que será la que utilizaré en este caso.

<!-- truncate -->

## **0- Previos**

Antes de empezar, habrá que conseguir el instalador, esto no tiene porqué ser fácil, ya que la web de IBM es volátil y cada cierto tiempo cambia.

En este caso, explicaré la instalación de la versión IBM DB2 Developer-C Edition 11.5 que es la que se despliega por defecto al instalar y no asignar ninguna otra licencia.

Hay que tener en cuenta, que el sistema de licencia entre la 11.1 y la 11.5 se ha reconstruido, anteriormente necesitabas la licencia de la Community Edition, ahora parece no existir y se despliega esta por defecto.

También, y aunque la web no lo indique, los límites de esta licencia son: 4 CPU, 16 GiB de RAM y 100 GiB de espacio en tabla.

Para descargar esta versión habrá que descargar la versión de prueba o community que se puede encontrar en alguna de estas webs:

[https://www.ibm.com/products/db2-database](https://www.ibm.com/products/db2-database)

[https://www.ibm.com/products/db2-database/developers](https://www.ibm.com/products/db2-database/developers)

Para ello se debe crear una cuenta de IBM.

En la instalación, utilizaré OpenSuSE Leap 15.1 y el usuario root, aunque se puede utilizar cualquier otra distribución sin apenas cambios.

Otro detalle es que DB2 puede ser instalado sin entorno gráfico, pero es absurdamente complejo, así que explicaré todo dando por hecho que se utiliza entorno gráfico, este se puede conseguir conectando directamente al servidor, o con VNC, o con Xming + PuTTY con las X11 activas.

## **1- Instalación**

Una vez se ha subido el fichero al servidor, se podrá empezar la instalación, empezando por descomprimir.

```
tar -xzf v11.5_linuxx64_dec.tar.gz
```

Debido a que el instalador revisa los requerimientos del sistema y OpenSuSE no está entre los soportados, hay que iniciar saltando esa revisión.

```
server_dec/db2setup -f sysreq
```

A partir de aquí, ya se inicia el entorno gráfico.

En los primeros pasos deberemos aceptar licencias, creación de instancias y elegir la carpeta de destino.

```
DB2 Version 11.5.0.0 Server Editions

Directory: /opt/IBM/db2/V11.5
Installation type: Typical
Create an instance: Checked
I agree to the IBM terms: Checked
```

Tras esto hay que crear los usuarios, en mi caso, siempre muevo el Home Directory, debido a que es el que tendrá todos los datos, y el disco de un servidor no suele tener el espacio en /home, si no en otro sitio, en mi caso es /opt

```
Instance Owner
	db2inst1
	Home directory: /opt/db2-data/db2inst1
Fenced User
	db2fenc1
	Home directory: /opt/db2-data/db2fenc1
```

Con esto, ya será aceptar todo y esperar a que termine la instalación.

Tras acabar, abrimos el puerto.

```
firewall-cmd --zone=public --add-port=50001/tcp --permanent
firewall-cmd --reload
```

Y acceder con el nuevo usuario db2inst1 para validar la instalación y confirmar el tipo de licencia.

```
su - db2inst1
db2val
db2licm -l

```

Por defecto, esta versión de DB2 tiene un auto iniciador, aunque es algo lento, así que, si lo probamos quizás nos de falsos negativos, dejándole tiempo veremos cómo realmente arranca por sí mismo, así que obviaré ese paso.

## **2- Crear bases de datos**

En DB2, cada base de datos es independiente, aunque utilice el mismo usuario, estas no tienen por qué ser visibles directamente entre ellas.

Al crear una base de datos, se deben elegir varias opciones, pero en mi caso, siempre utilizo los mismos, así que he creado un script para ello que se puede **[descargar aquí](/files/blog/db2CreateDB.zip)**.

Este script únicamente requiere indicar el usuario que será utilizado y los permisos suficientes para ejecutarlo.

| Nombre | Descripción | Valor por defecto |
| --- | --- | --- |
| DB2\_USER | Usuario de DB2 con el que se creará la base de datos | db2inst1 |

 

| Ruta | Permisos |
| --- | --- |
| /sbin/db2CreateDB.sh | 700 |

## **3- Copias de seguridad**

DB2 dispone de varios comandos para crear copias de seguridad, debido al sistema de transacciones, el sistema más seguro de todos es capaz de exportar incluso estas transacciones, si utilizamos una copia de seguridad sin transacciones tomada justo mientras una se estaba haciendo, es posible que nuestra copia sea inservible.

El otro detalle es que, a grandes rasgos, una copia de seguridad creada en una versión deberá ser importada en una exactamente igual, esto es algo peligroso cuando estás moviendo de una versión a otra o si no consigues reproducir una exactamente igual a ella, por estos casos, yo prefiero utilizar el comando **db2move**, que permite exportar e importar de un DB2 a otro, incluso entre versiones diferentes. No es la mejor opción, pero es la que me ha resultado más útil.

Este comando se utiliza desde el usuario db2inst1 y exporta todas las tablas en la carpeta donde esté situado el usuario, por defecto exporta todos los esquemas, pero es posible limitarlo, estos son los comandos básicos:

```
#Exportar todos los esquemas de una base de datos
db2move <dbName> export

#Exportar un esquema concreto y seguir exportando, aunque se encuentren warnings
db2move <dbName> export -sn \<schemaName\> -aw

#Importar los ficheros de la carpeta en la base de datos indicada
db2move <dbName> import
```

Para simplificar, he creado un script que, unificado al sistema que expliqué en [este artículo](/blog/2020/05/01/copias-seguridad-notificadas-nagios), permite a Nagios ir vigilando que se creen las copias de seguridad, este se puede [**descargar aquí**](/files/blog/backup_DB2.zip).

Solo contiene un fichero, esta es la explicación de las configuraciones que difieren del original:

| Nombre | Descripción | Valor por defecto |
| --- | --- | --- |
| DB2\_PORT | Puerto de DB2, únicamente utilizado para confirmar que esté activo | 50001 |
| DB2\_USER | Usuario que contiene las bases de datos a exportar | db2inst1 |
| DB2\_DATABASES | Lista de bases de datos, separadas por coma. Por defecto, exporta todos los esquemas, en caso de querer indicar uno concreto, se debe utilizar el formato \<dbName\>:\<schemaName\> |  |

Y los permisos del script:

| Ruta | Permisos |
| --- | --- |
| /opt/scripts/backup/backup\_DB2.sh | 700 |

## **Final**

La instalación en si es de las más simples, realmente tardé más tiempo en entender el nuevo sistema de licenciamiento y encontrar la descarga, debido a que todo el proceso ha cambiado y este no se explica en ningún lugar, lo que es una lástima, porque DB2 siempre me ha dado grandes resultados durante los años y ha sido muy sencilla de utilizar.
