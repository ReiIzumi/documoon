---
title: MariaDB en OpenSuSE Leap 15.1
authors: rei.izumi
tags: [MariaDB]
---

Aunque la tecnología no ha parado de cambiar, MariaDB/MySQL sigue siendo una de las bases de datos más utilizadas para pequeños proyectos, además de seguir siendo OpenSource.

Su simplicidad y buen funcionamiento la hace muy práctica en muchos entornos y es raro no acabar teniendo al menos una de estas instaladas, por ejemplo, el proyecto Wordpress en el que se basa este blog, únicamente acepta esta base de datos.

<!-- truncate -->

## **0- Previos**

Utilizaré OpenSuSE Leap 15.1, aunque con ligeros cambios a la hora de instalar, será lo mismo, o muy similar, en otras distribuciones.

La idea en general es:

- Instalar MariaDB
- Mover la carpeta de destino
- Crear una primera base de datos
- Automatizar las copias de seguridad

Los comandos los ejecutaré como root.

## **1- Instalación**

Lo primero será instalar, activar el auto inicio e iniciar el servicio.

```
zypper -n in mariadb mariadb-client
systemctl enable mysql
rcmysql start
```

Con esto en marcha, se ejecuta el asistente para cambiar contraseñas y permisos.

```
mysql_secure_installation
```

Por defecto, únicamente las conexiones provenientes de localhost podrán acceder, si nuestros sistemas están fuera de ese rango, deberemos modificar o anular este control.

```
vi /etc/my.cnf
```

Para desactivarlo, simplemente se puede comentar la línea.

```
#bind-address    = 127.0.0.1
```

Abrimos el puerto en el firewall.

```
firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --reload
```

Y reiniciamos para dejar listo el servicio.

```
rcmysql restart
```

## **2- Mover carpeta de datos**

El anterior paso es suficiente para dejar MariaDB funcionando, pero podría ocurrir que la carpeta donde se almacenan los datos de la base de datos no fuera aquella que dispone del espacio real en disco, o que no fuera suficiente segura, en este caso, es posible moverla a otro lugar.

Lo primero es averiguar dónde está la carpeta actualmente.

```
mysql -u root -p -e "SELECT @@datadir;"
```

Por defecto, en OpenSuSE Leap el directorio es **/var/lib/mysql**, así que partiré de esa idea en las siguientes.

Habrá que copiar esta carpeta hacia su nuevo destino, en mi caso lo moveré hacia **/opt**, así que paramos la base de datos, creamos el nuevo lugar y copiamos todos los ficheros manteniendo los permisos originales.

```
rcmysql stop
mkdir /opt/mariadb-data
chown -R mysql:mysql /opt/mariadb-data
cp -rp /var/lib/mysql/* /opt/mariadb-data/
```

Editamos el fichero de configuración.

```
vi /etc/my.cnf
```

E indicamos la nueva carpeta.

```
datadir = /opt/mariadb-data
```

Finalmente volvemos a iniciar el servicio y ya podremos comprobar que apunta a la nueva carpeta.

```
rcmysql start
mysql -u root -p -e "SELECT @@datadir;"
```

## **3- Crear una base de datos**

Primero debemos acceder al sistema

```
mysql -u root -p
```

Este sería un ejemplo para crear una nueva base de datos, asignarle un usuario que puede acceder desde localhost y desde la red 192.168.1.0 con la misma contraseña en ambos, y que tiene todos los privilegios en esa base de datos accediendo desde localhost o desde esa red.

```
CREATE DATABASE newdatabase;
CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'newpassword';
CREATE USER 'newuser'@'192.168.1.%' IDENTIFIED BY 'newpassword';
GRANT ALL PRIVILEGES ON newdatabase.* TO 'newuser'@'localhost';
GRANT ALL PRIVILEGES ON newdatabase.* TO 'newuser'@'192.168.1.%';
FLUSH PRIVILEGES;
```

Con esta plantilla se puede generar cualquier diseño que queramos en temas de seguridad.

Si por alguna razón quisiéramos que un usuario tuviera acceso desde cualquier lugar, en vez de indicar localhost, la IP o rango de IP, deberíamos colocar %.

A la hora de aplicar permisos, el ALL se puede cambiar por aquellos permisos que queremos que el usuario tenga.

## **4- Copias de seguridad notificadas con Nagios**

Lo primero a indicar es el proceso manual de importación y exportación de bases de datos.

Para exportar una base de datos sin añadidos, se utiliza el siguiente comando.

```
mysqldump -u user -p databaseName > BackupDatabaseName.sql
```

E importarlo es igual se sencillo.

```
mysql -u user -p databaseName < BackupDatabaseName.sql
```

En cualquier caso, el usuario a utilizar deberá tener permisos para ello.

Partiendo de esta idea, para crear las copias de seguridad he utilizado una variante del sistema que cree para notificar utilizando Nagios, las instrucciones de este se encuentran en [este artículo](/blog/2020/05/01/copias-seguridad-notificadas-nagios).

Una vez el sistema tiene todos los scripts y requerimientos que utiliza el script original, se copia el **[nuevo script para MariaDB](/files/blog/backupMariaDB.tar.gz)**.

Después de descomprimir el fichero, obtendremos 2 ficheros a configurar:

- mariadb.cnf

Este contiene el usuario y contraseña que se utilizará para conectar a MariaDB.

El fichero es del formato estándar de MariaDB, así que se pueden añadir otras configuraciones en caso de ser necesario.

- backup\_MariaDB.sh

Este fichero se replica por cada base de datos o configuración que se quiera tener y será el que se ejecutará con Cron u otra aplicación que tengamos para automatizar el proceso.

Ya que se basa en la misma plantilla que el original para copiar ficheros o carpetas, solo indicaré las diferencias.

| Nombre | Descripción | Valor por defecto |
| --- | --- | --- |
| MARIADB\_IP | IP del servidor | localhost |
| MARIADB\_PORT | Puerto del servidor | 3306 |
| MARIADB\_CONFIG | Ruta hacia el fichero de configuración | /opt/scripts/backup/mariadb.cnf |
| MARIADB\_DATABASES | Nombre de las bases de datos a crear la copia de seguridad. En caso de tener más de una, deben separarse con un espacio |  |

Una vez configurado, se deberá iniciar manualmente para confirmar que todo funciona correctamente y se pueda configurar en el servicio de Nagios.

Como detalle, si se crea una copia de seguridad de más de una base de datos y una de ellas falla, se informará a Nagios como un error en el proceso, y dentro tendrá el listado de las que ha ido bien y las que han fallado. Las que han funcionado seguirán creando sus copias correctamente, el proceso no se detiene por encontrar un error.

Estos son los permisos que deberían tener:

| Ruta | Permisos |
| --- | --- |
| /opt/scripts/backup/mariadb.cnf | 600 |
| /opt/scripts/backup/backup\_MariaDB.sh | 744 |

## **Final**

Desplegar una base de datos con MariaDB es un proceso bastante rápido una vez se conocen los mínimos, a partir de aquí, se puede complicar tanto como sea necesario para mejorar el rendimiento, pero la base, es muy sencilla.
