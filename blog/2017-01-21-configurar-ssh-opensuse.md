---
title: SSH en OpenSuSE Leap 42.2
authors: rei.izumi
tags: [SSH, OpenSuSE]
---

Una de las típicas preguntas que nos hacemos después de instalar un nuevo servidor con Linux es que seguridad necesitará, y la primera de ellas seguramente irá sobre el servicio SSH.

Lo primero a señalar es que el servicio SSH permite acceder a la totalidad del sistema de forma remota, así que, si esto no es necesario, lo más importante será que esté desconectado, OpenSuSE siempre lo tiene desconectado por defecto.

<!-- truncate -->

## **0- Previos**

En este post explicaré las diferentes configuraciones que podremos utilizar acorde al nivel de seguridad necesario, no es lo mismo un servidor en una red privada y controlada que un servidor con el SSH publicado a internet, aun así, recordad que vuestro primer enemigo son vuestros usuarios, si les dejáis una puerta abierta, la usarán.

Daré por hecho que el servicio ya está iniciado y el puerto 22 está abierto en el Firewall, si no es así, podéis activarlo fácilmente desde Yast o al instalarlo (última pantalla antes de instalar, entre las últimas opciones indica de habilitarlo y abrir el puerto).

En todo momento se utilizará el usuario root.

## **1- Configuración**

Únicamente dispone de un fichero de configuración en el que trabajaremos continuamente, así que lo editamos

```
vi /etc/ssh/sshd_config
```

El fichero tiene comentarios para casi todos los comandos, por defecto su configuración nos servirá para un acceso típico con acceso con todos los usuarios con sus contraseñas y/o claves privadas. Casi todas las opciones aquí explicadas las podremos buscar y habilitar ya que estarán comentadas.

Empezaremos **desactivando las opciones** que están **obsoletas**, en este caso permitiremos solo conexiones de protocolo 2 de SSH y desconectaremos accesos de Rhosts.

```
Protocol 2
RhostsRSAAuthentication no
HostbasedAuthentication no
IgnoreRhosts yes
```

Lo primero y más típico es **cambiar el puerto**, por defecto el servicio SSH utiliza el puerto 22 y siempre se intentará acceder primero a este, si no nos importa tenerlo en un puerto diferente al normal, lo cambiaremos por cualquier otro.

```
Port 22
```

Quizás tengamos varios usuarios y no nos interese que algunos de estos puedan acceder o directamente tengamos una lista con únicamente los que sí pueden, o quizás sea un grupo el que tendrá acceso, **si disponemos de una lista de usuarios que pueden o no acceder la añadimos**, para ello tenemos varias opciones y utilizaremos la mejor en nuestro caso.

```
AllowUsers Jiro Chiyo
DenyUsers Yukiho
AllowGroups IT
```

También podemos **bloquear directamente a root** ya que será el origen de la gran mayoría de ataques, obligando a los usuarios a convertirse en él una vez accedido, un sistema bastante limitado según el tipo de uso que le demos a nuestro servidor.

```
PermitRootLogin no
```

Si no disponemos de la lista de usuarios, quizás **disponemos de la lista de IPs que tendrá acceso**, también nos servirá.

```
ListenAddress 192.168.1.2
```

Ahora empezaremos con el control de contraseñas, por supuesto deberemos tener algún sistema para evitar que las contraseñas de nuestros usuarios sean sencillas, pero podemos ir más allá, lo primero es **desactivar todas las contraseñas que estén vacías**.

```
PermitEmptyPasswords no
```

Lo siguiente es plantear si queremos que puedan utilizarlas o no, si no nos fiamos de sus contraseñas o queremos poderles bloquear la conexión sin tenerlos que añadir al bloqueo de usuarios, **podemos desactivar el uso de contraseñas** y obligarles a tener unos ficheros de claves para acceder a este servidor, si esa es la opción que queremos primero desactivaremos el acceso por contraseña. Más adelante explicaré como generar estos ficheros de claves.

```
PasswordAuthentication no
ChallengeResponseAuthentication no
```

Finalmente, y no menos importante, **añadiremos un banner** que informará al usuario de que acceder al sistema sin permiso es un acto delictivo, sin ello, cualquier acceso no autorizado no es considerado ilegal según las leyes típicas de la gran mayoría de países. Indicamos el path del fichero que contendrá el texto, por supuesto este texto debería ser revisado por un abogado.

```
Banner /etc/ssh/banner
```

Con esto ya tenemos diferentes controles de acceso, pero lo siguiente es **desconectar a los usuarios que han conseguido entrar**, uno de los típicos problemas son los usuarios que conectan y descuidan su PC, permitiendo el acceso a otras personas, así que podremos controlarlos añadiendo un intervalo de tiempo de comprobación si el usuario sigue activo y el número de veces que se permitirá que falle estos controles hasta que sea desconectado.

```
ClientAliveInterval 300
ClientAliveCountMax 3

```

Ya que estamos, modificamos el **log de acceso**, normalmente con INFO será suficiente.

```
LogLevel INFO
```

Una vez terminados todos los cambios, reiniciaremos el servicio.

```
rcsshd restart
```

## **2- Crear fichero de claves**

Utilizando ficheros de claves no importará la contraseña del usuario, mientras la clave pública esté en el servidor y el usuario tenga la privada, podrá acceder, además el fichero puede tener su propia contraseña para evitar accesos por robo del fichero.

Cada fichero está asociado a uno o varios usuarios, además se debe conocer el nombre de usuario, si s![](/files/blog/images/ssh_crear_fichero_claves.png)e utiliza en otro no funcionará.

Para generar las claves existen varias herramientas pero yo utilizaré la de Windows, [PuTTYgen](http://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) que se encuentra tanto en PuTTY como en WinSCP, dos herramientas de acceso y de subida de ficheros muy utilizadas con SSH.

Al abrir el PuTTYgen simplemente tendremos que indicar el número de bits a utilizar, **recomendaría no utilizar menos de 4096** aunque utilizar más ralentizará el proceso de negociación ya que será bastante grande.

Clicamos en Generate y vamos pasando el ratón por la zona en blanco para que se genere la clave, al terminar tendremos algo como la captura.

Añadimos el comentario que deseemos y una contraseña si nos hace falta, clicamos en Save private key para generar el fichero privado, este es el que deberá utilizar el usuario y el que indicaremos en las configuraciones de PuTTY, WinSCP u otros.

En el servidor iremos a la carpeta home del usuario y crearemos el fichero donde se guardará la clave pública.

```
cd /root
mkdir .ssh
vi authorized_keys
```

En este pegaremos el texto que nos marga la primera casilla y ya habremos terminado.

Una recomendación sería tener un fichero para cada usuario, así simplemente borraremos la línea de este y ya no tendrá acceso, si además es para un usuario no root, podemos quitarle los permisos de escritura para que no se cree sus propias claves.

No es necesario reiniciar el servicio.

## **3- Bloqueo de intentos de acceso**

Con todo lo explicado, si hemos elegido por los sistemas más restrictivos, conseguir acceder de forma ilegal a nuestro sistema será realmente complicado, pero si nuestro servicio está en una red peligrosa, o internet, recibirá intentos de acceso casi continuamente llegando a cientos de intentos al día, seguramente todos sin éxito, pero pongámoslo aún más complicado.

En estos casos instalaremos un programa que revisará los intentos de acceso fallido y baneará a estas IPs de forma que muchos boots caerán directamente en la red. Para ello utilizaremos [fail2ban](http://www.fail2ban.org).

Cabe destacar que esta aplicación no solo bloquea los accesos de SSH, tiene muchos más servicios, pero en este post nos dedicaremos únicamente a este servicio.

Lo primero será instalarlo.

```
zypper in fail2ban
```

Este se instala en la carpeta _/etc/fail2ba_n y con el fichero _fail2ban.conf_ podremos revisar el log y algunas otras variables. Por defecto su log está en _/var/log/fail2ban.log_

La configuración de cada servicio está en jail.conf pero no lo modificaremos, solo lo utilizaremos por si tenemos dudas sobre cómo escribir exactamente una variable, todos los cambios se realizaran en jail.local, un fichero creado especialmente para ello. También se pueden separar en diferentes ficheros dentro de la carpeta jail.d.

Empezamos a editarlo

```
vi /etc/fail2ban/jail.local
```

Lo primero que añadimos son las IPs o rango que no serán revisados para evitar problemas en aquellas partes que confiamos totalmente, si quisiéramos aceptar todas las IPs del rango 192.168.1.X utilizaríamos la siguiente configuración. Tanto esta como la siguiente línea van dentro de la categoría default.

```
[DEFAULT]
ignoreip = 127.0.0.1/8 192.168.1.0/24
```

Añadimos el tiempo de baneo y la cantidad de fallos que aceptamos antes de ser baneado.

```
bantime = 3600
maxretry = 2
```

Finalmente activamos el servicio para SSH

```
[sshd]
enabled = true
```

Activamos el arranque automático y reiniciamos el servicio y empezará a revisar los logs de acceso

```
systemctl enable fail2ban
rcfail2ban restart
```

Debemos tener en cuenta que mientras la aplicación esté encendida seguirá baneando y quitando los baneos según el tiempo e intentos configurados, si está apagada y la encendemos, revisará el log desde donde lo dejó, así que baneará también los que incumplieran las normas mientras estuvo apagado.

Esta aplicación utiliza dos tipos de baneos, uno por iptables y otro por hosts.deny, en este caso se activa el iptables así que podremos revisar quienes están bloqueados con el siguiente comando, revisando la sección de f2b-sshd y REJECT

```
iptables -L -n
```

Si queremos desbloquear a un usuario manualmente disponemos de un comando para ello.

```
fail2ban-client set sshd unbanip <ip>
```

Para probarlo simplemente podriamos fallar los intentos que tuvieramos configurados desde una IP que no sea ignorada y en el log veremos que activa el ban, al volver a intentarlo ni siquiera podremos ver que el servicio SSH está activo.

## **Final**

Llegados a este punto ya tendremos configurado nuestro servicio SSH con diferentes políticas de seguridad según las que hubiéramos elegido cosa que nos permitirá dormir bastante mejor, por supuesto esto no es todo y podemos ir aún más allá creando jaulas, recibiendo mails de estado y otros tantos, pero la funcionalidad de este post es poder generar una seguridad bastante fuerte sin llegar a sobrepasar los límites normales.
