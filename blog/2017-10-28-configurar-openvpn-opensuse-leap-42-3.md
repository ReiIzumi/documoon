---
title: OpenVPN en OpenSuSE Leap 42.3
authors: rei.izumi
tags: [OpenVPN, VPN]
---

Debido a que no siempre puedes estar físicamente en el lugar donde está tu server, o incluso puede que necesites un servicio de dentro de tu red cuando estás fuera, las VPN se han convertido en una de las grandes bases de conexión estés donde estés.

OpenVPN es un servicio que permite configurar fácilmente desde cualquier sistema operativo, incluso desde móviles, lo cual se convierte en unas de las opciones más aconsejables y además es gratis lo cual es una gran ayuda viendo el precio de la mayoría de VPNs del mercado.

<!-- truncate -->

## **0- Previos**

Para estos pasos se requiere la versión 42.3 de OpenSuSE, la 42.2 no dispone de todos los elementos que se piden, si tienes una anterior te recomiendo actualizar cuanto antes.

Mi configuración se basa en que cada usuario dispondrá de un certificado único con contraseña, este certificado se puede revocar en cualquier momento.

## **1- Instalar OpenVPN y Easy RSA**

Aunque por defecto ya debe estar preparado el TUN/TAP, nunca está de más confirmarlo.

```
cat /dev/net/tun
```

Debe indicar:

```
cat: /dev/net/tun: File descriptor in bad state
```

Instalaremos tanto OpenVPN como Easy-RSA para facilitar la creación de los certificados.

```
zypper in openvpn easy-rsa
```

## **2- Configurar certificados**

Editamos la configuración por defecto de los certificados, lo que nos interesa son los datos de organización, país, e-mail, etc. También podemos aumentar la encriptación si queremos más seguridad.

```
vi /etc/easy-rsa/vars
```

Descomentamos y cambiamos las siguientes líneas.

```
set_var EASYRSA_REQ_COUNTRY     "US"
set_var EASYRSA_REQ_PROVINCE    "California"
set_var EASYRSA_REQ_CITY        "San Francisco"
set_var EASYRSA_REQ_ORG "Copyleft Certificate Co"
set_var EASYRSA_REQ_EMAIL       "me@example.net"
set_var EASYRSA_REQ_OU          "My Organizational Unit"

set_var EASYRSA_KEY_SIZE       2048
```

Empezamos generando la carpeta raíz.

```
easyrsa clean-all
```

Con esto ya tendremos la carpeta _/etc/easy-rsa/pki/_ donde irán todos los certificados.

Lo siguiente es generar el CA.

Este nos pedirá una contraseña, cada vez que incluyamos cualquier usuario nuevo necesitaremos esta contraseña. Todas las aplicaciones automáticas necesitan que este certificado no tenga contraseña, así que habrá que decidir si le ponemos contraseña o si lo securizamos desde el Linux a partir de qué queramos.

```
easyrsa build-ca
```

Generamos el certificado del servidor. También podremos elegir entre tener o no contraseña, con la diferencia de que no habrá problema en que la tenga, mientras esta esté en un fichero que OpenVPN pueda leer.

Para diferenciarlo más fácilmente del resto, lo llamaré "server". En el caso de utilizar contraseña, también creamos un fichero donde indicar esa contraseña.

```
easyrsa build-server-full server
vi /etc/easy-rsa/pki/private/server.pass
chmod 600 /etc/easy-rsa/pki/private/server.pass
```

Finalmente creamos un primer usuario. Recordar cambiar _\<userName\>_ por el nombre que utilizará el usuario. Este certificado también puede tener contraseña, cosa bastante recomendable. El usuario después puede cambiarla mediante el OpenVPN Client.

```
easyrsa build-client-full <userName>
```

Con todo esto creado, ya podemos generar el fichero maestro. Este puede tardar bastante según el nivel de encriptado y la CPU.

```
easyrsa gen-dh
```

También dejamos creado el fichero para revocar certificados.

```
easyrsa gen-crl
```

## **3- Configurar VPN**

Ahora tenemos que configurar OpenVPN de forma que utilice estos certificados.

Podemos tener varias configuraciones diferentes, pero en mi caso solo crearé una llamada igual que el certificado: server.

Creamos la carpeta de logs y el fichero de configuración.

```
mkdir /var/log/openvpn
vi /etc/openvpn/server.conf
```

Modificamos el siguiente acorde a nuestros datos, ya he dejado comentarios en el fichero para poder configurarlo al gusto. Como recomendación, suelo cambiar el puerto, debido a que el puerto por defecto tenderá a recibir más ataques que el resto.

En este ejemplo se considera que la red original es 192.168.1.X y la VPN tendrá la 192.168.240.X.

```
#VPN Port and protocol (UDP or TCP).
port 1194
proto udp

#Enable management. IP and port to access (localhost recommended).
management localhost 7505

#Routed type and topology.
dev tun
topology subnet

#Certificate Configuration

#CA certificate
ca /etc/easy-rsa/pki/ca.crt
#Server Certificate
cert /etc/easy-rsa/pki/issued/server.crt
#Revoked certificates
crl-verify /etc/easy-rsa/pki/crl.pem
#Server Key and password
askpass /etc/easy-rsa/pki/private/server.pass
key /etc/easy-rsa/pki/private/server.key
#Master
dh /etc/easy-rsa/pki/dh.pem

#VPN internal IP
server 192.168.240.0 255.255.255.0

#Select gateway to redirect all traffic through our OpenVPN or private to redirect only the internal network traffic.
#push "redirect-gateway def1"
push "redirect-private"
#Local network. Not recommended 192.168.0.X or 192.168.1.X because they are the most used.
push "route 192.168.1.0 255.255.255.0"

#Provide DNS servers to the client. One line for each DNS.
push "dhcp-option DNS 192.168.1.5"

#Allows to connect more than one connection to the same certificate.
#duplicate-cn

#Recommended security protocol.
cipher AES-256-CBC

keepalive 20 60
comp-lzo
persist-key
persist-tun
daemon

#Status log with each connected user.
status /var/log/openvpn/openvpn-status.log
#Log path and level.
log-append /var/log/openvpn/openvpn.log
verb 3
```

Con todo configurado, iniciamos el servicio y el autoarranque.

```
systemctl start openvpn@server
systemctl enable openvpn@server
```

Ahora toca configurar las rutas para que el servidor se encargue de hacer de gateway interno. Al mismo tiempo abrimos el puerto.

```
vi /etc/sysconfig/SuSEfirewall2
```

Tenemos que habilitar el enrutado, abrir el puerto UDP o TCP elegido y activar el uso de reglas personalizadas.

```
FW_ROUTE="yes"
FW_SERVICES_EXT_UDP="1194"
FW_CUSTOMRULES="/etc/sysconfig/scripts/SuSEfirewall2-custom"
```

Lo siguiente es utilizar ese fichero para reglas personalizadas.

```
vi /etc/sysconfig/scripts/SuSEfirewall2-custom
```

Dentro de la directiva _fw\_custom\_after\_chain\_creation_ añadimos nuestra regla acorde a la red interna.

```
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -s 192.168.240.0/24 -j ACCEPT
iptables -A FORWARD -j REJECT
iptables -t nat -A POSTROUTING -s 192.168.240.0/24 -o eth0 -j MASQUERADE
```

Activamos el forwarding.

```
vi /etc/sysctl.conf
```

Añadiendo la siguiente línea.

```
net.ipv4.ip_forward = 1
```

Después de todos los cambios, reiniciamos el firewall.

```
rcSuSEfirewall2 restart
```

El servidor ya está listo para ser utilizado.

## **4- Configurar cliente**

Cada sistema necesitará tener instalado el cliente de OpenVPN, ya sea Windows, Linux, iOS, etc.

Podemos encontrarlo desde el sistema de repositorio propio o desde la [web oficial](https://openvpn.net/index.php/open-source/downloads.html).

El usuario necesita el certificado **_ca.crt_** (/etc/easy-rsa/pki/ca.crt) y sus ficheros de claves **_crt_** (/etc/easy-rsa/pki/issued/\<userName\>.crt) y **_key_** (/etc/easy-rsa/pki/private/\<userName\>.key). Además, tenemos que generar un fichero **_ovpn_** con la configuración.

Para facilitar la creación de usuarios, he preparado un **[script](/files/blog/vpnCreateUser.txt)** en el que se indica el nombre del usuario, lo genera y comprime en un zip todos estos ficheros para que los enviamos al usuario. También tendremos que generar el fichero ovpn acorde a esos ficheros.

Este es un ejemplo del fichero ovpn.

```
client
remote vpn.domain.cat 1194
ca "prefix_ca.crt"
cert "prefix_<userName>.crt"
key "prefix_<userName>.key"
comp-lzo yes
dev tun
proto udp
nobind
auth-nocache
script-security 2
persist-key
persist-tun
cipher AES-256-CBC
remote-cert-tls server
```

Estos 4 ficheros se deben guardar en la carpeta de configuración (en Windows: C:\\Users\\\<user\>\\OpenVPN\\config).

El cliente de OpenVPN detectará automáticamente este fichero y ya podrá utilizarlo.

**Fix para iOS/Android:**

Los clientes para estos dispositivos móviles no aceptan el certificado encriptado, para ello previamente hay que modificarlo al formato RSA, lo podemos hacer desde el servidor reseteando la contraseña del certificado.

```
easyrsa set-rsa-pass /path/to/<userName>.key
```

## **5- Gestión de usuarios**

Disponemos de varias herramientas para gestionar los usuarios, aquí pongo algunas de las más útiles.

### 5.1- Revocar certificados

Utilizar el siguiente comando indicando el nombre del usuario.

```
easyrsa revoke <userName>
easyrsa gen-crl
```

Esto revoca el usuario y regenera el fichero de revocaciones, cuando ese usuario intente conectar recibirá el siguiente error.

```
Wed Oct 18 20:05:17 2017 TLS Error: TLS key negotiation failed to occur within 60 seconds (check your network connectivity)
Wed Oct 18 20:05:17 2017 TLS Error: TLS handshake failed
```

### 5.2- Listado de todos los usuarios y anular una revocación

No existe una forma de anular una revocación como tal, pero podemos regenerar el fichero.

Todos los usuarios se guardan en el siguiente fichero.

```
vi /etc/easy-rsa/pki/index.txt
```

En este fichero se indica quienes están revocados, si cambiamos la revocación por un V(alid) y quitamos la fecha de revocación, volveremos a permitir su acceso.

```
#Valid/Revoked	Creación		Fecha revocado	ID												Common name
V				271015150951Z                   AA900E5A8C2E0F7C5DFFA46AD89CC5DA        unknown /CN=validUser
R				271016144645Z   171018180142Z   9ADD03D7DB79EED1F5BCC2533F1FD2E4        unknown /CN=revokedUser
```

Si cambiamos las revocaciones, tendremos que regenerar el fichero.

```
easyrsa gen-crl
```

### 5.3- Usuarios conectados actualmente

Simplemente lo leemos del fichero de status.

```
cat /var/log/openvpn/openvpn-status.log
```

### 5.4- Desconectar un usuario

Al revocar un usuario, este seguirá conectado, así que debemos revocar y después echarlo. Si simplemente lo desconectamos, el OpenVPN Client volverá a reconectar.

Conectamos al management y lanzamos un kill al usuario. El puerto variará según nuestra configuración.

```
telnet localhost 7505
kill <userName>
```

## **Final**

La configuración y preparación de todo el entorno requiere bastante tiempo y pruebas, pero una vez acabado, OpenVPN se convierte en una VPN de las más fiables y estables.

Su capacidad para reconectar automáticamente si hubiera cualquier cambio permite que los clientes puedan estar conectados indefinidamente.
