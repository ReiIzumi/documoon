---
title: Web, Rasp Project y Swarm Project
authors: rei.izumi
tags: [Docker Swarm, Raspberry]
---

Podría mentir descaradamente diciendo que fue hace 4 días cuando empecé un nuevo proyecto, uno más grande que ningún otro de los que he llevado a cabo: renovar mi infraestructura para utilizar contenedores.

Claro que, cuando empiezas a poner una piedra, te das cuenta de que quizás deberías poner más piedras, y construir un muro, y mover ese río de ahí, construir un puente, el pueblo, el castillo, ¡una ciudad!

Y así es como se han llevado a cabo varios proyectos en paralelo: la [renovación de la web](https://www.moon.cat), el [Rasp Project](/docs/rasp/introduction), y el [Swarm Project](/docs/swarm/introduction).

<!-- truncate -->

## **Introducción**

Con los nuevos tiempos, tener máquinas virtuales está bien, pero no es lo más eficiente, los contenedores llegaron hace tiempo para quedarse, pero esto tampoco es sencillo, todo está orientado a usarse en modo cloud

¿Quieres un Kubernetes? ¡genial! compra el de una Cloud ya montado, ¿tienes servidores propios (baremetal)? pues lo siento, porque todo está diseñado para la Cloud.

Aunque el mundo cambia, mi preferencia por el baremetal no ha cambiado, así que empecé a plantear un proyecto con Kubernetes al que empecé a añadir por este blog, un spoiler: no funcionó.

## **Swarm Project**

Y así fue como nació el Swarm Project, un proyecto donde se construye un clúster con Docker Swarm, este contiene muchos de los servicios que ya había construido durante los años (como Nexus) y se han añadido unos nuevos (Keycloak, MinIO, Grafana, ...).

Este proyecto está [documentado](/docs/swarm/introduction/) para que cualquier persona pueda construir el mismo sistema, aunque requiere de conocimientos altos en Docker Swarm y es muy aconsejable tener conocimientos medio/altos en Linux e infraestructura (sobre todo en GlusterFS).

## **Rasp Project**

Al empezar a construir el Swarm Project detecté problemas a más bajo nivel, toda mi red depende demasiado de un único servidor que contiene máquinas virtuales, si este se apaga (por actualización, por mantenimiento), la red dejaba de funcionar completamente (perder las webs aun, pero sin DNS y DHCP no llegas muy lejos).

Eso me hizo quitar el polvo de una Raspberry Pi 4 que había comprado hacía tiempo para hacer pruebas, y las pruebas salieron bien, demasiado.

Movería los servicios esenciales hacía ella, pero seguiría teniendo el mismo problema: si la Raspberry entraba en mantenimiento, la red moriría, y eso me hizo plantear desplegar 2 sincronizadas.

Ahí me metí en el oscuro mundo de las Raspberry, donde hay cientos de proyectos y ninguno me acababa de convencer, con lo que construí el mío.

Así nació el [Rasp Project](/docs/rasp/introduction), otra locura de proyecto para gestionar los DNS, DHCP, NTP y otros.

## **Nueva web y documentación**

Construir todo eso requería una gran documentación, GitLab es perfecto para muchas cosas, pero no es el sitio ideal para documentar a mi gusto, y Wordpress está muy bien, pero la magnitud de estos proyectos era demasiado grande como para organizarla fácilmente en un blog, así fue como llegué a [Docusaurus](https://docusaurus.io).

Este proyecto permite crear webs, documentación y blogs de una forma bastante simple, sus blogs no me convencen, pero todo lo demás me iba perfecto, por desgracia aún estaba en unas fases muy iniciales de Alpha (¡¡justo acaba de salir la primera Beta!!) y no disponía de todo lo que yo necesitaba, algo que no fue un problema, ya que fueron añadiéndolo mientras yo iba trabajando en los 2 grandes proyectos.

Ya que estaba, esta nueva web sería el primer proyecto para desplegarse en producción y test utilizando el sistema de CI/CD, porque las cosas o se hacen a lo grande o ... bueno, no es que hubiera alternativa, ¡se hacen a lo grande!

Y así es como hoy, finalmente, después de más de medio año (en serio, más de medio año ...) de arduo trabajo y muchas, muchísimas, millones de pantallas de ERROR. ¡Se han desplegado y pasado a producción ambos proyectos y la nueva web!

Por supuesto, ninguno de estos proyectos ha terminado, cada uno tiene su historial con roadmap y nuevos proyectos aparecerán cuando todo eso esté acabado (o en paralelo, quien sabe). Mientras, seguiré utilizando este blog para avisar de los cambios, aunque la documentación pasará a la nueva web.
