---
title: Apache con proxy http
authors: rei.izumi
tags: [Apache, Proxy]
---

En caso de publicar una página que requiera de servidor de aplicaciones (cualquiera programada en Java u otros idiomas normalmente compilados), este servidor de aplicaciones podrá procesar los datos y publicar la web resultante, pero no tiene por qué ser eficiente para ser publicado a internet.

En estos casos se utiliza un servidor web que recibe las peticiones de internet e internamente hace una llamada al servidor de aplicaciones, de esta forma este servidor web (Apache en este caso) será el que recibirá todas las peticiones y se encargará de gestionarlas liberando parte de esta faena a nuestro servidor de aplicaciones.

<!-- truncate -->

## **0-Previos**

De servidor web utilizaré Apache junto a los módulos de proxy, existen muchos proxys por defecto y algunos ya están orientados a hacer esto mismo como el protocolo [AJP](https://es.wikipedia.org/wiki/Apache_JServ_Protocol), pero este no existe en todos los tipos de servidores así que utilizaré el proxy http que funciona para la gran mayoría de casos.

En todo momento se utilizará el usuario root.

## **1- Configurar proxy http**

Lo primero es activar los dos módulos necesarios.

 

```
a2enmod proxy
a2enmod proxy_http
```

Con esto activo, solo necesitamos añadir el virtual hosts para cada dominio que utilizaremos, cabe destacar que no podemos crear varios virtual hosts con el mismo dominio, en este caso ese fichero contendrá todas las direcciones de proxys.

El virtual hosts será similar al siguiente fichero.

```
<VirtualHost *:80>
        ServerName domain.cat

        DocumentRoot /srv/www/default

        ProxyPass /App1 http://<ip>[:<puerto>]/App1
        ProxyPassReverse /App1 http://<ip>[:<puerto>]/App1
        ProxyPass /App2 http://<ip>[:<puerto>]/App2
        ProxyPassReverse /App2 http://<ip>[:<puerto>]/App2
</VirtualHost>
```

Esto le permitirá enmascarar las direcciones hacia fuera y el servidor de aplicaciones recibirá la información externa.

Al reiniciar se aplicarán los cambios y ya podremos acceder indicando el dominio y el nombre de la aplicación.

## **Final**

Con estos pasos tan simples tenemos el proxy http activo, solo tenemos que recordar añadir todas las aplicaciones que utilicen el mismo dominio en el mismo fichero y, en la gran mayoría de casos, mantener el nombre de la aplicación igual al original, ya que es muy posible que la aplicación utilice direcciones relativas con su nombre de aplicación y, al no tenerlo, fallarían.
