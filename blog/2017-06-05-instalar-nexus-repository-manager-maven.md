---
title: Nexus Repository Manager para Maven
authors: rei.izumi
tags: [Nexus, Maven]
---

En el campo de la programación hay muchos servicios que son necesarios en el día a día, aparte de los repositorios de código, están los repositorios de librerías. Las librerías construidas por otros o por un equipo propio necesitan ser almacenadas en algún servidor donde puedan ser recuperadas fácilmente, por ejemplo, Maven permite conectar a los diferentes repositorios compatibles en busca de las librerías necesarias para compilar una aplicación. Entre los diferentes repositorios de código se encuentra Nexus Repository Manager, que dispone de una versión gratuita que es más que suficiente para el uso normal.

<!-- truncate -->

## **0- Previos**

Me centraré en la instalación de Nexus para funcionar sobre LDAP y con un proxy inverso de Apache que será quien publicará el servicio, Nexus dispone de muchas configuraciones que no voy a explicar. Para conocer todo lo que puede hacer, existe la [documentación oficial](https://books.sonatype.com/nexus-book/index.html).

Actualmente existe la versión 2 y 3 de Nexus, debido a que a mí me interesa el uso en Maven, descargaré la 2 que es compatible con este servicio.

Iré alternando entre el uso de root y el usuario propio de Nexus, a no ser que se indique lo contrario, utilizaré el de root.

## **1- Instalación**

Empezamos descargando la última versión de Nexus 2 desde la [web oficial](https://www.sonatype.com/download-oss-sonatype).

```
cd /opt
wget http://www.sonatype.org/downloads/nexus-latest-bundle.tar.gz
tar -zxf nexus-latest-bundle.tar.gz
rm nexus-latest-bundle.tar.gz
mv nexus-2.14.4-03 nexus
```

Esto nos deja la carpeta _nexus_ que contiene el sistema y la carpeta _sonatype-work_ donde irán los repositorios, tal como está ya puede ser utilizado Nexus pero vamos a ir un poco más allá.

Editamos el fichero de configuración.

```
vi nexus/conf/nexus.properties
```

En este encontramos el puerto, path de la URL y la carpeta de repositorios, por ahora cambiamos el puerto y la carpeta de repositorios si es necesario. Recordad abrir el puerto elegido.

Para arrancar el servicio tenemos 2 opciones, o lo configuramos para aceptar el arranque como root o le creamos un usuario propio, ya que la 2a opción es la recomendada y que es muy posible que este servidor se comparta con otros servicios, le crearé su propio usuario.

El usuario se puede crear con Yast con las opciones por defecto, así que saltaré ese paso, simplemente indicar que mi usuario se llamará **_nexus_**.

Pasamos el control de nexus al nuevo usuario.

```
chown -R nexus:root /opt/nexus
chown -R nexus:root /opt/sonatype-work
```

Nos convertimos en el nuevo usuario e iniciamos el servicio.

```
su - nexus
/opt/nexus/bin/nexus start
```

Después de dejarle suficiente tiempo, podremos acceder desde la URL:

```
http://<ip>:8081/nexus
```

Si funciona, lo apagamos para poder configurar el resto.

```
/opt/nexus/bin/nexus stop
```

## **2- Autoarranque**

Debido al arranque por usuario propio y que el script es un tanto especial, dejamos listo ya el autoarranque y los servicios para agilizar.

Volvemos a ser root, copiamos el script y empezamos a modificarlo.

```
cp /opt/nexus/bin/nexus /etc/init.d/nexus
vi /etc/init.d/nexus
```

Me centraré en tres variables necesarias para iniciarlo, la carpeta donde se encuentra Nexus, el usuario que lo inicia y la carpeta donde se almacenará el PID.

```
NEXUS_HOME="/opt/nexus"
RUN_AS_USER="nexus"
PIDDIR="/opt/nexus"
```

Con esto ya podemos activar el servicio como tal y volver a iniciarlo.

```
chown root:root /etc/init.d/nexus
chmod 755 /etc/init.d/nexus
insserv /etc/init.d/nexus
ln -s /etc/init.d/nexus /sbin/rcnexus
rcnexus start
```

## **3- LDAP y roles**

Accedemos a la administración con el usuario **admin** y contraseña **admin123**.

Primero activamos el uso del LDAP para no olvidarnos.

```
Administration - Server
Security Settings
```

Añadimos el **OSS LDAP Authentication Realm** y guardamos.

Ahora vamos a la configuración del LDAP.

```
Security - LDAP Configuration
```

La configuración variará según nuestro tipo de LDAP, si tenemos un Oracle DSEE hay que cambiar o añadir los siguientes:

- Añadir los datos de Connection y Authentication para acceder.
- Password Attribute: userPassword
- Group Member Format: \$\{dn\}

Si ha pasado los test y reconocido los usuarios, ya podemos ir a configurar los roles.

```
Security - Roles
```

Añadimos un rol externo nuevo para los administradores de Nexus, estos permisos se asignan a un grupo del LDAP. Para que sean administradores le asignamos el siguiente rol:

- Nexus Administrator Role

Salimos del usuario actual y entramos con un administrador según el grupo del LDAP.

Si ha funcionado, ya podemos deshabilitar todos los usuarios en Users y seguir creando nuevos roles según necesitemos.

_Recordar que para subir ficheros manualmente se necesita el permiso de **Artifact Upload** para poder ver esa pestaña en los repositorios a los que se tiene acceso._

## **4- Proxy inverso con Virtual Host de Apache**

Apache se encargará del SSL y de hacer de frontal, la conexión interna hacia Nexus se mantendrá en el puerto 8081 y no necesitamos que este tenga SSL ya que la conexión entre ellos debería ser físicamente segura en nuestra red.

Debido a que usaré un Virtual Host, que la URL tenga el contexto nexus al final quedaría feo, así que lo primero será quitarlo.

```
rcnexus stop
vi /opt/nexus/conf/nexus.properties
```

Cambiamos el contexto para usar la raiz.

```
nexus-webapp-context-path=/
```

Ya podemos volverlo a iniciar.

```
rcnexus start
```

Los siguientes pasos son en el servidor que contiene el Apache, daré por hecho que ya tiene los módulos para proxy inverso activos.

Creamos un nuevo Virtual Host, para HTTP contendrá:

```
<VirtualHost *:80>
        ServerName nexus.domain.cat

        ProxyPass / http://<serverName>:8081/
        ProxyPassReverse / http://<serverName>:8081/
</VirtualHost>
```

Si queremos activar Apache en SSL, creamos los certificados y el nuevo Virtual Host con:

```
<IfDefine SSL>
<IfDefine !NOSSL>

<VirtualHost *:443>
        ServerName nexus.domain.cat

        SSLEngine on

        SSLCertificateFile /etc/apache2/ssl.crt/nexus.domain.cat-server.crt
        SSLCertificateKeyFile /etc/apache2/ssl.key/nexus.domain.cat-server.key

        ProxyPass / http://<serverName>:8081/
        ProxyPassReverse / http://<serverName>:8081/
        RequestHeader set X-Forwarded-Proto "https"
</VirtualHost>

</IfDefine>
</IfDefine>
```

Después de recargar la configuración de Apache, podremos acceder desde él.

## **5- Configurar repositorios en cliente**

Nexus dispone por defecto varios repositorios activos pero cada PC cliente necesita saber la URL para acceder a cada repositorio.

En el caso de Maven, esto se gestiona desde el fichero de settings, en el caso de Windows se encuentra en el siguiente path.

```
C:/Users/<user>/.m2/settings.xml
```

Es posible que el fichero ya hubiera sido creado o modificado para otras configuraciones, en cualquier caso, este es un ejemplo para activar los repositorios de terceros y las releases propias.

```
<settings>
	<profiles>
		<profile>
			<id>NexusLocal</id>
			<repositories>
				<repository>
					<id>NexusLocal_ThirdParty</id>
					<name>Nexus Local - Third Party</name>
					<url>http://nexus.domain.cat/content/repositories/thirdparty/</url>
				</repository>
				<repository>
					<id>NexusMoon_Releases</id>
					<name>Nexus Local - Releases</name>
					<url>http://nexus.domain.cat/content/repositories/releases/</url>
				</repository>
			</repositories>
		</profile>
	</profiles>
	<activeProfiles>
		<activeProfile>NexusLocal</activeProfile>
	</activeProfiles>
</settings>
```

## **Final**

Además de estas configuraciones tendremos que revisar qué repositorios vamos a utilizar, permisos para acceder o controlarlos, etc.

Con los repositorios activos ya se puede empezar a subir todas las librerías necesarias para desarrollar las aplicaciones.
