---
title: OpenSuSE 13.2 a Leap 15.0
authors: rei.izumi
tags: [OpenSuSE]
---

La distribución de GNU/Linux que más he utilizado durante años es OpenSuSE, desde mucho antes de que cambiara de manos o de que tuviera esta versión open, en su día me enamoré de YaST y, posteriormente, de zypper.

Esta ofrece muchas ventajas que te permiten hacer rápidamente cualquier configuración típica, a su vez, es casi idéntico a SuSE, la versión de pago, la cual es compatible con la gran mayoría de aplicaciones empresariales, realmente solo RedHat y SuSE son consideradas compatibles con todo lo empresarial.

Pero entre sus grandes ventajas, no está la capacidad de actualizar rápidamente, hay que seguir algunos tediosos pasos, así que en este post intento explicar una versión mínima y rápida para estas actualizaciones.

<!-- truncate -->

## **0- Previos**

Estas instrucciones no son la mejor versión, si no la más rápida de hacer, si quieres algo más profesional, hay muchas webs que explican el paso a paso, incluso la [web oficial](https://en.opensuse.org/SDB:System_upgrade).

OpenSuSE se debe actualizar de versión en versión hasta llegar a la última, no se pueden saltar versiones entre medio.

Antes de actualizar **hacer un backup**, si es de toda la máquina mejor, cualquier paso podría fallar perdiendo todo el servidor.

Mientras se actualiza el sistema, el servidor seguirá activo, pero es muy posible que muchos de sus servicios dejen de funcionar, así que se tendrá que hacer en algún momento en el que no afecte a quienes lo necesiten.

Estos pasos explican desde la 13.2 a la Leap 42.3, pero se puede imitar para versiones anteriores a la 13.2.

## **1- Antes de empezar**

**Recordar crear una copia de seguridad del sistema ANTES de actualizar (si es VMWare, podemos hacer un backup de toda la máquina y/o crear un snapshot). Me he encontrado muchos casos de actualizar máquinas hermanas y una fallar totalmente en la actualización, teniendo que volver atrás, incluso actualizar el propio sistema puede ser un peligro.**

Los cambios entre versiones menores "no deberían" dar demasiados problemas, pero los cambios mayores (13 a Leap 42 o Leap 42 a Leap 15) SIEMPRE dan problemas, así que a cada cambio hay que investigar que todos los servicios funcionan, que el sistema reinicie, que las X, de tenerlas, sigan funcionando, y un largo etc.

Necesitamos saber la versión actual.

```
cat /etc/os-release
```

Para la actualización, debemos siempre actualizar todos los paquetes del sistema (si es que es posible) y después subir de versión, para ello debemos dejar activos los repositorios de OpenSuSE y desconectar todos los repositorios de terceros, así como dvd/ISO o similares que pudiéramos tener.

Para hacer esto tenemos varias herramientas.

Listar todos los repositorios.

```
zypper lr
```

Borrar un repositorio.

```
zypper rr <alias>
```

Habilitar un repositorio.

```
zypper modifyrepo --enable <alias>
```

Incluso podemos hacer un backup de los repositorios si lo consideramos necesario.

```
cp -Rv /etc/zypp/repos.d /etc/zypp/repos.d.bak
```

Una de las herramientas que vamos a necesitar es tmux, este nos permite ejecutar ordenes dentro de una consola que no se apagará y que podemos recuperar, por si nuestra conexión fallara en medio de la actualización.

```
zypper in tmux
```

## **2- 13.2 a Leap 42.1**

Lo primero que debemos hacer siempre es actualizar la versión que tenemos, esto quizás no sea posible ya que, si la versión está deprecada, no tendrá actualizaciones, aun así podremos actualizar a ella y saltar a la siguiente.

Primero intentamos actualizar.

```
zypper ref
zypper up
```

Ahora actualizamos los repositorios, dos de ellos han cambiado en la Leap así que habrá que borrarlos y construirlos con el nuevo formato.

```
sed -i 's/13\.2/leap\/42\.1/g' /etc/zypp/repos.d/*
zypper rr repo-update repo-update-non-oss
zypper addrepo --check --refresh --name 'openSUSE-42.1-Update' http://download.opensuse.org/update/leap/42.1/oss/ repo-update
zypper addrepo --check --refresh --name 'openSUSE-42.1-Update-Non-Oss' http://download.opensuse.org/update/leap/42.1/non-oss/ repo-update-non-oss
zypper ref
```

Con los repositorios nuevos ya podemos actualizar el sistema y, al terminar, reiniciar.

```
tmux
zypper dup
init 6
```

La actualización hacia Leap conlleva muchos cambios, en varios servidores me he encontrado que no se reiniciaban, teniendo que apagar la máquina físicamente y volviendo a encenderla, incluso el tiempo de iniciado puede ser bastante superior a lo normal.

Una vez se inicia, **hará falta volver a configurar la IP**, cosa que podemos hacer rápidamente desde YaST, pero al no tener IP tampoco tendremos SSH, así que necesitamos acceso físico o por consola del sistema de virtualización que tengamos.

## **3- Leap 42.1 a Leap 42.2**

Entre versiones Leap, el proceso es muy sencillo, además es una réplica del anterior, actualizar repositorios, actualizar y reiniciar.

```
sed -i 's/42\.1/42\.2/g' /etc/zypp/repos.d/*
zypper ref
tmux
zypper dup
init 6
```

## **4- Leap 42.2 a Leap 42.3**

Volvemos a repetir el paso anterior con la versión nueva.

```
sed -i 's/42\.2/42\.3/g' /etc/zypp/repos.d/*
zypper ref
tmux
zypper dup
init 6
```

Con este ya estaremos en la versión más actual (acorde al día en que escribo este post) y que está dentro de soporte, sería conveniente confirmar que tenemos las últimas actualizaciones e ir haciendo el debido mantenimiento para no ir quedando fuera de mantenimiento acorde a los repositorios.

## **4- Leap 42.3 a Leap 15.0**

En caso de que el sistema original provenga de 13.2 o anteriores, esta actualización puede hacer que el sistema no vuelva a iniciar.

Después de este update, el Firewall habrá cambiado totalmente (debería tener el puerto 22 habilitado, pero nada más), así que apuntar los puertos antes de actualizar es la mejor opción.

Repetimos el paso anterior con la nueva versión.

```
sed -i 's/42\.3/15\.0/g' /etc/zypp/repos.d/*
zypper ref
tmux
zypper dup
init 6
```

Esta nueva versión dispone de algunos servicios (además del cambio de Firewall) que debemos tener en cuenta, si no lo tenemos claro, es mejor instalar una máquina nueva de pruebas para habituarnos a todos estos.

Para empezar, el firewall ahora se gestiona manualmente, YaST no dispone de asistente para ello. También han desaparecido las viejas funciones de networking (netstat, ifconfig, ...), aunque pueden ser instaladas.

El firewall es el que más problemas me ha dado ya que es completamente nuevo y, además, el servicio viejo se mantiene, así que el nuevo no autoinicia. Para ello hay que deshabilitar el viejo y definir el nuevo.

```
systemctl disable SuSEfirewall2.service
systemctl disable SuSEfirewall2_init.service
systemctl enable firewalld
```

La nueva versión también tiene más servicios para btrfs, si nuestro OpenSuSE no fue instalado con este servicio, lo podemos apagar, en mi caso únicamente uno estaba habilitado.

```
systemctl disable btrfsmaintenance-refresh.service
```

Finalmente, encendemos el firewall y le configuramos los puertos que necesitemos, este es un ejemplo añadiendo el puerto 5666/TCP para Nagios:

```
rcfirewalld start
firewall-cmd --zone=public --add-port=5666/tcp --permanent
firewall-cmd --reload
rcfirewalld restart
```

## **Final**

Aunque no disponemos de comandos que ejecutan automáticamente la actualización como pueden tener otras distribuciones como Ubuntu, con estos pasos podemos actualizar rápidamente un sistema, aun así, el paso de una versión anterior a Leap hacia esta es bastante peligrosa y debemos ir siempre con cuidado, con copias de seguridad y revisando que todo esté correcto en cada paso.

## **Changelog**

Añadido pasos para actualizar de Leap 42.3 a Leap 15.0
