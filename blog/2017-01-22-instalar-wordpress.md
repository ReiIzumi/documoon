---
title: Instalar Wordpress
authors: rei.izumi
tags: [WordPress]
---

Cuando piensas en un blog la aplicación que suele venir primero es Wordpress, con un montón de años funcionando y siendo mejorada por la comunidad, se convierte en una de las herramientas con más posibilidades, con una lista enorme de plugins capaces de añadir cualquier cosa que necesitemos, puede llegar a convertirse incluso en una versión pequeña de Portal o CMS.

<!-- truncate -->

## **0- Previos**

Este posts detalla la instalación de Wordpress sobre un sistema LAMP tal como el explicado en [este post](/blog/2017/01/22/instalar-lamp-opensuse).

Una vez instalado, Wordpress puede ser utilizado sin añadidos, pero lo aconsejable es revisar todas las opciones, añadir las configuraciones acorde a nuestro servidor y necesidades, cambiar el tema y añadir los plugins que necesitemos, este posts solo hará referencia a pequeños consejos sobre plugins pero no a toda la configuración, ya que es muy extensa y la propia documentación ya dispone de todo ello.

Los datos de este post permiten publicar el blog en formato HTTP, pero con el template de HTTPS y la información del original se podría activar sin demasiados problemas, siempre y cuando nuestro Apache contenga los datos de SSL activos.

Los comandos de MariaDB son exactamente los de MySQL, así que cualquiera de los utilizados en este posts valdrá para los dos.

En todo momento se utilizará el usuario root.

## **1- Descargar Wordpress**

Lo primero será pensar en que idioma queremos nuestro blog, a partir de ello iremos a la web oficial de Wordpress en se idioma y descargaremos la última versión. En este caso descargaré la versión 4.7.1 en castellano que ya pondré en su destino final y con los permisos necesarios.

```
cd /srv/www
wget https://es.wordpress.org/wordpress-4.7.1-es_ES.zip
unzip wordpress-4.7.1-es_ES.zip
chown -R wwwrun:www wordpress
rm wordpress-4.7.1-es_ES.zip
```

## **2- Configurar Apache, PHP y MariaDB**

Para Wordpress necesitaremos unos cuantos plugins de PHP, no he encontrado ninguna información oficial sobre cuales necesita así que indagando por Internet he ido recopilando una lista de todos los que necesita, es posible que si no utilizamos todas las funciones de Wordpress algunos no sean necesarios pero prefiero dejarlos funcionando por si se diera el caso que los necesitara más adelante.

```
zypper in php7-curl php7-posix php7-ftp php7-gd php7-mbstring php7-openssl php7-sockets php7-zip php7-zlib
```

También necesitamos el mod rewrite de Apache, este permite configurar las URLs tipo SEO (aquellas que indica el nombre en la URL en vez del identificador) que será las que seguramente utilizaremos.

Primero verificaremos que no está activo, de no ser así, lo añadimos.

```
a2enmod -l
a2enmod rewrite
```

El punto final es la forma en que publicaremos nuestro blog. Principalmente tenemos 2 opciones:

- Añadir el blog dentro del dominio principal, como si fuera una carpeta, la URL sería http://tu\_dominio.cat/blog
- Crear un subdominio propio para el blog, la URL sería http://blog.tu\_dominio.cat

En este caso utilizaré la segunda opción ya que me parece más bonita, para ello utilizaré un Virtual Hosts de Apache.

Los Virtual Hosts se encuentran en _/etc/apache2/vhosts.d_, si no tenemos ninguno creado copiaremos el template que dispone de la información necesaria para crearlo o copiamos los datos del que añado más abajo, recordad cambiar el ServerName por vuestro dominio y cualquier otro dato que necesitéis.

Por costumbre cada Virtual Hosts que creo dispone de 3 dígitos, una barra y el nombre del servicio. El primer dígito hace referencia a si es http (0) o https (1) y los 2 siguientes corresponden al que sería su identificador único, pero esto solo es mi forma de ordenarlos, se puede utilizar cualquiera mientras el fichero tenga extensión _conf_.

```
cd /etc/apache2/vhosts.d
cp vhost.template 000-blog.conf
vi 000-blog.conf
```

Los datos del Virtual Hosts son los siguientes.

```
<VirtualHost *:80>
        ServerName blog.domain.cat

        DocumentRoot /srv/www/wordpress

        ErrorLog /var/log/apache2/wordpress-error_log
        CustomLog /var/log/apache2/wordpress-access_log combined

        HostnameLookups Off
        UseCanonicalName Off
        ServerSignature Off

        <Directory "/srv/www/wordpress">
                Options FollowSymLinks
                AllowOverride All
                <IfModule !mod_access_compat.c>
                        Require all granted
                </IfModule>
                <IfModule mod_access_compat.c>
                        Order allow,deny
                        Allow from all
                </IfModule>
        </Directory>
</VirtualHost>
```

Guardamos y reiniciamos Apache.

```
rcapache restart
```

Por último, falta crear la base de datos y el usuario que utilizará Wordpress, por supuesto que podría usar root pero sería un error de seguridad, su usuario tendrá acceso únicamente a su propia base de datos, no queremos que un error de seguridad nos pueda destruir la información del resto.

Lo primero es acceder a MariaDB.

```
mysql -u root -p
```

Creamos la base de datos y el usuario con acceso a este. En este ejemplo el usuario no tiene acceso remoto ya que no es necesario, solo se puede acceder desde localhost.

```
CREATE DATABASE wordpress;
GRANT ALL ON wordpress.* TO 'wordpressUser'@localhost IDENTIFIED BY 'contraseña';
```

## **3- Instalar Wordpress**

Ahora debemos acceder a la ruta indicada en el ServerName que hemos utilizado en Apache, para ello o configuramos nuestros DNS para que exista realmente o, si no queremos que sea publicado por ahora, modificamos el fichero hosts de nuestro PC para añadirla, este fichero requiere permisos administrativos y se encuentra en:

- Windows: C:\\Windows\\System32\\drivers\\etc\\hosts
- Unix/Linux/Mac: /etc/hosts

Simplemente añadiremos la IP de nuestro servidor y después el ServerName.

Una vez ya tenemos definida la URL, accedemos desde el navegador, este nos mostrará el asistente de Wordpress que simplemente nos pedirá la información de la base de datos que acabamos de crear y, al terminar, el nombre del blog junto a nuestro usuario.

Finalmente nos redirigirá a nuestro nuevo blog para que empecemos la configuración a nuestro gusto.

## **4- Consejos de temas y plugins**

Después de revisar todos los menús de la pantalla de administración y modificar la configuración, lo siguiente que recomiendo es mirar los temas, los plugins pueden quedar a último nivel ya que normalmente dependerán del tema que tengamos.

Sobre el tema a elegir, recomendaría dedicarle tiempo (horas, quizás algunos días) e instalar todos aquellos que quizás nos puedan coincidir, probar cada uno de ellos y revisar si disponen de una configuración suficiente dinámica, si no es así (o si nuestras necesidades son complicadas o muy diferentes al tema), nos tocará modificar directamente los ficheros del tema. Para modificarlos es recomendable tener unos mínimos conocimientos de HTML, CSS y PHP, además, en este caso sería preferible documentar qué cambios hemos realizado y disponer de un segundo Wordpress con el mismo tema donde probar las siguientes actualizaciones, ya que estas actualizaciones pueden borrar nuestros cambios.

Sobre los plugins, todos se pueden buscar directamente desde el menú para añadirlos solo indicando el nombre, para empezar recomiendo **TinyMCE Advanced** el cual mejora el editor de nuestros posts y páginas, estas nuevas opciones pueden ser muy útiles y necesarias según lo que queramos escribir.

Otro obligatorio es **EU Cookie Law** que añade el mensaje de aceptación de Cookies requerido según la normativa europea, una normativa realmente estúpida creada por gente que no comprende el funcionamiento real de las cosas, pero añadirlo nos podrá quitar problemas innecesarios. Para que fuera completamente legal deberíamos añadir también una página de explicación sobre las Cookies aunque la versión mínima de la ley solo requeriría este plugin por defecto, si pensáis poner publicidad o tener cualquier tipo de bien económico os aconsejo que os miréis bien los temas legales, ya que es en estos casos cuando la ley se vuelve más restrictiva.

Por último, si nuestro blog dispondrá de código en algún idioma, como es mi caso, **Crayon Syntax Highlighter** facilita enormemente añadir nuestro código, este se añade al menú de edición y tiene configuración para cualquier cosa que necesitemos.

A tener en cuenta también plugins que mejoren el SEO si queremos aumentar la capacidad de tener visitas (aunque una buena escritura y utilidad de los posts siempre mejorará más que cualquier post que escribamos), plugins de contacto si fuera necesario (y nuevamente cuidado con ellos, ya que estos formularios deben tener un texto legal sobre qué haremos de esa información recibida) y plugins para bloquear mensajes de SPAM.

## **Final**

Siguiendo estos pasos se despliega fácilmente un Wordpress pero aquí no acaba todo, la selección y configuración del tema puede llegar a ser un arduo, incluso infernal, camino al mismo tiempo que necesario, si además requiere modificaciones de CSS puede que más de una vez pensemos tirar la toalla, revisad siempre que los cambios no solo se muestran bien en la página inicial, algunos temas modifican las cabeceras y otras secciones según donde se encuentren fastidiándonos la configuración de la CSS.

Encontrar, probar y configurar los plugins que necesitaremos también requerirá mucho tiempo y dedicación, pero una vez terminado dispondremos de un blog con un potencial enorme que nos facilitará mucho la vida.
