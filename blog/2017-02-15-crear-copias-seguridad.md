---
title: Crear copias de seguridad
authors: rei.izumi
tags: [Backup]
---

Quizás el servicio más importante de un servidor son las copias de seguridad, no importa qué tenga configurado el servidor, si falla, todo se pierde, así que es importante tener una copia de seguridad de todas las configuraciones, bases de datos y cualquier fichero que cambie continuamente y sea importante.

De aquellos datos que no varían nunca o casi nunca (como la gran mayoría de configuraciones), no es necesario guardar una copia cada día, pero de todos los datos que cambian continuamente sí es necesario, por ello hay muchas aplicaciones que tienen su propio sistema de copias de seguridad o que existen diferentes aplicaciones para ellas.

<!-- truncate -->

En este post quiero presentar dos tipos de copias de seguridad, una genérica para cualquier tipo de fichero en disco y otra concreta para MySQL/MariaDB, ambas en sistemas Linux.

## **1- Backup de carpetas**

Para este he creado un script que crea copias de seguridad de una carpeta y las almacena en una carpeta creando una rotación, la más nueva tendrá la carpeta 01, la siguiente 02 y así hasta tantas como se indiquen.

Principalmente se configura según estos apartados:

Identificador tanto de la copia a realizar como del servidor, serán utilizados en el e-mail para distinguir rápidamente de donde procede.

La carpeta de origen y destino (que debería ser una carpeta compartida de otro servidor para mayor seguridad), cantidad de copias de seguridad almacenadas y si se comprimirá la carpeta directamente o se comprimirá cada fichero y carpeta del origen de forma separada.

Finalmente, los datos de e-mail y si debe enviar siempre e-mails, solo para error o nunca.

Tiene una licencia Creative Commons y se puede descargar desde [aquí](/files/blog/backupGeneric.txt), contiene descripciones para cada campo a configurar.

## **2- MySQL/MariaDB**

Para las copias de esta base de datos llevo años utilizando la misma aplicación y funciona tan bien que no vale la pena cambiar, esta es [AutoMySQLBackup](https://sourceforge.net/projects/automysqlbackup/) y tiene una licencia GNU.

Permite copiar todas o unas bases de datos definidas, mantener copias de seguridad diarias, semanales, mensuales, envío de e-mail y casi cualquier tipo de configuración que puedas necesitar.

Para instalar simplemente descargar la última versión, descomprimir y lanzar el script de instalación que creará las carpetas y moverá los ficheros al lugar por defecto de esta.

```
wget https://downloads.sourceforge.net/project/automysqlbackup/AutoMySQLBackup/AutoMySQLBackup%20VER%203.0/automysqlbackup-v3.0_rc6.tar.gz
tar xvf automysqlbackup-v3.0_rc6.tar.gz
./install.sh
```

El fichero de configuración es _myserver.conf_ que será copiado a la carpeta que le indicáramos en la instalación, por defecto **/etc/automysqlbackup**.

El fichero describe cada uno de los campos y existen multitud de guías por internet explicándolo, así que solo me centraré en el envío de e-mail, que no he visto ninguna guía.

Por defecto, las tareas realizadas se muestran por pantalla, esto se puede modificar en la siguiente línea.

```
CONFIG_mailcontent='stdout'
```

Cambiando el valor a _'log'_, las tareas se enviarán al e-mail indicado más abajo, en el caso de fallar, además envía otro con el motivo del error.

Para ejecutar el proceso se utiliza el siguiente comando.

```
automysqlbackup /etc/automysqlbackup/myserver.conf
```

## **3- Automatizar copias**

Con todos los scripts configurados, el siguiente paso es automatizar su ejecución, para ello tenemos cron, un programa que ejecuta cualquier proceso que le indiquemos a las horas indicadas y viene instalado por defecto.

Cada usuario puede tener sus propios procesos en cron, así que las copias de seguridad normalmente se programan con root para evitar problemas de permisos.

Editamos la configuración de cron.

```
crontab -e
```

La [Wikipedia](https://es.wikipedia.org/wiki/Cron_(Unix)) dispone de muchos ejemplos para configurarlo cada día, semana, mes o a ciclos, de allí extraigo la versión básica.

```
.--------------- minuto (0-59) 
|  .------------ hora (0-23)
|  |  .--------- día del mes (1-31)
|  |  |  .------ mes (1-12) o jan,feb,mar,apr,may,jun,jul... (meses en inglés)
|  |  |  |  .--- día de la semana (0-6) (domingo=0 ó 7) o sun,mon,tue,wed,thu,fri,sat (días en inglés) 
|  |  |  |  |
*  *  *  *  *  comando a ejecutar
00 10 *  *  *  script_ejecutado_cada_dia_a_las_10am

```

Cuando un script escribe por pantalla, cron envía estos datos por e-mail, si no nos interesa podemos desviar la salida normal y de error a _/dev/null_ o anular el envío de e-mails directamente.

En mi caso, todos los scripts disponen de un sistema propio de envío de e-mails, así que directamente desconecto el de cron con la siguiente configuración.

```
MAILTO=""
```

Para ver la configuración tenemos el siguiente comando.

```
crontab -l
```

## **Final**

Al llegar a este punto ya tenemos las copias de seguridad activas y automatizadas para cada servicio.

Es importante recordar que el destino de las copias de seguridad debería ser una carpeta compartida en otro servidor, incluso si está fuera del mismo edificio sería lo perfecto (e incluso lo legal si se es una empresa que gestiona datos críticos o privados).

También es importante recordar que, aunque las copias se suelen hacer de bases de datos o aplicaciones, la configuración de estos es tan importante como el resto, si copiamos una página web y su base de datos, pero no la configuración de Apache, podría resultar muy complicado de volverla a replicar si la configuración disponía de muchas personalizaciones.

Incluso los logs de Apache y otros que no se suelen tener en cuenta pueden resultar importantes, desde temas legales a usos de estadísticas.
