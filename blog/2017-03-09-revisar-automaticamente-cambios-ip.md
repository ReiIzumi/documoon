---
title: Revisar automáticamente cambios en tu IP
authors: rei.izumi
tags: [IP]
---

En la actualidad, para tener un servidor o contratas un servicio de Hosting (ya sea alquilando un espacio/servidor o enviando tu servidor a ellos) o eres una empresa y contratas unas condiciones especiales en tu Internet.

En el caso de querer tener tu servidor en casa tienes bastantes trabas: desde limitaciones de velocidad de subida (que es la que realmente utilizarás) a problemas de cambio de IP.

Muchos de los problemas se han ido resolviendo con el tiempo, y dinero, pero existe un gran problema difícil e incluso imposible de resolver: los cambios de IP, al cambiarla tu dominio apunta a la IP anterior así que deja de funcionar. Aun así, siempre hay una solución posible.

<!-- truncate -->

## **1- Panorama actual**

La gran mayoría de empresas de servicios de Internet no ofrecen la posibilidad de contratar una IP fija a no ser que seas una empresa, incluso si puedes contratarla el precio es inadmisible, peor aún, en algunas lo que contratas no es una IP fija, si no que pagas para que cuando ellos reinicien su DHCP te avisen de ello, así tu te puedes preparar para actualizar tus DNS.

## **2- Un poco de teoría**

En el funcionamiento actual del ADSL/Fibras ópticas, tienes un router que siempre está conectado y recibe una IP por el DHCP del proveedor, si se apaga por cualquier motivo, aun tienes cierto margen hasta que tu IP se libere, a no ser que ellos reinicien el DHCP, en cuyo caso tu IP cambia.

Esto puede suceder en cualquier momento aunque no es nada habitual normalmente.

## **3- Una solución**

Para encontrar una solución me he basado en la idea del e-mail avisándote de que van a reiniciar y tu IP cambiará.

He creado un script que revisa cuál es tu IP actual y la compara con un fichero que contiene la IP que tenías, en caso de cambiar, te envía un e-mail con la nueva IP y actualiza el fichero. Estés donde estés recibirás ese e-mail y podrás conectarte a tus DNS para actualizarlas, no es la solución perfecta, pero ofrece algo de tranquilidad.

El script se puede descargar aquí: [checkIp](/files/blog/checkIp.txt) y tiene una licencia Creative Commons Attribution 4.0 como se indica en el fichero.

Requiere indicar el e-mail que recibirá esta información y el lugar donde se guardará el fichero con la IP actual, la idea es ejecutarlo con el Cron cada cierto tiempo, según necesitemos.
