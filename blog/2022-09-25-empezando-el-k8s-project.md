---
title: Empezando el K8s Project
authors: rei.izumi
tags: [k8s, kubernetes]
---

Hace mucho tiempo inicié un proyecto de gestión de contenedores con la tecnología Docker Swarm, pero los tiempos modernos utilizan Kubernetes, así que hacer crecer este proyecto no parece buena idea.

Para ello empieza una nueva trayectoria, esta vez con Kubernetes, replicando los mismos servicios, mejorando algunas secciones gracias a la experiencia y añadiendo nuevos servicios.

Y así es como se estrena el **K8s Project**.
