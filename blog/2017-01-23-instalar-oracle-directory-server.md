---
title: ODSEE en OpenSuSE Leap 42.2
authors: rei.izumi
tags: [Oracle Directory Server, ODSEE, LDAP]
---

Una herramienta crucial en cualquier sistema es el LDAP, el lugar donde se almacenarán todos los datos de usuario y se utilizará para iniciar sesión en todas las aplicaciones, sin él tendríamos los usuarios (y sus contraseñas) creados directamente en cada aplicación, la gestión de estos sería imposible.

Actualmente existen muchas opciones diferentes de LDAP a elegir, las más común posiblemente sea Active Directory de Microsoft, aunque existen opciones libres como OpenLDAP.

En mi caso utilizaré Oracle Directory Server (ODSEE en adelante) que proviene del extinguido Sun, un LDAP muy ligero y fácil de configurar e instalar que además es compatible con cualquier sistema.

<!-- truncate -->

## **0-Previos**

En general, todos los pasos indicados funcionaran en cualquier otra distribución de Linux.

El proceso explicado configurará el LDAP en modo normal, no activa el modo SSL.

En todo momento se utilizará el usuario root.

## **1- Instalación**

Lo primero, y posiblemente lo más difícil, será descargar el ODSEE desde la [página oficial](http://www.oracle.com/technetwork/middleware/downloads/oid-11g-161194.html).

Oracle se ha encargado de hacerlo especialmente difícil y no he encontrado ni la forma de descargarlo con wget con sus últimos cambios a no ser que generara una Cookie válida desde el navegador y la subiera al servidor, así que me he decidido descargarlo en local y subirlo al servidor directamente, gracias Oracle por hacernos la vida cada vez más sencilla.

En este caso he copiado el fichero en /root/dsee, ahora habrá que descomprimir el fichero que nos interesa y moverlo al lugar de destino, que en mi caso será /opt

```
cd /root/dsee
unzip ofm_odsee_linux_11.1.1.7.0_64_disk1_1of1.zip
cd ODSEE_ZIP_Distribution/
unzip sun-dsee7.zip
mv dsee7 /opt
cd /opt/dsee7/bin
rm -rf /root/dsee
```

Una vez instalado procedemos a crear la instancia indicando la carpeta de destino, este contendrá todos los datos de usuarios y grupos así que simplemente copiando la carpeta podremos hacer copias de seguridad incluso en caliente.

Si no indicamos puerto se utilizará el de por defecto, que siendo root será 389TCP, si no lo fuéramos sería 1389TCP.

```
./dsadm create /opt/dsee7/ds
```

Esto nos preguntará la contraseña de administrador, el usuario siempre será: '**cn=Directory Manager**' (sin las comillas)

Tal como nos ha indicado, iniciamos la instancia.

```
./dsadm start '/opt/dsee7/ds'
```

Creamos el sufijo.

```
./dsconf create-suffix -h localhost -p 389 -e dc=moon,dc=cat
```

Y ahora tendremos que añadir el esquema, para ello la forma más sencilla es modificar una del template, esta contiene toda la estructura base y también grupos y usuarios de prueba, si no los necesitamos los podemos borrar del esquema antes de importarlo o después.

Creamos una copia del template para editarlo.

```
cp /opt/dsee7/resources/ldif/Example.ldif /opt/dsee7/resources/ldif/moon.ldif
vi /opt/dsee7/resources/ldif/moon.ldif
```

Podemos reemplazar rápidamente todos los sufijos por defecto por el nuestro con el siguiente comando de vi.

```
:%s/dc=example,dc=com/dc=moon,dc=cat/g
```

También tenemos que modificar el **dc: example** del principio por nuestro primer dc, si nos olvidamos la importación fallará.

Importamos el esquema.

```
./dsconf import -h localhost -p 389 -e /opt/dsee7/resources/ldif/moon.ldif dc=moon,dc=cat

```

Ahora abrimos el puerto en el firewall para que las aplicaciones puedan acceder a él remotamente, desde Yast podremos abrirlo.

```
Security and users - Firewall
Allowed services
Advanced
Añadir el puerto 389 y aceptar
```

Si accedemos al LDAP ya sea desde el cliente de Yast o cualquier cliente externo como [JXplorer](http://jxplorer.org/), podremos confirmar que funciona, allí veremos que se han creado las secciones básicas de Groups y People.

Como último apunte, para apagar el servicio utilizaremos el siguiente comando.

```
./dsadm stop '/opt/dsee7/ds'
```

## **2- Arranque automático**

Una vez tenemos el LDAP funcionando nos interesará que se inicie automáticamente, la aplicación dispone de un comando para ello, pero todo y que en la versión de Windows funciona, la de Linux imagino que es demasiado antigua.

En cualquier caso, podemos crear nuestro propio script de arranque, yo he preparado un script que podéis descargar desde [aquí](/files/blog/sunLdapMoon.txt), dispone de una licencia Creative Commons, dentro contiene más información.

Descargamos el script, le damos los permisos de ejecución y lo editamos con vi.

```
cd /etc/init.d
wget https://www.moon.cat/files/blog/sunLdapMoon.txt
mv sunLdapMoon.txt sunLdapMoon
chown root:root sunLdapMoon
chmod 755 sunLdapMoon
vi sunLdapMoon

```

Ahora lo editaremos, si hemos cambiado el nombre, al principio encontraremos 2 líneas (10 y 11) que hacen referencia al nombre, deberemos actualizarlas. Más abajo (líneas 26 y 27) están las variables de **FOLDER\_DSEE** y **INSTANCE**, en estas actualizaremos acorde a nuestros datos.

Finalmente creamos el servicio, con esto ya podremos iniciar o apagar el servicio con rcsunLdapMoon.

```
insserv /etc/init.d/sunLdapMoon
ln -s /etc/init.d/sunLdapMoon /sbin/rcsunLdapMoon
```

Si tuviéramos más de una instancia se copiaría este fichero para cada una de ellas y se repetiría el proceso.

## **Final**

Una vez el LDAP está en funcionamiento, se pueden seguir instalando el resto de aplicaciones que estarán conectadas a este.

Como último apunte, recordad que el usuario administrador siempre será 'cn=Directory Manager'.
