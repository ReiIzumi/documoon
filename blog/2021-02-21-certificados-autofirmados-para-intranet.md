---
title: Certificados autofirmados para intranet
authors: rei.izumi
tags: [SSL, Certificados]
---

Desde hace años, todas las webs deben estar en HTTPS, si no es así, es muy posible que navegadores o servicios se nieguen a utilizarlas.

Cuando nuestra web está publicada a internet, es buena idea contratar un certificado o utilizar sistemas gratuitos como [Let's Encrypt](https://letsencrypt.org), pero cuando tu web no está publicada a Internet, si no que forma parte de tu intranet, la cosa se complica.

En estos casos, se utilizan certificados autofirmados, estos son completamente funcionales, pero tienen la pega de que todos los sistemas muestran un mensaje para avisar de que ellos no confían en el certificado, aun así, si están bien hechos, es posible minimizar o eliminar este problema.

<!-- truncate -->

## **1- Crear root CA**

Lo primero que necesitamos es un único certificado root, este es el que se utilizará para firmar todos los demás, así que, si instalamos este certificado en un sistema, cualquier "hijo" de este, será aceptado.

Para simplificar el proceso, he creado un [**script**](/files/blog/certificate.zip) que se encarga tanto de este como de los siguientes.

El script requiere ser descomprimido, dar permisos de ejecución y que el Linux disponga de openssl.

Un certificado root únicamente necesita el nombre y el subject, este son los típicos datos de ciudad, estado, localización, organización, ... si este no es indicado en el script, mostrará el asistente original de openssl.

```
./certificate.sh create-ca -n certificateCA_name \
  -s '/C=ES/ST=Barcelona/L=Barcelona/O=Domain Cat'
```

El proceso generará el fichero **key,** **crt** y **pem**.

- **crt**: Este es necesario por todos los PCs y servicios, ya que deben instalarlo para no mostrar los mensajes de que el certificado no se considera seguro.
- **pem**: Con el tiempo he encontrado varios servicios que requieren el fichero **pem** generado del root para considerarlo como válido. Es similar al **crt**, simplemente cambia el formato.
- **key**: Este permite firmar cualquier certificado bajo el nombre de nuestro certificado root, así que deberá estar en buen recaudo.

## **2- Crear certificado**

Con el root creado, ya podemos crear tantos certificados como necesitemos. Estos pueden ser únicos (apuntar a un dominio especifico) o wilcards (por ejemplo **\*.domain.intranet**, donde se acepta cualquier nombre dentro de domain.intranet).

Crear un certificado es bastante similar, pero este tiene como requisito tener el subject de CN igual al nombre que queremos utilizar, si no es así, no funcionará. Este mismo deberá ser indicado con el argumento **\-d** o **\--dns** para que se aplique como alias y así el certificado sea compatible según el último estándar.

Este es un ejemplo para crear un certificado simple y uno de wildcard. Tener en cuenta que el wildcard requiere 2 dominios de DNS y que se deben separar con **\\\\n**.

```
./certificate.sh create-cert -n certificate_name -r certificateCA_name \
  -s '/C=ES/ST=Barcelona/L=Barcelona/O=Domain Cat/CN=blog.domain.cat' \
  -d DNS.1=blog.domain.cat

./certificate.sh create-cert -n wildcard_certificate -r certificateCA_name \
  -s '/C=ES/ST=Barcelona/L=Barcelona/O=Domain Cat/CN=*.domain.cat' \
  -d DNS.1=domain.cat\\nDNS.2=*.domain.cat

```

Esto generará el fichero **crt** y **key** que, normalmente, es suficiente para todos los sistemas.

## **3- Instalar root CA**

Una vez despleguemos el certificado en nuestro servidor, ya sea Apache, Nginx, Traefik, Tomcat y tantos otros, al acceder nos avisará de que el certificado no es de fiar, si lo revisamos, veremos que nuestro navegador no sabe quién ha firmado ese certificado. Para ello tendremos que instalar el certificado root.

La instalación varía mucho según el sistema operativo o el servicio, así que me limitaré a la instalación en Windows que suele ser la más común (y quizás la más puñetera si no se conoce el proceso).

Lo primero que necesitamos es el fichero **crt** del root, ya que, si únicamente tenemos el **crt** del hijo, deberíamos instalar cada uno de ellos, es más eficiente instalar el del root.

Windows entiende los ficheros crt, así que lo abrimos, estos son los pasos:

1. Clicamos en Instalar certificado...
2. Elegimos si se instalará para el usuario local o todos.
3. Debemos elegir el **Almacén de certificados** y elegir "**Entidades de certificación raíz de confianza**".
4. Finalizamos.

Si volvemos a recargar la web, veremos que ya no muestra el error, si revisamos el certificado, nos mostrará la ruta hasta el root y que ambos son válidos ya que confía en el root.

## **Final**

La generación de los certificados auto firmados se ha convertido en un gran quebradero de cabeza debido a la diferencia entre versiones, pero con estos simples pasos, podemos proteger correctamente toda nuestra intranet y evitar los molestos mensajes de advertencia.
