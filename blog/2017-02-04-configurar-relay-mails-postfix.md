---
title: Mail relay con postfix
authors: rei.izumi
tags: [Mail relay, Postfix]
---

El servicio de e-mail es uno de los más importantes y complicados de mantener ya que requiere estar en continuo funcionamiento pase lo que pase, para ello tienen sus propias políticas y configuraciones capaces de reenviar el e-mail a otro servidor si el primero ha caído. Precisamente por esta complejidad normalmente se delega a servicios de otros, ya sean de pago o gratuitos.

En cualquier caso, nuestros servidores es posible que necesiten enviar e-mails para notificar por ejemplo de las copias de seguridad. Para ello tendremos que configurar nuestro postfix en modo de relay, este permite enviar utilizando el servicio contratado, sin necesidad de que el propio postfix sea el servidor.

<!-- truncate -->

## **0- Previos**

Postfix debe estar instalado en el Linux, en el caso de OpenSuSE este viene instalado por defecto a no ser que se indique lo contrario en la instalación, en cualquier caso, es fácil de instalar con los comandos de repositorio según la distribución (zypper, apt-get, etc.).

La finalidad de esta configuración es que el servidor pueda enviar e-mails utilizando nuestro sistema contratado de correo, para ello tendremos que tener los datos de envío (smtp) del servidor a utilizar, normalmente requerirá de un usuario y contraseña para acceder que es cómo se explicará aquí.

Existen infinitas maneras de configurar postfix siendo uno de los servicios más complicados que he encontrado, una alternativa para configurar algo tan simple como un relay sería utilizar otra aplicación, pero me decanto por esta por su gran importancia y la cantidad de información por internet.

En este post intento explicar la versión más simple de todas las que he conseguido para configurar el relay, a partir de esta se puede complicar tanto como se necesite.

En todo momento se utilizará el usuario root.

## **1- Configurar Postfix**

Toda la configuración se añade al fichero principal.

```
cd /etc/postfix
vi main.cf
```

Por defecto ya está configurado para enviar mensajes internos, con esto añadiremos los datos para conectar a un servidor externo y así enviar los e-mails. Al añadir estos datos hay que revisar que no esté duplicado, la mayoría de ellos ya están configurados o están vacíos así que o comentamos las líneas originales o las actualizamos según nuestros datos.

```
relayhost = smtp.domain.cat
smtp_sasl_auth_enable = yes
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_generic_maps = hash:/etc/postfix/generic
myhostname = <ServerName>
```

Lo siguiente es añadir el usuario y contraseña del servidor smtp.

```
vi sasl_passwd
```

Este debe indicar el servidor y seguidamente el usuario:contraseña.

```
smtp.domain.cat user@domain.cat:password
```

Una vez guardado, creamos el formato final.

```
postmap sasl_passwd
```

Esto es suficiente para configurar el servicio, pero con estos datos, el remitente por defecto será algo similar a _root@ServerName.localdomain_ y muy posiblemente nuestro servidor de e-mail no lo acepte, hay varias soluciones para ello, pero la que menos problemas me ofrece es crear un mapeo de este usuario hacia el real.

```
vi generic
```

Indicamos el e-mail que genera el servidor y el e-mail receptor por defecto.

```
root@ServerName.localdomain tu-mail@domain.cat
```

Y lo creamos en versión final.

```
postmap generic
```

Con esto ya tenemos todo configurado así que terminamos reiniciando la aplicación.

```
rcpostfix restart
```

## **2- Asignar alias de root**

Algunos servicios pueden enviar e-mails internos a root, si queremos podemos asignar un e-mail a este para que los envíen a donde deseemos.

```
vi /etc/aliases
```

Al final del todo indicamos a dónde debe ir root.

```
root: tu-mail@domain.cat
```

## **3- Pruebas y logs**

El log del servicio se escribe en el _journalctl_, este nos puede servir para confirmar que nombre se está utilizando por defecto al enviar los e-mails y así ajustar el mapeo indicado previamente, también nos servirá para revisar que se esté enviando correctamente, si no es así indicará un _reject_ y un motivo.

Con el siguiente comando podemos mantener el log activo para ir viendo los cambios que se van sucediendo mientras vamos configurando y probando.

```
journalctl -f
```

Para enviar un e-mail simple podemos utilizar el siguiente comando, la primera parte indica el cuerpo del mensaje, el **\-s** el asunto y finalmente el destinatario, si creamos un alias a root, podemos indicar root como destinatario para confirmar que esté funcionando correctamente.

```
echo "Texto de prueba" | mail -s Pruebas tu-mail@domain.cat
```

## **Final**

Con esto tenemos nuestro pequeño relay que podemos configurar fácilmente en cada servidor y así recibir cualquier notificación que sea necesaria, desde copias de seguridad a aplicaciones que notifiquen de los diferentes sucesos.
