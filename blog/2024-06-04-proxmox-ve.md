---
title: Bye VMWare. Hello Proxmox
authors: rei.izumi
tags: [proxmox, vmware]
---

Tras muchos y muchos años batallando con **VMWare ESXi**, es hora de cambiar.

Empecé con la versión que se instalaba sobre un Linux, donde temblabas de medio tras cada actualización de Kernel sabiendo que iba a dejar de funcionar, y tendrías que pasar noches en vela para encontrar una solución (donde quizás, ni existía), para saltar a ESXi Hypervisor. Esta versión utilizaba su propio sistema operativo y tenía versión gratuita con grandes limitaciones (sin API, las copias de seguridad iban a ser un infierno, pero todo se puede solucionar).

Y no, no era un camino de rosas. El hardware era descartado rápidamente, lo que obligaba a modificar las ISO para añadir la compatibilidad, o simplemente aceptar que ya no podrías actualizar más.

Todo para finalmente darnos una **bonita patada**, eliminar la versión gratuita y aumentar el precio de todo tras la compra de **Broadcom**. Si ya era caro, pues ahora te apuñalo 2 veces, no sea que te plantees pagar.

Así que toca abrir la puerta y largarse.

<!-- truncate -->

Tras mirar opciones para un uso doméstico, [Proxmox VE](/docs/it/proxmox) es, posiblemente, el mejor camino.

:::warning
Aunque lo llame para uso doméstico, es similar al que usaría una PYME. Las de verdad, no las que se llaman PYMEs y tienen una red inferior a la de tu casa, ejem, ejem.
:::

Tras ello y con intención de aprovechar para automatizar todo (con muuuucho tiempo y aún más dedicación), me pongo a iniciar una nueva etapa.

:::tip
Y aún con todo, le doy las gracias a Broadcom. Sin la patada que nos ha dado, aún utilizaría VMWare, y no me habría dado cuenta de todo lo que me estaba perdiendo.
:::
