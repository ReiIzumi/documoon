---
title: LAMP en OpenSuSE Leap 42.2
authors: rei.izumi
tags: [Apache, LAMP, MariaDB, PHP7]
---

Algo básico cuando tienes un servidor es tener tu propio sistema de publicación web, actualmente tenemos un montón de opciones entre servidores web, bases de datos e idiomas de programación, pero por mucho que pasen los años parece que hay una unión que nunca muere: LAMP.

LAMP son las iniciales de los 4 componentes que lo componen: Linux, Apache, MySQL y PHP, todo y que en mi caso cambiaré MySQL por MariaDB ya que es su descendiente dentro de la comunidad opensource.

<!-- truncate -->

## **0- Previos**

Esta instalación dejará preparados los 3 servicios con lo mínimo necesario para conectarse entre ellos y poco más, estos pueden necesitar más componentes (normalmente Apache y PHP) para las aplicaciones web que se utilicen.

En todo momento se utilizará el usuario root.

## **1- Apache**

La instalación básica de Apache no puede ser más sencilla, empezaremos instalando y activando el arranque automático.

```
zypper in apache2
systemctl enable apache2
```

Abrimos los puertos que necesitemos, por defecto Apache, como toda web, utiliza el puerto 80, además es posible que queramos abrir el 443 para utilizar HTTPS, en cualquier caso lo activaremos desde Yast.

```
Security and Users
Firewall
Allowed Services

Seleccionar HTTP Server (y HTTPS Server si es necesario)
Añadirlos y aceptar

```

Todos los ficheros de configuración se encuentran en _/etc/apache2_, allí deberíamos empezar modificando el _default-server._

```
vi /etc/apache2/default-server.conf
```

Este contiene la web por defecto que estará vacía, podemos modificar la ruta o seguir utilizando la de por defecto, en cualquier caso, sería recomendable añadir el ServerName indicando el nombre de dominio que utilizará.

```
ServerName moon.cat
```

Finalmente iniciamos el servicio.

```
rcapache start
```

Ahora ya está iniciado, pero si no creamos ninguna página nos mostrará un error indicando que no tenemos permiso, aun así esto es suficiente para confirmar que funciona desde http://\<server\_ip\>

Para el uso real seguramente necesitaremos crear virtual hosts pero eso lo dejaré para los posts que lo necesiten.

## **2- MariaDB**

Lo primero a tener en cuenta es que MariaDB proviene de MySQL, así que todos los comandos de este provienen del nombre original, también es compatible con cualquier aplicación de MySQL.

Con esto claro, empezamos instalando, activando el inicio automático e iniciando el servicio.

```
zypper in mariadb mariadb-client
systemctl enable mysql
rcmysql start
```

Por defecto la contraseña de root está vacía, así que lanzamos el asistente de configuración que nos preguntará la nueva contraseña, desconectar el acceso a usuarios anónimos, si queremos permitir el acceso remoto y borrar la tabla de test, elegiremos según nuestras necesidades.

```
mysql_secure_installation
```

Reiniciamos el servicio para aplicar los cambios.

```
rcmysql restart
```

Y finalmente comprobamos que podemos conectar.

```
mysql -u root -p
```

En el caso de querer conexión remota, además deberemos abrir el puerto 3306TCP desde Yast tal como se explicó en la sección de Apache. Además, debemos añadir el rango de IPs permitidos, en este ejemplo se va a permitir todo el rango de 192.168.1.X para root aunque utilizando % aceptaríamos cualquier IP.

```
GRANT ALL PRIVILEGES ON *.* TO 'root'@'192.168.1.%' IDENTIFIED BY 'password' WITH GRANT OPTION;
```

## **3- PHP7**

En la instalación de PHP añadiremos algunos módulos extras para poder conectar con Apache y MariaDB, de paso he añadido algunos básicos, pero seguramente las aplicaciones que utilicemos tengan más requisitos que deberemos revisar en el momento de instalación.

Como siempre, empezamos instalando.

```
zypper in php7 apache2-mod_php7 php7-mysql php7-gd php7-mbstring php7-zlib
```

Activamos el módulo de PHP7 en Apache y lo reiniciamos.

```
a2enmod php7
rcapache2 restart
```

Para confirmar que funciona crearemos una página de prueba que nos muestre todos los datos de nuestro PHP, en este caso la publicamos en el sitio por defecto, pero deberemos recordad borrarla antes de publicar nuestro Apache a Internet ya que publicar estos datos sería peligroso.

```
cd /srv/www/htdocs
echo "<?php phpinfo(); ?>" > index.php
```

Ahora nos dirigimos a http://\<server\_ip\> que nos mostrará todos los datos de nuestro PHP.

## **Final**

Llegados a este punto ya tenemos nuestro servidor web completo para ser utilizado en lo que sea necesario, así que lo siguiente será revisar todo aquello que querremos publicar en él, ¿quizás nuestro propio [blog](/blog/2017/01/22/instalar-wordpress)?.
