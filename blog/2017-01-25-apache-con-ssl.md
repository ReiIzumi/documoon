---
title: Apache con SSL
authors: rei.izumi
tags: [Apache, SSL]
---

En la actualidad toda página que contenga gestión de datos (desde iniciar sesión a rellenar un formulario) debería tener la posibilidad de acceder por HTTPS (o incluso ser la única opción), para ello se requiere tener activo la seguridad SSL en nuestro Apache.

Quizás uno de los grandes inconvenientes es el alto precio de los certificados válidos, pero según el uso que le demos quizás no nos sea necesario, una web interna o que no sea para el público general podrá tener un certificado autogenerado, el navegador nos avisará del peligro pero al ser propia sabemos que no es un problema y podremos seguir adelante teniendo la seguridad SSL activa y sin tener que pagar.

En cambio, si nuestra página es pública sí que debería de tener una válida para dar más confianza el usuario, por ejemplo, yo nunca compraría en una tienda online que no dispusiera de SSL o que, de disponerlo, no sea válido.

<!-- truncate -->

## **0- Previos**

Estos pasos explican el proceso de creación de certificados para Apache y la configuración de este, así que hará falta tener un Apache completamente configurado previamente. Si aún no tienes Apache, en [este post](/blog/2017/01/22/instalar-lamp-opensuse) tienes las instrucciones para instalarlo.

Los certificados que explicaré son autofirmados, así que al acceder desde un navegador no reconocerá el origen de estos y nos avisará del peligro. También hay que tener en cuenta que estos certificados son para un único dominio (o subdominio), así que tendremos que crear uno para cada dominio o subdominio.

Si nos decantamos por comprar un certificado veremos que existen de varios tipos, pero en realidad es bastante sencillo, existen los certificados de un único dominio o subdominio que varían dependiendo de la seguridad. Por otro lado, existen los Wildcard que permiten tener tantos subdominios del mismo dominio como queramos, por supuesto estos últimos son mucho más caros. No soy un experto en el tema, pero a no ser que se requiera algo concreto tanto [RapidSSL](https://www.rapidssl.com/) como [Thawte](https://www.thawte.com/) deberían ser buenas opciones, el primero está más orientado al lado doméstico y el segundo al profesional.

En todo momento se utilizará el usuario root.

## **1- Activar SSL en Apache**

El primer paso es activar el SSL en Apache, para ello debemos mirar si está en la lista de módulos o podemos lanzar el comando directamente, si ya estaba nos avisará de ello y no lo añadirá.

```
a2enmod ssl
a2enflag SSL
```

Ahora generaremos nuestro propio certificado, existe la opción de hacerlo directamente con openssl pero utilizaré una herramienta que ya se encargará de dejarlos en su lugar e igualmente utiliza openssl.

```
gensslcert -C "blog.domain.cat" -N "Certificado blog.domain.cat" -c CT -s "BCN" -l "BCN" -o "Moon" -u "Moon" -n "blog.domain.cat" -e "webmaster@domain.cat" -y 3600
```

La ayuda indica el significado de cada punto, pero añado aquí todos los obligatorios.

```
-C common name, nombre de server
-N commentario
-c país (2 letras)
-s estado
-l ciudad
-o organización
-u unidad organizativa
-n nombre completo cualificado
-e e-mail del webmaster
-y días de validez para el certificado
```

Al ser un certificado autofirmado los datos de este no es tan importante (menos el common name y el nombre completo cualificado que deben ser exactamente los del dominio o subdominio), pero en caso de utilizarse para un certificado de pago, todos los datos deberán ser reales.

Una vez terminado los ficheros estarán ya en sus carpetas correspondientes.

Por último, solo quedará abrir el puerto 443TCP en nuestro Firewall, Yast lo tiene definido como HTTPS Server.

## **2- Añadir SSL a las páginas publicadas**

En mi caso utilizo Virtual Hosts así que haré la configuración para uno de estos, concretamente habilitaré la versión SSL del Virtual Hosts creado en la explicación del [blog](/blog/2017/01/22/instalar-wordpress).

Lo primero será copiar o bien la plantilla de ssl o bien el Virtual Hosts que ya tuviéramos y añadirle las configuraciones de seguridad, para ir más rápido yo usaré la segunda opción utilizando mi nomenclatura de nombres y lo editaré para añadirle la seguridad.

```
cd /etc/apache2/vhosts.d
cp 000-blog.conf 100-blog.conf
vi 100-blog.conf
```

Al editarlo nos debería quedar algo similar a estos datos.

```
<IfDefine SSL>
<IfDefine !NOSSL>

<VirtualHost *:443>
        ServerName blog.domain.cat

        DocumentRoot /srv/www/blog

        ErrorLog /var/log/apache2/blog-error_log
        TransferLog /var/log/apache2/blog-access_log

        SSLEngine on

        SSLCertificateFile /etc/apache2/ssl.crt/blog.domain.cat-server.crt
        SSLCertificateKeyFile /etc/apache2/ssl.key/blog.domain.cat-server.key

        CustomLog /var/log/apache2/ssl_request_log   ssl_combined

        HostnameLookups Off
        UseCanonicalName Off
        ServerSignature Off

        <Directory "/srv/www/blog">
                Options FollowSymLinks
                AllowOverride All
                <IfModule !mod_access_compat.c>
                        Require all granted
                </IfModule>
                <IfModule mod_access_compat.c>
                        Order allow,deny
                        Allow from all
                </IfModule>
        </Directory>
</VirtualHost>

</IfDefine>
</IfDefine>
```

## **3- Obligar el uso de HTTPS**

Este paso es opcional.

Normalmente a una web podemos acceder desde HTTP o HTTPS, la aplicación debería cambiar automáticamente a HTTPS cuando tenga un proceso de gestión de datos, desde un formulario de contacto a una pantalla de inicio de sesión, pero muchas aplicaciones no son capaces de hacer el cambio automáticamente.

En este caso, o si simplemente queremos que la aplicación siempre esté en modo seguro, deberemos configurar igualmente el HTTP pero creando un redireccionamiento permanente hacia el HTTPS, si no lo damos de alta cuando una persona intente acceder sin el modo seguro, no encontrará la página.

Para ello modificaremos el Virtual Host de HTTP.

```
vi 000-blog.conf
```

Lo modificamos para redireccionar al modo seguro y añadimos el código 301 para indicar que este redireccionamiento es permanente, esto permite a los buscadores indexar, en este caso, la URL en modo seguro y olvidar la normal.

```
<VirtualHost *:80>
        ServerName blog.domain.cat

        Redirect 301 / https://blog.domain.cat/
</VirtualHost>
```

## **Final**

Ahora nuestras páginas serán más seguras que antes y en unos simples pasos podremos generar nuevos certificados para cada subdominio que añadamos a nuestro servidor.
