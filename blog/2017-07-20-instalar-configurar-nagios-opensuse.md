---
title: Nagios en OpenSuSE Leap 42.2
authors: rei.izumi
tags: [Nagios]
---

title: Instalar OpenSuSE 42.3
authors: rei.izumi
tags: [OpenSuSE]Cuantos más servidores y servicios se tienen, más difícil es comprobar que todos estén funcionando correctamente, para ello hay aplicaciones que permiten revisar el estado de estos y avisar si alguno falla.

Uno de los más antiguos es Nagios, desde su web puedes ver rápidamente el estado de toda la red, tanto servidores como servicios, si alguno de estos falla, envía un e-mail para avisar de ello. También dispone de todo tipo de módulos nuevos e incluso puedes programar módulos nuevos, así que tienes total libertad para crear aquello que necesites.

<!-- truncate -->

## **0- Previos**

Nagios dispone de la versión libre original y una nueva de pago, viendo el precio, yo elegiré la versión original, llamada Nagios Core.

Desde el repositorio de cada distribución Linux se puede encontrar tanto el server como el client, estos facilitan enormemente la instalación, aun así, yo compilaré el original para tener la última versión.

He de avisar que utilizaré la instalación normal, pero después me cargaré toda la configuración por defecto (o casi casi) y la haré a mi estilo. La documentación oficial de Nagios y la configuración por defecto no me gustan en absoluto, me parecen caóticas, así que ajustaré todo tal como creo que debería funcionar.

Estos pasos son para la instalación en OpenSuSE, pero con ligeros cambios de dependencias y servicios funcionarán en cualquier otro.

El servidor de Nagios Core requiere de un Apache o similar, yo utilizaré el Apache como [ya expliqué en su día](/blog/2017/01/22/instalar-lamp-opensuse).

## **1- Instalar servidor**

Para funcionar, se necesitan 3 piezas: [Nagios Core](https://www.nagios.org/downloads/nagios-core/) que contiene el servicio y la web, [Nagios Plugins](https://www.nagios.org/downloads/nagios-plugins/) que contiene los componentes que realizan las pruebas y [NRPE](https://exchange.nagios.org/directory/Addons/Monitoring-Agents/NRPE--2D-Nagios-Remote-Plugin-Executor/details) que se encarga de la conexión entre el servidor y los diferentes clientes, existen diferentes conectores pero yo me quedaré con NRPE por ser el que más conozco ya que tiene sus años.

Empezamos descargando y descomprimiendo todo lo necesario.

```
cd /tmp
wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.2.tar.gz
wget https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-3.2.0/nrpe-3.2.0.tar.gz
tar -zxf nagios-4.3.2.tar.gz
tar -zxf nagios-plugins-2.2.1.tar.gz
tar -zxf nrpe-3.2.0.tar.gz
```

Ya que vamos a compilar, necesitamos todas las herramientas para ello y también las de certificados para el NRPE.

```
zypper in gcc make gd-devel libopenssl-devel
```

Todo Nagios funcionará desde su propio usuario y Apache también necesita acceso a los ficheros, así que hay que crear el usuario y asignar los grupos necesarios.

```
groupadd nagios
groupadd nagcmd
useradd -c "User for Nagios" -M -d "/var/lib/nagios" -s "/bin/false" -r -G "nagios" nagios
usermod -a -G nagcmd nagios
usermod -a -G nagios nagios
usermod -a -G nagcmd wwwrun
```

#### **Instalar Nagios Core**

Con todos los requisitos terminados, empezamos a compilar e instalar los servicios del core.

```
cd nagios-4.3.2
./configure --with-command-group=nagcmd
make all
make install
make install-init
make install-config
make install-commandmode
make install-webconf

```

Colocamos las herramientas en su sitio y asignamos los permisos correctos.

```
cp -R contrib/eventhandlers /usr/local/nagios/libexec/
chown -R nagios:nagios /usr/local/nagios/libexec/eventhandlers
```

Comprobamos que la configuración por defecto sea correcta.

```
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

```

Creamos el servicio.

```
vi /etc/systemd/system/nagios.service
```

Con los siguientes datos.

```
[Unit]
Description=Nagios
BindTo=network.target

[Install]
WantedBy=multi-user.target

[Service]
User=nagios
Group=nagios
Type=simple
ExecStart=/usr/local/nagios/bin/nagios /usr/local/nagios/etc/nagios.cfg
```

Terminamos activando los servicios de autoarranque. Revisar desde Yast que realmente se ha encendido.

```
insserv /etc/init.d/nagios
ln -s /etc/init.d/nagios /sbin/rcnagios
chkconfig nagios on
```

#### **Instalar Nagios Plugins**

Lo siguiente será configurar los plugins, sin ellos, el Core devolverá errores al no conectar a nada.

```
cd /tmp/nagios-plugins-2.2.1
./configure
make
make install
```

Ahora reiniciamos el servicio para que empiece a funcionar acorde la configuración por defecto.

```
rcnagios restart
```

#### **Configurar Virtual Host de Apache**

Con estos pasos nos habrá dejado un fichero de ejemplo que encontraremos en la configuración de Apache.

```
/etc/apache2/conf.d/nagios.conf
```

Podemos ajustarlo según nuestra versión de Apache o, en mi caso, borrarlo y construir un Virtual Host para Nagios.

```
rm /etc/apache2/conf.d/nagios.conf
vi /etc/apache2/vhosts/005-nagios.conf
```

La web de Nagios requiere autorización mediante Apache para funcionar correctamente, podemos asociar esa autorización a un fichero o cualquier otro modo que permite Apache, estos pasos son para utilizar el fichero.

```
<VirtualHost *:80>
        ServerName nagios.domain.cat

        ScriptAlias /nagios/cgi-bin "/usr/local/nagios/sbin/"
        <Directory "/usr/local/nagios/sbin/">
                Options ExecCGI
                AllowOverride None
                Require all denied

                AuthName "Nagios Access"
                AuthType Basic
                AuthUserFile /etc/apache2/users/nagios.users
                Require valid-user
        </Directory>

        Alias /nagios "/usr/local/nagios/share/"
        <Directory "/usr/local/nagios/share/">
                Options None
                AllowOverride None
                Require all denied

                AuthName "Nagios Access"
                AuthType Basic
                AuthUserFile /etc/apache2/users/nagios.users
                Require valid-user
        </Directory>

        ErrorLog /var/log/apache2/nagios-error_log
        CustomLog /var/log/apache2/nagios-access_log combined
</VirtualHost>
```

Con ello ya solo falta crear un usuario en el fichero y reiniciar Apache.

```
htpasswd -c /etc/apache2/users/nagios.users nagiosadmin
rcapache2 restart
```

Ya podremos acceder desde la siguiente URL.

```
http://nagios.domain.cat/nagios/
```

Si todo ha funcionado bien, veremos el servidor y varios servicios de este siendo escaneados, ya solo nos queda el NRPE para activar la conexión a otros servidores.

#### **Instalar NRPE**

Puede funcionar como servicio independiente o dentro del servicio XINET, por defecto ya está configurado en el XINET de OpenSuSE, así que simplemente lo compilamos e instalamos el servicio.

```
/tmp/nrpe-3.2.0/
./configure
make all
make install-plugin
make install-daemon

```

Con esto ya tenemos el servidor instalado y configurado para funcionar por defecto, pero aún queda mucho trabajo para ajustarlo todo.

## **2- Envío de e-mails**

Empezamos una de las configuraciones del servidor, el envío de e-mails y la lista de contactos.

Lo primero que se necesita es tener Postfix configurado tal como se explica en [este post](/blog/2017/02/04/configurar-relay-mails-postfix).

Para confirmar que funcionará el mismo proceso que ejecuta Nagios, enviamos un e-mail imitándole.

```
/usr/bin/printf "%b" "Test, please ignore.\n notify-service-by-email alert. \n" | /usr/bin/mail -s "Testing Send Mail" "mail@domain.cat"
```

Lo siguiente es configurar los contactos y grupos, para ello dejamos una copia de la configuración por defecto y editamos el fichero de contactos.

```
cd /usr/local/nagios/etc/objects
cp contacts.cfg contacts.cfg_ORIGINAL
vi contacts.cfg
```

Este contiene comentarios y un ejemplo de contacto y grupo.

```
define contact{
        contact_name                    ContactName
        use                             generic-contact
        alias                           Contact Alias
        email                           contact@domain.cat
        service_notification_period     24x7
        service_notification_options    w,u,c,r,f
        service_notification_commands   notify-service-by-email
        host_notification_period        24x7
        host_notification_options       d,u,r,f
        host_notification_commands      notify-host-by-email
}

define contactgroup{
        contactgroup_name               admins
        alias                           Nagios Administrators
        members                         ContactName
}

```

Partiendo de este ejemplo, indicamos la configuración acorde a cada contacto,

Los valores para las notificaciones de servicio son las siguientes:

- w: WARNING
- u: UNKNOWN
- c: CRITICAL
- r: RECOVERY
- f: Notificar cuando el servicio deja de cambiar de estado continuamente (FLAPPING)
- n: No notificar

Y para hosts:

- d: Host DOWN
- u: UNREACHABLE
- r: RECOVERY
- f: Notificar cuando el host deja de cambiar de estado continuamente (FLAPPING)
- s: Enviar notificación cuando empieza o acaba un parón controlado del host o servicio
- n: No notificar

En el grupo de contactos, en member añadimos cada contacto (contact\_name) separado por coma.

Lo siguiente es configurar el e-mail que se utilizará para hacer el envío.

```
vi commands.cfg
```

Los dos primeros comandos serán notify-host-by-email y notify-service-by-email, al final de su command\_line añadimos un nuevo argumento al final para especificar el from del e-mail.

```
define command {
	command_name    notify-host-by-email
	command_line    /usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\nHost: $HOSTNAME$\nState: $HOSTSTATE$\nAddress: $HOSTADDRESS$\nInfo: $HOSTOUTPUT$\n\nDate/Time: $LONGDATETIME$\n" | /usr/bin/mail -s "** $NOTIFICATIONTYPE$ Host Alert: $HOSTNAME$ is $HOSTSTATE$ **" $CONTACTEMAIL$ -r sender@domain.cat
}

define command {
	command_name    notify-service-by-email
	command_line    /usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\n\nService: $SERVICEDESC$\nHost: $HOSTALIAS$\nAddress: $HOSTADDRESS$\nState: $SERVICESTATE$\n\nDate/Time: $LONGDATETIME$\n\nAdditional Info:\n\n$SERVICEOUTPUT$\n" | /usr/bin/mail -s "** $NOTIFICATIONTYPE$ Service Alert: $HOSTALIAS$/$SERVICEDESC$ is $SERVICESTATE$ **" $CONTACTEMAIL$ -r sender@domain.cat
}
```

Una vez terminado, reiniciamos el Nagios y ya estará configurada esta parte.

## **3- Instalar cliente en OpenSuSE**

Cada servidor que será revisado requiere instalar el cliente para que Nagios le pueda consultar utilizando NRPE, estos son más simples de instalar que el servidor y además comparten gran parte.

Descargamos y descomprimimos lo necesario.

```
cd /tmp
wget https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-3.2.0/nrpe-3.2.0.tar.gz
tar -zxf nagios-plugins-2.2.1.tar.gz
tar -zxf nrpe-3.2.0.tar.gz
```

Instalamos las herramientas de compilación y certificados.

```
zypper in gcc make gd-devel libopenssl-devel
```

Añadimos el usuario y grupo.

```
groupadd nagios
useradd -c "User for Nagios" -M -d "/var/lib/nagios" -s "/bin/false" -r -G "nagios" nagios
usermod -a -G nagios nagios
```

Y empezamos instalando los plugins.

```
cd nagios-plugins-2.2.1
./configure
make
make install
```

Terminamos asignando los permisos.

```
chown nagios:nagios /usr/local/nagios
```

El siguiente paso es instalar el NRPE.

```
cd /tmp/nrpe-3.2.0
./configure
make all
make install-plugin
make install-daemon
make install-inetd
```

Para poderlo ejecutar se tiene que configurar previamente.

```
vi /etc/xinetd.d/nrpe
```

Básicamente necesitamos cambiar dos valores para activarlo e indicar la IP del servidor de Nagios para permitirle acceder.

```
disable = no
only_from = 127.0.0.1 localhost <nagios_ip_address>
```

Este requiere de otro fichero donde se configurarán los diferentes servicios que vamos a utilizar, por ahora lo creamos ya que es necesario para iniciar el servicio.

```
mkdir /usr/local/nagios/etc
touch /usr/local/nagios/etc/nrpe.cfg
```

Abrimos el puerto TCP5666 y reiniciamos el servicio. **(Esto no es suficiente para que inicie automáticamente, para ello habrá que activarlo desde Yast)**

```
rcxinetd restart
```

Si todo ha funcionado, el siguiente comando nos dará la versión del NRPE instalado.

```
/usr/local/nagios/lib/check_nrpe -H localhost
```

Después de abrir el puerto TCP/5666, desde el servidor podemos usar el mismo comando hacia el nuevo cliente para confirmar que tiene acceso.

## **4- Configurar servicios del cliente**

Lo primero que hay que entender es que hay dos tipos de servicios:

- Servicios remotos: estos se ejecutan desde el servidor hacia el cliente. No necesitan instalar nada en el cliente, así se puede gestionar remotamente algunos servicios de un router, switch gestionable o impresora. Por ejemplo, para revisar si un puerto está abierto.
- Servicios locales: estos se ejecutan desde el cliente, así que es necesario que el servidor ejecute el servicio NRPE para que ejecute localmente el test y devuelva la respuesta al servidor, estos deben estar configurado en el NRPE del cliente. Por ejemplo, el espacio libre en disco.

Ya que los servicios remotos no requieren ser configurados desde el cliente, en este punto nos centraremos en los servicios locales.

Editamos el fichero de configuración de servicios.

```
vi /usr/local/nagios/etc/nrpe.cfg
```

Para configurar un servicio, primero debemos tener ese servicio, por defecto tenemos todos los que instalamos con el Nagios Plugin, pero podemos instalar otros desde la [comunidad oficial](https://exchange.nagios.org/) o crear nuestros propios plugins manualmente.

Para cada servicio hay que revisar que parámetros necesitan, pero casi todos ellos contienen 2 argumentos obligatorios:

- w: Valor para advertir que el servicio puede no estar funcionando bien o estar en problemas. Por ejemplo, se accede a una URL de Apache pero responde con un mensaje de no acceso.
- c: El servicio se considera caído o en gran riesgo. Por ejemplo: casi no queda espacio en disco disponible.

Este es un ejemplo para activar el servicio de comprobación de espacio libre en disco para la partición raíz (/). Si disponemos de más de un disco montado en diferentes particiones deberíamos tener un test para cada uno de ellos.

```
command[check_disk]=/usr/local/nagios/lib/check_disk -w 20 -c 10 -p /
```

Este fichero es consultado cada vez que se recibe una petición así que no es necesario reiniciar el servicio.

Los clientes por defecto no tienen ningún ejemplo, pero el servidor sí, partiendo de este obtenemos los siguientes servicios locales que podrían componer la base de todos nuestros servidores cliente además de cualquier otro servicio específico que necesitemos.

```
command[check_disk]=/usr/local/nagios/lib/check_disk -w 20 -c 10 -p /
command[check_users]=/usr/local/nagios/lib/check_users -w 20 -c 50
command[check_procs]=/usr/local/nagios/lib/check_procs -w 250 -c 400 -s RSZDT
command[check_load]=/usr/local/nagios/lib/check_load -w 5.0,4.0,3.0 -c 10.0,6.0,4.0
command[check_swap]=/usr/local/nagios/lib/check_swap -w 20 -c 10
```

## **5- Configurar servicios del server**

Esta es la parte más importante y complicada de todo el proceso: configurar el servidor acorde a nuestras necesidades.

La base de toda la configuración se encuentre en este fichero:

```
/usr/local/nagios/etc/nagios.cfg
```

Este contiene las llamadas al resto de ficheros (la mayoría en la carpeta objects) y podemos crear nuevos ficheros para separar aún más la configuración.

La lista de todos los parámetros se encuentra en la [documentación oficial](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/objectdefinitions.html).

Todos los ficheros son ejemplos de la configuración por defecto, esto quiere decir que funcionan con el sistema local y están explicados, pero no significa que sean óptimos para un sistema real, así que todos ellos pueden ser reconstruidos bajo nuestras necesidades.

En cualquier caso, yo voy a indicar los mínimos necesarios para encender los servicios, pero dejo una pequeña guía para entender qué hacen a grandes rasgos y de ahí decidir qué hacer:

- **commands:** Contiene la lista de todos los servicios que puede utilizar Nagios, empezando por los obligatorios de envío de e-mail (notify-host-by-email y notify-service-by-email) y la comprobación de host funcionando (check-host-alive) que no deja de ser un ping. A partir de aquí, podemos crear tantos servicios como queramos, pero hay que recordar que los servicios locales solo funcionaran hacia el servidor, todos los demás deben ir por NRPE hacia el cliente.
- **contacts:** Ya lo hemos visto anteriormente, contiene los contactos y los grupos de estos contactos.
- **localhost:** Sirve como ejemplo de configuración para un Linux, este se habilita por defecto, pero muchos de sus servicios son locales, así que no se puede copiar tal cual para el resto de servidores Linux.
- **printer:** Ejemplo para configurar impresoras que estén conectadas a la red.
- **switch:** Igual que el anterior, sirve de ejemplo para comprobar un switch gestionable (uno que tenga IP propia), también puede servir de base para routers.
- **templates:** Este junto a commands es uno de los archivos más importantes y complejos, contiene toda la información sobre las plantillas a utilizar por el resto de ficheros, la configuración básica de los contactos, servicios, hosts, etc. Todo utiliza una plantilla y se pueden sobrescribir los valores por defecto, incluso una plantilla puede proceder de otra plantilla.
- **timeperiods:** Contiene la lista de todos los tiempos definidos, por ejemplo, para indicar la lista en que un contacto recibirá e-mails en vez de recibirlo incluso estando de vacaciones, durmiendo o simplemente de festivo, aunque no recibir los e-mails no te salvará de que te echen la culpa por no arreglarlo igualmente.
- **windows:** Igual que la versión localhost, este contiene un ejemplo para servidores Windows.

#### **commands.cfg**

Necesitamos un nuevo servicio que nos permita utilizar el NRPE, este tendrá de argumento el nombre configurado en el cliente, así que desde este llamaremos a todos los servicios locales del cliente.

```
define command {
	command_name	check_nrpe
	command_line	$USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
}
```

#### **groups.cfg**

Cada fichero de host contiene la lista del grupo, pero esto se convierte en un caos cuanto más grande es nuestra red, así que creamos un fichero nuevo llamado groups.cfg para crear todos los grupos y en este iremos añadiendo cada host.

Para añadir un grupo de host creamos la siguiente fila y en members vamos indicando cada servidor.

```
define hostgroup {
        hostgroup_name  PRO
        alias           Production environment
        members         ServerName1
}
```

En cambio, en los grupos de servicios es mejor definir el grupo y dejar la asignación hacia el grupo en el servicio o en la template del servicio.

```
define servicegroup {
        servicegroup_name       HDD
        alias                   Hard Disk
}
```

Para este caso podemos configurar un servicio concreto asignado a este grupo.

```
define service{
	use						local-service
	host_name				localhost
	service_description		Root Partition
	check_command			check_local_disk!20%!10%!/
	servicegroups			HDD
}
```

Hay que añadirlo a los ficheros que llama el nagios.cfg o no será utilizado.

#### **nuevo host**

Para crear un nuevo host simplemente añadimos un fichero con la extensión cfg y definimos la información del host y todos sus servicios. Aquellos servicios que sean locales tendrán que ser definidos previamente en el cliente y utilizaremos el servicio NRPE para llamarlos.

En este ejemplo se utiliza el servicio NRPE para llamar al test de espacio en disco.

```
define host {
        use                     linux-server
        host_name               server1
        alias                   Server1
        address                 server1.domain.intranet
}

define service {
        use                     local-service
        host_name               server1
        service_description     Hard disk
        check_command           check_nrpe!check_disk
}
```

Hay que añadirlo a los ficheros que llama el nagios.cfg o no será utilizado.

## **Final**

Activar y configurar Nagios requiere mucho esfuerzo y dedicación, pero al final se compensa al saber que se están monitorizando continuamente todos los dispositivos y servicios importantes, si alguno de ellos falla, recibiremos el aviso de Nagios en vez de recibirlo de alguien que se ha dado cuenta que el servicio murió hace días y ahora se requiere urgentemente.

Con sus warnings también obtenemos la posibilidad de saber si algo está bajo riesgo y prepararnos para ello. ¿Cuántas veces un servidor se ha quedado sin espacio en el peor momento y no lo habíamos tenido en cuenta? Con Nagios esto no volverá a pasar.
