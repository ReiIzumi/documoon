---
title: Instalación de Active Directory
authors: rei.izumi
tags: [Active Directory, LDAP]
---

Como ya comenté en [otra entrada](/blog/2017/01/23/instalar-oracle-directory-server), los LDAP son uno de los servicios más importantes, este centraliza la información del usuario en un único punto y todos los servicios utilizan a este, simplificando enormemente la gestión, actualizando datos en tiempo real o casi real y evitando que los usuarios tengan tantos usuarios y contraseñas diferentes que les sea imposible encontrar cual es el correcto.

Debido a la importancia del LDAP, también lo es seleccionar cuál utilizaremos, actualmente existen varias opciones gratuitas como OpenLDAP u Oracle Directory Server (ODSEE), así como otras de pago, la más conocida es Active Directory (AD en adelante) de Microsoft.

En mi caso, siempre me he decantado por ODSEE, pero este provenía de Sun y, al ser comprado por Oracle, ha sido totalmente abandonado, así que no queda más remedio que aceptar tener un LDAP obsoleto o migrar a otro, la opción más lógica en este caso sería OpenLDAP, pero debido a mis necesidades este no es compatible con algunas aplicaciones que necesito, así que me he decantado por AD.

Migrar de un LDAP no-AD a un AD o a la inversa no es sencillo, ya que ninguna configuración de nuestros servicios funcionará por defecto, AD es diferente al resto, siempre, y hay que tenerlo en cuenta.

<!-- truncate -->

## **0-Previos**

Lo primero que debo decir es que no soy ni remotamente un experto en tecnologías Microsoft (y aún menos en Active Directory), en realidad el simple hecho de tener que tener un Windows Server en mi red ya ha sido una decisión realmente dura de tomar, y me he decantado únicamente por ello ya que no he tenido ninguna más opción, así esta guía indica los pasos mínimos para activar el AD y nada más, si se busca más información, AD es el LDAP más conocido así que hay cientos de webs que podrán detallar mejor que yo cada parte.

Utilizaré un Windows Server 2016 Standard (en inglés), aunque Active Directory está disponible en todos los Windows Server. En mi caso opté por descargar la versión de prueba (que fue infernal de encontrar, gracias Microsoft por hacerlo tan difícil) y, una vez me decidí por ello, pasar esta versión a la versión real (y eso falla por defecto, pero por comandos se puede solucionar, gracias nuevamente Microsoft, yo también os quiero ... lejos).

## **1- Instalación**

Al acceder a un Windows Server ya nos muestra el server manager, pero si no es el caso o lo hemos cerrado, lo primero será encenderlo:

```
ServerManager.exe
```

Aquí seguir los siguientes pasos:

- Add roles and features
- Role-based or feature-based installation
- Seleccionar tu servidor local
- Seleccionar "Active Directory Domain Services"
- Añadir Features
- Dejar las Features por defecto
- Instalar y esperar
- Clicar en "Promote this server to a domain controller"
- Ya que es nuevo, seleccionar Add a new forest e indicar el nombre de dominio y la contraseña de DSRM (Directory Services Restore Mode)
- DNS por defecto
- NetBIOS por defecto
- Carpeta y logs por defecto
- Si todo está correcto en el Summary y la prueba de requisitos, terminar con la instalación. Al terminar se reiniciará automáticamente

Después de estos simples pasos, Active Directory ya está en funcionamiento, podemos abrir el gestor con el siguiente comando:

```
dsac.exe
```

Active Directory a su vez hace de DNS, así que este proceso añadirá su IP a las DNS propias automáticamente, además de otras que pudiera tener, si esto no nos interesa (por ya tener un DNS) lo podremos quitar.

El usuario de inicio de sesión para este Windows Server por supuesto será actualizado para existir en el AD, así que podremos utilizarlo para iniciar sesión desde aplicaciones de LDAP remotas hasta que creemos otros usuarios con permisos más concretos, cuidado con la contraseña de este usuario ya que es el administrador de dominio.

Al acceder al cliente de LDAP, veremos que se ha creado la carpeta Users (CN=Users), en esta se almacenan tanto usuarios como grupos (incluido el administrador), podemos utilizar este mismo formato o crear nuestro propio formato creando nuevas Organization Units (OU) en la raíz y de aquí ir creciendo, quizás queramos montar una OU diferente para cada entorno o división.

También podemos seguir utilizando el JXplorer, aunque AD es un tanto peculiar así que recomiendo aprender a utilizar los mínimos de su cliente.

## **2- Activación de Windows Server de prueba a licencia final**

Si te has decantado por utilizar la versión de prueba descargable y después comprar una licencia (comprar, sí, de dónde la compres ya es decisión tuya, pero no apoyo la piratería en ninguno de sus sentidos), verás que el asistente de añadir licencias no funciona (sí que funciona, pero falla al introducir y validar la licencia).

Para activar la licencia necesitas activar el PowerShell con permisos de administrador y utilizar los siguientes comandos (este es para la edición Standard, de tener la enterprise hay que cambiar el Set-Edition por el tuyo):

```
Dism /online /Set-Edition:ServerStandard /AcceptEula /ProductKey:12345-67890-12345-67890-12345
Slui 3
```

Este proceso puede tardar una barbaridad (horas, en serio, ¡¡horas!!), en mi caso lo llegué a cancelar y volver a intentar, de alguna forma acabó funcionando, pero leí casos de más de 1 día.

## **Final**

Realmente, lo complicado de Active Directory no es activarlo, que es bastante rápido y sencillo si no necesitamos vincularlo a otros LDAP ni tener esclavos, lo realmente complicado es plantear qué jerarquía de LDAP queremos hacer y configurar todas las aplicaciones para este, se debe tener siempre en cuenta que tenemos un Active Directory, así que cualquier ejemplo para otro no nos servirá.
