---
title: Privatizar una web con Apache
authors: rei.izumi
tags: [Apache]
---

Algo normal en el transcurso de una web son los cambios y actualizaciones, quizás tu web se ha quedado obsoleta y quieres hacer una nueva, pero sin dejar de publicar la anterior, quizás ha salido una actualización de tu Wordpress y tienes que actualizar pero no te fías de que todo funcione a la primera. En estos casos la mejor opción es tener un servidor que sea un clon del original, allí puedes hacer todas las pruebas que quieras sin afectar nada, pero en condiciones normales tener dos servidores no es una opción, así que no te queda otra que crear un clon de tu página web y publicarla en el mismo Apache. Este tendrá una URL diferente, pero seguirá estando expuesta a Internet, difícilmente alguien la encontrará si no conoce la URL, pero ya que no es imposible, lo mejor es privatizarla para que requiera un usuario, de esta manera nos evitamos cualquier tipo de problema si alguien la encontrara y le diera un mal uso.

<!-- truncate -->

## **0- Previos**

Estos pasos indican cómo configurar Apache para tener un Virtual Hosts privatizado con usuario, también se puede utilizar directamente a una carpeta concreta con .htaccess pero no será explicado aquí.

El nuevo Virtual Hosts estará vacío, solo se creará la estructura de publicación, después tendremos que desplegar la aplicación que necesitemos.

En todo momento se utilizará el usuario root.

## **1- Configurar Apache**

Creamos el nuevo Virtual Hosts

```
vi /etc/apache2/vhosts.d/010-private.conf
```

Indicamos los datos según la URL de publicación, la carpeta donde está nuestra página y el sitio donde crearemos el fichero donde almacenaremos los usuarios que tendrán acceso.

```
<VirtualHost *:80>
        ServerName private.blog.domain.cat

        DocumentRoot /srv/www/privateBlog

        ErrorLog /var/log/apache2/privateBlog-error_log
        TransferLog /var/log/apache2/privateBlog-access_log

        HostnameLookups Off
        UseCanonicalName Off
        ServerSignature Off

        <Directory "/srv/www/privateBlog">
                AuthType Basic
                AuthName "Require user access"
                AuthUserFile /srv/privateBlog.users

                Require valid-user

                Options FollowSymLinks
                AllowOverride All
                Require all denied
        </Directory>
</VirtualHost>
```

Acabamos reiniciando el servidor.

```
rcapache2 restart
```

## **2- Crear usuarios**

Creamos el fichero junto al primer usuario.

```
htpasswd -c /srv/privateBlog.users usuario1
```

Para crear más usuarios se utiliza el mismo comando, pero sin el **\-c** de creación del fichero.

## **Final**

Con estos pocos pasos tenemos un nuevo Virtual Hosts para poder publicar cualquier web que necesitemos sin temor a que un usuario o bot lo puedan utilizar, muy importante cuando la web es, por ejemplo, una tienda web que es capaz de funcionar de verdad, permitiendo la compra, pero que su catálogo es solo de pruebas.
