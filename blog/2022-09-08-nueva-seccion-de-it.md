---
title: Nueva sección de IT
authors: rei.izumi
tags: [IT, MariaDB, PostgreSQL, Ubuntu]
---

Después de mucho tiempo, inauguro una nueva sección de [IT](/docs/it/introduction).

Esta sección está dedicada a servicios que no estén en contenedores (ya que para eso están los proyectos específicos), ni aquellos que sean servicios básicos, que se despliegan dentro del [Rasp Project](/docs/rasp/introduction).

<!-- truncate -->

Por ahora esta sección contempla la instalación de Ubuntu 22.04.1 LTS junto con todos los componentes típicos de la red (NTP, monitor de Nagios, copias de seguridad, ...), y también las 2 bases de datos de tipo SQL (PostgreSQL y MariaDB) que se encargarán de los nuevos servicios.

En el futuro espero poder añadir nuevas secciones, aunque estas tardarán un tiempo, ya que en la mesa de trabajo hay un proyecto aún más grande y que es quien ha provocado la necesidad de esta nueva sección.
