---
title: Sora Project
authors: rei.izumi
tags: [sora, terraform, fluxcd]
---

En 2022 empecé el proyecto *final* para abandonar Docker Swarm y cambiar a Kubernetes. Aunque tenía conocimientos en Kubernetes, cuando te pones a construir toda la base, decidir el tipo de *engine* para los contenedores, el tipo de red, solucionar las piezas que no tienes "de serie" ya que usas bare-metal, ... Por suerte internet está lleno de gente loca que se dedica a documentar y responder a todos estos temas. Por desgracia, según la posición del sol la siguiente 💣 es diferente. Tienes que prepararte para cualquier improvisto, ¡incluso ese que aún no has visto/deducido!

Aun así, terminé el 2023 con el sistema funcionando y documentado. Un éxito. Bueno, menos por el mantenimiento **enorme** que NO te explican. Y es que Kubernetes puede ser muy bonito, pero es absurdamente complejo, y muchos de los servicios también lo son.

Tocaba buscar una solución. Simplificar mantenimiento, simplificar creación. Así nació **Sora Project**.

<!-- truncate -->

Lo primero era identificar los problemas:
- Crear una máquina virtual es **demasiado lento y complejo**. Terraform podría simplificarlo.
- Pi Hole gestiona el DNS interno + DHCP. Debido a que sigue sin tener API, no se puede hacer nada (por ahora).
- La empresa de mi dominio no tiene un **DNS configurable**. Tras revisar el tema decidí gestionarlo a través de CloudFlare. Posiblemente una de las mejores ideas del año.
- **Actualizar los Linux + Kubernetes** es un trabajo titánico. Pasar a sistemas inmutables sería lo ideal.
- Cuando pestañeas, un **servicio se ha actualizado**. Aunque tengas sistemas que te avisen de las actualizaciones, lo que necesitas es que se actualicen solos. Peeeero, algunos explotan tras actualizar. Longhorn, Keycloak, CNPG, os miro a vosotros 😒, así que "auto-actualizar" puede ser peligroso, y al mismo tiempo es necesario.

Me encontré con una lista enorme y solo un año por delante. Podía alargarlo, claro, pero a cada 💣 veía que debía acelerar y no ralentizar. No es la mejor forma de recibir ánimos, pero es la que hay 🤯.

## Terraform
Sin duda lo primero es empezar con Terraform. Como usaba la gratuita de VMWare (no tiene API), las opciones son limitadas, pero parecía viable.

Entonces me despisté, VMWare cambió las reglas y me **chutaron** hacia Proxmox.

:::info
Antes de elegir Proxmox, revisé varias opciones, pero solo este me gustó. Comunidad grande, años de experiencia, copias de seguridad nativas y un Terraform viable funcionando. Le faltaría algunos detalles como disponer del control total de su Debian, lo que evitaría algunos problemas inesperados (como sí hace VMWare), pero no podemos tener todo, ¿no? 😃
:::

Cambiar el motor de virtualización no es fácil, primero tenía que hacerme con un servidor solo para ello donde hacer **pruebas realistas**. Si esto te falla, todo lo que tienes encima muere. No puedes decidirlo de hoy para mañana.

:::tip
Con esto también pasé a no actualizar servidores. Añades uno nuevo al nodo, borras el anterior. **¡Inmutables!**
:::

## Pi Hole
Cuando el mundo te sonríe, te creces, y empiezas a plantear que no hay límites, que esa pared la puedes romper, que ese precipicio lo saltas como si nada, y ese volcán ... 🔥

Así empezó mi aventura con Pi Hole. Necesitaba ajustar DNS y DHCP desde Terraform, pero no existe nada para el tema, su código PHP no ofrece una API, pero quizás podía *piratear* el sistema enviando el token, o incluso conectando vía SSH. Construir un *provider* de Terraform y montar yo todo el "tinglao".

**Fue una inmensa pérdida de tiempo**.

Tenía que decidir:
- Invertir lo que quedaba de año en esto, pensando que en un futuro saldría la API y tendría que reescribirlo todo.
- Plantear un sistema donde no fuera tan malo que funcionara a mano y seguir adelante.

No me gustaba la 2a opción, "si lo automatizo, lo automatizo" me decía. Por suerte mi parte lógica indicó que podría tener algo o no tener nada. Así que opté por seguir adelante.

:::warning
Pi Hole añadirá la API en su versión 3.
:::

## CloudFlare
Tras el fracaso con Pi Hole, venía el siguiente: añadir certificados de Let's Encrypt para sustituir mi wildcard de pago hacia internet y mi wildcard auto-firmada en la red interna. Fácil, conectas Cert-Manager a tu DNS para modificar por su lado y oh, mi DNS no lo permite, ¡pues claro que no! 😃

Tras hablarlo con la empresa de mi dominio supe lo que ya imaginaba: **estaba solo ante el peligro**.

Por suerte, Cert-Manager tiene una lista con los sistemas compatibles, opté por probar CloudFlare y hacer algunas pruebas iniciales. Todo funcionó, fácil, gratuito, incluso daba más soporte del que antes tenía (ninguno 😂). ¿Qué más podría pedir?

## FluxCD
Sin duda, el más complejo.

FluxCD no es que sea difícil de entender y utilizar. Bueno, sí, pero una vez aprendes Kustomize, entiendes las piezas de FluxCD, los modos de encriptación y las opciones para estructurar el ... vaya.

Tras mucho tiempo entendí el sistema y funcionaba, pero yo debía desplegar toda la infraestructura, hacerlo en un orden especifico y simplificarlo para mantener 2 entornos. Todo ello con el mínimo trabajo posible de mantenimiento. El punto vital no era tanto saber usar FluxCD (que también), era plantear cómo organizarlo.

Y aquí es donde empieza la diversión.

:::info
Cualquiera que estudie *agile* sabrá que es un sistema que te da "ideales" y "puntos", pero no te dice cómo hacer las cosas. ¡Cada equipo es diferente, tienes que buscar tu sistema! Gracias, pero plantear unas bases para estandarizar el sistema hubiera sido un DETALLAZO, ¿no? FluxCD es algo similar.
:::

Tras revisar los ejemplos de cómo estructurar un repositorio de FluxCD (que se agradecen), ves que no es realista antes un sistema de "producción". Internet está lleno de ideas, pero apenas hay información sobre cómo lo estructura cada persona/empresa, o simplemente no es tan fácil de encontrar. Así que tocaba construir algo desde 0.

:::info
FluxCD aporta algo que Kubernetes no tiene: dependencias. La infraestructura tiene muchas dependencias (¡¡cíclicas!!). De ahí me decidí a separar en niveles que requerirían el anterior para seguir adelante.
:::

Reconvertir mi Kubernetes "a mano" hacia FluxCD no era tan complicado debido a mi documentación y tener un entorno funcionando. El problema era cómo plantear la organización para que fuera lo más homogénea posible, cómo encriptar, y cómo mover todos los contenedores + helms fuera de Nexus (ya que este iba fuera). Además de la inclusión de nuevos servicios y mejorar otros como el *monitoring*.

Hoy puedo decir que no fue *difícil*, el problema es que fue un tira y afloja, donde crees que lo tienes y entonces te ataca por flanco y con veneno.

### Slack
He reducido las notificaciones vía e-mail, y no me interesaba que FluxCD usara algo que estaba abandonando.

Este tiene varios sistemas por defecto, así que opté por aquellos que más me interesaban, para ver que ninguno era viable 🤯.

Finalmente me dije "oye, si Slack se usa tanto en empresarial, ¿por qué no?". Nunca lo había usado así que inicialmente puede parecer un sistema bastante raro, pero funcionar, funciona.

### Loki, la broma
Uno de los puntos más importantes era mejorar la monitorización de todo el sistema. Mejorar Prometheus y Grafana no fueron demasiado problema (excepto los dashboards de Grafana, eso siempre son un problema).

Siguiendo la familia de Grafana, Loki y Tempo eran otra opción. Por trabajo uso mucho a Tempo y es crucial si quieres seguir vivo, pero no era mi caso ya que apenas tengo desarrollos propios y ninguno lo necesita realmente.

**Loki era un indiscutible.** Las siguientes semanas se explican con: 💣 😩 🔥 😢

:::warning
No, en serio, ¡huye!, aun estás a tiempo, quienes lo hemos probado ya no tenemos esperanzas 😣.
:::

### Longhorn
Siendo sincero, Longhorn es uno de los servicios que más problemas me da y el culpable (quizás indirectamente) por lo que todo esto se retrasara durante meses.

La réplica de los discos acaba provocando que el servicio se caiga en un nodo, así que este decide replicar en otro (4 nodos, 3 replicas), sobrecargando al sistema, lo que provoca que otro se caiga, active una alerta roja, todo entre en pánico y Nagios te envíe 500 mails de ERROR al día (en realidad eran unos cuantos más ...).

Seguramente sea uno de los puntos que más tendré que controlar durante el siguiente año ...

### CNPG
PostgreSQL desplegado dentro del clúster, capacidad de replica nativa, cambio de réplica a máster automática, copias de seguridad configurables desde FluxCD. ¿Qué es esta joya? ¡es demasiado bueno! **Una mentira como una casa, eso era.**

:::warning
Debo aclarar que uso CNPG sobre un EKS y con un sistema de despliegue 100% construido por mí y no me he encontrado ni uno solo de los errores que en mi bare-metal han sido el **pan de cada día**.
:::

CNPG dispone de un Helm para desplegar el clúster. En Beta y con cambios menores que **rompen** todo. Es más, incorporan bugs en algunos cambios. Gracias. Mala idea no plantear construir una versión final mía, pero uno de los ideales de Sora Project era la simplificación, crear mi propio Helm y tener que mantenerlo no parecía la mejor idea (lo hubiera sido ...).

Debo recalcar que Sora NO utiliza a CNPG. Tras meses de modificaciones y perder entornos por culpa explícita de CNPG vs Longhorn + errores masivos en copias de seguridad, he decidido desconectarlo y usar un PostgreSQL standalone fuera del clúster.

Eso no quiere decir que abandone, volveré, pero no con un sistema en producción. La siguiente vez lo mantendré en pruebas en un entorno más cerrado y ya veremos cómo evoluciona.

### Actualización de servicios
FluxCD permite indicar cómo debe actualizar, lo que es bastante sencillo ya que todos los servicios usan Helm y este siempre se diseña bajo Semver.

:::info
[Semver](https://semver.org/) es una estándar que diseña versiones según `x.y.z` donde:
- `x`: Cambio mayor que puede provocar errores en actualizaciones. Requiere revisar las instrucciones de actualización.
- `y`: Cambio menor. Ningún problema.
- `z`: Arreglo de errores o cambios menores. Ningún problema.
:::

Tras llegar aquí, supongo que te estás riendo bastante. Una estándar que se debe seguir. Ja, ja, ja ...

Siendo así, para cada servicio tenía que revisar cuándo provocaban explosiones y limitar FluxCD hacia esas. Esto es un problema ya que no tienes 100% claro si realmente explotarán o no, y no todos los servicios se actualizarán solos debido a ello. Lo ideal sería que el entorno de `Test` siempre actualice y te notifique si todo ha funcionado, para que ajustes manualmente `Producción`, al menos en los que sabes que son más peligrosos.

Sin duda, una mejora para el siguiente año.
