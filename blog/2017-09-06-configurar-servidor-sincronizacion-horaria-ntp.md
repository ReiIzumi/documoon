---
title: Servidor de sincronización horaria
authors: rei.izumi
tags: [NTP]
---

El tiempo pasa, y aunque creamos que en todos los lugares va a la misma velocidad, no es así.

Con el tiempo, algunos servidores pueden desviar mucho la fecha, tanto que pueden llegar a superar varios minutos u horas respecto a la hora real, esto acaba siendo un problema, ya que muchas conexiones o sincronizaciones pueden fallar debido a este desvío entre dos servidores.

Para solucionar esto, existe el protocolo NTP que permite revisar la fecha de un servidor y sincronizar con ella. Con una simple llamada UDP se puede actualizar rápidamente la fecha, el problema es que, normalmente, todos los servidores actualizan 1 vez por semana, como mínimo, esto supone poco consumo de Internet, pero es más eficiente configurar un servidor local que preguntará a fuera y todos los demás preguntarán a este, así no malgastaremos ancho de banda y también evitaremos consumir recursos de servidores externos, que muchas veces son voluntarios.

<!-- truncate -->

**Desde OpenSuSE Leap 15.0, NTP ya no es el sistema por defecto, se ha cambiado a chrony.** **He creado un [artículo nuevo](/blog/2020/04/22/servidor-sincronizacion-horaria-chrony) para explicar su configuración.**

## **0- Previos**

Configuraré un servidor Linux, en este caso un OpenSuSE, pero el servicio es el mismo para el resto de Linux. En OpenSuSE el servicio NTP está instalado por defecto, si no lo está, simplemente se debe instalar con el sistema de paquetes propio del Linux. Esto será así tanto en el servidor como en los clientes.

Todos los comandos son con el usuario root.

## **1- Servidor**

El servidor se conectará a varios servidores externos, cuanto más cerca geográficamente más rápido será, para elegirlos se pueden buscar desde la página del proyecto [pool ntp](http://www.pool.ntp.org/), buscamos según nuestra zona o país y nos dará una lista.

En mi caso utilizaré directamente los de Europa.

Lo primero es actualizar la fecha del servidor ya para evitar problemas.

```
ntpdate -u pool.ntp.org
```

Ahora editamos el fichero de configuración.

```
vi /etc/ntp.conf
```

Quitamos los servidores por defecto y añadimos los nuestros, los de Europa en mi caso. También añadimos un nuevo restrict para permitir a los servidores de la red que puedan preguntar a este servidor, en mi caso son de la red 192.168.1.X

```
server 1.europe.pool.ntp.org
server 2.europe.pool.ntp.org
server 3.europe.pool.ntp.org

restrict 192.168.1.0 netmask 255.255.255.0 nomodify notrap
```

Finalmente abrimos el puerto 123/UDP desde Yast y reiniciamos el servicio.

```
rcntpd restart
```

Para confirmar que funciona utilizamos el siguiente comando. Este nos indicará qué IPs está utilizando, después de un rato (varios minutos como mínimo), uno de ellos tendrá un \* al inicio, esto significa que ya ha sincronizado con ese servidor.

```
ntpq -p
```

## **2- Cliente**

Los pasos en el cliente son muy similares.

Primero actualizamos la fecha contra el nuevo servidor.

```
ntpdate -u <ntpServer>
```

Editamos el fichero de configuración.

```
vi /etc/ntp.conf
```

Cambiamos los servers por el nuestro.

```
server <ntpServer>
```

Y reiniciamos.

```
rcntpd restart
```

## **Final**

Estos pasos tan sencillos nos asegurarán que los servidores tengan unas fechas con desvíos de muy pocos segundos en el peor de los casos, así evitamos posibles problemas que acaban siendo verdaderos dolor de cabeza si no encontramos el motivo.
