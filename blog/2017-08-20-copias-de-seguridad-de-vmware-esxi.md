---
title: Copias de seguridad de VMWare ESXi
authors: rei.izumi
tags: [VMWare ESXi, XSIBackup]
---

Normalmente, cada servidor crea copias de seguridad de sus datos cambiantes, si tuviéramos cualquier problema, solo tendríamos que volver a pasar estos datos a la máquina o a una nueva con la misma configuración, pero construir una máquina igual no es tan sencillo, ni rápido.

En estos casos, lo más factible es tener una copia de seguridad de toda la máquina, en este caso simplemente la restauramos, copiamos la última copia de seguridad y ya volveremos a tener la máquina disponible. Si los servidores son máquinas virtuales, esto será bastante sencillo, ya que únicamente tendemos que copiar sus ficheros.

VMWare ESXi dispone de varias aplicaciones, normalmente de pago, que, mediante la API, permiten crear copias de seguridad. El problema es que el acceso a la API requiere tener la versión licenciada de ESXi, si solo dispones de ESXi Hypervisor (la versión gratuita), no tendrás acceso a esa API. Por si fuera poco, las aplicaciones que crean copias de seguridad suelen ser de pago e incluso más caras que el propio VMWare.

Así que, en vez de la API, se utilizará un proceso interno, un software gratuito que funciona en cualquier ESXi y crea copias de seguridad en caliente.

<!-- truncate -->

## **0- Previos**

Es necesario tener acceso al SSH de VMWare, este se puede habilitar desde la consola administrativa (ya sea por web o app), solo es necesario mientras configuramos este servicio, después lo podemos deshabilitar.

Utilizaré la aplicación [XSIBackup](https://33hops.com/xsibackup-vmware-esxi-backup.html), dispone de una versión free y otra de pago, básicamente la opción de pago permite restaurar, una opción que puede ser muy útil para una empresa, pero en mi caso no es necesaria.

Los pasos que yo indico son un resumen del manual oficial, para obtener toda la información revisa la [documentación oficial](https://33hops.com/xsibackup-help-man-page.html).

El VMWare ESXi debe tener un lugar donde guardar las copias de seguridad, normalmente será una unidad montada con NFS, esta unidad se puede montar desde la consola administrativa fácilmente, así que no explicaré el proceso.

## **1- Instalar y configurar XSIBackup**

Lo primero es acceder a la web de [XSIBackup](https://33hops.com/xsibackup-vmware-esxi-backup.html), clicar en "download xsibackup" e indicar nuestros datos, esto nos enviará un e-mail con el proceso de descarga.

Cuando recibimos el e-mail, simplemente copiamos los comandos cambiando la carpeta de destino del software.

Este es un ejemplo de ese código, tendréis que añadir la key de vuestro e-mail.

```
cd /vmfs/volumes/datastore1/xsi-dir 2>/dev/null || mkdir /vmfs/volumes/datastore1/xsi-dir && \
cd /vmfs/volumes/datastore1/xsi-dir && \
esxcli network firewall unload && \
wget http://a.33hops.com/downloads/?key= -O xsibackup.zip && \
unzip -o xsibackup.zip || cat xsibackup.zip && echo "" && \
chmod -R 0700 xsibackup* bin && \
rm -rf xsibackup.zip && \
esxcli network firewall load
```

Lo siguiente es ejecutarlo por primera vez para validar que todo funciona y aceptar la licencia, para ello lo ejecutamos en modo test, de paso configuramos el e-mail ya para confirmar que también tiene acceso a todo.

Se tendrá que cambiar la carpeta donde se guardó el software, la carpeta de guardado (backup-point) y todos los datos de mail.

```
/vmfs/volumes/datastore1/xsi-dir/xsibackup --backup-point=/vmfs/volumes/backup --backup-type=running --mail-from=email.sender@yourdomain.com --mail-to=email.recipient@anotherdomain.com --smtp-srv=smtp.33hops.com --smtp-port=25 --smtp-usr=username --smtp-pwd=password --test-mode=true
```

Una vez aceptada la licencia, revisará todos los datos y creará un falso proceso, finalmente envía un e-mail con los resultados.

Ahora ya podemos utilizar el proceso real, simplemente quitando la opción de test. Este comando crea una copia de seguridad de todas las máquinas virtuales iniciadas.

```
/vmfs/volumes/datastore1/xsi-dir/xsibackup --backup-point=/vmfs/volumes/backup --backup-type=running --mail-from=email.sender@yourdomain.com --mail-to=email.recipient@anotherdomain.com --smtp-srv=smtp.33hops.com --smtp-port=25 --smtp-usr=username --smtp-pwd=password
```

O también podemos indicar una lista de máquinas.

```
/vmfs/volumes/datastore1/xsi-dir/xsibackup --backup-point=/vmfs/volumes/backup --backup-type=custom --backup-vms="New Virtual Machine,Newer Virtual Machine" --mail-from=email.sender@yourdomain.com --mail-to=email.recipient@anotherdomain.com --smtp-srv=smtp.33hops.com:25 --smtp-port=25 --smtp-usr=username --smtp-pwd=password
```

Cada máquina virtual se copia en una carpeta con su nombre, la anterior es borrada mientras se crea la nueva, si queremos archivarlas podremos utilizar otro proceso para ello.

Si tenemos curiosidad por el proceso, lo que hace es crear un snapshot de la máquina, copiar todos los discos duros asociados a esta máquina y después borrar el snapshot, lo que permite copiar la máquina sin riesgo de que sea modificada mientras se crea la copia.

## **2- Cron**

Primero instalamos el sistema de cron, para ello ejecutamos el siguiente comando.

```
/vmfs/volumes/datastore1/xsi-dir/xsibackup --install-cron
```

Esto crea un fichero llamado xsibackup-cron y un log. Toda la configuración se indica en ese fichero, así que empezamos editándolo.

```
vi /vmfs/volumes/datastore1/xsi-dir/xsibackup-cron
```

En la parte inicial se explica la configuración, básicamente utilizamos los mismos comandos, uno en cada línea, pero añadiendo un --time="Day HH:mm", donde se indica el día, horas y minutos.

Esto nos permite configurar unas máquinas especificas a clonar en un día y otras diferentes otros días, así minimizamos el impacto de trabajo.

## **Final**

Este software es realmente sencillo de utilizar y cualquier dato extra que necesitemos lo encontramos en su documentación oficial o los blogs/foros que dispone.
