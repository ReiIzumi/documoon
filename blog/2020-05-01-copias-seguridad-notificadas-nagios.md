---
title: Copias notificadas con Nagios
authors: rei.izumi
tags: [Backup, Nagios]
---

Los días se vuelven más tranquilos cuando sabes que todos los sistemas están ejecutando las copias de seguridad automatizadas, y estos te avisan de que todo está funcionando correctamente.

A su vez, no hay nada como recibir decenas, o cientos de e-mails de cada servicio de copia de seguridad, avisando de que todo ha funcionado correctamente, o no. Esto provoca que, o no se lean los mensajes, o se activen reglas para desviarlos, pero en definitiva obtenemos lo mismo: nadie revisa los mensajes, así que no sabemos si realmente funciona o no.

La opción más clásica es únicamente notificar si ha fallado, pero esto también es un problema, ya que si un proceso falla al punto de no poder enviar mensajes (por ejemplo, si se queda sin espacio), no lo sabremos nunca, así que nos obliga a ir revisando el sistema manualmente.

Para solucionar esto, podemos redirigir las notificaciones hacia otro sistema que pueda revisar si ha funcionado o fallado y avisarnos únicamente si ha fallado o si no ve ningún mensaje cuando sí debería haberlo recibido, para ello disponemos de Nagios.

<!-- truncate -->

Nagios es un monitor de sistema que puede utilizar cualquier tipo de script, incluso cada servidor puede tener sus propios scripts y el servidor de Nagios llamarlos cada cierto tiempo, mientras estos scripts devuelvan el mensaje en el formato de Nagios, podremos monitorizar cualquier cosa.

Con todo esto en mente, he creado varios scripts que mejoran el sistema que [anteriormente hice](/blog/2017/02/15/crear-copias-seguridad/).

## **1- Scripts**

He empaquetado todos los scripts en un fichero que ya dispone del formato de carpetas, los permisos y la configuración básica para utilizarse, se puede descargar **[aquí](/files/blog/backup.tar.gz)**.

Todos los scripts tienen licencia de [Creative Commons](https://creativecommons.org/licenses/by/4.0/) con toda la libertad de modificar o usar en cualquier lugar, únicamente requieren atribución.

Cada uno contiene la licencia, descripción, lista de argumentos (en caso de tener) y la configuración, aun así, describiré cada uno de ellos.

Esta es la lista de ficheros y carpetas por defecto junto a los permisos, debido a que los scripts se llaman entre ellos, si se modifica esta estructura, se debe actualizar la configuración de los scripts.

| Ruta | Permisos |
| --- | --- |
| /opt/scripts/backup/backupTemplate.sh | 744 |
| /opt/scripts/notification | 755 |
| /opt/scripts/mount.sh | 744 |
| /opt/scripts/notificationChecker.pl | 755 |
| /opt/scripts/notifyCSV.sh | 744 |
| /opt/scripts/notifyEmail.sh | 744 |
| /opt/scripts/rotateFiles.sh | 744 |

## **2- Configuración de scripts de utilidades**

Junto con el sistema de copias de seguridad, se incluyen varias utilidades que son necesarias según la estructura de nuestro sistema.

### **mount.sh**

Normalmente las copias de seguridad se almacenan en una unidad aparte, montada como una carpeta compartida, para ello se debe crear una carpeta local y configurar en el **/etc/fstab** la unidad que se montará en esa carpeta.

Este es un ejemplo:

```
mkdir /mnt/backup
vi /etc/fstab
```

Indicamos la carpeta de montaje.

```
nas.domain.intranet:/BKP_ServerName /mnt/backup     nfs     defaults 0 0
```

Y reiniciamos el servicio.

```
mount -a
```

A partir de aquí, podemos guardar las copias de seguridad en esa carpeta compartida que se almacenará fuera de nuestro servidor, el problema radica en que, si el servidor al que conectamos se reinicia, muchas veces se desconectará la carpeta compartida. Esto es un problema debido a que las copias de seguridad, no solo no se guardarán en el sistema externo, si no que nos dejarán sin espacio en el disco duro.

Este script se encarga de detectar si la unidad está montada, y, en caso de no estarlo, volver a montarla.

### **rotateFiles.sh**

Cada copia de seguridad se almacena en una carpeta acorde al nombre del identificador, dentro de esta se genera una carpeta numerada hasta el máximo que indiquemos (5 por defecto). Cada vez que se ejecuta el script de copia de seguridad, cada carpeta se rota hacia la anterior, y la última es borrada si supera el máximo que almacenamos.

Este script únicamente se encarga de esa rotación y no requiere ninguna configuración.

El límite de copias de seguridad que permite es 99.

### **notifyEmail.sh**

Aunque la idea es notificar utilizando a Nagios, he incluido el soporte para notificar por e-mail en caso de que sea necesario en algún proceso.

El sistema enviará un e-mail en cualquier caso, funcione o no, con el resultado final.

El script requiere indicar un nombre y entorno para añadirlo al mensaje y que así sea más fácil identificar quien ha producido este aviso. Debido a que utiliza el relay propio del sistema, se deberá configurar previamente el [relay de Postfix](/blog/2017/02/04/configurar-relay-mails-postfix) como ya indiqué en su día.

### **notifyCSV.sh**

Este es el sistema por defecto de notificación, cada resultado se almacena en un único fichero de tipo CSV separado por #, según si funcionó o no, su extensión cambiará y contiene la lista de ficheros que se han generado.

Cada proceso genera una notificación y esta es la que se leerá por el sistema de Nagios.

## **3- Configuración del script de copia de seguridad**

Por cada fichero o carpeta que queramos guardar, se debe copiar este fichero, por ello está en una carpeta aparte donde tendremos cada script acorde a la configuración.

Esta es la lista de configuraciones:

| Nombre | Descripción | Valor por defecto |
| --- | --- | --- |
| IDENTIFIER\_NAME | Identificador de la copia, se utiliza para diferenciarla de otras en la carpeta de destino y notificaciones |  |
| SOURCE\_FOLDER | Fichero o carpeta que deben ser guardados |  |
| BACKUP\_FOLDER | Destino de la copia | /mnt/backup/$IDENTIFIER\_NAME |
| BACKUP\_MAXIMUM\_FILES | Cantidad de copias que se almacenan | 5 |
| BACKUP\_ROTATION\_SCRIPT | Script de rotación | /opt/scripts/rotateFiles.sh |
| BACKUP\_SEPARATE\_FILES | En caso de querer copiar una carpeta, todo su contenido se almacena en un único fichero, si se quiere crear un fichero por cada fichero o carpeta que este contiene, se debe configurar a true | true |
| BACKUP\_COMPRESSION\_TYPE | Tipo de compresor, de más rápido a mejor compresión: gzip, bzip2, xz. Requiere estar instalado en el sistema. | xz |
| BACKUP\_COMPRESSION\_LEVEL | Nivel de compresión, de más rápido a mejor compresión: 1 a 9 | 9 |
| BACKUP\_FILENAME\_DATE | Fecha añadida al nombre del fichero | \`date +\_%Y-%m-%d\_%H-%M-%S\` |
| AUTOMOUNT\_SCRIPT | Script de auto montaje | /opt/scripts/mount.sh |
| NOTIFICATION\_SCRIPT | Script de notificación | /opt/scripts/notifyCSV.sh |
| LOG | Mostrar los logs | true |

Es recomendable ejecutar el script por cada copia que tengamos, para confirmar que todo funciona y, también, para crear el primer fichero de notificación para Nagios.

## **4- Configuración de script para Nagios**

Primero de todo debemos tener un Nagios Core funcionando tal como [expliqué anteriormente](/blog/2017/07/20/instalar-configurar-nagios-opensuse), cada servidor publicará por NRPE una o varias llamadas que el servidor podrá usar para preguntar por el estado de las copias de seguridad.

Este es el funcionamiento:

- Utilizando cron, el sistema iniciará el fichero de copia de seguridad (backup.sh) cada cierto tiempo.
- Al terminar, cada copia de seguridad generará un fichero CSV en la carpeta de notificaciones.
- El servidor de Nagios Core preguntará a cada cliente que tenga esto configurado, por el estado de la copia.
- El servidor que es preguntado por Nagios, utilizará el script de revisión (notificationChecker.pl) para confirmar si la notificación existe o no.
- Si la notificación existe, el servidor responderá a Nagios con un Ok, si no, responderá con un error.
- Si Nagios recibe un Ok, no hará nada, si responde con error, notificará a las personas necesarias.

Para lograr esto, primero debemos preparar el fichero notificationChecker.pl que tiene algunos requisitos, el primero de ellos es que debe tener permisos de ejecución a otros o al usuario/grupo que es utilizado por NRPE.

El otro es Perl con varias dependencias, en los últimos sistemas Perl suele estar instalado, pero requeriremos las dependencias de DateTime.

```
zypper -n in gcc
export PERL_MM_USE_DEFAULT=1
cpan install CPAN
cpan install DateTime DateTime::Format::Strptime
```

Una vez instalado se podrá confirmar si funciona, estos son los argumentos que requiere.

| Argumento | Descripción |
| --- | --- |
| Identifier Name | Identificador de la copia de seguridad |
| Start time | Hora en la que se inicia. Formato hh:mm:ss |
| Minutes for warning | Minutos de espera desde el inicio hasta que se considera que puede existir un problema del que Nagios debe avisar. |
| Minutes for critical | Máximo tiempo de espera hasta considerar que ha fallado. |

Este es un ejemplo para un servicio llamado Apache, iniciado a las 15pm, con una espera de 5 minutos para avisar de Warning y 10 para Critical.

```
/opt/scripts/notificationChecker.pl Apache 15:00:00 5 10
```

Una vez confirmemos que el script funciona, se deberá preparar Nagios para utilizarlo.

## **5- Configuración de Nagios**

Debido a que el script requiere unos argumentos, en Nagios tenemos 2 opciones:

- Permitir que el servicio NRPE acepte argumentos de Nagios, así en Nagios configuraremos un solo comando y para cada copia de seguridad le indicaremos los argumentos que debe enviar. Esto es más práctico ya que solo habrá 1 comando para todos los tipos de copias de seguridad que tengamos, pero es menos seguro ya que estaremos permitiendo que NRPE acepte argumentos en las funciones que dispone.
- Para cada copia de seguridad, crear un comando diferente que ya disponga de los argumentos, y en el servidor de Nagios configurar cada uno de esos comandos. Esto nos permite que los datos de inicio, tiempos de warning/critical estén en el mismo servidor, con lo que está mejor ordenado, pero tendremos que configurar cada comando en el servidor lo que será un trabajo bastante tedioso.

Cada opción tendrá una configuración diferente, así que las separaré en bloques para que sea más fácil de seguir.

### **5.1- Envío de argumentos**

Si nos decantamos por anular la seguridad de NRPE y utilizar argumentos, lo primero será configurar el NRPE del cliente y definir el único comando que tendremos.

```
vi /etc/nrpe.cfg
```

Modificaremos el dont\_blame y añadiremos el nuevo comando.

```
dont_blame_nrpe=1
command[check_bkp]=/opt/scripts/notificationChecker.pl $ARG1$
```

Lo siguiente será añadir este comando en el servidor de Nagios. La ruta al fichero puede variar según cómo se instaló el sistema.

```
vi /etc/nagios/objects/commands.cfg
```

Añadimos el comando.

```
define command {
        command_name    check_nrpe_bkp
        command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_bkp -a '$ARG1$'
}
```

Ya solo queda ir añadiendo cada servicio para los servidores.

```
vi /etc/nagios/servers/Server.cfg
```

Este es un ejemplo acorde al ejemplo anterior, de paso lo he añadido a un grupo de Backup para agruparlos en la UI de Nagios

```
define service {
        use                     local-service
        host_name               Server
        service_description     Backup Apache
        check_command           check_nrpe_bkp!Apache 15:00:00 5 10
        max_check_attempts      1
        servicegroups           Backup
}
```

### **5.2- Comandos para cada script**

Si queremos mantener la seguridad de NRPE y dejar que cada servidor contenga sus propios datos, estos son los pasos que seguir.

Editamos el fichero de NRPE para añadir cada uno de los comandos.

```
vi /etc/nrpe.cfg
```

Este es un ejemplo siguiendo los anteriores ejemplos, pero se debe replicar para cada script que tengamos.

```
command[check_bkp_apache]=/opt/scripts/notificationChecker.pl Apache 15:00:00 5 10
```

En el servidor de Nagios, debemos replicar cada uno de los comandos de todos los servidores que tenemos, este es un ejemplo para definir este comando.

Primero debemos editar la lista de comandos. La ruta puede cambiar según la instalación del sistema.

```
vi /etc/nagios/objects/commands.cfg
```

El comando acorde al ejemplo anterior.

```
define command {
        command_name    check_nrpe_bkp_apache
        command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_bkp_apache
}
```

Finalmente editamos la configuración de cada servidor.

```
vi /etc/nagios/servers/Server.cfg
```

Y añadimos el nuevo servicio.

```
define service {
        use                     local-service
        host_name               Server
        service_description     Backup Apache
        check_command           check_nrpe_bkp_apache
        max_check_attempts      1
        servicegroups           Backup
}
```

### **5.3- Probando las notificaciones**

Sin importar el camino que hemos seguido, lo siguiente será reiniciar el NRPE del cliente.

```
rcxinetd restart
```

Con esto ya podremos acceder, pero antes es preferible hacer una prueba desde el servidor de Nagios para confirmar que todo funciona como debería.

Si elegimos la opción de argumentos:

```
/usr/lib/nagios/plugins/check_nrpe -H server.domain.intranet -c check_bkp -a 'Apache 15:00:00 5 10'
```

Si elegimos la opción de réplica de comandos:

```
/usr/lib/nagios/plugins/check_nrpe -H server.domain.intranet -c check_bkp_apache
```

Si todo ha funcionado correctamente, ya podemos reiniciar el servidor de Nagios.

```
rcnagios restart
```

## **Final**

Es un proceso tedioso configurar Nagios y NRPE para ello, pero a la larga, vale la pena, debido a que no habrá error humano ni "spam" de notificaciones, el sistema únicamente nos informará si ha habido un error, y si no nos informa de nada, es que todo funciona.

Como detalle debo avisar de que Nagios, por defecto, hace 4 comprobaciones antes de notificar de Warning o Critical, y que espera un tiempo entre ellas, por eso he cambiado la configuración a únicamente hacer 1 intento antes de avisar, debido a que mis tiempos entre Warning y Critical son tan pequeños, que salta directo al Critical, pero si nuestros intervalos van a ser grandes, es mejor mantener al menos 2 comprobaciones por si hubiera un error momentáneo en la red que nos diera un falso negativo.
