---
title: Instalar OpenSuSE 42.3
authors: rei.izumi
tags: [OpenSuSE]
---

Quizás lo más complicado no es instalar un nuevo servicio, ya que una vez está instalado y configurado, este ya funciona por sí mismo normalmente. El problema radica en instalar una nueva máquina que requiera de ciertas configuraciones para hacer uso de ese servicio, y eso en sí, tampoco es un problema, el problema es cuando son 10 o más servicios, ya no es solo instalar el Linux que toque y configurar la red, ahora toca configurar todos los servicios necesarios para que la red lo "acepte".

Este artículo trata de una instalación simple de OpenSuSE, lo más sencilla posible, para su uso como servidor donde se empezará a instalar nuevos servicios y este ya se conectará a los servicios básicos de la red.

<!-- truncate -->

## **0- Previos**

Este post contiene la instalación de un OpenSuSE nuevo, a partir de aquí, el resto de las configuraciones son según otros posts, solo indicaré lo mínimo necesario (y la forma más rápida en algunos casos) para configurar el nuevo servidor y configurar el servicio en el servidor original para aceptar a este nuevo. Por ello, no explicaré en detalle cada configuración ya que fue explicada en el post original de ese servicio.

Si un servicio no está en tu red, simplemente salta ese apartado, también es posible que algunas configuraciones del host original sean un tanto diferente (las rutas de carpetas, por ejemplo).

## **1- Instalar OpenSuSE**

Al iniciar el servidor con la ISO en el formato que sea, nos muestra la pantalla de para poder instalar.

En servidores siempre la mantengo por defecto y en inglés, elegimos instalar.

!(/files/blog/opensuse/Install_OpenSuSE_01.png)

Aunque el idioma del Linux sea inglés, cambio el teclado a castellano ya que es lo que tengo a mano y confirmo que las teclas propias funcionan.

!(/files/blog/opensuse/Install_OpenSuSE_02.png)

La selección de discos original está más orientada a un PC desktop, así que la borro por completo, asigno 3 unidades primarias:

- SWAP: Su espacio es igual a la mitad de la RAM y nunca superior a 8 GiB
- Raíz (/): Con 15-25 GiB suele tener más que suficiente, suelo restar el espacio que consumiera la SWAP.
- OPT (/opt): Casi todas las aplicaciones no propias del sistema suelen instalarse en esta carpeta, así que requiere mucho más espacio que el resto, por ello es la que más espacio le reservo, si es una máquina virtual, le asigno otro disco para él cosa que me permitiría aumentarlo en un futuro si fuera necesario.

Tanto la raíz como opt suelo utilizar EXT4 por su compatibilidad y sencillez de uso.

!(/files/blog/opensuse/Install_OpenSuSE_03.png)


Para evitar problemas, todos los servidores deberían estar en la misma zona horaria y contener la misma hora.

Tenemos varias opciones de escritorios o simplemente no utilizar escritorio, por las aplicaciones que utilizo, en muchos casos necesito tener escritorio activo, pero KDE y Gnome consumen demasiado para mi gusto, así que simplemente clico en Custom y activo XFCE que es más que suficiente para el uso que le doy.

Para no tener problemas a futuro, activo las siguientes herramientas:

- XFCE Desktop Environment
- Console Tools
- 64-Bit Runtime Environment
- x86 Runtime Environment
- Base Development
- C/C++ Development

!(/files/blog/opensuse/Install_OpenSuSE_04.png)


Por seguridad, defino un usuario, pero desmarco las casillas de auto-login y utilizar la misma contraseña que el administrador. Root debe tener una contraseña diferente y ese usuario también requiere tener una contraseña que no permita el acceso a cualquiera, si tuviera auto-login también sería un problema de seguridad.

En el resumen final, activamos el servicio SSH y lo abrimos en el firewall.

!(/files/blog/opensuse/Install_OpenSuSE_05.png)

## **2- Red**

Lo primero que configuramos es la red, si estamos en el escritorio, lo podemos hacer en pocos pasos desde **YaST**, lo encontramos en la barra de **Inicio - Settings**.

Accedemos al menú **System - Network Settings**, siguiendo los siguientes pasos según la pestaña:

- Overview: Editamos la red para indicar la IP estática, máscara de red y nombre de host.
- Hostname/DNS: Servidor de DNS y el nombre de dominio.
- Routing: IP del gateway.

Al guardar ya tendremos red y no necesitaremos más el desktop, así que podremos cerrar sesión.

## **3- DNS**

En el servidor de DNS tenemos que añadir la información del nuevo servidor.

```
cd /var/lib/named/
vi domain.intranet.zone
vi 192.168.1.0.zone
rcnamed restart
```

## **3- SSH**

Accedemos al servidor por SSH.

```
vi /etc/ssh/sshd_config
```

Activamos y/o añadimos los siguientes:

```
Port 22
Protocol 2
LogLevel INFO
PermitRootLogin yes
RhostsRSAAuthentication no
HostbasedAuthentication no
IgnoreRhosts yes
PermitEmptyPasswords no
ChallengeResponseAuthentication no
ClientAliveInterval 7200
ClientAliveCountMax 0
Banner /etc/ssh/banner
AllowUsers root
```

Escribimos nuestro banner:

```
vi /etc/ssh/banner
```

Desde la aplicación de PuTTYgen generamos unas nuevas claves de 4096 bits o superior para cada usuario con acceso y añadimos algunos datos para identificarlo más rápido:

- Key comment: "\<nombre del servidor\> - \<nombre del usuario o PC con este certificado\> - \<nombre del usuario en el servidor\>"
- File: \<nombre del servidor\>\_\<nombre del usuario con este certificado\>\_\<nombre del usuario en el servidor\>.ppk

Y las vamos añadiendo en la carpeta acorde al usuario, si es la de root:

```
mkdir /root/.ssh
vi /root/.ssh/authorized_keys
```

Con todas las claves preparadas, reiniciamos el servicio y lo configuramos en el PuTTY local para tener el acceso al servidor siempre a mano.

```
rcsshd restart
```

## **4- Actualizar OpenSuSE**

Dejamos una consola abierta que vaya actualizando el sistema y lo reiniciamos cuando sea posible.

```
zypper up
```

## **5- Postfix (relay)**

```
vi /etc/postfix/main.cf
```

Indicamos los datos del servidor y el de relay:

```
myhostname = <ServerName>
relayhost = smtp.domain.cat
smtp_sasl_auth_enable = yes
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_generic_maps = hash:/etc/postfix/generic
```

Añadimos la contraseña.

```
vi /etc/postfix/sasl_passwd
```

Según el siguiente formato:

```
smtp.domain.cat user@domain.cat:password
```

Definimos el sender.

```
vi /etc/postfix/generic
```

Según el siguiente formato:

```
root@ServerName.localdomain tu-mail@domain.cat
```

Compilamos los anteriores y reiniciamos el servicio.

```
postmap /etc/postfix/sasl_passwd
postmap /etc/postfix/generic
rcpostfix restart
```

Añadimos un alias para los e-mails de root.

```
vi /etc/aliases
```

Según el siguiente formato:

```
root: tu-mail@domain.cat
```

Terminamos testeando que todo funcione bien.

```
echo "Texto de prueba" | mail -s Pruebas tu-mail@domain.cat
```

## **6- NTP**

Sincronizamos con nuestro NTP server y después lo añadimos a la configuración.

```
ntpdate -u <ntpServer>
vi /etc/ntp.conf
```

Según el siguiente formato:

```
server <ntpServer>
```

Reiniciamos el servicio.

```
rcntpd restart
```

## **7- Montar carpeta de backup**

Doy por hecho que ya se ha creado una carpeta en otro servidor y compartido por NTP con permisos de escritura en este nuevo servidor.

Creamos la carpeta destino y accedemos a YaST para utilizar su asistente.

```
mkdir /mnt/backup
yast
```

Desde **Network Services - NFS Client** indicamos los datos del servidor origen y esta nueva carpeta para que monte siempre la unidad. Network Services - NFS Client

## **8- Nagios**

Empezamos configurado el nuevo servidor para que tenga el cliente de Nagios instalando de la forma más rápida.

```
zypper in nrpe
vi /etc/xinetd.d/nrpe
```

Habilitamos el servicio.

```
disable = no
only_from = 127.0.0.1 localhost <nagios_ip_address>
```

Añadimos los servicios a revisar.

```
vi /etc/nrpe.cfg
```

Estos serán los básicos, deberemos ir añadiendo más según lo que tenga el servidor.

```
command[check_disk_root]=/usr/lib/nagios/plugins/check_disk -w 20 -c 10 -p /
command[check_disk_opt]=/usr/lib/nagios/plugins/check_disk -w 20 -c 10 -p /opt
command[check_users]=/usr/lib/nagios/plugins/check_users -w 5 -c 10
command[check_load]=/usr/lib/nagios/plugins/check_load -w 15,10,5 -c 30,25,20
command[check_zombie_procs]=/usr/lib/nagios/plugins/check_procs -w 5 -c 10 -s Z
command[check_total_procs]=/usr/lib/nagios/plugins/check_procs -w 250 -c 300
```

Reiniciamos y abrimos el puerto desde Yast (NRPE Service o TCP/5666)

```
rcxinetd restart
```

Ahora vamos al servidor para copiar una configuración existente y actualizarla con el nuevo.

```
cd /usr/local/nagios/etc/servers
cp OldServer.cfg NewServer.cfg
vi NewServer.cfg
```

Actualizamos los datos del fichero con el siguiente comando, **este NO actualiza el address, hay que actualizarlo manualmente**.

```
:%s/OldServer/NewServer/g
```

Editamos el group para añadirlo donde toque.

```
vi ../objects/groups.cfg
```

Acabamos reiniciando-

```
rcnagios restart
```

## **Final**

Con esto ya habremos acabado las bases, pero aun faltará activar las partes específicas como las copias de seguridad, procesos que requieran ser revisados desde el propio servidor o configuraciones más específicas para Nagios según el uso final de este nuevo servidor.
