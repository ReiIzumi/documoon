---
title: DNS con BIND en OpenSuSE Leap 42.2
authors: rei.izumi
tags: [DNS, BIND]
---

En la actualidad es imposible recordar todas las IPs de dominio, para ello utilizamos los servidores DNS para que se encargan de resolver las IPs a partir del dominio, incluso varios servicios (Virtual Hosts de Apache por ejemplo) solo responderán como esperamos si utilizamos el dominio correcto, y no directamente su IP, por todo ello es tan importante tener un servidor DNS.

Hacia internet, al contratar un dominio público, este suele llevar su propio DNS que podemos configurar para crear nuevos subdominios del nuestro, pero en la red privada este no nos servirá, para ello la mejor opción es crear uno interno.

De entre todas las opciones, BIND es el servicio más extendido.

<!-- truncate -->

## **0- Previos**

Esta configuración permite crear la configuración del maestro y los forwarders hacia las DNS externas, así los clientes solo necesitarán tener acceso únicamente a nuestro DNS.

En todo momento utilizaré el usuario root.

## **1- Instalar servidor**

Empezamos por instalar el servicio.

```
zypper in bind
```

La configuración la realizamos en el fichero base.

```
vi /etc/named.conf
```

Lo primero que vemos es la carpeta donde se almacenan todas las zonas, esta es _'/var/lib/named'_, al final de la configuración la veremos.

Si queremos que nuestro DNS sea capaz de resolver también las DNS de otros, necesitamos añadir los forwarders, para este ejemplo indico los DNS públicos de Google aunque normalmente recomiendo usar los contratados con el servicio de internet.

Bajando un poco en el fichero, encontraremos los forwarders comentados, añadimos nuestra configuración allí.

```
forwarders {
	8.8.4.4;
	8.8.8.8;
};
forward only;

```

Finalmente hay que definir todas las zonas que queremos, cada zona corresponde a un dominio y rango de IP, si tenemos varios dominios internos creamos tantas zonas como dominios. Además, para cada zona tenemos que crear su versión invertida para poder resolver el dominio a partir de la IP.

En este ejemplo crearé un dominio y su inverso, para ello añado 2 zonas debajo de las zonas actuales.

```
zone "example.intranet" in {
	type master;
	file "example.intranet.zone";
};

zone "5.168.192.in-addr.arpa" in {
	type master;
	file "192.168.5.0.zone";
};

```

Ahora cambiamos a la carpeta donde se almacenan las zonas para crear estas dos nuevas a partir del nombre indicado en _file_. El primero será la DNS normal.

```
cd /var/lib/named
vi example.intranet.zone
```

Para configurar este fichero, hay que tener en cuenta varias cosas:

- Todos los dominios acaban en punto (.) ya que ese es el inicio de todos los dominios.
- En mi caso, he llamado al dominio 'example.intranet', así que habrá que reemplazar este texto por el dominio deseado en todos los casos.
- El servidor encargado del DNS en mi caso es '_ServerDNS_', hay que cambiarlo por el nombre real del servidor (y ese es un nombre falso, espero de todo/a administrador/a que utilice nombres frikys en su red ;D).
- Cada dominio se indica su IP con el comando '_A_', para crear un alias de un dominio hacia otro dominio se utiliza '_CNAME_', aquel que gestiona el DNS se indica con '_NS_'. Existen otros, pero estos son los más utilizados.

```
$TTL 1W
example.intranet.	IN SOA  ServerDNS.example.intranet. root.ServerDNS.example.intranet. (
								42
								2D
								4H
								6W
								1W )

example.intranet.					IN NS			ServerDNS
router.example.intranet.			IN A			192.168.5.1
ServerWeb.example.intranet.			IN A			192.168.5.2
ServerDNS.example.intranet.			IN A			192.168.5.3
blog.exmaple.cat.					IN CNAME		ServerWeb.example.intranet.
```

El siguiente es el DNS invertido.

```
vi 192.168.5.0.zone
```

El DNS invertido no es obligatorio, pero si recomendable, igual que en el anterior los dominios acaban en punto, '_NS_' hace referencia al servidor de DNS y '_PTR_' funciona a la inversa que el '_A_' anterior, indica el dominio según la IP.

En este caso estoy utilizando la IP 192.168.5.X, debido a que es una clase C (255.255.255.0), solo hay que indicar el último valor para cada IP, si tuviera una clase A o B tendría que indicar los dígitos faltantes.

```
$TTL 1W
5.168.192.in-addr.arpa.		IN SOA		example.intranet.	root.example.intranet. (
							42
							2D
							4H
							6W
							1W )

			IN NS		ServerDNS.example.intranet.

1			IN PTR		router.example.intranet.
2			IN PTR		ServerWeb.example.intranet.
3			IN PTR		ServerDNS.example.intranet.
```

Con todo configurado, lo siguiente es abrir el puerto en el firewall (puerto TCP/UDP 53) y reiniciar el servicio.

```
rcnamed restart
```

## **2- Configuración de clientes**

Con el DNS en marcha, falta cambiar los DNS que utiliza cada PC o servidor, esto se puede cambiar en cada servidor o cambiando la configuración del DHCP para que indique nuestra DNS.

En cualquier caso, para cambiar la configuración de un servidor modificamos el fichero de resolución de nombres.

```
vi /etc/resolv.conf
```

En el que indicamos el dominio de búsqueda (o varios de tener más de uno) y la IP del servidor de DNS.

```
search example.intranet
nameserver 192.168.5.3
```

De esta manera, podremos resolver el nombre de un servidor con su nombre completo (ServerWeb.example.intranet) o simplemente con su nombre corto (ServerWeb).

Para confirmar que la configuración funciona, podemos utilizar el comando ping hacia un dominio y ver que IP resuelve o utilizar el comando host y el dominio o IP para comprobar tanto la resolución de dominio como la invertida.

## **Final**

Estos simples pasos son suficientes para tener nuestro propio DNS interno, aun así, cabe recordar que si el servicio deja de funcionar por el motivo que sea y este es el único DNS que disponemos, todas las máquinas que lo utilizan dejaran de poder resolver dominios, esto es importante a la hora de considerar la arquitectura de toda la red, por si queremos que las máquinas tengan también DNS públicos o por si debemos crear DNS esclavos de este por si dejara de funcionar.
