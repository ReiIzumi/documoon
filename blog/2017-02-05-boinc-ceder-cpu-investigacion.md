---
title: Boinc, ceder tu CPU a la investigación
authors: rei.izumi
tags: [Boinc]
---

¿Has pensado alguna vez en el tiempo que tus PCs están encendidas sin realizar nada? Cada día hay millones y millones de PCs encendidas pero que no tienen trabajo a realizar, ya sea porque nadie está utilizando sus servicios o incluso se están utilizando, pero no llegan a consumir apenas procesamiento, servidores web que nadie está consultando o estaciones de trabajo que están encendidas porque se olvidaron de apagarlas, su propietario está haciendo el café o mil opciones más.

Todas estas PCs siguen consumiendo electricidad, dando órdenes a la CPU de espera, quizás el sistema operativo aproveche para organizar algunos datos, crear algunos índices, pero en la mayoría de casos, simplemente estará parada, así que, ¿y si pudiéramos darle una utilidad a nuestras CPUs paradas por el bien común?

<!-- truncate -->

## **¿Qué es Boinc?**

Las investigaciones requieren una cantidad enorme de tiempo de cálculo, para ello se requieren grandes servidores que, en la mayoría de casos, no pueden comprar estos investigadores, incluso alquilarlos puede resultar imposible.

Esto llevó a idear un nuevo sistema, si es imposible conseguir un servidor de estas dimensiones, será más fácil reunir cientos de PCs normales, juntas tendrán la misma potencia, el problema es que el coste de comprar y mantener tampoco lo hacía viable, así que, ¿y utilizar las CPUs de la gente?

La aplicación [Boinc](https://boinc.berkeley.edu/) nació hace muchos años para dar soporte a esta idea, junto al proyecto [SETI@Home](https://setiathome.berkeley.edu/) que mucha gente conoce, pero realmente no sabe que es.

Los investigadores asignan un proyecto a calcular y este lo separa en millones de partes, cada una se envía a los diferentes PCs que están dados de alta en este proyecto para que los procesen y al terminar los devuelven al servidor de los investigadores para recibir nuevos cálculos.

**Para tu PC o servidor, Boinc es una aplicación que se instala como cualquier otra en tu sistema operativo y queda en espera**, cuando la CPU está parada o trabajando a un nivel bajo (según la configuración), empieza a utilizarla para procesar el trozo que le han enviado, al terminar la envía y vuelve a recibir más trabajo. **Boinc únicamente se enciende cuando la CPU está libre y se apaga si la necesitas, jamás accederá a tus datos, así que es totalmente segura y ni notarás que está ahí**.

No necesitas hacer nada una vez está configurado, tampoco verás que está calculando, no tendría sentido de todas formas ya que solo es una parte del conjunto, simplemente tendrás la lista de procesos que debe completar y el porcentaje que llevas.

## **¿Qué gano yo por utilizar Boinc?**

Esta es la gran pregunta que me suelen hacer y la respuesta simple es: nada.

Los diferentes proyectos de Boinc te dan estadísticas generales, estadísticas tuyas, incluso los usuarios se pueden juntar en equipos y "competir" para ver quien consigue más capacidad de cálculo, pero eso es todo. Tú no ganas nada directamente, pero estás ayudando a una comunidad de investigación, quizás gracias a que tú y millones se unieron, el día de mañana, o el siglo que viene, se consiga un resultado. La cura para una enfermedad, encontrar vida extraterrestre, mejorar sistemas de energía renovable o resolver una ecuación matemática que parecía imposible.

¿No lo tienes claro? Recuerda que en cualquier momento lo puedes dejar, simplemente desinstalas la aplicación y ya está.

## **¿Te convence?**

Si has llegado hasta aquí, ¡genial!

En estos pasos explicaré como instalarlo en Linux. Si tienes Windows o Mac puedes seguir el [manual oficial](http://boinc.berkeley.edu/wiki/User_manual), la versión sin Virtual Box es muy sencilla de instalar y todas tienen interfície gráfica.

En cualquier caso, **lo primero es elegir a qué proyecto nos uniremos**, aunque hay muchos proyectos, lo más fácil es unirnos a un proyecto agregado a la web oficial, estos se encuentran [aquí](http://boinc.berkeley.edu/projects.php) y están separados acorde a la categoría y área junto a su descripción, también se indica que sistemas operativos aceptan.

Algunos de estos proyectos quizás no tengan trabajo continuo, así que podemos darnos de alta en más de uno, pero yo aconsejaría empezar por el que más nos guste y si vemos que tienen poca faena, añadir algún otro. En mi caso estoy en [World Community Grid](https://www.worldcommunitygrid.org/) que nunca dejan de tener trabajo y ya han conseguido cerrar varios proyectos a lo largo de los años.

## **0- Previos**

Los pasos que realizaré son para OpenSuSE, pero solo cambia la forma de instalar, Boinc está disponible para ser descargado utilizando el sistema de repositorios propio.

En todo momento se utilizará el usuario root.

## **1- Instalar Boinc**

Lo primero es instalar la aplicación y la interfície gráfica para facilitar la configuración.

```
zypper in boinc-client boinc-manager
```

El instalador también crea el servicio así que podremos iniciar y apagar con **rcboinc-client**, aun así, yo lo configuraré para que arranque automáticamente.

```
systemctl enable boinc-client
```

Todos los datos de Boinc se guardan en la carpeta por defecto: _/var/lib/boinc_

## **2- Configuración de Boinc**

La forma más sencilla de configurar es desde la interfície gráfica, así que todo se realizará desde esta, se puede iniciar con el siguiente comando.

```
boinc-gui
```

Si es la primera vez que lo iniciamos nos pedirá el proyecto en el que participaremos, si es de la lista oficial simplemente elegimos Add project y seleccionamos el nuestro, indicamos el usuario o lo creamos y eso es todo.

Inicialmente hará una comprobación del sistema para hacerse una idea de que CPU tenemos y finalmente se descargará los proyectos y empezará con ello.

En las opciones de Tools podemos configurar el espacio máximo en disco que le cedemos, el porcentaje al que estará libre la CPU para que él se encienda, idioma, datos de proxys, etc.

Si queremos ver más detalles podemos cambiar a la vista avanzada que nos indicará la lista de proyectos que tenemos, los archivos que ha recibido y el porcentaje completado, el tiempo que tardará en procesarlos, el tiempo máximo que los podemos tener hasta que se considere que no llegamos a tiempo o hemos abandonado entre otros tantos datos.

## **Final**

Con estos simples pasos estaremos ayudando a la investigación, quizás nuestra ayuda no sea mucha, pero si cientos o miles también se unen a la causa, conseguiremos resultados que un día se convertirán en algo útil.
