---
id: introduction
title: Introducción
sidebar_position: 1
---

:::note
Aquesta documentació està només disponible en castellà
:::

## Sistema operativo
Durante los años he saltado de una distribución de Linux a otra, incluso he utilizado algunos Windows Server para mi desgracia.

Durante años OpenSuSE fue mi principal, pero cada cambio mayor era un reto, incluso las actualizaciones de sistemas con 4 o 5 años podían ser prácticamente inviables. Así que debía buscar una alternativa.

Al decidirme por un Linux, la parte más importante es la cantidad de documentación y la estabilidad, debido a ello opté por Ubuntu, con su versión Server y LTS, te permite ahorrar memoria y evitar quedar desactualizado fácilmente.

:::tip
Sin importar el tamaño de la red, es recomendable optar por un máximo de 2 sistemas operativos. La complejidad para actualizar, gestionar y monitorizar aumenta demasiado rápido con varios tipos.
:::

Debido a que mi plataforma se conforma de OpenSuSE, Ubuntu, Windows Server y Raspbian, he decidido iniciar un nuevo proyecto de reunificación hacia Ubuntu y con la misma configuración entre todos los servidores, lo que requerirá mucho tiempo, pero hará la vida más sencilla.

## Servidores
Mi plataforma se basa en servidores [bare-metal](https://en.wikipedia.org/wiki/Bare-metal_server), concretamente en 3 tipos:
- Servidor con tecnología x64 con máquinas virtuales gestionadas con VMWare ESXi. Mi licencia es gratuita (Hypervisor) así que no tengo acceso por API ni prácticamente ninguna tecnología de alto nivel que haría mi vida más sencilla, pero por ahora no he encontrado nada tan estable.
- Raspberrys encargadas de los servicios básicos explicados en el [Rasp Project](/docs/rasp/introduction).
- Un NAS de la marca QNAP para copias de seguridad. Ofrece carpetas compartidas vía NFS que los sistemas operativos deben montar.

Los Ubuntus desplegados provendrán de máquinas virtuales de mi servidor.

:::info
Aunque mi red intenta ser independiente, algunos servicios son gratuitos cuando se utilizan con OpenSource. Debido a no tener que mantenerlos he optado por alguno de ellos como [GitLab](https://gitlab.com/ReiIzumi) o [Docker](https://hub.docker.com/u/reiizumi).
:::

## Arquitectura de red
Los sistemas que despliego son para uso doméstico y pueden llegar a ser replicados en una PYME, pero no en una gran empresa, debido a que estas requieren un nivel mayor de control y seguridad.

:::danger
La documentación explicada en esta web no tiene en cuenta temas de seguridad, ni legales. Siempre se basa en una ideología simple y doméstica.

En usos profesionales se deberá contratar a una empresa especialista, corre a riesgo de cada uno los problemas que pueda tener.
:::

Para simplificar, sigo las siguientes reglas:
- Los servidores y servicios se publican en la misma red, siendo las [VPN](/docs/rasp/config/pivpn) y las redes privadas de contenedores excepciones a esta regla.
- El servidor de [Pi Hole](/docs/rasp/config/pihole) se encarga tanto de ofrecer IP vía DHCP como de resolver los nombres de servidores y servicios (todo configurado manualmente).
- Cualquier acceso desde un servidor hacia otro servidor o servicio se realiza mediante nombres de dominio, nunca por IP.
- Los servidores y contenedores tienen acceso a internet.
- Desde internet no se tiene acceso a ningún servicio a no ser que este lo indique.
- Ningún servidor tiene firewall.
- Los servidores son accesibles mediante SSH con claves privadas con un usuario no-root.
- Ningún servidor dedicado a producción o contenedores puede obtener permisos de root, requieren contraseña.

## Tipo de servicios
Divido los servicios en 3 tipos:

- **Servicios básicos**: Estos requieren estar siempre en activo, a ser posible en clúster. Por ejemplo, DHCP o DNS.
- **Servicios no escalables**: Aquellos que no permiten escalar o que lograrlo requiere una complejidad demasiado grande, pero que al mismo tiempo requieren acceso a disco a alta velocidad. Por ejemplo, PostgreSQL o MariaDB.
- **Servicios escalables**: Cualquier servicio que no esté en las categorías anteriores.

:::tip
Los servicios básicos son desplegados en las Raspberry, los no escalables en máquinas virtuales y los escalables en contenedores.
:::