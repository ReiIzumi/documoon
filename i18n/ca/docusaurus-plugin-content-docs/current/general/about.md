---
id: about
title: Sobre mi
sidebar_position: 1
---

:::tip
Quan em vaig adonar ja estava en mig d'una guerra sense esperança, perdre era sempre el camí per seguir, però abandonar no era una opció.
Un món rodejat de computadores, servidors i altres enemics del ser humà.
:::

## ¿Sobre mi?
La millor forma de descriure'm es unint `freak-geek-otaku`. Ràpid, fàcil i per a tota la família ... o potser no, ja que faltaria entendre què significa cada una d'aquestes, i igual creus que ho saps, però la societat ha distorsionat tant els seus veritables significats que ja no s'assemblen.

I tampoc vull dir que faci servir els seus modes originals, però vaja, serveix per fer-se una idea.

## ¿Por a que escrit aquesta web?
Quan inverteixes hores i hores buscant, provant, configurant, esborrant, pensant en l'angle de llançament millor per destruir la teva servidor al ser llençada per la finestra ... acabes pensant: "ey, i si ho documento tot?".
¡I aquí estem!

I, per cert, vaig calcular el vector de llançament, però després vaig pensar que si la llancés per la finestra, segurament saltaria darrere per salvar-la, ¡no té sentit! :confounded:

## ¿Mes coses?
:house: Vaig néixer i visc a les rodalies de Barcelona, ciutat a la que odio i m'encanta al mateix temps. Tot i que mai hi viuria.

:school: Als 12 ens vam fer omplir què 3 oficis voldríem fer al futur. Com es normal la majoria va mirar la fulla com lo que realment era: **un despropòsit**. Als 12 tens que decidir a què et dedicaràs? Tens problemes més importants a aconseguir durant la setmana.

A mi em van fotre la bronca per ficar 3 cos informàtica. Ho tenia clar. Tampoc vaig entendre perquè jo ho vaig fer malament i necessitava "ajuda", la resta ni tan sols havien escollit res. No serien ells qui necessitaven la ajuda?

Amb això en ment em vaig treure els dos mòduls superiors de la època (això ha sonat a vell :anguished:), i em vaig llençar al ~~gloriós~~ món laboral on vaig aprendre tècniques de l'ofici com el "salt al precipici mentre reses per a que aquest cop no tingui lava." **Spoiler**: tenia lava :wink:

Mentre treballava vaig pensar "ey, ara que cobro una merda i a dures penes tinc temps, perquè no aprofito per treure'm la carrera a distancia?. Sí, jo tampoc ho entenc, però així va passar.

Em vaig unir a la Universitat Oberta de Catalunya (UOC per amics i enemics) per millorar en els meus estudis, la crua realitat es que no aprenia res. Soc una persona autodidacta, la universitat era un llast pel meu temps i cartera.

I parlant d'això, el nostre sistema educatiu es un error que només va a pitjor. La educació no es basa en descarregar la Santa Wikipedia dins el teu cervell. No, de debò, la tens al mòbil, per a què la vols al teu cap? ni que els teres d'espai fossin gratuits!!.

:::info
L'únic que necessites per sobreviure es aprendre a desafiar als reptes, aprendre a superar-los i, encara més important, a tornar a aixecar-te després de cada pallissa rebuda.
:::

M'apassiona l'ensenyament, però no donar dades tècniques, si no iniciar les flames de la passió per l'ofici. Ni importa quants projectes acabis amb èxit a la teva vida, el moment en que aconsegueixes despertar la il·lusió per l'ofici en una persona ho supera tot. En aquest moment saps que has aconseguir un èxit a la teva vida.

:raising_hand: Soc una persona amb idees clares, potser d'extrems, però crec que tot es té que poder dialogar per arribar a la conclusió més correcte. No m'agraden les coses que son així perquè si, ni deixar les coses a mitges, ni deixar penjada a la gent, ni abandonar res que hagués iniciat.

Tot es planifica, s'inicia, es revisa i es dona per conclòs per anar a per la següent tasca. Sempre donant el millor de tu.

## Altres coses
La informàtica es tant un treball com un hobby (compte, la informàtica, no treballar, cal evitar confusions). Però no es la única:
- La filosofia podria haver sigut el meu ofici, però vaig concloure que seria millor deixar-la com a hobby.
- La cultura japonesa es part del meu món, la meva ètica, i tota la meva ideologia. Per a bé o dolent.
- El rol es com una segona vida (o tercera, segons quan trigui el maleït goblin a apunyalar-te per l'espatlla).
- La lectura (i escriptura) m'han acompanyat tota la vida. Principalment la fantasia i pràcticament qualsevol estil de manga.
- La pintura de miniatures (coneixes Warhammer= doncs va ser el culpable del meu inici). Sempre m'ha apassionat l'art i m'agradava dibuixar, fins que no dibuixes un quadre que estàs mirant no arribes a entendre tots els seus detalls, la seva complexitat ... Tot i així, mai va arribar a res, fins que em van mostrar aquest joc. I la meva cartera se'n va anar en orris :cry:.
- :guitar: La meva guitarra elèctrica, **Yui**, es un hobby peculiar. Jo intento controlar-la, ella em mira malament, jo li dic que ho aconseguiré!, ella treu la carta magna, jo perdo. Potser algun dia guanyaré aquesta àrdua batalla. **Spoiler**: ella sap que no.

Contant que el treball es porta gran part de la nostre vida (més que dormir si ho mires!), els hobbies i el treball acaben estant a una balança per mantenir un dur equilibri. Ningú va dir que fos fàcil.

:airplane: Viatjar no m'apassiona, però he estat varis cops al Japó i es possible que torni uns quants cops. Un dia em van deixar llegir un manga, havia llegit molts (però que molts) llibres, i alguns còmics, però alguna cosa va passar allà. Des de aleshores la cultura japonesa sempre ha estat al meu costat, i viatjar al pais del sol naixent era bàsic.

La resta del món? Sent realista, no em criden, no m'interessen, però no tindria problemes en veure'ls.

I sense més dilació, aquest punt acaba aquí.

:::tip
Rei Izumi < rei.izumi arrova moon punt cat >
:::