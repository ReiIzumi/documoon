# documoon-chart



Moon.cat website made with the Docusaurus technology

![Version: 1.19.0](https://img.shields.io/badge/Version-1.19.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.27.0](https://img.shields.io/badge/AppVersion-1.27.0-informational?style=flat-square) 

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm upgrade --install my-release documoon-chart -f documoon-chart-values.yaml -n documoon
```

This `documoon-chart-values.yaml` has the most typical configurations:
```yaml
ingress:
  enabled: true
  url: documoon.domain.cat
  tls: true
autoscaling:
  enabled: true
```

## Uninstalling the Chart
To uninstall/delete the my-release deployment:

```console
$ helm delete my-release -n documoon
```

This will delete all components.



## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| image.repository | string | `"reiizumi/documoon"` | Name of the image |
| image.pullPolicy | string | `"IfNotPresent"` | Download the image only if not exist (IfNotPresent) or always (Always) |
| image.tag | string | `""` | Overrides the image tag defined in the Chart |
| nameOverride | string | `""` |  |
| fullnameOverride | string | `""` |  |
| imagePullSecrets | list | `[]` |  |
| service.type | string | `"ClusterIP"` |  |
| service.port | int | `80` |  |
| ingress.enabled | bool | `false` | Enable Ingress |
| ingress.url | string | `"documoon.domain.cat"` | URL to publish the service |
| ingress.className | string | `""` | Ingress class name. Empty to use the default one |
| ingress.annotations | object | `{"cert-manager.io/cluster-issuer":"letsencrypt-http"}` | Annotations to configure the Ingress |
| ingress.tls | bool | `false` | Enable the TLS configuration |
| ingress.path | string | `"/"` | Limit access to the endpoints according to a base path |
| ingress.pathType | string | `"ImplementationSpecific"` | Type of path route |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| securityContext | object | `{}` |  |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.automount | bool | `true` | Automatically mount a ServiceAccount's API credentials? |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| nodeSelector | object | `{}` |  |
| tolerations | list | `[]` |  |
| affinity | object | `{}` |  |
| replicaCount | int | `1` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.maxReplicas | int | `5` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` |  |
| resources | object | `{"limits":{"cpu":0.1,"memory":"64Mi"},"requests":{"cpu":0.02,"memory":"10Mi"}}` | CPU and memory required and limits for the service |
| livenessProbe.httpGet.path | string | `"/"` |  |
| livenessProbe.httpGet.port | string | `"http"` |  |
| readinessProbe.httpGet.path | string | `"/"` |  |
| readinessProbe.httpGet.port | string | `"http"` |  |
| volumes | list | `[]` | Additional volumes on the output Deployment definition. |
| volumeMounts | list | `[]` | Additional volumeMounts on the output Deployment definition. |
| extraEnvs | object | `{}` | Add extra environment variables |
