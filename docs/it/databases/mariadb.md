---
id: mariadb
title: MariaDB
sidebar_position: 2
---

Algunos servicios como WordPress no aceptan PostgreSQL de forma nativa, así que como segunda base de datos desplegaré un [MariaDB](https://mariadb.org/). Al igual que PostgreSQL, es OpenSource y ha liderado el mercado en aplicaciones pequeñas durante años.

Debido a que no espero que ninguna tenga un gran volumen de datos ni carga de trabajo, convivirán en el mismo servidor.

## Instalación
Instalamos MariaDB y ejecutamos el proceso de configuración inicial.

```bash
sudo apt update
sudo apt install mariadb-server -y
sudo mysql_secure_installation
```

El acceso `root` será únicamente usable en local, así que el sistema ya se encarga de protegerlo. También eliminamos la base de datos de pruebas.

Estos son los campos indicados:
```
Enter current password for root (enter for none):
Switch to unix_socket authentication [Y/n] n
Change the root password? [Y/n] n
Remove anonymous users? [Y/n] y
Disallow root login remotely? [Y/n] y
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
```

### Acceso remoto
Al igual que PostgreSQL, MariaDB es únicamente accesible desde `localhost`, así que habrá que modificar la configuración.

```bash
sudo vi /etc/mysql/mariadb.conf.d/50-server.cnf
```

Cambiando el `bind` para aceptar cualquier IP.

```vim title="/etc/mysql/mariadb.conf.d/50-server.cnf"
bind-address            = 0.0.0.0
```

Tras reiniciar ya será accesible
```bash
sudo systemctl restart mariadb
```

## Crear bases de datos
Accedemos a la consola de MariaDB.
```bash
sudo mariadb
```

Desde ella se crea la base de datos, usuario con contraseña, y se asigna los permisos.

Este es un ejemplo dando todos los permisos a un usuario que puede acceder desde local y desde la red `192.168.1.X`, pero no sobre otras.
```sql
CREATE DATABASE database_name;
CREATE USER 'user_name'@'localhost' IDENTIFIED BY 'my_password';
CREATE USER 'user_name'@'192.168.1.%' IDENTIFIED BY 'my_password';
GRANT ALL PRIVILEGES ON database_name.* TO 'user_name'@'localhost';
GRANT ALL PRIVILEGES ON database_name.* TO 'user_name'@'192.168.1.%';
FLUSH PRIVILEGES;
```

## Copias de seguridad
Siguiendo el sistema definido anteriormente, se utilizará el sistema de notificaciones de Nagios previamente preparado en [Ubuntu](/docs/it/ubuntu22lts#copias-de-seguridad).

Para este hay que descargar el script para PostgreSQL que utiliza `pg_dump` para crear las copias de seguridad.
```bash
cd /opt/scripts
sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/backup_MariaDB.sh
sudo chmod 744 backup_MariaDB.sh
```

Si no lo hemos instalado para PostgreSQL, necesitaremos instalar `nc` ya que es una dependencia del script para confirmar que el puerto está habilitado.
```bash
sudo apt install netcat-traditional -y
```

Editamos el fichero.
```bash
sudo vi backup_MariaDB.sh
```

Actualizamos la lista de bases de datos.
```vim title="backup_MariaDB.sh"
MARIADB_DATABASES="database1 database2"
```

Es recomendable ejecutar una copia para confirmar que todo funciona.
```bash
sudo ./backup_MariaDB.sh
```

### Cron
Si no lo habíamos instalado previamente, instalamos `cron`.
```bash
sudo apt install cron -y
sudo crontab -e
```

Este ejemplo ejecutará la copia de seguridad cada día a las 10 de la mañana.
```vim
00 10 * * * /opt/scripts/backup_MariaDB.sh
```

:::tip
Es recomendable que cada servidor y servicio tengan unas horas diferentes para las copias de seguridad, lo que evitará una sobrecarga en el servidor que contiene las copias de seguridad y en la red.
:::

### Nagios
Por último, añadimos MariaDB al monitor de este servidor y configuramos la hora y tiempos de espera para las copias de seguridad.

```vim
define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     MariaDB
        check_command           check_tcp!3306
        servicegroups           DB
}

define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     Backup MariaDB
        check_command           check_nrpe_bkp!MariaDB 10:00:00 5 10
        servicegroups           Backup
        max_check_attempts      1
}
```
