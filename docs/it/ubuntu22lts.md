---
id: ubuntu22lts
title: Ubuntu Server 22.04.1 LTS
sidebar_position: 3
---

Los servicios documentados dan por hecho que se dispone de un [Ubuntu Server 22.04.1 LTS](https://ubuntu.com/download/server), en el que se han seguido estos pasos para su instalación y configuración.

:::caution
Distribuciones y versiones diferentes pueden hacer que los comandos explicados sean diferentes.
:::

La instalación la realizo mediante la ISO montada en una máquina virtual.

## Instalación
La instalación se realiza desde la consola de VMWare ESXi que permite acceder a la máquina virtual remotamente.

### Idioma
Siempre mantener el idioma `English`. No tiene sentido traducirlo, únicamente complica la búsqueda de documentación si tenemos algún error.

### Teclado
Los servidores son accesibles mediante SSH el cual absorbe el idioma del cliente, pero cambiarlo a `Spanish` hace más sencillo si requerimos conectar por consola en algún momento.

### Tipo de instalación
Normalmente los servidores no requieren de acceso de usuarios una vez están funcionando, así que es preferible elegir la versión minimizada e instalar únicamente las piezas que necesitamos.

### Network connection
Obtiene su nombre, IP y DNS mediante DHCP.

:::caution
Estos datos deben ser configurados previamente en [Pi Hole](/docs/rasp/config/pihole).
:::

### Proxy
Una red pequeña no suele disponer de proxy.

### Mirror address
Por defecto los servidores utilizan el repositorio oficial para descargar las aplicaciones.

:::info
Tener un repositorio espejo únicamente es necesario en redes grandes y unificadas, en redes donde los servidores no tienen internet y/o si se requiere de un control total sobre qué se puede instalar.
:::

### Storage
Para los discos y particiones sigo estas reglas:
- Deshabilito LVM ya que no suelo depender de ello al disponer de máquinas virtuales.
- Dispongo de un único disco que contiene todas las particiones necesarias.
- Si una aplicación requiere una partición por requisito, este está en un segundo disco de la máquina virtual.
- No utilizo partición de SWAP.

Ubuntu sin LVM crea por defecto unas particiones útiles como las siguientes:

| Tipo      | Montaje | Espacio |
| --------- | :-----: | ------: |
| bios_grub | -       | 1M      |
| ext4      | /       | 50G     |


### Hostname y usuario
Indicamos el nombre del servidor y el usuario que tendrá acceso.

### SSH Setup
Habilitar el acceso SSH para conexión remota.

### Server snaps
Esto permite preinstalar servicios. Aunque necesitara alguno, prefiero hacerlo una vez toda la base esté configurada.

### Finalizando instalación
Si dispone de internet, al acabar la instalación aprovechará para actualizar, lo que es aconsejable para evitar problemas de seguridad.

:::danger
Con máquinas virtuales, el sistema intenta expulsar sin éxito la ISO, con lo que no reinicia automáticamente al terminar. Requiere que el usuario acepte.
:::

## Configuración
Una vez está instalado, se accede mediante SSH con el nombre de dominio, el usuario y la contraseña temporalmente.

### Editor de texto
En la versión minimalista, no disponemos de un editor, pero nos facilita enormemente la vida.

Yo utilizo `vim`, pero cada persona deberá instalar el que se adapte mejor a su experiencia.

```bash
sudo apt update
sudo apt install vim -y
```

### Claves de acceso
El acceso a los servidores se debe realizar mediante claves privadas/públicas, nunca permitir el uso de contraseñas. Estas pueden ser creadas desde diversas aplicaciones como [PuTTYgen](https://www.puttygen.com) en caso de utilizar Windows y PuTTY. Desde Linux o MacOS habrá que elegir otras opciones.

Existen varios tipos de encriptación, pero la más recomendada en el momento de escribir este artículo es la `Ed25519`.

Una vez generada, deberemos utilizar la clave pública en el servidor.
```bash
vi ~/.ssh/authorized_keys
```

La clave privada será la utilizada para acceder.

### SSH
Editamos la configuración.
```bash
sudo vi /etc/ssh/sshd_config
```

Descomentamos el Banner y anulamos el acceso con contraseña.
```vim title="/etc/ssh/sshd_config"
Banner /etc/ssh/banner
PasswordAuthentication no
```

Creamos el banner.
```bash
sudo vi /etc/ssh/banner
```

Añadimos un mensaje de bienvenida.

```vim title="/etc/ssh/banner"
========== ========== ==========
   Welcome to <serverName>
========== ========== ==========
You are accessing a private server
Unauthorized access is not allowed
```

:::danger
Según la ley, si una puerta o servicio no indica que es de acceso restringido, entonces se puede entrar, para ello el banner.
:::

Reiniciamos el servicio para aplicar los cambios.
```bash
sudo systemctl restart ssh
```

:::caution
Es recomendable comprobar que podemos acceder con nuestra clave privada antes de cerrar la consola, ya que, si ha habido algún error y la hemos cerrado, no podremos volver a acceder para arreglarlo.
:::

### Asistente de reinicio
Esta versión de Ubuntu incorpora el servicio `needrestart`, el cual informa de qué servicios tienen pendiente reiniciarse, normalmente tras una actualización.

En mi caso prefiero que todos los servicios que requieran reiniciarse lo hagan inmediatamente y sin pedir confirmación, así que edito los valores por defecto.

```bash
sudo vi /etc/needrestart/needrestart.conf
```

Si un servicio requiere reiniciar, lo hará automáticamente.
```vim title="/etc/needrestart/needrestart.conf"
$nrconf{restart} = 'a';
```

### Actualizar
Si en la instalación no hemos actualizado, seguramente quedarán muchos componentes por actualizar. En cualquier caso, es recomendable confirmar que el sistema está actualizado.

```bash
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
```

### Alertas sobre discos
Los discos generados por VMWare ESXi no tienen UUID propio, lo que genera logs de aviso en Ubuntu. Esto se puede deshabilitar.

```bash
sudo vi /etc/multipath.conf
```

Esta lista descarta varios tipos de nombre de discos, así que podría variar según la que se disponga.
```vim title="/etc/multipath.conf"
blacklist {
    devnode "^(ram|raw|loop|fd|md|dm-|sr|scd|st|sda|sdb|sdc)[0-9]*"
}
```

### NTP
Los servidores de una red deberían sincronizarse utilizando el mismo servidor para evitar problemas. Si disponemos de un servidor como del [Rasp Project](/docs/rasp/config/ntp), se debería utilizar.

Únicamente necesitamos hacer de cliente, así que podemos optar por la versión minimalista preinstalada en Ubuntu.

```bash
sudo vi /etc/systemd/timesyncd.conf
```

Indicar el servidor que ofrece este servicio. Si disponemos de un segundo, quien tenga más trabajo debido a otros servicios deberá ser el `Fallback`.
```vim title="/etc/systemd/timesyncd.conf"
NTP=ntp1.domain.intranet
FallbackNTP=ntp2.domain.intranet
```

:::tip
Este servicio no permite configurar más de un NTP como sí hace el servicio original. En caso de requerir de 2 o más servidores configurados, deberemos optar por `ntp`.
:::

Tras reiniciar se sincronizará, es recomendable confirmar que así es.
```bash
sudo systemctl restart systemd-timesyncd
timedatectl timesync-status
```

### Copias de seguridad
Todo servicio debe tener copias de seguridad, estas se realizan mediante scripts y se almacenan en una carpeta compartida mediante `NFS`. Así que hay que instalar ese servicio.

```bash
sudo apt install nfs-common -y
```

[Anteriormente](/docs/swarm/backup) ya escribí sobre el proceso que sigo para las copias de seguridad, así que en esta sección me limitaré a los comandos a ejecutar para descargar, preparar y confirmar que todo funciona.

Lo primero es descargar los scripts necesarios en la carpeta correspondiente.
```bash
sudo mkdir -p /opt/scripts
cd /opt/scripts/
sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/mount.sh
sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/rotateFiles.sh
sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/notifyCSV.sh
sudo wget https://gitlab.com/ReiIzumi/scripts-backup/-/raw/master/notificationChecker.pl
sudo chmod 744 *
sudo chmod 745 notificationChecker.pl
```

Si mantenemos las carpetas por defecto, solo hará falta modificar un fichero.
```bash
sudo vi mount.sh
```

Indicar el servidor de NFS y la carpeta
```vim title="mount.sh"
NFS_SERVER="nas.domain.intranet"
NFS_FOLDER="/BKP_Test"
```

Lo siguiente será probar cada script, para dejar el entorno listo y confirmar que todo funciona según lo esperado.
```bash
sudo ./mount.sh
```

Creará y montará la careta compartida
```
Creating mount folder
Mounting shared folder
```

El notificador requiere librerías extras de perl.
```bash
sudo apt install libdatetime-perl libtemplate-plugin-datetime-perl -y
sudo ./notificationChecker.pl
```

Tras iniciarlo sin argumentos, mostrará el siguiente mensaje.
```
UNKNOWN - Arguments are missing. Required: 'Identifier Name' 'Start Time' 'Minutes for warning' 'Minutes for critical'
```

:::tip
Además de estos hará falta incluir los ficheros que realizarán la copia de seguridad, pero estos varían según los servicios desplegados, así que serán añadidos cuando sea necesario.
:::

### Nagios
Las partes críticas de cada servidor serán monitorizadas por el Nagios desplegado en el [Rasp Project](/docs/rasp/config/nagios).

Para ello debemos instalar y configurar el NRPE de Nagios.
```bash
sudo apt install nagios-nrpe-server nagios-nrpe-plugin -y
sudo vi /etc/nagios/nrpe.cfg
```

Permitimos su uso al servidor de Nagios, aceptamos el uso de argumentos para el notificador que acabamos de desplegar y añadimos los servicios básicos con algunos cambios en el nivel de alerta.
```vim title="/etc/nagios/nrpe.cfg"
allowed_hosts=127.0.0.1,::1,nagios.domain.intranet
dont_blame_nrpe=1

command[check_users]=/usr/lib/nagios/plugins/check_users -w 5 -c 10
command[check_load]=/usr/lib/nagios/plugins/check_load -r -w .80,.75,.70 -c .90,.85,.80
command[check_disk_root]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
command[check_zombie_procs]=/usr/lib/nagios/plugins/check_procs -w 5 -c 10 -s Z
command[check_total_procs]=/usr/lib/nagios/plugins/check_procs -w 400 -c 600
command[check_bkp]=/opt/scripts/notificationChecker.pl $ARG1$
```

Aplicamos los cambios.
```bash
sudo systemctl restart nagios-nrpe-server
```

:::tip
Después de configurar Nagios, deberemos añadirlo al servidor y revisar que todos los servicios se detectan correctamente.
:::

Si disponemos de un Nagios Server como el explicado para el [Rasp Project](/docs/rasp/config/nagios), habrá que crear un nuevo fichero para el servidor.

```bash
sudo vi /usr/local/nagios/etc/servers/Ubuntu1.cfg
```

En este se definen los servicios básicos.
```vim title="/usr/local/nagios/etc/servers/Ubuntu1.cfg"
define host {
        use                     linux-server
        host_name               Ubuntu1
        alias                   Ubuntu1
        address                 Ubuntu1.domain.intranet
        hostgroups              PROD
}

define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     Hard disk - Root
        check_command           check_nrpe_disk_root
        servicegroups           HDD
}

define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     Current Users
        check_command           check_nrpe_users
}
define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     Total Processes
        check_command           check_nrpe_total_procs
}

define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     Zombie Processes
        check_command           check_nrpe_zombie_procs
}

define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     Current Load
        check_command           check_nrpe_load
}
define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     PING
        check_command           check_ping!100.0,20%!500.0,60%
}

define service {
        use                     local-service
        host_name               Ubuntu1
        service_description     SSH
        check_command           check_ssh
}
```

Tras reiniciar el sistema empezará a escanear todos los servicios.
```bash
sudo systemctl restart nagios
```
