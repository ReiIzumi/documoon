---
sidebar_position: 99
---

# Historial

## Versión

### 2024/08/15
* Métricas en Proxmox VE

### 2024/06/04
* Promox VE

### 2022/09/07
* Instalación de PostgreSQL
* Instalación de MariaDB

### 2022/09/06
Nueva sección de IT:
* Introducción
* Instalación de Ubuntu Server LTS

