# Proxmox VE
Después de tantos años utilizando VMWare ESXi Hypervisor, nos vemos "en la calle" tras la eliminación de su versión gratuita y el aumento de precios. Siendo así, y para un uso doméstico, moverse hacia [Promox VE](https://www.proxmox.com/en/) era la decisión lógica.

Promox VE en su versión gratuita aporta más funcionalidades que la gratuita de VMWare ESXi. Por si fuera poco, tiene resuelto de serie las copias de seguridad.

En general tiene algunos detalles a tener en cuenta:
- Se basa en Debian, así que podemos "tocar" la base para hacer reajustes.
- Tiene una UI con bastantes opciones, pero ojo, toca tirar de comandos.
- En VMWare necesitas un servidor para controlar el clúster (únicamente en la versión de pago). Proxmox VE permite usar la UI de cualquier nodo para controlarlos a todos. Nada de anillo único, ¡todos controlan a todos!
- Las máquinas virtuales (VM) pueden utilizar cloud-init, aunque la versión nativa es un tanto limitada y requiere ser extendida.
- Existe un [provider](https://registry.terraform.io/providers/bpg/proxmox/latest) de Terraform, que además permite extender el cloud-init.

## Instalación
La [página oficial](https://pve.proxmox.com/wiki/Prepare_Installation_Media) tiene el enlace para descargar la ISO y diferentes opciones para crear un pendrive. En mi caso, usé [Balena Etcher](https://etcher.balena.io/) en Windows.

:::warning
Proxmox VE requiere que la CPU tenga la virtualización activa, las BIOS suelen tenerlo apagado de serie.
:::

El asistente no tiene pérdida:
- Aceptar licencia.
- En mi caso cambio la partición a `ZFS` (Raid0 si solo tienes 1 disco).
- Seleccionar país para timezone y teclado.
- Contraseña de root y e-mail.
- Definir datos de red.

:::tip
Entre otros beneficios, la partición `ZFS` permite crear snapshots sin duplicar el disco.
:::

Al terminar se podrá acceder mediante la UI.
```
https://serverName:8006
```

:::note
Recordar mantener el `realm` en `linux pam` para poder acceder con el usuario `root` creado en la instalación.
:::

## Configuración
Algunas partes de la configuración y uso se realizan desde la UI y otras vía SSH. En cualquier caso, es posible acceder a la Shell de cada nodo desde la UI.

### Configuración básica
Lo más sencillo es lanzar un script que ajusta repositorios, anula el aviso de suscripción y actualiza el sistema.

```bash
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/post-pve-install.sh)"
```

Las opciones para elegir variarán según la utilidad que le demos. Estas activan todas las funcionalidades gratuitas:
```
 ✓ Corrected Proxmox VE Sources
 ✓ Disabled 'pve-enterprise' repository
 ✓ Enabled 'pve-no-subscription' repository
 ✓ Corrected 'ceph package repositories'
 ✗ Selected no to Adding 'pvetest' repository
 ✓ Disabled subscription nag (Delete browser cache)
 ✗ Selected no to Disabling high availability
 ✓ Updated Proxmox VE
 ✓ Completed Post Install Routines
```

### Copias de seguridad
Por defecto, el disco `local` permite almacenar copias de seguridad, lo que es una muy mala idea.

La opción realista es crear una carpeta compartida por `NFS` para almacenar las copias de seguridad e imágenes. También se puede utilizar para snippets.

Acceder a:
```
Datacenter > Storage - Add - NFS
```
Y definir la carpeta compartida:

| Key     | Value                                   |
| ------- | --------------------------------------- |
| ID      | local name                              |
| Server  | server name                             |
| Export  | /NFS_Folder                             |
| Content | ISO Image, VZDump backup file, Snippets |

Antes de nada hay que darle una vuelta por los `pools`:
```
Datacenter > Permissions - Pools
```
Al definir una VM se le puede asignar a qué pool pertenece, por ejemplo, para indicar el tipo de entorno (producción, test, ...). Estos pools también se pueden asignar a una configuración de copia de seguridad, permitiendo que estas afecten a las VMs de ese pool, lo que nos evita tener que ajustar la configuración cada vez que creamos o eliminamos una VM.

```
Datacenter > Backup - Add
```

La configuración dependerá de cuándo y cómo queremos crear copias de seguridad, siendo importante acordarse de asignar la carpeta compartida como destino.

### Certificado SSL
La UI utiliza HTTPS con un certificado auto firmado que puede ser modificando mediante un ACME (como Let's Encrypt) o simplemente subiendo un certificado propio de nuestra intranet.
```
Nodes - <Nodename> - System - Certificates - Upload Custom Certificate
```

### Clúster
Para conectar dos o más Proxmox VE, es tan fácil como definir uno como principal, extraer el código y utilizarlo en el resto para que se conecten

Crear un nuevo clúster.
```
Datacenter - Cluster > Create Cluster
```

Tras ello extraer el código.
```
Join Information
```

En el nuevo nodo ir a la misma sección para unir al clúster indicando el código y la contraseña del nodo maestro.
```
Datacenter - Cluster > Join Cluster
```

El nuevo nodo absorverá muchas de las secciones del maestro:
- Resource Pool
- Storages via NFS

### Editor de texto
Como fan de vim, no podía faltar el editor.
```bash
apt install vim -y
vi /usr/share/vim/vim90/defaults.vim
```

Y la capacidad para pegar con el clic derecho añadiendo el siguiente al final:
```vim
set mouse=r
```

## Comandos
### Forzar apagado
La gestión de las VMs requiere que tengan el agente instalado, algo que no hacen por defecto. Sin el agente, varias funcionalidades como *Reboot* o *Shutdown* no funcionan y requieren usar las funciones de *Reset* o *Stop*, entre otros problemas. Así que es aconsejable que siempre tengan el agente instalado. 

Por desgracia, algunas veces una VM puede quedarse bloqueada, sin opción a hacer nada mediante la UI, tenga o no el agente.

Vía SSH podemos mostrar todas las VM junto a su ID
```bash
cat /etc/pve/.vmlist
```

En condiciones normales podremos desbloquearla y parar, pero es posible que ambos fallen.
```bash
qm unlock VMID
qm stop VMID
```

Si no ha funcionado, habrá que encontrar el `PID`, matar la aplicación y apagarlo ya que tiende a hacer un autoarranque.
```bash
ps aux | grep "/usr/bin/kvm -id VMID"
kill -9 PID
qm stop VMID
```
