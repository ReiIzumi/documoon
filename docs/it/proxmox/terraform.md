---
sidebar_position: 1
---

# Terraform
La configuración de Terraform para el [provider de bpg](https://registry.terraform.io/providers/bpg/proxmox/0.57.1) requiere un token y conexión vía SSH para usar todas las opciones.

## Usuario + Token
Acceder por SSH a un nodo para crear el usuario `terraform` y asignar roles.
```bash
pveum user add terraform@pve
pveum role add Terraform -privs "Datastore.Allocate Datastore.AllocateSpace Datastore.AllocateTemplate Datastore.Audit Pool.Allocate Sys.Audit Sys.Console Sys.Modify SDN.Use VM.Allocate VM.Audit VM.Clone VM.Config.CDROM VM.Config.Cloudinit VM.Config.CPU VM.Config.Disk VM.Config.HWType VM.Config.Memory VM.Config.Network VM.Config.Options VM.Migrate VM.Monitor VM.PowerMgmt User.Modify"
pveum aclmod / -user terraform@pve -role Terraform
pveum user token add terraform@pve provider --privsep=0
```

Debemos apuntar el token y añadirlo al prefijo según el formato: `terraform@pve!provider=00000000-0000-0000-0000-000000000000`

## Usuario SSH
Definir el usuario en cada nodo.

```bash
apt update
apt install sudo
sudo useradd -m terraform
sudo visudo
```

Añadir las siguientes líneas al final:
```vim
terraform ALL=(root) NOPASSWD: /sbin/pvesm
terraform ALL=(root) NOPASSWD: /sbin/qm
terraform ALL=(root) NOPASSWD: /usr/bin/tee
terraform ALL=(root) NOPASSWD: /var/lib/vz/*
```

Crear un `public key` y añadirlo a la configuración.
```bash
mkdir /home/terraform/.ssh
vi /home/terraform/.ssh/authorized_keys
```

## Provider
Tras ello, configuramos el provider.

```vim title="provider.tf"
terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
    }
  }
}

provider "proxmox" {
  endpoint = var.proxmox_endpoint.endpoint
  insecure = var.proxmox_endpoint.insecure

  api_token = var.proxmox_auth.api_token
  ssh {
    agent       = true
    username    = var.proxmox_auth.username
    private_key = fileexists(var.proxmox_auth.private_key) ? file(var.proxmox_auth.private_key) : var.proxmox_auth.private_key
  }
}
```

Las variables.
```vim title="variables.tf
variable "proxmox_endpoint" {
  description = "Proxmox endpoint"
  type = object({
    endpoint = string
    insecure = bool
  })
}

variable "proxmox_auth" {
  description = "Proxmox authentication"
  type = object({
    api_token   = string
    username    = string
    private_key = string
  })
  sensitive = true
}
```

Este es un ejemplo del fichero de valores donde el fichero con la **private key** se encuentra en la misma carpeta.
```vim title="terraform.tfvars"
proxmox_endpoint = {
  endpoint = "https://serverName:8006/"
  insecure = true
}

proxmox_auth = {
  api_token   = "terraform@pve!provider=00000000-0000-0000-0000-000000000000"
  username    = "terraform"
  private_key = "proxmox.ppk"
}

```