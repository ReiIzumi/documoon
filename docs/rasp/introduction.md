---
id: introduction
title: Introducción
sidebar_position: 1
---

## Rasp Project
Empecemos por el inicio, ¿qué es el **`Rasp Project`**?

Este proyecto se basa en crear un clúster de servidores donde los servicios están replicados y sincronizados, si uno de estos servidores cae (o simplemente está en mantenimiento), el resto seguirá funcionando, lo que evitará hasta cierto punto la caída o cortes de servicios fundamentales.

Entonces, ¡vamos a desplegar un Kubernetes! pues no.

Los sistemas pequeños de Kubernetes utilizan entre 2 y 3 servidores, si uno de ellos cae, transfiere los contenedores a otro nodo, incluso se pueden desplegar varias réplicas de este para evitar cortes del servicio, pero si solo tienes un maestro y cae, el resto de los nodos se convierten en pisapapeles.

Debido a que los servicios de este proyecto son esenciales y muy pequeños, he preferido mantener la idea lo más simple posible.

## Especificaciones
La sección de construcción detalla mejor todo lo utilizado para este proyecto, pero es preferible tener una ligera idea antes de continuar.

Se utilizan únicamente 2 [Raspberry](https://www.raspberrypi.org), concretamente son [Raspberry Pi 4B](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/) de 4 GiB de RAM. **Este proyecto puede funcionar con una más antigua**, no se va a aprovechar apenas su verdadero potencial. En mi caso ya disponía de una para hacer pruebas, y cuento incrementar los servicios de este proyecto más adelante.

Debido a que se utilizaran como servidores **el sistema operativo tiene que ser estable, consumir lo mínimo posible y no tener pantalla gráfica**, por ello me he decantado por [Raspberry Pi OS Lite](https://www.raspberrypi.org/software/operating-systems/#raspberry-pi-os-32-bit), el sistema operativo original de las Raspberry y basado en Debian. Mucha gente está recomendando Ubuntu debido a su gran extensión y los 64 bits, pero en el momento de escribir este artículo dispongo de la 20.04 LTS que ha demostrado no ser tan estable como la Raspbian actual y, seamos sinceros, no necesitamos 64 bits para los servicios que serán desplegados.

## Servicios
Esta es la lista de servicios que se desplegarán:
* DNS
* DHCP
* VPN
* NTP
* Nagios
* LDAP

Algunos de estos servicios estarán siempre en activo debido a que son compatibles y otros estarán deshabilitados, pero contendrán la misma configuración que el original, en caso de caer, se iniciará el secundario.

![img](../../static/img/raspberry/diagram.png)

### DNS
El servicio [DNS](https://es.wikipedia.org/wiki/Sistema_de_nombres_de_dominio) se encarga de resolver la IP a cada dominio al que intentamos llegar, algo que nos hace más fácil la vida.

Todos los servicios se conectarán a nuestro DNS local, si es algo que este no sabe resolver, lo irá a preguntar a los DNS externos, así tendremos todo centralizado.

Este servicio nos permitirá:
* Registrar el acceso tanto interno como externo de nuestra red a otros lugares.
* Bloquear el acceso a webs que no queremos que sean accesibles en nuestra red (¡publicidad te miro a ti!).
* Tener nuestra propia resolución de nombres, creando así una red de 'intranet'.

Debido al funcionamiento de DNS, lo normal es tener 2 configuradas en cada PC, lo que será ideal en nuestro caso, ya que precisamente tendremos 2 funcionando, si uno de ellos falla, no notaremos ningún cambio en la red ya que el resto se encargará de todo.

### DHCP
Cuando un dispositivo se conecta a nuestra red, este preguntará al servicio [DHCP](https://es.wikipedia.org/wiki/Protocolo_de_configuración_dinámica_de_host) por una IP y resto de configuración (como las DNS), otra opción es definirlo manualmente en cada dispositivo, pero esto no siempre es fácil, ¡algunas veces ni siquiera es posible!

En un caso doméstico o en una red profesional entre pequeña y mediana, es muy posible que del DHCP se encargue el propio router de la compañía contratada, pero eh aquí la sorpresa, **es posible que el router no nos permite asignar nuestra DNS**, están bloqueadas para protegernos de que un ente nos quiera poner unas 'DNS malas', así que necesitaremos nuestro propio servicio.

> Un conspiranoico diría que las compañías nos obligan a usar sus DNS y así bloquearnos el acceso a 'ciertas webs' (al no poder resolver la IP).

Ventajas de este servicio:
* Nos olvidamos casi totalmente de configurar IPs a mano, algo que no siempre es tan sencillo de hacer o que directamente no queremos hacer (es mala idea configurar una IP a mano en un portátil, ya que cada red donde nos conectemos será diferente).
* Podemos definir el rango de IPs que reservamos para dispositivos desconocidos o 'libres', lo que evita que los dispositivos choquen entre ellos por tener la misma IP.
* Para los dispositivos fijos de nuestra red, podemos asignar siempre la misma IP acorde a su MAC. Así podremos rastrearlos más fácilmente o evitarnos problemas de conexión (¿cuántas impresoras Wifi han 'dejado de funcionar' después de reiniciarlas?)

DHCP es un servicio peculiar, nuestra red solo puede (debería) tener uno de estos en cada rango (y lo normal es tener solo 1 rango). Es posible hacer trampas para tener 2 funcionando, pero queremos mantener el sistema simple, así que solo 1 nodo del clúster tendrá este servicio activo, el otro tendrá la misma configuración, pero no estará iniciado. Si el primer nodo cae, iniciaremos manualmente este servicio en el segundo nodo.

Esto es mejorable, lo sé, pero es un inicio, y el tiempo hará que esto cambie.

### VPN
Una [VPN](https://es.wikipedia.org/wiki/Red_privada_virtual) permite que un dispositivo se conecte a otra red, como si estuviera físicamente conectado a ella. El ejemplo típico es conectarse a una oficina desde nuestra casa, usando Internet como intermediario. Por supuesto, las conexiones entre ambos lados están cifradas.

Debido a que **nunca publicaremos todos los servicios a Internet por el riesgo que eso conlleva**, necesitaremos una VPN para poder acceder a la red desde cualquier lugar, y así tener acceso al resto de servicios.

### NTP
Este es quizás uno de los servicios más importantes y olvidados, [NTP](https://es.wikipedia.org/wiki/Network_Time_Protocol) es un servicio que permite preguntar la hora y calibrar la hora local acorde a ella. **TODOS los servidores requieren calibrar la hora para que toda la red tenga siempre la misma**. No se le suele dar mucha importancia, pero hay servicios de sincronización que fallan si la diferencia entre servidores es demasiado grande, cosa que acaba sucediendo, dale años si es necesario, pero al final, te fallará, ¡así que mejor lo resolvemos de inicio y nos olvidamos de ello!

La gran mayoría de dispositivos tienen un NTP configurado por defecto y cada día preguntan a este para sincronizarse, pero cuando tenemos muchos dispositivos locales no vamos a querer 'saturar' nuestra conexión a Internet con estas llamadas, ni tampoco saturar esos servicios públicos.

Nuestro clúster preguntará a un NTP externo para estar siempre sincronizados, mientras los dispositivos locales le preguntarán a nuestro clúster. Es habitual que este servicio permita definir una lista de nodos así que indicaremos ambos.

### Nagios
Nuestra red crece, tenemos más dispositivos, más servicios y estos se caen, un día vamos a usar uno, no funciona, miramos registros, logs y ... hace 2 semanas que no funciona, ¡pues nadie se ha dado cuenta! Quizás no sea importante, pero ojo, ese servicio era el que generaba las copias de seguridad, oh vaya, hace 2 semanas que no tenemos copias de seguridad ...

Si alguna vez te ha pasado algo así, entonces necesitas un [Nagios Core](https://www.nagios.org/projects/nagios-core/), y si no te ha pasado, ¡estás de suerte! y la suerte se acaba, así que **también lo necesitas**.

Nagios Core es una aplicación que consulta a otros sobre su estado, es totalmente configurable e incluso podemos crear módulos propios, la única limitación que tiene es nuestra imaginación. Si un servidor o servicio se cae, nos avisará de ello para que lo resolvamos. Por supuesto, Nagios Core no se encarga de resolverlo, únicamente es un sistema de alertas.

> Quizás pienses: va, yo uso Kubernetes, si algo se cae, el sistema lo vuelve a iniciar, y ¡tienes razón! si se cae, los volverá a lanzar en otro nodo, y si se vuelve a caer, repetirá una y otra vez hasta el aburrimiento, el servicio no estará disponible y tú serás feliz creyendo que todo funciona, pero no. Nagios te habría avisado de que algo está pasando.

### LDAP
Otro de los grandes pilares en cualquier red es el [LDAP](https://es.wikipedia.org/wiki/Protocolo_ligero_de_acceso_a_directorios), este se encarga de mantener y gestionar los usuarios y contraseñas, cada servicio se conectará a él, con lo que podremos acceder a los diferentes servicios con el mismo usuario.

Esto es excepcionalmente importante cuando tienes que cambiar de contraseña, es frustrante cuando tienes que memorizar la contraseña de 4 o 5 servicios que además cambian habitualmente.
