---
id: build-components
title: Componentes
slug: components
sidebar_position: 2
---

## Descripción
Para construir este proyecto hay varios temas a resolver:
* Caja
* Ventilación
* Fuente de alimentación
* Switch

Antes de empezar, me rijo bajo unas ideas simples:
* Todos los componentes irán dentro de una caja, esta protegerá físicamente (y del polvo) a todos los componentes.
* Normalmente cada raspberry debe tener un mini-ventilador, pero incluso los ventiladores de alta calidad hacen un ruido excepcional, así que lo cambiaré por un gran ventilador.
* Aunque el proyecto requiere únicamente 2 Raspberrys, la caja será preparada acorde a la idea de 4, lo que permite incrementar la potencia del clúster para nuevos proyectos sin apenas incrementar el coste.

### Caja
Hay que diferenciar en 2 cajas, la primera es un pilar de metacrilato donde se atornilla una Raspberry en cada piso. Su única función es permitir montarlas como una torre, no tiene ventiladores y está totalmente abierta.

Todos los componentes irán dentro de una caja de mini-PC, esta es la que protegerá al resto y organizará los cables.

### Ventilación
La caja que he elegido dispone de un gran ventilador que debe encargarse de todo, pero tiene un gran problema: **los ventiladores de PC funcionan con 12v, las Raspberrys con 5v**.

Los proyectos de Raspberry utilizan siempre ventiladores de 5v conectados a una de ellas, pero no he encontrado ningún ventilador de 5v que tenga una potencia/ruido que considere aceptable, así que me decanto por mantener un ventilador de 12v, además, si en el futuro se daña, puedo optar por cambiarlo fácilmente por otro de gran calidad.

### Fuente de alimentación
La Raspberry Pi 4B utiliza un USB-C para la alimentación, concretamente 5v y 3A, 15w en total.

Un USB normal dispone de 5v y 2,2A así que no debería funcionar, pero mucha gente ha confirmado que sí funciona en cargadores USB con 60w.

### Switch
Todas las Raspberry se conectarán por ethernet, un servidor no puede funcionar por Wifi, así que se acoplará un switch propio para el clúster y así se evitará tener que sacar un cable para cada una y obligar a tener un switch externo.

## Lista de la compra
Antes de empezar, lo primero es obtener todos los componentes, debido a la complejidad de algunos, he optado por comprar un trozo aquí y allá.

### Esenciales
Lo primero que necesitamos son las 2 Raspberrys completas, la fuente de alimentación y el switch junto a los cables para conectar todo entre ellos.


| Cantidad | Componente | Precio |
| -------- | ---------- | -----: |
| 2        | [Raspberry Pi 4B 4GB](https://www.tiendatec.es/raspberry-pi/gama-raspberry-pi/1100-raspberry-pi-4-modelo-b-4gb-765756931182.html) | 120€ |
| 1        | [Kit de disipadores para 2 Raspberry](https://www.amazon.es/gp/product/B07VMM3H93/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1) | 6€ |
| 2        | [SanDisk Ultra 64GB Clase 10](https://www.amazon.es/SanDisk-SDSQUA4-064G-GN6MA-microSDXC-Adaptador-Rendimiento/dp/B08GYBBBBH) | 26€ |
| 1        | [Anker Cargador USB de 60W](https://www.amazon.es/gp/product/B00PTLSH9G) | 35€ |
| 1        | [2 cables USB a USB-C de 0,50m](https://es.aliexpress.com/item/1005002467256372.html) | 5€ |
| 1        | [Switch GigaLan de 5 puertos](https://www.amazon.es/D-Link-GO-SW-5GE-Gigabit-Ethernet-switch/dp/B009TZE41I/ref=sr_1_24) | 15€ |
| 1        | [Varios cables planos Cat7 de 0,25m](https://www.amazon.es/gp/product/B06XNRPBV2) | 12€ |
| 1        | [Cualquier cable Cat6 o superior](https://www.amazon.es/gp/product/B00VC075EQ) | 7€ |

Es raro, pero lo más difícil de encontrar son las Raspberry Pi 4B 4GB, debido a que la última versión en este momento es la 1.2 y es casi imposible de encontrar. Después de devolver varias, he optado por una distribuidora oficial.

El cargador de Anker nos ofrece 60w, que es suficiente para 4 Raspberrys (ojo porque tiene 6 salidas, ¡2 de ellas no podrán ser utilizadas si tenemos todas las 4 Raspberrys funcionando!).

El proyecto permitirá 4 Raspberrys a futuro así que necesitamos un switch de 5 puertos. He hecho varias pruebas con switch domésticos y este D-Link ha dado un rendimiento casi igual a un Cisco, además su tamaño excepcionalmente pequeño es ideal (ya no hablemos del coste).

:::caution
Si nos decidimos por poner todo en la caja, debemos tener cuidado con los cables de USB-C, ya que el espacio entre ellos y el ventilador es muy pequeño y chocarán.
:::

Con esto llevamos 226€ (costes de envío aparte).

### Caja
Con lo que tenemos sería suficiente para montar el proyecto (aun sin ventilador), pero como ya he indicado, mi idea es ir más allá.

| Cantidad | Componente | Precio |
| -------- | ---------- | -----: |
| 1        | [Carcasa para 4 capas](https://es.aliexpress.com/item/32813460207.html?spm=a2g0s.9042311.0.0.791b63c074F9XC) | 8€ |
| 1        | [Cool Master MasterCase H100](https://www.coolmod.com/cooler-master-mastercase-h100-caja-torre-precio) | 70€ |
| 1        | [Fuente de alimentación de 12v 1A](https://es.aliexpress.com/item/32859196804.html?spm=a2g0s.9042311.0.0.791b63c074F9XC) | 2€ |
| 1        | [Cable DC a PIN](https://es.aliexpress.com/item/1005001604639114.html?spm=a2g0s.9042311.0.0.791b63c074F9XC) | 2€ |
| 1        | [Regleta de 2 enchufes y 2 USB](https://www.amazon.es/Allocacoc-1402GY-DEEUPC-PowerCube-Extended/dp/B00GAGOMW0/) | 22€ |

La caja que he elegido dispone de un gran ventilador de 12v 0,3A, así que he optado por una fuente de alimentación de 12v 1A que me permite alimentar a este y a futuros ventiladores que requiera la caja, también he elegido un conector de DC a 2 PIN que es el que utilizan estos ventiladores, el 2o PIN es para un futuro ventilador, prefiero tenerlo por si acaso.

Entre la fuente de alimentación USB, el ventilador y el switch, se requiere una regleta de 3 enchufes, pero es posible hacer trampa, así que he optado por la regleta en cubo, pero se puede optar por una regleta de 3 enchufes simple por la mitad de precio.

Esto añade 104€.

### Mas allá
Con lo que tenemos es más que suficiente, pero podemos ir aún más allá y convertir esta caja en algo más similar a un servidor en clúster de verdad.

:::important
La salida de pantalla suele tener problemas, si el cable no está conectado a una pantalla al iniciar, no funcionará. Al arrancar la Raspberry, el conmutador debería estar conectado a este. Una vez a iniciado y vemos la pantalla, ya podemos cambiar sin problemas.
:::

| Cantidad | Componente | Precio |
| -------- | ---------- | -----: |
| 1        | [Conmutador HDMI/USB](https://www.amazon.es/gp/product/B08KY66F1X/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) | 55€ |
| 2        | [Cables HDMI a mini-HDMI con conector a derecha de 0,5m](https://es.aliexpress.com/item/1005001752071442.html?spm=a2g0s.9042311.0.0.6c2963c0YrwrKr) | 7€ |

Este conmutador nos permite tener acceso a la pantalla y teclado de las Raspberry sin tener que abrir la caja, totalmente innecesario una vez están configuradas, pero la experiencia me dice que es algo que voy a echar de menos algún día.

:::caution
Si el espacio del USB-C de las Raspberry casi roza el ventilador, el HDMI es peor. No he encontrado ningún cable que no choque, así que he optado por uno con cable lateral en dirección contraria al USB-C
:::

Aun así, debido a que el sistema es desmontable, este es claramente opcional.

Añade 62€.

### Reduciendo costes
Siendo sinceros, para este proyecto no necesitamos tanta potencia, si creemos que a futuro no optaremos por darles más trabajo, se puede utilizar modelos anteriores:

| Cantidad | Componente | Precio |
| -------- | ---------- | -----: |
| 2        | [Raspberry Pi 3B+](https://www.tiendatec.es/raspberry-pi/gama-raspberry-pi/752-raspberry-pi-3-modelo-b-plus-5060214370165.html) | 80€ |
| 2        | [SanDisk de 32 GB](https://www.amazon.es/SanDisk-SDSQUA4-064G-GN6MA-microSDXC-Adaptador-Rendimiento/dp/B08GY9NYRM) | 14€ |

En este caso habrá que comprar unos cables acordes (ya que no tiene USB C), e incluso podemos buscar unas SD aún más pequeñas.

La otra parte donde se invierte la mayoría de dinero es la caja, así que se puede optar por comprar una torre de metacrilato con ventiladores de 5v acoplados a cada Raspberry, lo que reduce el coste y el trabajo.

También podemos optar por quitar el switch y conectarlos a alguno que ya tuviéramos (NO utilizar la wifi en servidores), y, por supuesto, quitar el conmutador.

## Total
Tenemos 226€ de esenciales, 104€ de la caja y 62€ del conmutador, esto suma 392€, más gastos de envío.

Con esto tenemos:
* 2 Raspberry Pi 4B con RAM y SD más que suficiente para futuros proyectos.
* Una caja con espacio y conexiones para 2 futuras Raspberry.
* Todo unido dentro de una caja transportable e independiente, solo necesita un enchufe y una conexión de ethernet.

Por 164€ podríamos añadir las 2 Raspberrys completas (mismos disipadores y SD, 2 cables USB-C y 2 mini-HDMI, los de ethernet ya los tenemos), y usar las 4 Raspberry para sumar un clúster de K3s además de este proyecto.