---
id: build-assemble
title: Ensamblado
slug: assemble
sidebar_position: 1
---

## Introducción
Esta es la parte más **bricomanía** y menos informática de todas las secciones: ensamblar todo lo comprado para formar una única caja con todo.

Esto se puede hacer de muchas formas, yo he ido adaptando según mis conocimientos y herramientas, así que hay múltiples opciones de hacer esto.

## Ensamblando
Para seguir estos pasos, hará falta destornilladores, taladros, diferentes tornillos, pegamentos o , nuevos cables y otras herramientas que no he tenido en cuenta en la lista de la compra. Todo puede variar según qué se decida instalar.

Todos los componentes irán dentro de la caja, de esta únicamente saldrán 3 cables:
* Enchufe de la regleta
* Cable de ethernet
* Control del conmutador para cambiar entre Raspberrys

Por fuera, estará accesible tanto la salida HDMI como un USB del conmutador, todo lo demás quedará dentro de la caja y no será accesible sin abrirla.

Para evitar problemas, los componentes irán enganchados (con Command 3M) o atornillados, todos los cables estarán atados con diferentes herramientas, sea como sea, todas las piezas son desmontables y estarán acopladas a la base de la caja, no a las tapas desmontables (laterales, superior y frontal), ya que haría más complicado el proceso de apertura.

### Quitando cosas
Tenemos una caja para mini-PC, eso significa que tenemos un bonito frontal con botones, luces y USB que no vamos a utilizar, solo ocupa espacio y añade cables, así que irá fuera.

Primero debemos quitar el frontal de la caja, este no tiene tornillos, abajo tiene un hueco desde donde empujar hacia fuera.

![img](../../../static/img/raspberry/RaspProject_Frontal.jpg)

Desde el frontal tendremos acceso a los 2 tornillos que sujetan la tapa superior, por dentro tiene 4 más.

![img](../../../static/img/raspberry/RaspProject_Tornillos_superiores.jpg)

Con la parte superior quitada, ya podemos desmontar los tornillos que sujetan el panel.

![img](../../../static/img/raspberry/RaspProject_Extraccion_panel.jpg)

Con esto ganamos un espacio que será útil para los cables del ventilador.

### Atornillando el pilar
La caja tiene unos agujeros que encajan a la perfección con el pilar de metraquilado, aunque solo 2 de ellos, así que habrá que hacer otros, esto requiere una Dremel y mucha paciencia o un taladro más potente, debido a que la caja es más dura de lo que parece.

![img](../../../static/img/raspberry/RaspProject_Tornillos_inferiores.jpg)

Aquí se puede ver el resultado con una Raspberry ya atornillada.

![img](../../../static/img/raspberry/RaspProject_Pilar_metraquilato.jpg)

:::caution
Las Raspberrys deben atornillarse a cada piso antes de montar el pilar, o no tendremos acceso para atornillarlas.
:::

Aunque se puede dejar así, las patas inferiores del pilar dejan la primera Raspberry fuera de la zona de acción del ventilador, lo que no es buena idea.

Después de hacer varias pruebas, he preferido cambiar esas patas por otras más largas y he añadido las gomas para discos duros que vienen con la caja para proteger mejor la zona.

### Switch, conmutador y regleta
Primero un detalle: el conmutador viene con un cable DC 5,5x2,1 a USB para alimentarse, el switch viene con un adaptador a corriente, pero puede usar el mismo cable (tiene el mismo voltage). Comprando un cable igual se ahorra espacio, los USB se pueden conectar a diferentes Raspberry, aunque he preferido conectarlos a la regleta y así tener electricidad directa, ya que si esa Raspberry se apaga o falla, mataría a todas las demás.

:::caution
Hay suficiente espacio en la fuente de alimentación USB de las Raspberry para estos 2 USB, pero NO tiene la potencia suficiente para alimentarlo todo.
:::

Me he decidido por una regleta de tipo cubo, se puede atornillar perdiendo un enchufe, y pierdo otro ya que choca con una tapa, pero me da 2 enchufes y 2 USB, ¡justo lo que necesito y gratis!, ya que lleva años cogiendo polvo.

Este es el cómo quedará, he colocado las piezas pensando en que los cables no choquen entre ellos y que el HDMI y 1 USB del conmutador queden fuera de la caja, accesibles:

![img](../../../static/img/raspberry/RaspProject_Organización.jpg)

:::tip
Detalles de Logitech, tengo un teclado permite hasta 3 receptores inalambricos, así que he dejado uno puesto en uno de los USB superiores del conmutador, esa zona es de difícil acceso, pero se quedará allí fijo, y así sigo manteniendo el USB accesible libre por si un día lo necesito. Si no disponemos de esto, podemos utilizar el USB que queda fuera para conectar el teclado.
:::

Lo primero es enganchar el switch y conmutador, y atornillar el cubo arriba, por ahora la fuente de alimentación de las Raspberry se quedará en espera, así que hay que empezar desatornillando la tapa izquierda, también hay que quitar los tornillos donde se pone la placa base (con un alicate se saca fácilmente) y romper una pieza que sobresale en la izquierda que choca con el conmutador.

![img](../../../static/img/raspberry/RaspProject_Tornillos_izquierda.jpg)

Me he decantado por poner 2 tiras de Command 3M a cada uno dispositivo para evitar que caigan por peso o presión de los cables. El cubo se atornilla arriba, este requiere que esté ligeramente inclinado para bloquearlo y que no se caiga.

![img](../../../static/img/raspberry/RaspProject_Enganches.jpg)

Una vez puesto todo, debería empezar a quedar así:

![img](../../../static/img/raspberry/RaspProject_Cables1.jpg)

Antes de poner todo, hay que atornillar la parte superior, ya que algunos tornillos quedan debajo del switch y/o conmutador.

### Ventilación
El ventilador de la caja tiene 2 cables, uno para la electricidad y otro para las luces.

Para hacerlo funcionar, tengo el adaptador de 12v, le he puesto un cable de DC a PIN, en mi caso este tiene 2 salidas a PIN por si un día necesitara tener un 2o ventilador arriba (la caja no lo permite, pero no va de 4 agujeros más), todos estos cables van recogidos donde estaba el panel delantero, junto al cable para las luces, que no lo quiero para nada.

Como ya indiqué al inicio, una parte vital para mi es el ruido, y aunque este ventilador debería ser bastante silencioso, no lo es suficiente para mi gusto.

Noctua dispone de cables que reducen el ruido (reduciendo la velocidad), debido a que suelen regalarlos al comprar un ventilador, he echado mano de uno de ellos. La reducción de ruido es enorme y a cambio he "ganado" entre 3 y 5 grados en el interior de la caja, algo asumible.

Con un poco de tiempo, se pueden atar todos estos cables, he usado unos [organizadores de cables](https://www.amazon.es/gp/product/B07P6PYL95/) para evitar que se muevan:

![img](../../../static/img/raspberry/RaspProject_Cables_ventilador.jpg)

Con esto terminado, ya podemos poner el frontal.

### Fuente de alimentación USB
Aunque el cable original es funcional, lo he cambiado por uno corto y con conectores a 90º, lo que me da más espacio para maniobrar y reduce la cantidad de cable por el lugar, más fácil de organizar y menos problemas para circular el aire.

Finalmente lo he enganchado también utilizando los Command 3M.

![img](../../../static/img/raspberry/RaspProject_FuenteAlimentacion.jpg)
### Cables
En estos casos, se suele olvidar la importáncia de organizar bien los cables, estos entorpecen enormemente la labor del ventilador, así que vale la pena invertir tiempo y dinero en anclarlos a los laterales de la caja, dejando que el aire circule lo mejor posible por el centro.

Para facilitarme esa labor, he cambiado muchos de los cables originales por otros más cortos, ligeros y/o con conectores de 90º, aunque estos no siempre son fáciles de conseguir, y aumentan el coste final, al final vale la pena

Como último detalle, he añadido unas etiquetas a cada cable para saber más fácilmente quien es quien.

## Resultado final
He añadido algunos detalles finales, como un filtro de polvo en la trasera, este es el resultado:

![img](../../../static/img/raspberry/RaspProject_Frontal_Final.jpg)

![img](../../../static/img/raspberry/RaspProject_Lateral.jpg)

![img](../../../static/img/raspberry/RaspProject_Trasera.jpg)
