---
id: rasp-history
title: Historial
slug: history
sidebar_position: 4
---

Los cambios en el `Rasp Project` se registran en esta sección.

## Versión

### 2024/04/29
Certbot para gestionar certificados.

### 2021/06/11
Terminado el proceso de [ensamblado](/docs/rasp/build/assemble).

### 2021/05/09
Primera versión del proyecto.

## Roadmap
* Mejorar la sincronización de Pi Hole
* Permitir añadir y quitar DNS de Pi Hole con servicios REST
* Sincronizar los OpenLDAP
