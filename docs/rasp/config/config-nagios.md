---
id: config-nagios
title: Nagios
slug: nagios
sidebar_position: 6
---

## Introducción 
Nagios es una empresa que se dedica a todo tipo de monitores de la red, entre ellos está Nagios Core, que es su versión gratuita.

Los anteriores han sido extremadamente fáciles de configurar, así que Nagios se encargará de justo lo contrario, ¡si todo es tan fácil hasta parece aburrido!

:::note
Si replicamos la configuración en ambos nodos, nos llegarán el doble de notificaciones. Es preferible dejar un nodo monitorizando toda la red (incluido ambos nodos) y el otro nodo para vigilar que el primero siga vivo. No sería la primera vez que algo falla y no se notifica porque el que ha fallado, es el nodo que tiene Nagios.
:::

## Instalación
Debido a que tenemos que compilar, habrá que instalar todas las dependencias.

```bash
sudo apt update
sudo apt install -y autoconf gcc libc6 libmcrypt-dev make \
    libssl-dev wget bc gawk dc build-essential snmp \
    libnet-snmp-perl gettext unzip libgd-dev
```

Para hacerlo funcionar necesitamos 3 aplicaciones:
* [**Nagios Core**](https://github.com/NagiosEnterprises/nagioscore/releases) es el motor principal.
* [**Nagios Plugins**](https://nagios-plugins.org/downloads/) tiene un kit de pruebas.
* [**NRPE**](https://exchange.nagios.org/directory/Addons/Monitoring-Agents/NRPE--2D-Nagios-Remote-Plugin-Executor/details) es el servicio utilizado para consultar a otros servicios.

Empezamos descargando y descomprimiendo todos.

:::warning
Hay que ajustar estos enlaces según la última versión.
:::

```bash
wget https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.6.tar.gz -O nagios.tar.gz
wget http://nagios-plugins.org/download/nagios-plugins-2.3.3.tar.gz -O nagios-plugins.tar.gz
wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-4.0.2/nrpe-4.0.2.tar.gz -O nrpe.tar.gz

tar xzf nagios.tar.gz
tar xzf nagios-plugins.tar.gz
tar xzf nrpe.tar.gz
```

### Nagios Core
Creamos los usuarios y grupos de Nagios.
```bash
sudo useradd -m -s /bin/bash nagios
sudo groupadd nagcmd
sudo groupadd nagios
sudo usermod -a -G nagcmd nagios
sudo usermod -a -G nagios nagios
```

Compilamos y ejecutamos los procesos para su instalación.
```bash
cd  nagioscore-nagios-4.4.6/
./configure --with-nagios-group=nagios --with-command-group=nagcmd --with-gd-lib=/usr/local/lib
make all
sudo make install
sudo make install-daemoninit
sudo make install-commandmode
sudo make install-config
cd ..
```

### Nagios Plugins
Repetimos con los plugins, mucho más sencillo.

```bash
cd nagios-plugins-2.3.3/
./configure
make
sudo make install
cd ..
```

### Nagios NRPE
Y acabamos con el NRPE (aviso que este es leeeeeento).

```bash
cd nrpe-4.0.2/
./configure
make all
sudo make install
sudo make install-config
sudo make install-init
sudo systemctl enable nrpe.service
```

Antes de iniciarlo, se debe configurar.

```bash
sudo vi /usr/local/nagios/etc/nrpe.cfg
```

Básicamente tenemos 3 puntos a ajustar:
* Qué IP puede hacer preguntas por el servicio NRPE, deberíamos dejar activo localhost y las IP de ambas Raspberry (la 2a solo requiere acceso a la 1a, pero es buena idea dejarlo preparado por si un día tenemos cambios).
* Aceptar comandos con argumentos (``dont_blame_nrpe``), normalmente se dejaría apagado, pero en mi caso lo necesito para los [backups notificados](/blog/2020/05/01/copias-seguridad-notificadas-nagios).
* Lista de comandos aceptados, por defecto NRPE tiene varios, y podemos crear tantos como queramos. En mi caso, suelo crear uno para cada partición y apunto a ellas en vez de al disco como hace por defecto, y reajusto el warning para cargas de trabajo, ya que suele saltar a la mínima y no me es relevante.

```vim title=/usr/local/nagios/etc/nrpe.cfg
allowed_hosts=127.0.0.1,::1,192.168.1.5,192.168.1.6
dont_blame_nrpe=1

command[check_disk_root]=/usr/local/nagios/libexec/check_disk -w 20% -c 10% -p /
command[check_load]=/usr/local/nagios/libexec/check_load -r -w .20,.15,.10 -c .30,.25,.20
```

Una vez configurado, iniciamos ambos servicios y comprobamos el NRPE.
```bash
sudo systemctl start nagios.service
sudo systemctl start nrpe.service
/usr/local/nagios/libexec/check_nrpe -H 127.0.0.1
```

### Lighttpd
Nagios dispone de una web para visualizar los procesos, pero su instalación está pensada para Apache, y tenemos un Lighttpd debido a Pi Hole, lo mejor es adaptarnos a ello.

Primero debemos darle permiso al sistema de comandos, ya que sin ello no podremos hacer cosas tan importantes como decirle que vuelva a analizar un proceso.
```bash
sudo chown -R nagios:www-data /usr/local/nagios/var/rw/
```

Nagios tampoco tiene un sistema de inicio de sesión, pero da por hecho que accedemos con el usuario **nagiosadmin**, así que lo generamos. Es posible generar un fichero de estos a mano, pero me es más sencillo instalar las tools de Apache.

```bash
sudo apt install apache2-utils -y
sudo htpasswd -c /etc/lighttpd/nagios.passwd nagiosadmin
```

Con todo preparado, añadimos la configuración.

```bash
sudo vi /etc/lighttpd/external.conf
```

Nagios requiere algunos extras que Pi Hole no añade, así que los añadimos y configuramos sus rutas de acceso.
```vim title="/etc/lighttpd/external.conf"
# Nagios
server.modules += (
    "mod_alias",
    "mod_authn_file",
    "mod_cgi"
)
alias.url = (
    "/nagios/cgi-bin" => "/usr/local/nagios/sbin",
    "/nagios" => "/usr/local/nagios/share" 
)
$HTTP["url"] =~ "^/nagios/cgi-bin" {
    cgi.assign = ( "" => "" )
}
$HTTP["url"] =~ "nagios" {
    auth.backend = "htpasswd" 
    auth.backend.htpasswd.userfile = "/etc/lighttpd/nagios.passwd" 
    auth.require = ( "" => (
            "method" => "basic",
            "realm" => "nagios",
            "require" => "user=nagiosadmin" 
        )
    )
}
```

Reiniciamos.

```bash
sudo service lighttpd restart
```

Si todo ha funcionado, podremos acceder desde la misma URL que Pi Hole, cambiando ``admin`` por ``nagios``:
* https://serverName.domain.intranet/nagios

## Configuración 
Podemos creer que instalarlo ha sido complicado, pero lo cierto es que configurar Nagios es lo verdaderamente complicado.

No hay una única forma de configurar Nagios, este es uno de tantos métodos.

### Base
Lo primero es definir las bases: comandos a utilizar, grupos, carpetas de donde recuperar las configuraciones y otros tantos.

```bash
sudo vi /usr/local/nagios/etc/nagios.cfg
```

```vim title="/usr/local/nagios/etc/nagios.cfg"
cfg_file=/usr/local/nagios/etc/objects/commands.cfg
cfg_file=/usr/local/nagios/etc/objects/contacts.cfg
cfg_file=/usr/local/nagios/etc/objects/timeperiods.cfg
cfg_file=/usr/local/nagios/etc/objects/templates.cfg
cfg_file=/usr/local/nagios/etc/objects/groups.cfg

#cfg_file=/usr/local/nagios/etc/objects/localhost.cfg

cfg_dir=/usr/local/nagios/etc/servers
cfg_dir=/usr/local/nagios/etc/printers
#cfg_dir=/usr/local/nagios/etc/switches
cfg_dir=/usr/local/nagios/etc/routers
cfg_dir=/usr/local/nagios/etc/nas
cfg_dir=/usr/local/nagios/etc/tools
```

:::tip
Por defecto, Nagios analiza a ``localhost``, pero prefiero redirigirlo con NRPE para que todo se valide con el mismo protocolo y poder copiar la configuración en el otro Nagios.
:::

En la actualidad hay muchos dispositivos que se conectan a la red, ya no es únicamente servidores, NAS o routers/switch, también podemos tener impresoras y aparatos de domótica o similares.

```bash
sudo mkdir /usr/local/nagios/etc/{nas,printers,routers,servers,tools}
```

#### Comandos
Este fichero tiene la lista de todos los comandos que Nagios utiliza y también los que puede analizar.

```bash
sudo vi /usr/local/nagios/etc/objects/commands.cfg
```

Nagios utiliza como e-mail de envío el del sistema, pero podemos modificarlo para tener uno fijo.

```vim title=/usr/local/nagios/etc/objects/commands.cfg
define command {

    command_name    notify-host-by-email
    command_line    /usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\nHost: $HOSTNAME$\nState: $HOSTSTATE$\nAddress: $HOSTADDRESS$\nInfo: $HOSTOUTPUT$\n\nDate/Time: $LONGDATETIME$\n" | /usr/bin/mail -s "** $NOTIFICATIONTYPE$ Host Alert: $HOSTNAME$ is $HOSTSTATE$ **" $CONTACTEMAIL$ -r nagios@domain.cat
}

define command {
    command_name    notify-service-by-email
    command_line    /usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\n\nService: $SERVICEDESC$\nHost: $HOSTALIAS$\nAddress: $HOSTADDRESS$\nState: $SERVICESTATE$\n\nDate/Time: $LONGDATETIME$\n\nAdditional Info:\n\n$SERVICEOUTPUT$\n" | /usr/bin/mail -s "** $NOTIFICATIONTYPE$ Service Alert: $HOSTALIAS$/$SERVICEDESC$ is $SERVICESTATE$ **" $CONTACTEMAIL$ -r nagios@domain.cat
}
```

Nagios dispone de NRPE para comunicarse con otros servidores, pero por defecto, no tiene ningún comando para utilizarlo. Si un servidor publica un servicio, pero el servidor de Nagios no tiene el comando, no podrá utilizarlo, así que, si se crea uno nuevo, habrá que añadirlo a este.

Esta es una lista que he ido recopilando con el tiempo.

```vim title=/usr/local/nagios/etc/objects/commands.cfg
############################################################
#
# NRPE COMMANDS
#
############################################################

define command {
    command_name    check_nrpe
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
}
define command {
    command_name    check_nrpe_disk_root
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_disk_root
}
define command {
    command_name    check_nrpe_disk_opt
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_disk_opt
}
define command {
    command_name    check_nrpe_disk_var
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_disk_var
}
define command {
    command_name    check_nrpe_disk_var_lib
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_disk_var_lib
}
define command {
    command_name    check_nrpe_disk_share
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_disk_share
}
define command {
    command_name    check_nrpe_disk_storage
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_disk_storage
}
define command {
    command_name    check_nrpe_users
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_users
}
define command {
    command_name    check_nrpe_procs
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_procs
}
define command {
    command_name    check_nrpe_load
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_load
}
define command {
    command_name    check_nrpe_swap
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_swap
}
define command {
    command_name    check_nrpe_total_procs
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_total_procs
}
define command {
    command_name    check_nrpe_zombie_procs
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_zombie_procs
}
define command {
    command_name    check_nrpe_bkp
    command_line    $USER1$/check_nrpe -H $HOSTADDRESS$ -c check_bkp -a '$ARG1$'
}
```

Entre los analizadores normales, suele faltan uno para revisar ``virtual hosts``.

```vim title=/usr/local/nagios/etc/objects/commands.cfg
define command {
    command_name    check_http_virtual
    command_line    $USER1$/check_http -H $ARG1$ $ARG2$
}
```

#### Alertas
Podemos definir varios usuarios y el momento en el que están "trabajando", para enviar alertas únicamente en ese rango. También se pueden asignar grupos por servicios.

```bash
sudo vi /usr/local/nagios/etc/objects/contacts.cfg
```

Este es un ejemplo donde las notificaciones se envían siempre, sin importar la hora.

```vim title=/usr/local/nagios/etc/objects/contacts.cfg
define contact{
    contact_name                    userName
    use                             generic-contact
    alias                           User Name
    email                           userName@domain.cat
    service_notification_period     24x7
    service_notification_options    w,u,c,r,f
    service_notification_commands   notify-service-by-email
    host_notification_period        24x7
    host_notification_options       d,u,r,f
    host_notification_commands      notify-host-by-email
}
define contactgroup {
    contactgroup_name       admins
    alias                   Nagios Administrators
    members                 userName
}
```

#### Grupos
Nagios muestra el estado de todos los hosts y servicios en su UI, pero podemos organizar aquellos que no sean de más importancia en grupos.

Únicamente los que tengan miembros, aparecerán en la UI.

```bash
sudo vi /usr/local/nagios/etc/objects/groups.cfg
```

```vim title=/usr/local/nagios/etc/objects/groups.cfg
############################################################
#
# HOST GROUPS
#
############################################################

define hostgroup {
    hostgroup_name  PROD
    alias           Production environment
}

define hostgroup {
    hostgroup_name  TEST
    alias           Testing environment
}

define hostgroup {
    hostgroup_name  DEV
    alias           Development environment
}

define hostgroup {
    hostgroup_name  printers
    alias           Impresoras
}

define hostgroup {
    hostgroup_name  NAS
    alias           NAS
}
define hostgroup {
    hostgroup_name  router
    alias           Router
}

define hostgroup {
    hostgroup_name  tools
    alias           Tools
}

############################################################
#
# SERVICE GROUPS
#
############################################################
define servicegroup {
    servicegroup_name       HDD
    alias                   Hard Disk
}
define servicegroup {
    servicegroup_name       Web
    alias                   Web Services
}
define servicegroup {
    servicegroup_name       HealthStatus
    alias                   Health Status
}
define servicegroup {
    servicegroup_name       DB
    alias                   Databases
}
define servicegroup {
    servicegroup_name       Backup
    alias                   Backup
}
```

### Dispositivos simples
Si un dispositivo no permite instalar NRPE (como una impresora), solo podremos configurar opciones básicas.

```bash
sudo vi /usr/local/nagios/etc/routers/router.cfg
```

Para estos, simplemente podremos definirlos para que sean revisados por ``ping``, lo que suele ser suficiente.

```vim /usr/local/nagios/etc/routers/router.cfg
define host {
    use                     generic-switch
    host_name               router
    alias                   router
    address                 router.domain.intranet
    hostgroups              router
}
define service {
    use                     local-service
    host_name               router
    service_description     PING
    check_command           check_ping!100.0,20%!500.0,60%
}
```

Habrá que replicarlo para cada dispositivo, indicando el host_name, el address y al grupo al que pertenece.

### Dispositivos con NRPE
Siempre que sea posible, instalaremos NRPE para tener mejor control. Al instalar NRPE, se le indica qué comandos podrá ejecutar, así que podemos construir scripts nuevos. No hay límites al control que podemos aplicar.

```bash
sudo vi /usr/local/nagios/etc/servers/raspberry1.cfg
```

Este es un ejemplo para una Raspberry, de aquí podemos ir ampliando.

```vim title=/usr/local/nagios/etc/servers/raspberry1.cfg
define host {
    use                     linux-server
    host_name               raspberry1
    alias                   Raspberry1
    address                 raspberry1.domain.intranet
    hostgroups              PROD
}
define service {
    use                     local-service
    host_name               raspberry1
    service_description     Hard disk - Root
    check_command           check_nrpe_disk_root
    servicegroups           HDD
}
define service {
    use                     local-service
    host_name               raspberry1
    service_description     Current Users
    check_command           check_nrpe_users
}
define service {
    use                     local-service
    host_name               raspberry1
    service_description     Total Processes
    check_command           check_nrpe_total_procs
}
define service {
    use                     local-service
    host_name               raspberry1
    service_description     Zombie Processes
    check_command           check_nrpe_zombie_procs
}
define service {
    use                     local-service
    host_name               raspberry1
    service_description     Current Load
    check_command           check_nrpe_load
}
define service {
    use                     local-service
    host_name               raspberry1
    service_description     PING
    check_command           check_ping!100.0,20%!500.0,60%
}
define service {
    use                     local-service
    host_name               raspberry1
    service_description     SSH
    check_command           check_ssh
}

define service {
    use                     local-service
    host_name               raspberry1
    service_description     HTTP Lighttpd
    check_command           check_http
    servicegroups           Web
}

define service {
    use                     local-service
    host_name               raspberry1
    service_description     HTTPS Lighttpd
    check_command           check_http_virtual!raspberry1.moon.intranet!-S
    servicegroups           Web
}
```

### Verificar configuración
Antes de reiniciar el servicio, es muy aconsejable revisar que sea correcto.
```bash
sudo /usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
```

Reiniciamos para aplicar los cambios.
```bash
sudo systemctl restart nagios.service
```