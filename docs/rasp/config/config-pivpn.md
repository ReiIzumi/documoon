---
id: config-pivpn
title: PiVPN
slug: pivpn
sidebar_position: 3
---

## Introducción
La mayoría de los servicios que desplegamos no tiene sentido que estén publicados a internet, pero es posible que necesitemos conectarnos a ellos aun estando fuera de nuestra red, para ello están las VPN.

En este caso, utilizaré el proyecto [PiVPN](https://www.pivpn.io) que es otro proyecto simple donde nos desplegará una VPN y comandos simples para configurarlo.

Si ser capaz de desplegar una VPN en 5 minutos no es suficientemente insultante, además su asistente nos permite elegir entre [OpenVPN](https://openvpn.net) o [WireGuard](https://www.wireguard.com), y además ser compatible con Pi Hole. En serio, estos proyectos mejoran la vida.

En mi caso opto por utilizar WireGuard, los resultados de rendimiento han sido excelentes y su encriptación es suficiente para mí.

:::tip
Tenemos 2 Raspberrys, así que tenemos 3 opciones: activar una VPN diferente en cada puerto (y publicarlas a Internet), mantener los usuarios en un nodo e intentar sincronizarlos en el otro, o simplemente crear usuarios diferentes en cada nodo. Variará según la importancia que le demos a este servicio. 
:::

## Instalación
:::important
La VPN requiere que tengamos una IP estática en Internet, si no es el caso, deberemos crear un dominio y asignarlo.
:::

Igual que Pi Hole, ejecutamos el instalador y nos mostrará un asistente.
```bash
sudo curl -L https://install.pivpn.io | bash
```

Igual que Pi Hole, la instalación advierte que necesitamos una IP estática ya que es un servidor. Nos preguntará si queremos seguir utilizando la IP desde DHCP o si queremos pasarla a estática, elegimos **No** para seguir en estática y de ahí nos dará exactamente los mismos avisos que ya indicó Pi Hole.

Seguimos indicando el usuario que se utilizará, por defecto únicamente tenemos el usuario pi.

Podemos optar por **WireGuard** u **OpenVPN**, este último ha sido el gran vencedor durante años (o décadas), pero WireGuard es más práctico si no queremos complicarnos la vida, además de ser más rápido.

El puerto por defecto de WireGuard es el **51820**, deberemos configurarlo en el router para publicarlo a Internet y redirigir a la Raspberry.

Debido a que tenemos Pi Hole, PiVPN le puede utilizar como servidor DNS, lo que es más que ideal.

Una VPN requiere una IP estática a la que conectarse o un dominio en Internet si esta IP va a cambiar, sea como sea, esa IP o dominio no pueden cambiar nunca ya que este está dentro del fichero de configuración de cada cliente.

Antes de terminar, nos avisará de que deberíamos tener activas las actualizaciones automáticas, estas no reiniciaran la Raspberry, únicamente actualizará automáticamente los servicios, lo que es buena idea para evitar problemas de seguridad, ya que la Raspberry estará publicada a Internet.

Después de reiniciar, la VPN estará disponible, únicamente debemos publicar el puerto **51820/UDP** a Internet hacia la IP de la Raspberry.

## Usuarios
Con la VPN ya creada, tenemos que generar los nuevos usuarios, lo podemos hacer desde los comandos de PiVPN.

```bash
pivpn add --name <userName>
```

Al terminar, tendremos el fichero de configuración en ``/home/pi/configs``, también podemos generar un QR y enviarlo, más sencillo para dispositivos móviles: ``pivpn -qr``.