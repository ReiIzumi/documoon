---
id: config-ntp
title: NTP
slug: ntp
sidebar_position: 4
---

## Introducción
Cada uno de los nodos se conectará a un NTP externo para sincronizarse y ofrecerá esta sincronización a nuestra red, para ello deberemos configurar cada servidor para utilizar nuestro NTP y no los que tiene por defecto.

NTP es muy importante para una Raspberry o similar, ya que, al perder la electricidad, no tiene forma de mantener la hora correcta. Después de iniciar, hacen una petición para actualizar su hora, así que durante unos instantes veremos que su hora es incorrecta.

## Configuración
Después de varias pruebas, he decidido modificar el servicio NTP de Raspbian por el que conozco mejor, así que lo primero es instalarlo.

```bash
sudo apt update
sudo apt install ntpsec -y
```

Probamos de hacer una sincronización y revisar que esté conectando a algún servidor (el que empieza por ** * ** es el servidor al que se ha conectado).

```bash
sudo ntpd -u pool.ntp.org
ntpq -p
```

Debemos elegir unos servidores a los que se conectará, por defecto nos crea el pool de Debian, pero es mejor idea utilizar los que tengamos más cercanos, algo que podemos ver en la [web oficial](http://support.ntp.org/bin/view/Servers/NTPPoolServers). En mi caso utilizo los de Europa.

```bash
sudo vi /etc/ntpsec/ntp.conf
```

Además de indicar el nuevo pool, debemos cambiar el **restrict** para permitir peticiones de cualquier servidor en la red.

```vim title="/etc/ntpsec/ntp.conf"
pool 1.europe.pool.ntp.org
pool 2.europe.pool.ntp.org
pool 3.europe.pool.ntp.org

#restrict 127.0.0.1
#restrict ::1	
restrict 192.168.1.0/24
```

Después de reiniciar ya estará funcionando el servicio.
```bash
sudo systemctl restart ntpsec
```

:::important
El servicio de DNS es muy sencillo ya que se aplica automáticamente por DHCP, pero NTP debe ser configurado manualmente en cada uno de nuestros dispositivos.
:::
