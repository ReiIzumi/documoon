---
id: config-pihole
title: Pi Hole
slug: pihole
sidebar_position: 2
---

## Introducción
[Pi Hole](https://pi-hole.net) es un proyecto todo en uno: DNS, DHCP, bloqueador de DNS, monitor de DNS y todo en una bonita y simple web. ¿Qué más podríamos pedir? Que sea gratis, ¡lo es! Estos proyectos hacen que la vida sea algo mejor (aceptan donaciones, por cierto).

En ambos nodos desplegaremos Pi Hole como DNS y uno de ellos tendrá el DHCP donde indicaremos que envíe las 2 IP como DNS, así si uno de ellos falla, los DNS seguirán funcionando por el otro y deberemos activar el DHCP en el otro nodo.

:::tip
Si reiniciamos el nodo con DHCP por mantenimiento, no será necesario activar el DHCP del otro nodo, únicamente será necesario si estará más de 24h no disponible o si nuevos dispositivos se van a añadir justo en ese momento.
:::

## Instalación
Ejecutamos el instalador y este nos mostrará un asistente.

```bash
sudo curl -sSL https://install.pi-hole.net | bash
```

La instalación nos avisará de que necesitamos IP estática, ya que es un servidor, ya lo hemos configurado así que seguimos adelante. En caso de que pregunte qué tarjeta de red utilizar, será ``eth0``.

Debemos elegir qué tipo de DNS externa queremos utilizar para resolver los nombres en Internet, en mi caso utilizo las de **OpenDNS**.

Pi Hole se conecta a un proyecto para descargarse la lista de DNS con spam, lo mantenemos activo.

Únicamente estamos utilizando IPv4, pero podemos dejar activa la resolución para este y IPv6 por si en un futuro hiciera falta.

Nuevamente nos mostrará la IP y avisará de que, si esta proviene de DHCP, podríamos tener problemas ya que la convertirá a estática. No es el caso, así que seguimos.

Para gestionar Pi Hole, nos da la opción de instalar una consola UI, que vamos a necesitar más adelante, así nos hará la vida más sencilla y la reutilizaremos para otras cosas. Este requiere instalar lighttpd.

Finalmente preguntará si queremos registrar las consultas y el nivel de estas, es posible cambiarlas en cualquier momento, así que no es preocupante. Aun así, debemos tener cuidado según donde estemos registrando y a qué nivel, en una empresa podría no ser legal registrar todo.

A partir de aquí, el instalador hará su magia, ¡así de fácil! Al terminar podremos acceder mediante su IP o por resolución de nombres si nuestra PC está usando la Raspberry como DNS:

:::caution
Al terminar, nos indicará la contraseña de administración, si la perdemos se puede cambiar con ``pihole -a -p``.
:::

* http://pi.hole/admin
* http://192.168.1.200/admin

## Configuración
Desde la UI podemos ver un resumen del estado, este únicamente muestra los clientes, cantidad de peticiones y bloqueadas, para ver y configurar todo se requiere hacer login.

Pi Hole permite bloquear DNS a nivel de rangos de IP, dominios, etc, pero aquí únicamente me centraré en crear nuevas DNS y configurar el DHCP, para cualquier otra configuración, utilizar la [documentación](https://docs.pi-hole.net).

### DNS
En **Local DNS** tenemos 2 opciones:
* **DNS Records**: Permite definir un nombre a una IP.
* **CNAME Records**: Permite definir otro nombre a uno existente, esto es buena idea por ejemplo para definir un DNS que apunte a un servicio, y crear nuevos nombres hacia ese para cada servicio que publica, si un día cambia la IP, únicamente habrá que ajustarlo en el DNS original.

Configurarlos no tiene misterio alguno, en ambos casos debemos indicar el nuevo nombre (si está repetido nos mostrará un error por duplicado) y la IP o el nombre al que debe redireccionar.

### DHCP
:::note
PiHole no está pensado para utilizar una DNS secundaria, aunque se puede modificar. Si nuestro router tiene las mismas capacidades que PiHole, quizás sea conveniente utilizarlo.
:::

Para poder utilizar el DNS, necesitaremos que el DHCP indique que la IP de ambas Raspberrys son el DNS al que preguntar, debido a que los routers que "regalan" las compañías suelen tener esto bloqueado, Pi Hole tiene un servidor DHCP propio, este además permite asignar IP fijas a un dispositivo según su MAC, lo que es muy práctico para tener mejor control de toda la red.

Antes de nada, hay que recordar que solo puede existir un DHCP, así que antes de activar este, habría que desconectar el del router, y únicamente una Raspberry lo debe tener activo.

Debemos rellenar los siguientes datos:
* Activar **DHCP server enabled**.
* Indicar el rango de IPs que se reserva para el DHCP.
* La IP del router, que normalmente es 192.168.1.1.
* El **Pi-Hole domain name** es el dominio de nuestra intranet, en todos estos ejemplos utilizo **domain.intranet**, donde domain se debería cambiar por el nombre que tendrá la red. Estos dominios permiten hacer búsquedas DNS sin añadir el dominio, por ejemplo, si tenemos ``router.domain.intranet``, podríamos hacer un ping a ``router`` y funcionaría (aunque depende del sistema operativo).
* Por defecto, cada 24h los dispositivos deben renovar la IP, no es aconsejable asignarlo a infinito ni a tiempos mayores a 1 semana.

Una vez lo tengamos activo, deberemos esperar a que caduque la IP de cada dispositivo y pregunte por una nueva IP, momento en que responderá Pi Hole, este además irá mostrando la información de las IPs asignadas, así que, si no tenemos la MAC de un dispositivo, la podremos ver aquí y aprovechar para cambiarlo a una IP estática.

En un formato óptimo, todos los dispositivos de una red deberían tener una IP estática asignada desde el DHCP, dejando el rango de IPs dinámicas para dispositivos externos (típico móvil o tablet de un familiar).

:::important
Las IPs estáticas no deberían estar en el mismo rango que el rango que hemos configurado para el DHCP.
:::

PiHole únicamente se asigna a si mismo como servidor de DNS, así que debemos modificar esto para asignar ambas y no es posible hacerlo desde la UI, se debe modificar en la configuración.

Para evitar que se sobre escriba por PiHole, creamos un nuevo fichero.
```bash
sudo vi /etc/dnsmasq.d/99-dns.conf
```

En este debemos indicar la IP de ambos servidores de DNS.
```vim title="/etc/dnsmasq.d/99-dns.conf"
dhcp-option=option:dns-server,192.168.1.5,192.168.1.6
```

Reiniciamos para aplicar los cambios.
```bash
sudo /etc/init.d/pihole-FTL restart
```

## SSL
Aunque sea un servicio interno, me gusta que todo funcione por HTTPS, así que habrá que generar unas claves de acceso.

Primero deberemos generar los ficheros, escribí [una guía aquí](/blog/2021/02/21/certificados-autofirmados-para-intranet), después copiamos el **key** y **crt** a **/etc/lighttpd**.

Lighttpd es el servidor web que utiliza Pi Hole, y este utiliza ficheros pem, así que unificamos ambos.
```bash
cat file.key file.crt > file.pem
sudo mv file.pem /etc/lighttpd
```

Ahora habrá que reconfigurar el servicio.
```bash
sudo vi /etc/lighttpd/external.conf
```

Esto habilitará el SSL, obligando la redirección del 80 al 443. Recordar actualizar el nombre del host y añadirlo como DNS, el certificado únicamente será válido usando ese nombre, no por IP.

```vim title="/etc/lighttpd/external.conf"
server.modules += ( "mod_openssl" )
$HTTP["host"] == "serverName.domain.intranet" {
  # Ensure the Pi-hole Block Page knows that this is not a blocked domain
  setenv.add-environment = ("fqdn" => "true")

  # Enable the SSL engine with a LE cert, only for this specific host
  $SERVER["socket"] == ":443" {
    ssl.engine = "enable"
    ssl.pemfile = "/etc/lighttpd/file.pem"
    ssl.honor-cipher-order = "enable"
    ssl.cipher-list = "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH"
    ssl.use-sslv2 = "disable"
    ssl.use-sslv3 = "disable"       
  }

  # Redirect HTTP to HTTPS
  $HTTP["scheme"] == "http" {
    $HTTP["host"] =~ ".*" {
      url.redirect = (".*" => "https://%0$0")
    }
  }
}
```

Finalmente reiniciamos para aplicar los cambios.
```bash
sudo service lighttpd restart
```

## Resolución DNS interna
Después de varios días utilizando Pi Hole, algunas veces me ha pasado que este ha dejado de utilizarse a si mismo.

En estos casos lo más efectivo es reajustarlo en la configuración de red, que ya habíamos modificado previamente.

```vim title="/etc/dhcpd.conf"
static domain_name_servers=127.0.0.1
```

Después de reiniciar, volverá a funcionar.
```bash
sudo reboot
```

## Sincronización
Pi Hole no dispone de un sistema para sincronizar ambos nodos, así que se deberán replicar los cambios en ambos nodos o importar y exportar, dependerá de qué sea más rápido. Este último se encuentra **Settings > Teleporter**.

La importación nos permite elegir qué debemos importar, pero en condiciones normales, podremos importar todo. De la sección de **Settings**, únicamente importa la lista de DHCP estáticos, pero no su configuración, así que no iniciará este servicio.

:::caution
Después de hacer una importación, el servicio parece no conseguir reiniciar y las opciones para ello en la UI no me han funcionado, requiriendo reiniciar a mano con el comando ``pihole restartdns``.
:::

## Actualización
La UI nos mostrará si hay actualizaciones, pero se puede comprobar por comandos para automatizarlo en otro proceso:

```bash
pihole -v
```

La actualización no requiere ningún cambio, únicamente lanzar el proceso.
```bash
pihole -up
```
