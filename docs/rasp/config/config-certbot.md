---
id: config-certbot
title: Certbot
slug: certbot
sidebar_position: 8
---

## Introducción
Tras añadir certificados en el [Ingress de K8s](/docs/k8s/system/cert-manager), no era demasiado añadir lo mismo a los servicios web de la Raspberry.

En este caso, Let's Encrypt se gestiona a partir de [Certbot](https://certbot.eff.org/) vía comandos y también permite el uso de [validación con DNS01](/docs/k8s/system/cert-manager#dns01).

## Instalación
Antes de instalar, hay que revisar los plugins disponibles para encontrar el válido según nuestro registrador de dominio.
```bash
apt-cache search certbot
```

En mi caso es Cloudflare.
```bash
sudo apt install -y certbot python3-certbot-dns-cloudflare
```

:::tip
Cada plugin tiene un fichero y argumentos diferentes, así que habrá que ajustar estos comandos según su documentación.
:::

Creo el fichero que contiene el token [previamente creado](/docs/k8s/system/cert-manager#cloudflare).
```bash
mkdir certbot
vi /home/user/cerbot/cloudflare.ini > 
```

```vim title="cloudflare.ini"
# Cloudflare API token used by Certbot
dns_cloudflare_api_token = <token>
```

Tras esto se activa con la información del registrador del dominio, el dominio que se desea utilizar y ajusto a los paths ya que no ejecuto como root.
```bash
certbot certonly -n \
  --config-dir /home/user/certbot \
  --work-dir /home/user/certbot \
  --logs-dir /home/user/certbot \
  --agree-tos \
  --dns-cloudflare \
  --dns-cloudflare-credentials /home/user/certbot/cloudflare.ini \
  --dns-cloudflare-propagation-seconds 180 \
  -d rasp.intranet.domain.cat \
  -m webmaster@domain.cat
```

:::tip
Por defecto se crean certificados de tipo `ECDS` que son válidos con lighttpd. Si los queremos reutilizar en otro servicio, es posible que solo se acepten los `RSA`.

Si es el caso, añadir el siguiente argumento: `--key-type rsa`
:::

:::info
DNS01 puede usar wildcards, así que es posible cambiar `rasp` por `*` para reutilizar estos certificados en otro lugar.
:::

Tras un tiempo de espera, se crearán los certificados y se registrará para auto renovarlos.


## Lighttpd
Tras añadir los certificados, solo queda actualizar la configuración original para utilizar el nuevo dominio y certificados.
```bash
sudo vi /etc/lighttpd/external.conf
```

```vim title="external.conf"
$HTTP["host"] == "rasp.intranet.domain.cat" {

#    ssl.pemfile = "/etc/lighttpd/file.pem"
    ssl.pemfile = "/home/pi/certbot/live/rasp.intranet.domain.cat/fullchain.pem"
    ssl.privKey = "/home/pi/certbot/live/rasp.intranet.domain.cat/privkey.pem"
```

Tras reiniciar, probar que todo funciona según lo esperado.
```bash
sudo service lighttpd restart
```
