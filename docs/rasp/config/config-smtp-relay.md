---
id: config-smtp-relay
title: SMTP Relay
slug: smtp-relay
sidebar_position: 5
---

## Introducción
Nagios requiere enviar e-mails para notificarnos de los errores o recuperaciones, y es posible que futuros servicios también lo requieran.

Para reducir configuraciones, se configurará Exim para hacer de ``relay`` hacia nuestro servidor de SMTP, este únicamente reenvia los e-mails, NO es un servidor de e-mail.

Muchos Linux utilizan Postfix, pero Exim requiere menos recursos, algo interesante cuando utilizas una Raspberry.

## Configuración
:::important
Si el usuario que utilizamos para conectar a nuestro servidor de SMTP permite "inventar" nombres, podremos asignar nombres según el nombre del servidor o del servicio, si no, deberemos indicar siempre el nombre del usuario que conecta al SMTP.
:::

Iniciamos instalando el servicio y configurando.
```bash
sudo apt update
sudo apt install exim4 -y
sudo dpkg-reconfigure exim4-config
```

Estas son las opciones que elegir:
```
General type of mail configuration: mail sent by smarthost; no local mail
System mail name: serverName
IP-addresses to listen on for incoming SMTP connections: 127.0.0.1 ; ::1
Other destinations for which mail is accepted: serverName
Visible domain name for local users: serverName
IP address or host name of the outgoing smarthost: <smtp.domain.cat>
Keep number of DNS-queries minimal (Dial-on-Demand): No
Split configuration into small files: No
Root and postmanster mail recipient: mail@domain.cat
```

:::caution
Actualizar las variables de **serverName** acorde al nombre del servidor (lo hace por defecto), **smtp.domain.cat** es el nombre hacia nuestro servidor de SMTP y **mail@domain.cat** es el usuario que se utilizará como **from**.
:::

A partir de aquí debemos asignar la configuración según nuestro SMTP, esta parte puede cambiar, así que es posible que requiera cierta investigación y pruebas hasta dar con la correcta.

```bash
sudo vi /etc/exim4/exim4.conf.localmacros
```

Permitimos contraseñas sin TLS.
```vim title="/etc/exim4/exim4.conf.localmacros"
AUTH_CLIENT_ALLOW_NOTLS_PASSWORDS = 1
```

E indicamos el usuario y contraseña para acceder al servidor de SMTP.
```bash
sudo vi /etc/exim4/passwd.client
```

```vim title="/etc/exim4/passwd.client"
*:mail@domain.com:password
```

Por último, añadimos qué e-mail utilizará cada usuario de sistema, si no lo indicamos, los servicios deberán siempre indicarlo, algo que no siempre hacen.

```bash
sudo vi /etc/email-addresses
```

```vim title="/etc/email-addresses"
pi: mail@domain.cat
root: mail@domain.cat
```

Reiniciamos para aplicar los cambios.
```bash
sudo update-exim4.conf
sudo service exim4 restart
```

## Test
Nunca debemos confiar en que un relay funcionará a la primera, al terminar se debe hacer enviar un e-mail hacia un usuario del dominio (o fuera de este si queremos configurar que tiene permiso para ello).

```bash
echo "Texto de prueba" | mail -s Pruebas mail@domain.cat
```
