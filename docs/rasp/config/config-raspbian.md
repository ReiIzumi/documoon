---
id: config-raspbian
title: Raspbian
slug: raspbian
sidebar_position: 1
---

:::caution
Vamos a necesitar un teclado, monitor/TV y un cable micro HDMI a HDMI para hacer la configuración básica.
:::

## Sistema operativo
Empezaremos copiando el sistema operativo a la micro-SD, para ello la pondremos en cualquier PC con lector y descargamos el [Raspberry Pi Imager](https://www.raspberrypi.org/software/), este es un asistente que permite descargar y preparar el sistema operativo que queramos.

De sistema operativo elegimos **Raspberry Pi OS (other)** > **Raspberry Pi OS Lite (32 bits)**, elegimos la SD y Write. Al terminar quitamos la SD y la insertamos en la Raspberry que ya podrá iniciarse con su nuevo sistema operativo.

Una vez se inicie utilizaremos los datos por defecto:

* **Usuario**: pi
* **Contraseña**: raspberry

## Configuración básica
Antes de activar la conexión remota, prefiero modificar ciertas configuraciones que suelen ser bastante molestas.

Estos pasos habrá que repetirlos en cada nodo.

### Teclado
Por defecto se configura el teclado en inglés, si no es el caso, habrá que cambiarlo.
```bash
sudo vi /etc/default/keyboard
```

En mi caso el teclado está en castellano:
```vim title="/etc/default/keyboard"
XKBLAYOUT="es"
```

:::caution
Para salir y guardar de vi tenemos que escribir :wq, pero el teclado está en inglés, así que el carácter : está en la ñ.
:::

Ya que es molesto tener el teclado en otro idioma, reiniciamos para aplicar este cambio.

```bash
sudo reboot
```

### Actualizar repositorios
Aun no vamos a actualizar, pero sí que necesitamos que se actualicen los repositorios para instalar algunas aplicaciones

```bash
sudo apt update
```

### VIM
Por defecto Raspbian tiene ``vi``, personalmente me gusta más ``vim`` y con algunos arreglos. Instalamos y empezamos con los arreglos.
```bash
sudo apt install vim -y
vi /home/pi/.vimrc
```

Esta configuración permite utilizar el ratón para copiar, activar la sintaxis, cambiar el tabulador a 2 espacios y disponer de búsquedas.

```vim title="/home/pi/.vimrc"
set mouse=
syntax enable
set tabstop=2
set hlsearch
```

Lo necesitaremos también para el usuario root. 
```bash
sudo vi /root/.vimrc
```
```vim title="/root/.vimrc"
set mouse=
syntax enable
set tabstop=2
set hlsearch
```

### IP
Debido a que estos nodos se encargarán del DHCP, deben tener IP estática.

```bash
sudo vi /etc/dhcpcd.conf
```

Debemos asignar su IP en la red, el gateway y las DNS externas, en mi caso voy a utilizar siempre las de [OpenDNS](https://www.opendns.com).

```vim title="/etc/dhcpd.conf"
interface eth0
static ip_address=192.168.1.200/24
static routers=192.168.1.1
static domain_name_servers=208.67.220.220 208.67.222.222
```

Reiniciamos para asignar todos los cambios.
```bash
sudo reboot
```

### Resto de configuraciones
A partir de aquí utilizo el asistente, que para algo lo han hecho.
```bash
sudo raspi-config
```

Asignamos la contraseña y el nombre.
```
1 System Options
	S3 Password
	S4 Hostname
```

Activamos SSH.
```
3 Interface Options
	F2 SSH
		Yes
```

Reducimos la potencia de la GPU al mínimo para ahorrar memoria RAM.
```
4 Performance Options
	P2 GPU Memory
		16
```

Añadimos el castellano como opción de idioma, aunque mantendremos el inglés como idioma por defecto para evitar problemas con logs. También cambiamos el timezone.
```
5 Localisation Options
	L1 Locale
		es_ES.UTF-8 UTF-8
		en.GB.UTF-8
	L2 Timezone
		Europe / Madrid
```

:::tip
A partir de aquí ya podemos conectarnos remotamente con SSH.
:::

## SSH
SSH está habilitado, pero tenemos que añadir un banner y crear los certificados para acceder.

### Banner
El banner es un tema legal, si alguien accede a un servidor que no indica que está prohibido el acceso, entonces será legal que acceda. Además, es bonito.
```bash
sudo vi /etc/ssh/sshd_config
```
Descomentamos la línea de Banner e indicamos la ruta.
```vim title="/etc/ssh/sshd_config"
Banner /etc/ssh/banner
```
Creamos el banner.
```bash
sudo vi /etc/ssh/banner
```
Lo escribimos a gusto:
```vim title="/etc/ssh/banner"
========== ========== ==========
   Welcome to <serverName>
========== ========== ==========
You are accessing a private server
Unauthorized access is not allowed
```
Reiniciamos para que se aplique el cambio.
```bash
sudo systemctl restart ssh
```

### Claves de acceso
Hay muchas opciones para generar las claves, como yo utilizo un Windows, los genero desde [PuTTYgen](https://www.puttygen.com), desde Linux o MacOS hay otras opciones (y también este).

Generamos el fichero con encriptación de **8192 bits**, elegimos un nombre (**Key comment**) para identificarlo más fácilmente cuando tengamos varios y guardamos el **private key** que será el que utilizaremos para acceder.

En el servidor, creamos el fichero y pegamos el **public key**.
```bash
mkdir /home/pi/.ssh
vi /home/pi/.ssh/authorized_keys
```

Una vez terminado, tendremos que configurar nuestro cliente para acceder utilizando ese fichero de claves.

## Bluetooth y Wifi
Este modelo de Raspberry viene con Bluetooth y Wifi, algo que no vamos a necesitar.
```bash
sudo vi /boot/config.txt
```

Desactivamos ambos.
```vim title="/boot/config.txt"
dtoverlay=disable-wifi
dtoverlay=disable-bt
```

Reiniciamos para aplicar los cambios.
```bash
sudo reboot
```

## Alias
Después de tantos años utilizando los mismos alias, para mí son algo esenciales, aunque cada cuál debería asignar los suyos.

```bash
vi /home/pi/.bashrc
```

Descomentamos los siguientes.
```vim title="/home/pi/.bashrc"
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
```

Repetimos lo mismo para root.
```bash
sudo vi /root/.bashrc
```

Añadimos unos cuantos más.
```vim title="/root/.bashrc"
export LS_OPTIONS='--color=auto'
eval "`dircolors`"
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
```

Aplicar los cambios requerirá reiniciar la sesión.

## Actualizar
Antes de terminar, actualizamos el sistema operativo y confirmamos que la BIOS esté actualizada.

```bash
sudo apt update
sudo apt full-upgrade -y
sudo rpi-eeprom-update -a
sudo reboot
```
