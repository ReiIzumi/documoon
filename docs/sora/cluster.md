---
sidebar_position: 2
---

# Clúster

## Preparar los servidores
Para empezar, hay que desplegar uno de los entornos (preferiblemente *Test*, ya que si fallas en muchos lados, siempre puedes tirarlo a la basura y volver a empezar fácilmente).

:::caution[Spoiler]
¡Fallar es el estado natural de todo esto!
:::

La documentación existente de [Proxmox](/docs/it/proxmox/) es más que suficiente, siendo la base:
- Proxmox operativo, ya sea en standalone o en clúster
- [Terraform](/docs/it/proxmox/terraform) configurado para crear máquinas virtuales fácilmente
- Copias de seguridad automáticas dependiendo de los *Resource Pool* definidos

## Terraform
[Este repositorio](https://gitlab.com/ReiIzumi/proxmox-terraform) tiene varias plantillas, incluyendo las necesarias para desplegar el Control Plane, los Node (duplicar el mismo por tantos nodos) y la base de datos con PostgreSQL preconfigurado y con [copias de seguridad](/docs/it/databases/postgresql#copias-de-seguridad).

Lo primero es descargar ambas plantillas y añadir los ficheros de clave para que pueda acceder al servidor de Proxmox. El fichero `terraform.tfvars` tiene un ejemplo de las variables necesarias y el `variables.tf` contiene el esquema.

### Configuración de Proxmox
Ambos requieren los datos para conectar a Proxmox, el token y el fichero de claves (debido a que se requiere subir el snippet).

```hcl
# Proxmox authentication
proxmox_endpoint = {
  endpoint = "https://proxmox-name.intranet:8006/"
  insecure = true
}
proxmox_auth = {
  api_token   = "terraform@pve!provider=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
  username    = "terraform"
  private_key = "private.ppk"
}
```

### Información del servidor
La ISO debe estar previamente descargado (la template 00 es para ello).

:::danger
En caso de tener múltiples nodos en clúster, se debe indicar en cuál se desplegará el servidor. **El private key utilizado en el paso anterior debe poder conectar a ese nodo**.
:::

```hcl
# Proxmox config
proxmox_config = {
  node_name         = "proxmox-name"
  snippet_datastore = "local"
  vm_datastore      = "local-zfs"
  iso               = "store-name:iso/ubuntu-24.04-minimal-cloudimg-amd64.img"
}
```

### Datos básicos del servidor
Indicar el `Resource Pool` correcto para asociarse a las copias de seguridad, su nombre y demás detalles.
```hcl
vm = {
  pool_id       = "test"
  name          = "controlplane-name"
  description   = "Kubernetes Control-Plane - Test environment"
  tags          = ["k8s", "control", "test"]
  startup_order = "1"
  user_name     = "user"
  public_key    = "test.pub"
}
```

### Hardware
Ajustar el hardware según el entorno e indicar la MAC. Pi Hole (o cualquier servidor DHCP) tendrá que asignar la IP según la MAC asociada.
```hcl
vm_hardware = {
  cpus           = 2
  memory         = 4096
  disk_root_size = 50
  mac_address    = "00:00:00:00:00:00"
}
```

:::tip
El nodo tiene el segundo disco utilizado para longhorn `disk_longhorn_size = 75`
:::

### Servicios externos
Datos del servidor de NTP y Nagios para sincronizar y monitorizar el servidor.
```hcl
vm_ntp = {
  ntp_primary   = "ntp1.domain.intranet"
  ntp_secondary = "ntp2.domain.intranet"
}

vm_nagios = {
  nagios_server = "nagios1.domain.intranet,nagios2.domain.intranet"
}
```

### Versión de Kubernetes
El proceso extrae este dato para asignar el repositorio de CRI-O y Kubernetes.
```hcl
k8s_version = "v1.31"
```

:::caution
**CRI-O y Kubernetes DEBEN tener la misma versión** (utilizan semver, `x.y.z` donde `x.y` debe ser igual, `z` no es relevante). Cuando sale una versión nueva de Kubernetes, CRI-O suele tardar unos días o semanas hasta tener disponible la actualización, así que mucho ojo. El despliegue se quedará bloqueado eternamente o devolverá un error si CRI-O no está disponible para la versión indicada.
:::
