---
sidebar_position: 1
---

# Introducción
## Antes de empezar
Para embarcarse en un proyecto, primero hay que entenderlo, así que *¡sofá, palomitas, y acción!*

Podría decir que cada proyecto ha sido tras entender la evolución de los sistemas y querer probarlo en real. No en un modo "doméstico", quería replicar (siempre que fuera posible) una versión empresarial.

Bajo esa idea, empecé el [Swarm Project](/docs/swarm/introduction) con intención de hacerlo evolucionar hacia el [K8s Project](/docs/k8s/introduction). Y evolucionó antes de lo esperado debido a la explosión de Docker Swarm. El destino iba a ser el mismo.

**Estos proyectos se basan en tener suficiente tiempo para probar y entender, hasta cierto nivel, pero la cruda realidad es que esto no siempre es posible. Las explosiones del día a día y los mantenimientos manuales acaban siendo un dolor de cabeza.**

Así que podríamos separar en dos tipos de evoluciones:
- Aquellas que quieres hacer.
- Aquellas que necesitas hacer.

La diferencia es sencilla y compleja a la vez. **Quieres evolucionar un sistema**, y por tanto lo llevas a cabo. **Si no se logra, puedes evolucionarlo hacia otro lado o simplemente abandonarlo**. Por otro lado, están **aquellas que necesitas** para reducir las explosiones, o para tener algo de descanso. **Abandonarlas es posible, pero seguirás teniendo los problemas actuales**.

Mientras que los dos proyectos mencionados anteriormente eran algo que quería hacer para mejorar mi red y aprender, otros (como [Proxmox](/docs/it/proxmox/)) se volvieron una necesidad tras explosiones continuas y la dificultad de uso.

:::tip
¿Sabes lo que es tirar 20 minutos de tu vida cada vez que quieres preparar una VM desde 0 con toda la base de la red? Pues ahora multiplica por un clúster.
:::

Y de aquí empieza el ideal hacia este nuevo proyecto. Algo que aprendí tras desplegar y mantener el K8s Project, es que **no era viable actualizar manualmente el sistema**. Algunos servicios simplemente explotan tras su actualización (no siempre es fácil encontrar las información de ello, aun menos cómo solucionarlo), otros simplemente actualizan tanto que no tienes tiempo de mantenerte al día.

:::warning
Por supuesto, puedes no actualizar, igual que hacen la mayoría de las empresas. Pero aquí la idea es acercarse a un sistema lo más "ideal" posible. Para mierda, el mundo está lleno. Por favor, no incluyamos más 😃.
:::

Entonces, ¿qué es necesario esta vez? Varios puntos:
- Simplificar la creación de máquinas virtuales según el sistema de Kubernetes. Esto sirve tanto para desplegar nuevos entornos como actualizar nodos existentes (asociar uno nuevo y destruir el anterior, nada de actualizar el pre-existente).
- Desplegar clones de los servicios. Si explotan, poderlos regenerar fácilmente (hasta cierto límite) o para desplegar uno nuevo.
- Mantener cierto nivel de actualizaciones. Automáticas, por favor.
- Todo lo anterior, dentro del control de la red existente.

Así, sin dolor.

Para lograr esto, había que dividir y vencer en cada punto. Y ya sabemos cómo funciona esto, cuando consigues algo, encuentras que la meta se ha movido unos metros más allá.

:::info[Sora Project]
El nombre de este proyecto viene de la palabra japonesa *sora*, la cual significa cielo. Tras mucho tiempo caminando alrededor de edificios en llamas, este proyecto intenta poder despegar hacia el cielo. Eso no significa que los incendios se apaguen, ¡pero te da cierto descanso de ellos!
:::

## Arquitectura
Para plantear el nuevo proyecto, primero hay que asentar la idea de todo lo que está alrededor, ya que este proyecto no solo no deberá solucionar ciertos puntos, requerirá utilizar lo existente.

- La base es construir el sistema acorde a **[IaC](https://es.wikipedia.org/wiki/Infraestructura_como_código)** para simplificar su mantenimiento.
- Virtualización para gestionar **máquinas virtuales**. Estas tienen copias de seguridad y capacidad para crear *snapshots* por defecto.
- Los **servidores NO se actualizan** a no ser que exista una razón de peso. Se crea un nuevo servidor, se asocia al grupo preexistente y se borra el anterior.
- **CRI-O + Kubernetes + Calico** siguen siendo las bases del clúster tal como proceden del [K8s Project](/docs/k8s/introduction).
- Mantener un **entorno de producción y uno de test**, siendo el segundo un clon prácticamente idéntico al primero.
- [Pi Hole](/docs/rasp/config/pihole) se encarga de gestionar la **DNS y asignar IP según la MAC**. La DNS es la utilizada dentro de la red, pero también se encarga del dominio para uso interno definida para algunos servicios con Let's Encrypt. En mi caso, el sub-dominio `intranet.moon.cat` que únicamente existe dentro de la red.
- Los servidores siguen bajo **[Ubuntu](/docs/it/ubuntu22lts)**, sincronizan con el **[NTP](/docs/rasp/config/ntp)** local y son monitorizados por **[Nagios](/docs/rasp/config/nagios)**.
- Un NAS con QNAP se encarga de recibir copias de seguridad y datos. Acepta protocolos **NFS y S3** con certificado válido (bendito [Let's Encrypt vía DNS](/docs/k8s/system/cert-manager#dns01-con-cloudflare) con ciertos ajustes).

:::danger
Pi Hole debe tener las DNS e IPs previamente configurados. Este proyecto no se encarga de ello.
:::

## Aviso
Como he indicado al principio, Sora Project **NO plantea un sistema doméstico**, se enfoca en una **ideología empresarial**. Esto significa que no es útil para montar un sistema en casa con pocos recursos. Puedes plantear sistemas mucho más simplistas (incluso con una base similar a esta) y con menos hardware.

Este proyecto tiene tantas piezas que no es viable explicar el funcionamiento de cada una, así que **conocer las tecnologías básicas utilizadas es necesario** para entender este proyecto.

## Tecnología empleada
Las bases para el proyecto son:
- **Motor de virtualización**: Proxmox
- **Creación de servidores**: Terraform
- **Sistema Operativo**: Ubuntu
- **Gestión de servicios**: Kubernetes
- **Mantenimiento de servicios**: FluxCD

Aparte de ello están los servicios originales del K8s Project para publicar webs, mantener volúmenes, monitorizar el sistema ... y otros que han sido absorbidos hacia este como las bases de datos.

## Hardware
Los entornos son espejos, pero *test* tiene menos hardware:

| Entorno    | Cantidad | Tipo          | CPU | Memoria | Disco 1 | Disco 2 |
| ---------- | -------- | ------------- | --- | ------- | ------- | ------- |
| Test       | 1        | Control Plane | 2   | 4Gi     | 50Gi    | -       | 
| Test       | 4        | Node          | 2   | 4Gi     | 50Gi    | 75Gi    | 
| Test       | 1        | Database      | 2   | 4Gi     | 100Gi   | -       | 
| Producción | 1        | Control Plane | 4   | 6Gi     | 50Gi    | -       | 
| Producción | 4        | Node          | 4   | 8Gi     | 50Gi    | 150Gi   |
| Producción | 1        | Control Plane | 4   | 6Gi     | 100Gi   | -       | 

*Aunque inicialmente el proyecto está escalado y da soporte para tener las bases de datos SQL dentro del clúster, hasta la fecha no he conseguido que este realmente funcione bajo bare-metal con los servicios que tengo disponibles. Por ello he añadido un servidor extra, fuera del clúster, para ello.*

::::info
Los Node disponen de un segundo disco usado para volúmenes gestionados por Longhorn. Este crea una RAID de 3, así que el espacio final no es la suma de todos.

:::warning
Debido a que Longhorn requiere de 3 nodos para su funcionamiento mínimo, es altamente aconsejable tener al menos 4 nodos, ya que, si uno se detiene, Longhorn entrará en estado crítico. Tocar la moral a Longhorn es un suicidio y tienes todas las de perder ...
:::
::::


Esto nos da una suma total de:

| Entorno    | Cantidad | CPU | Memoria | Disco  |
| ---------- | -------- | --- | ------- | ------ |
| Test       | 6        | 12  | 24Gi    | 650Gi  | 
| Producción | 6        | 24  | 44Gi    | 950Gi  |
| Total      | 12       | 30  | 68Gi    | 1600Gi | 

Una cantidad nada despreciable, donde diría que únicamente los discos secundarios de Test pueden verse ligeramente disminuidos (y solo si sabes bien lo que haces).

::::info[CPUs]
Aunque el sistema utiliza muchas CPUs, la realidad es que prácticamente nunca las utiliza al 100%. Esto nos permite no reservar ese hardware, al mismo tiempo que añade el riesgo de saturaciones totales del servidor físico cuando varios nodos requieren todo su potencial. Un riesgo que no queda otro que asumir.

:::caution
Proxmox parece quedarse bloqueado si algo llega a superar estos límites. *Poner al límite Longhorn tiene ese peculiar efecto secundario.*
::::

:::danger
Debido a que las bases de datos están fuera del clúster, se puede reducir el hardware utilizado. Aun así, haría falta cierto tiempo y datos de monitorización para encontrar los valores correctos. Nada nunca es fácil cuando se quiere ahorrar 😢.
:::

:::danger[SSD]
Olvídate de usar HDD. SSD o no funcionará.
:::
