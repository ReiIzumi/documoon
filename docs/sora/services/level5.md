---
sidebar_position: 5
---

# Level 5

## Servicios
- **[Keycloak](/docs/k8s/management/keycloak)**: Identity Provider para la autenticación y autorización de los servicios

## Keycloak
Keycloak requiere configurar Ingress y el usuario por defecto.

```yaml title="keycloak-values.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: keycloak-values
    namespace: keycloak-system
stringData:
    values.yaml: |-
        auth:
          adminUser: admin
          adminPassword: replace-me
```

Los datos se almacenan en PostgreSQL. Los datos de conexión se almacenan en un *secret*.

```yaml title="keycloak-pg-auth.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: keycloak-pg-auth
    namespace: keycloak-system
stringData:
    values.yaml: |-
        backups:
          s3:
            bucket: replace-me
            accessKey: replace-me
            secretKey: replace-me
```
