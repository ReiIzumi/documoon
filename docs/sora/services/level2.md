---
sidebar_position: 2
---

# Level 2

## Servicios
- **[Ingress](/docs/k8s/system/ingress)**: Permite publicar webs hacia fuera del clúster.
- **[Cert-Manager](/docs/k8s/system/cert-manager)**: Añade certificados a los Ingress publicados. En este caso, mediante Let's Encrypt.

## Ingress
Ajusta las DNS previamente permitidas por el MetalLB.

Se despliega un Ingress para publicar hacia internet (Extranet) y otro para la red interna (Intranet).

## Cert-Manager
La publicación mediante DNS requiere que conecte con tu dominio y ajuste datos para hacer la validación. Estos son necesarios para la Intranet (y también para "falsas" Extranet si no están publicadas a Internet, como los entornos de Test).

El token se configura en un secret.

```yaml title="cluster-issuer-dns-secret.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: cloudflare-api-token-secret
    namespace: cert-manager-system
type: Opaque
stringData:
    api-token: replace-me
```
