---
sidebar_position: 1
---

# Level 1

## Servicios
- **[MetalLB](/docs/k8s/system/metallb)**: Asigna valores a los *services*  de tipo *LoadBalanced* como los utilizados por Ingress.
- **CNPG Operator**: Operador para desplegar PostgreSQL con los CRD de [CloudNativePG](https://cloudnative-pg.io/).
- **[Email Relay](/docs/k8s/management/email-relay)**: Para evitar configurar las credenciales del e-mail en cada servicio, este servicio se encarga de hacer de intermediario, bloqueando el acceso hacia el mediante Kubernetes.
- **Notificaciones**: Envía notificaciones en Slack según los cambios en FluxCD.

## MetalLB
Define las IPs utilizadas por Ingress y otras para futuros servicios.

El rango de IPs se configura mediante CRDs, así que están en la carpeta de `configs`.

## CNPG Operator
Sin cambios. Prepara el Operador y los CRD para desplegar y configurar los PostgreSQL

## Email Relay
Encriptar el secret con los datos para conectar al servidor smtp.

```yaml title="smtp-relay-values.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: smtp-relay-values
    namespace: email-relay-system
stringData:
    values.yaml: |-
        smtp:
            host: replace-me
            username: replace-me
            password: replace-me
            mynetworks:
            - 127.0.0.0/8
            - 0.0.0.0/0
```

## Notificaciones
Todas las notificaciones se configuran desde `configs`. Este requiere los datos definidos en Slack (URL, canal, ...) además del token que se registra en un secret.

```yaml title="notification-provider-secret.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: slack-token
    namespace: flux-system
stringData:
    token: replace-me
```

:::note
Envía notificaciones y errores de los datos de FluxCD configurados en su namespace, pero no del resto. Se requiere una configuración de notificaciones en cada namespace.
:::
