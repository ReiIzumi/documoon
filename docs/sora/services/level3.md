---
sidebar_position: 3
---

# Level 3

## Servicios
- **[Longhorn](/docs/k8s/system/volumes/longhorn)**: Gestión de volúmenes y copias de seguridad.
- **Weave GitOps**: UI para la configuración de FluxCD.

## Longhorn
Longhorn tiene dos puntos importantes:
- Su UI permite configurar cualquier punto, pero no tiene seguridad para acceder. Para ello se configura un usuario directamente en Ingress.
- Lo importante de los volúmenes es tener copias de seguridad, y cualquier volumen creado automáticamente sigue la configuración por defecto. Lo más eficiente es configurar la ruta NFS y el temporizador mediante los CRD.

Ingress requiere un formato específico para definir el usuario:
```bash
USER=foo; PASSWORD='your password'; echo "${USER}:$(openssl passwd -stdin -apr1 <<< ${PASSWORD})" >> auth
```

Copiar el contenido de `auth` dentro del secret:

```yaml
apiVersion: v1
kind: Secret
metadata:
    name: longhorn-auth
    namespace: longhorn-system
type: Opaque
stringData:
    auth: replace-me
```

## Weave GitOps
Esta UI requiere configurar el Ingress y la autenticación. No requiere el usuario y contraseña como en otros casos, utiliza un `hash`.

Una opción es utilizar su propio comando.

```bash
brew tap weaveworks/tap
brew install weaveworks/tap/gitops
```

Tras instalarlo, este sería un ejemplo con Unix:
```bash
PASSWORD='your password'; echo -n $PASSWORD | gitops get bcrypt-hash
```

Tras crear el `hash`, se define en el secret.
```yaml
apiVersion: v1
kind: Secret
metadata:
    name: weave-gitops-values
    namespace: flux-system
stringData:
    values.yaml: |-
        adminUser:
          create: true
          username: admin
          passwordHash: replace-me
```
