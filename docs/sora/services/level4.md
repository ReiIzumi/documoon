---
sidebar_position: 4
---

# Level 4

## Servicios
- **[Prometheus](/docs/k8s/management/monitoring)**: El sistema principal de monitorización.
- **Grafana**: Dashboard que unifica diferentes aplicaciones de monitorización.
- **Grafana Loki**: Almacena los logs de todos los pods para mostrarlos mediante Grafana.

## Prometheus
Este Helm despliega varios servicios para monitorizar el clúster, no únicamente a Prometheus.

La configuración principal limita el espacio máximo y tiempo de vida de los datos que almacena.

## Grafana
Aunque el pack de Prometheus dispone de Grafana, este despliega el Operator, lo que nos permite configurarlo mediante CRD.

Mediante `configs` se despliega y configura. Este tiene una configuración especial para definir el usuario por defecto mediante un *secret*.

```yaml
apiVersion: v1
kind: Secret
metadata:
    name: grafana-secrets
    namespace: grafana-system
stringData:
    GF_SECURITY_ADMIN_USER: admin
    GF_SECURITY_ADMIN_PASSWORD: replace-me
```

:::tip
Este es el punto ideal para configurar los tipos de dashboards y las métricas tanto internas como a sistemas externos.
:::

## Grafana Loki
No voy a engañar a nadie, *Loki* es un muy buen nombre para este servicio. Te hace creer que podrás controlar los logs unificados en un único sitio, sin configurar nada. ¡Mentira!

*Loki* es uno de los procesos que más hardware consume, no siempre tienes claro si hace lo que crees que le has configurado (seguramente no), y además, son 2 servicios en uno: Loki + Promtail. Si Loki falla, el segundo va detrás.

Es debido a ello que la configuración indicada reduce su cantidad de replicas y hardware disponible, y aun así ...

:::danger
Hasta este momento, no he conseguido una configuración aceptable de Loki. Cada cierto tiempo provoca saturaciones en el sistema y apenas nunca es capaz de solucionarlo por sí mismo. Sigo en ello.
:::

Sea como sea, requiere un S3 para almacenar los datos históricos, y estos se indican en un *secret*.

```yaml
apiVersion: v1
kind: Secret
metadata:
    name: loki-values
    namespace: loki-system
stringData:
    values.yaml: |-
        loki:
            storage:
                bucketNames:
                    admin: Test_Loki
                    chunks: Test_Loki
                    ruler: Test_Loki
                s3:
                    endpoint: pyrrhanikos.intranet.moon.cat:8010
                    accessKeyId: replace-me
                    secretAccessKey: replace-me
                    s3ForcePathStyle: true
```

:::caution
En mi caso S3 proviene de QNAP, el cuál es similar a un MinIO, así que la forma de conectar tiene un endpoint diferente al estándar y requiere forzar el uso de paths, ya que por defecto usa subdominio.
:::
