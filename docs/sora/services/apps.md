---
sidebar_position: 6
---

# Apps

## Servicios
Thor services: DocuMoon, Shizen, Toranku y Kirin.

- **[DocuMoon](https://gitlab.com/ReiIzumi/documoon)**: Página principal de Moon.cat
- **[Shizen](https://gitlab.com/ReiIzumi/shizen)**: Shizen es una web generadora de escenarios para Warhammer Fantasy
- **[Toranku](https://gitlab.com/moon-toranku)**: Toranku es un gestor de categorías utilizando mediante una API REST
- **[Kirin](https://gitlab.com/moon-kirin)**: Gestor de economía

## DocuMoon
No requiere más cambios que indicar la URL.

## Shizen
Igual que *DocuMoon*, únicamente requiere la URL. 

## Toranku
Principalmente requiere los datos de OIDC en Keycloak y la conexión hacia PostgreSQL.

El usuario y contraseña se almacenan en un *secret*.
```yaml title="toranku-pg-auth.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: toranku-pg-auth
    namespace: toranku
stringData:
    user: toranku
    password: password
```

## Kirin
Kirin requiere los datos hacia su propio OIDC, además de un Client Id para autenticar la conexión hacia *Toranku*. El secret del Client Id se almacena en un *secret*.
```yaml title="kirin-values.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: kirin-values
    namespace: kirin
stringData:
    values.yaml: |-
        toranku:
          oidc:
            secret: clientid-secret
```

Todos los datos de conexión hacia PostgreSQL se almacenan en un *secret*.
```yaml title="kirin-pg-auth.yaml"
apiVersion: v1
kind: Secret
metadata:
    name: kirin-pg-auth
    namespace: kirin
stringData:
    jdbc-uri: jdbc:postgresql://mioakiyama.moon.intranet:5432/kirin
    user: kirin
    password: password
```
