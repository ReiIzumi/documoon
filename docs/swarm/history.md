---
id: swarm-history
title: Historial
slug: history
sidebar_position: 7
---

Los cambios en el `Swarm Project` se registran en esta sección.

## Versión

### 2021/05/20
Añadido proceso de [copias de seguridad](/docs/swarm/backup)

### 2021/05/09
Primera versión del proyecto.

## Roadmap
* Automatizar arranque de Docker
* Revisar o cambiar ELK
* Añadir los registros de Linux y comandos a ELK
* Revisar dashboards y alertas de Grafana
* Autolimpieza del Runner
