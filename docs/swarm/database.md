---
id: database
title: Base de datos
sidebar_position: 2
---

## Introducción
La gran mayoría de servicios actuales son compatibles con [PostgreSQL](https://www.postgresql.org) lo que la convierte en la base de datos más importante del momento, por eso la he elegido para ser la principal, en todos los servicios donde sea posible elegir, se elegirá siempre PostgreSQL.

Esta es una base de datos gratuita y con unas capacidades que no requiere envidiar al resto, aunque como todo, también tiene sus problemas, principalmente hay que tener cuidado cuando se desee actualizar, ya que se debe actualizar cada base de datos definida a la nueva versión, dejando esto de lado, funciona muy bien.

Aun cuando PostgreSQL está cada vez más extendida, hay veces que no siempre tendremos la opción de elegir, por eso también contaré con [MariaDB](https://mariadb.org) como segunda base de datos, esta únicamente se utilizará en servicios que no tienen otra opción (como Wordpress).

Ambas estarán en la misma VM, ya que la carga de trabajo conjunta difícilmente llegará a sobrecargar la VM, aunque es posible separarlas si se prevé problemas, también es posible hacerlo a futuro.

## Despliegue
En mi caso, ya dispongo de estas desplegadas en una VM con OpenSUSE Leap 15.1, así que obviaré este paso.

Las guías para su despliegue se encuentran en mi blog:
* [Instalación de PostgreSQL en OpenSUSE Leap 15.1](/blog/2020/08/04/instalacion-postgresql-opensuse-leap-15-1)
* [Instalación de MariaDB en OpenSUSE Leap 15.1](/blog/2020/06/02/instalacion-mariadb-opensuse-leap-15-1)

Además, ambas disponen de los procesos para crear [copias de seguridad notificadas con Nagios](/blog/2020/05/01/copias-seguridad-notificadas-nagios) que es reutilizable al Nagios del [`Rasp Project`](/docs/rasp/config/nagios) o a cualquier otro.
