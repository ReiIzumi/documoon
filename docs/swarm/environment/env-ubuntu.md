---
id: env-ubuntu
title: Ubuntu
slug: ubuntu
sidebar_position: 1
---

## Introducción
Todos los nodos que compondrán el clúster tienen la misma versión de sistema operativo: [Ubuntu Server LTS 20.04.2](https://ubuntu.com/download/server).

Las versiones de LTS de Ubuntu suelen tener 5 años de actualizaciones de seguridad, algo que siempre viene bien. Ubuntu además tiene servicios para grandes despliegues, pero en mi caso utilizaré la ISO de toda la vida conectada a la máquina virtual.

## Instalación
Para la instalación seguiré el asistente de Ubuntu

### Idioma
Como recomendación, los sistemas operativos deben ir siempre en `English`, ya que es más sencillo encontrar documentación para configurar o en sus logs.

### Teclado
Aunque el idioma esté en inglés, el teclado puede utilizar otra, en mi caso lo defino en `Spanish`.

### Network connection
En este punto podemos escoger cambiar la IP a estática o asignar una IP prefijada en nuestro DHCP utilizando la MAC del servidor.

Sea cual sea la opción elegida, debemos asegurar que la IP no cambiará nunca.

Si elegimos configurarla en estática, debemos indicar todos los valores, este es un ejemplo:

* **Subnet**: 192.168.1.0/24
* **Address**: 192.168.1.100
* **Gateway**: 192.168.1.1
* **Name servers**: 192.168.1.200 192.168.1.201
* **Search domains**: domain.intranet

### Proxy
Si disponemos de proxy, debemos indicarlo, en caso contrario dejarlo en blanco.

### Mirror address
En una red pequeña es muy difícil que dispongamos de un servidor propio para los repositorios, así que mantenemos el de por defecto.

En caso de que los **servidores no tuvieran acceso a Internet o fuéramos a desplegar muchos Ubuntu en nuestra red, sería bastante importante plantearse tener un repositorio propio**, este evitaría que cada servidor requiriera conectarse a Internet para descargar actualizaciones o nuevas aplicaciones.

### Storage
Vamos a configurar únicamente el primer disco `/dev/sda/`, debido a que utilizo VM y no necesito que el disco esté encriptado, **deshabilito la opción de LVM**.

Para este disco configuraremos los siguientes:

| Tipo      | Montaje | Espacio |
| --------- | :-----: | ------: |
| bios_grub | -       | 1M      |
| swap      | -       | 3G      |
| ext4      | /       | 96,997G |

:::tip
Si dejamos la partición para root `/` para el final, podemos dejar vacío la selección de tamaño y el sistema asignará todo el espacio restante.
:::

### Hostname y usuario
Indicamos el nombre del servidor y el usuario que tendrá acceso.

### SSH Setup
Para poder acceder remotamente, debemos habilitar el SSH, más adelante configuraremos los certificados.

### Server snaps
Ubuntu nos ofrece una lista de servicios para desplegar junto a la instalación, en algunos casos estos son versiones viejas, así que prefiero instalar únicamente aquello que requiero y sabiendo exactamente qué se ha instalado y dónde, por tanto, no habilito ninguno.

### Finalizando instalación
Solo hay que esperar a que termine la instalación, tener en cuenta que Ubuntu requerirá aceptar el reinicio y la expulsión de la ISO al terminar (en VM no puede expulsarlo así que mostrará una alerta), así que aun requerirá de estar presente.

Si disponen de acceso a Internet, aprovechará para actualizarse, podemos dejarlo o indicarle que reinicie y actualizar más adelante.

:::important
Cada uno de los servidores debería tener su nombre registrado en el DNS.
:::

## Configuración
A partir de aquí accedemos a las VM por SSH, debido a que aun no tenemos certificados, nos pedirá la contraseña.

### SSH
Los pasos para configurar el SSH son los mismos que para el `Rasp project`, únicamente cambia la home del usuario, se pueden ver [**aquí**](/docs/rasp/config/raspbian#ssh).

### SWAP
Si en la instalación hemos seguido los pasos, habremos creado una partición para la memoria SWAP, pero Ubuntu genera un fichero de SWAP en el disco (igual que hace Windows), así que anulamos este fichero.

```bash
sudo vi /etc/fstab
```

Aquí veremos una partición para SWAP y un fichero que apunta a `/swap.img`, eliminamos esta última y reiniciamos para aplicar los cambios.
```bash
sudo init 6
```

Cuando vuelva a iniciarse, borramos este fichero para recuperar el espacio en disco.
```
sudo rm /swap.img
```

### Actualización y utilidades
Es preferible en este momento actualizar el sistema y aprovechar para instalar el servicio de NFS que nos hará falta para montar carpetas compartidas.

```bash
sudo apt update
sudo apt upgrade -y
sudo apt autoremove
sudo apt install nfs-common -y
```

:::caution
Ojo con copiar y pegar los comandos que utilizan `apt`, ya que la anidación no suele funcionar, así que se pierden los siguientes.
:::

### UUID message for disks
En mi caso utilizo VMware ESXi como virtualizador, este no genera UUID en los discos, así que Ubuntu llena el log de avisos sobre ello (y debido a que lo vamos a centralizar en ELK, nos llenará el disco de cosas innecesarias).

En este caso es posible o modificar la configuración de cada VM desde el VMware, algo no del todo recomendable, ya que si el día de mañana copiáramos esos discos, deberíamos también modificar su UUID, o simplemente anular estos mensajes.

Editamos el fichero para ello.
```bash
sudo vi /etc/multipath.conf
```

Debemos evitar todos los de disco, este evade muchos más, por si tuviéramos otra estructura.
```vim title="/etc/multipath.conf"
blacklist {
    devnode "^(ram|raw|loop|fd|md|dm-|sr|scd|st|sda|sdb|sdc)[0-9]*"
}
```

### NTP
Siempre debemos configurar la sincronización horaria en todos los servidores.

Empezamos asignando el timezone, instalamos el servicio y lo editamos
```bash
sudo timedatectl set-timezone Europe/Madrid
sudo timedatectl set-ntp no
sudo apt install ntp -y
sudo vi /etc/ntp.conf
```

Cambiamos los servidores de NTP por los de nuestra red.
```vim title="/etc/ntp.conf"
pool ntp1.domain.intranet iburst
pool ntp2.domain.intranet iburst
```

Reiniciamos el servicio para aplicar los cambios.
```bash
sudo systemctl restart ntp
```

### Nagios
Cada servidor será monitorizado por Prometheus/Grafana, aun así, los años de experiencia con Nagios me llaman a seguir usándole a él para notificar servicios que han caído o problemas graves.

El servidor de Nagios fue instalado en el `Rasp project`, así que únicamente tenemos que instalar el cliente en cada Ubuntu y añadir los nuevos comandos y servidores en el servidor tal como se explica [**aquí**](/docs/rasp/config/nagios).

Nagios requiere que el servidor tenga configurado el servidor y plugins de ``NRPE``, además de permitir el acceso al servidor de Nagios.

```bash
sudo apt install nagios-nrpe-server nagios-nrpe-plugin -y
sudo vi /etc/nagios/nrpe.cfg
```

En este fichero se debe indicar la IP o nombres de los 2 servidores de Nagios. La idea es que únicamente uno de los servidores se encargue de gestionar toda la red, pero prefiero habilitarlo para ambos por si más adelante me fuera necesario.

También activaré el ``dont_blame_nrpe`` debido a que lo necesitaré para las copias de seguridad notificadas.
```vim title="/etc/nagios/nrpe.cfg"
allowed_hosts=127.0.0.1,::1,nagios1.domain.intranet,nagios2.domain.intranet
dont_blame_nrpe=1
```

Por defecto NRPE tiene una lista de comandos, pero algunas veces saltan alarmas antes de lo necesario, así que ajusto algunos para permitir más carga antes de considerarlo un error. También modifico el control de los discos duros acorde a cada partición que dispongo, también dejo configurado el de GlusterFS de ``/dev/sdb1`` aunque en este momento aún no está creado.
```vim title="/etc/nagios/nrpe.cfg"
command[check_users]=/usr/lib/nagios/plugins/check_users -w 5 -c 10
command[check_load]=/usr/lib/nagios/plugins/check_load -r -w .80,.75,.70 -c .90,.85,.80
command[check_zombie_procs]=/usr/lib/nagios/plugins/check_procs -w 5 -c 10 -s Z
command[check_total_procs]=/usr/lib/nagios/plugins/check_procs -w 400 -c 600
command[check_bkp]=/opt/scripts/notificationChecker.pl $ARG1$
command[check_disk_root]=/usr/lib/nagios/plugins/check_disk -w 20 -c 10 -p /
command[check_disk_docker]=/usr/lib/nagios/plugins/check_disk -w 20 -c 10 -p /var/lib/docker
command[check_disk_gluster]=/usr/lib/nagios/plugins/check_disk -w 20 -c 10 -p /dev/sdb1
```

Después de reiniciar, será accesibles por los servidores.
```bash
sudo systemctl restart nagios-nrpe-server
sudo systemctl enable nagios-nrpe-server
```

:::info
Los comandos del disco no estaban en la configuración original del ``Rasp Project``, así que se deberán añadir. También hay que tener en cuenta que la comprobación del Gluster fallará hasta crearse la partición.
:::

### Reiniciar
Después de todo el trabajo e instalaciones, es aconsejable reiniciar el sistema.

```bash
sudo init 6
```