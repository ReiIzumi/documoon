---
id: swarm-backup
title: Copias de seguridad
slug: backup
sidebar_position: 6
---

## Introducción
Los volúmenes requieren copias de seguridad, es irrelevante si detrás existe una RAID de software, de hardware, una Cloud, ... requiere copias de seguridad.

Podemos creer que nuestra magnifica RAID es infalible, pero puede pasar de todo, que los datos se corrompan, que alguien borre por accidente (o no) algo importante, que la RAID salga ardiendo, lo que sea.

Los volúmenes se asocian a GlusterFS o NFS. Normalmente los servidores de NFS disponen de copias de seguridad, así que me centraré en el de GlusterFS, aun así, el proceso es el mismo, pero recordar siempre que quien genera la copia de seguridad debería tener los datos localmente para acelerar el proceso.

La idea será crear las copias de seguridad con un `cron`, al terminar se genera un fichero de notificación y `Nagios` irá revisando el contenido para notificar únicamente si ha habido un error (o si el fichero no está). El proceso utilizado es el que expliqué en [copias de seguridad notificadas con Nagios](/blog/2020/05/01/copias-seguridad-notificadas-nagios).

## Descarga
Estos y otros scripts de copias de seguridad se encuentran en [Git](https://gitlab.com/ReiIzumi/scripts-backup).

```bash
git clone https://gitlab.com/ReiIzumi/scripts-backup.git
cd backups
```

## Instalación
Los scripts están configurados para copiarse en `/opt/scripts` aunque la ruta se puede cambiar en su configuración.

Copiamos los ficheros y asignamos permisos.
```bash
sudo mv backups /opt/scripts
sudo chown -R root:root /opt/scripts
sudo chmod u+x /opt/scripts/*.sh
```

No todos los scripts son necesarios, únicamente indicaré aquellos que se van a utilizar, el resto pueden ser borrados.

### Auto montaje de NFS
`mount.sh` se encarga de montar siempre la carpeta compartida de NFS para evitar que los datos acaben en el disco local.

| Clave        | Descripción                         | Valor por defecto   |
| ------------ | ----------------------------------- | ------------------- |
| NFS_SERVER   | URL o nombre del servidor de NFS    | nas.domain.intranet |
| NFS_FOLDER   | Carpeta de destino en NFS           | /BKP_Test           |
| MOUNT_FOLDER | Carpeta local donde se monta el NFS | mnt/backup          |

### Rotación de carpetas
`rotateFiles.sh` no requiere configuración, simplemente rota las carpetas para borrar la más vieja y mantener las nuevas ordenadas por números.

### Notificaciones
`notifyCSV.sh` almacena los datos en un CSV, con este se puede saber el resultado y es el que utilizará otro proceso para indicar a Nagios si ha habido un error o no.

```bash
sudo mkdir /opt/scripts/notification
```

### Copia de seguridad
`backup.sh` este es el fichero principal que llama a todos los demás.

Por cada volumen se debe copiar y configurar, debido a que pueden ser muchos, es mejor organizarlos todos en una carpeta propia.

```bash
sudo mkdir /opt/scripts/backup
sudo cp backup.sh /opt/scripts/backup/bkp_portainer.sh
```

Estas son las configuraciones que permite:

| Clave        | Descripción                         | Valor por defecto   |
| ------------ | ----------------------------------- | ------------------- |
| IDENTIFIER_NAME          | Identificador de la copia, se utiliza para diferenciarla de otras en la carpeta de destino y notificaciones |  |
| SOURCE_FOLDER            | Fichero o carpeta que deben ser guardados |  |
| BACKUP_FOLDER            | Destino de la copia | /mnt/backup/$IDENTIFIER_NAME |
| BACKUP_MAXIMUM_FILES     | Cantidad de copias que se almacenan | 5 |
| BACKUP_ROTATION_SCRIPT   | Script de rotación | /opt/scripts/rotateFiles.sh |
| BACKUP_SEPARATE_FILES    | En caso de querer copiar una carpeta, todo su contenido se almacena en un único fichero, si se quiere crear un fichero por cada fichero o carpeta que este contiene, se debe configurar a true | true |
| BACKUP_COMPRESSION_TYPE  | Tipo de compresor, de más rápido a mejor compresión: gzip, bzip2, xz. Requiere estar instalado en el sistema. | gzip |
| BACKUP_COMPRESSION_LEVEL | Nivel de compresión, de más rápido a mejor compresión: 1 a 9 | 1 |
| BACKUP_FILENAME_DATE     | Fecha añadida al nombre del fichero | date +_%Y-%m-%d_%H-%M-%S |
| AUTOMOUNT_SCRIPT         | Script de auto montaje | /opt/scripts/mount.sh |
| NOTIFICATION_SCRIPT      | Script de notificación | /opt/scripts/notifyCSV.sh |
| LOG                      | Mostrar los logs | true |

### Notificaciones de Nagios
Nagios puede ejecutar cualquier tipo de script, en este caso se utiliza `notificationChecker.pl`, un script en Perl que revisa la existencia y contenido del fichero CSV de notificaciones para que Nagios sepa qué estado debe devolver.

El fichero requiere tener permisos de ejecución por el usuario que utilizará el NRPE, también es posible darles permisos a todos si el servidor tiene suficiente seguridad.
```bash
sudo chmod +x /opt/scripts/notificationChecker.pl
```

Tiene de dependencia `DateTime`, así que debemos instalarlo, lo que requiere un rato bastante largo.
```bash
sudo apt install -y gcc make
sudo cpan install CPAN
sudo cpan install DateTime DateTime::Format::Strptime
```

Antes de configurarlo con Nagios, hay que confirmar que todo funcione, este ejemplo es para confirmar si la copia de seguridad llamada `portainer` se ha creado iniciando a las 2h 5' 0'', después de 5 minutos sin encontrar el fichero, se cambiará a `warning`, después de 10 minutos pasará a `critical`.
```bash
/opt/scripts/notificationChecker.pl portainer 02:05:00 5 10
```

:::important
Si no se ha creado nunca una copia de seguridad no existirá el fichero de notificación y devolverá un `unknown`.
:::

### Cron
Cada script se debe añadir al `cron` para que sea ejecutado automáticamente.
```bash
sudo crontab -e
```

Este es un ejemplo para iniciar la copia de seguridad de portainer a las 2:05.
```vim
05 2 * * * /opt/scripts/backup/bkp_portainer.sh
```

## Nagios
Si se siguieron los pasos de [Rasp Project - Nagios](/docs/rasp/config/nagios) ya estará configurado el comando para comprobar estas notificaciones así que únicamente tenemos que definir la comprobación en cada servidor donde esté desplegado.

Este es un ejemplo para portainer:

```vim
define service {
    use                     local-service
    host_name               Swarm2
    service_description     Backup Portainer
    check_command           check_nrpe_bkp!portainer 02:05:00 5 10
    servicegroups           Backup
    max_check_attempts      1
}
```
