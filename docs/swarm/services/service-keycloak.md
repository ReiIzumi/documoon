---
id: service-keycloak
title: Keycloak
slug: keycloak
sidebar_position: 8
---

## Introducción
Aquellos servicios que permitan OAuth2, se conectarán a [Keycloak](https://www.keycloak.org).

En caso de disponer de un LDAP como el del [Rasp Project](/docs/rasp/config/openldap) lo podemos conectar a Keycloak para unificar todos los usuarios, debido a que hay servicios (como Nexus) que no son capaces de usar OAuth2, aunque no tengamos SSO entre todos los servicios, al menos tendremos el mismo usuario y se podrá gestionar desde Keycloak.

:::important
En caso de ELK y Grafana, la autenticación externa es de pago, algo bastante común en muchas aplicaciones empresariales, estos tendrán usuarios propios.
:::

Se desplegará 2 Keycloaks: uno en la `intranet` y otro en la `extranet`, ambos tienen la misma base de datos y configuración, es únicamente por facilidad de uso, ya que algunos servicios podrían no tener conexión a Internet, y otros necesitarán Keycloak para autenticarse desde Internet.

## Descarga
El fichero de despliegue se encuentra en [Git](https://gitlab.com/ReiIzumi/swarm-project/-/tree/master/06-Keycloak).

```bash
git clone https://gitlab.com/ReiIzumi/swarm-project.git
cd swarm-project/06-Keycloak
```

## Preparación
Keycloak requiere una base de datos, así que habrá que crearla previamente en PostgreSQL.

Este requiere, al menos, permisos de creación de tablas y lectura/escritura sobre estas.

## Despliegue
1. Creamos los secrets para el usuario de Keycloak y el usuario para conectar al PostgreSQL.
* `keycloak-user`
* `keycloak-password`
* `keycloak-psql-user`
* `keycloak-psql-password`
2. Actualizar la URL y base de datos hacia PostgreSQL:
* `DB_ADD`
* `DB_DATABASE`
3. Actualizar ambas URL: `traefik.http.routers.keycloak.rule`.
4. Desplegar en Portainer.

## Configuración
Cada `Realm` de Keycloak es independiente del resto, tiene sus propias configuraciones y usuarios. Un usuario conectado a un Realm, no podrá conectar con servicios de otro Realm. A partir de esto deberíamos concluir si nuestro entorno necesitará uno o más Realms.

La configuración dependerá totalmente de los permisos, necesidades y servicios que utilicemos, así que aquí únicamente indico los más típicos.

### LDAP
En el `Realm`, creamos un nuevo `User Federation` de tipo ldap.

Estos son los campos más relevantes para configurar:
* **Console Display Name**: OpenLDAP
* **Edit Mode**: WRITABLE
* **Sync Registrations**: ON
* **Vendor**: Other
* **Connection URL**: ldap://ldap1.domain.intranet
* **Users DN**: ou=users,ou=test,dc=domain,dc=cat
* **Bind DN**: cn=admin,dc=domain,dc=cat
* **Bind credential**: password

**Hay que ajustar el Users DN acorde al entorno**.

En `Sync Settings` activar ambos para que se sincronicen usuarios que pudieran ser creados en el LDAP o fuera de Keycloak.

Probamos la conexión del usuario Bind, guardamos y sincronizamos todos los usuarios para confirmar que los encuentre y pueda leerlos.

:::warning
Keycloak permite conectarse mediante LDAPS para mayor seguridad, pero debido a que nuestro LDAP utiliza un certificado autofirmado y Keycloak es Java, debemos incluir el certificado CA en el Java. Keycloak no tiene forma de hacer esto automáticamente en su contenedor, así que habría que modificarlo para ello, algo que haré más adelante.
:::

Keycloak únicamente sincronizará usuarios del grupo de `users`, no los de admin, que debido a sus peculiaridades se quedarán ocultos para servicios que los necesiten.

Los usuarios pueden gestionar su usuario desde:
```
https://keycloak.domain.intranet/auth/realms/CHANGE_TO_REALM_NAME/account/
```

### Email
Dentro del `Realm`, en la pestaña de `Email` se configuran los datos del servicio de relay.
* **Host**: tasks.relay
* **Port**: 25
A partir de aquí los datos que utilizaremos para enviar el e-mail.

Para poder hacer la prueba, el usuario con el que iniciáramos sesión (admin seguramente) deberá tener un e-mail asignado, esto se puede hacer desde el `Manage account` de ese usuario.

### Doble autenticación
Es posible obligar a todos los usuarios o a unos concretos que utilicen la doble autenticación.

Para habilitarla para todos los usuarios, en `Authentication` - `Required Actions`, aquí podemos habilitar la acción por defecto de **Configure OTP**.

Los usuarios existentes deben ser modificados manualmente, para ello debemos acceder al usuario y en `Required User Actions` elegir **Configure OTP**. La siguiente vez que inicie sesión, Keycloak le pedirá que lo habilite.

### Política de contraseña
Por defecto los usuarios podrán utilizar cualquier contraseña, algo inviable, podemos habilitar muchas políticas a partir de las predefinidas en `Authentication` - `Password Policy`.

Lo mínima política aceptable hoy en día es:
* Expirar al menos 1 vez al año (1-3 meses en empresas)
* Contraseña diferente a las 3 anteriores utilizadas
* Mínimo 8 carácteres con al menos 1 mayúsculas, 1 minúsculas, 1 dígito y 1 símbolo
* No se puede utilizar ni el nombre ni el e-mail

