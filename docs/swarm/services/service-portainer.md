---
id: service-portainer
title: Portainer
slug: portainer
sidebar_position: 3
---

## Introducción
Docker Swarm está pensado para funcionar por comandos, lo que es perfecto para poder automatizar los procesos de despliegue, pero también está bien tener una UI que nos facilite los trabajos no automatizados o simplemente para ver más fácilmente todas las opciones.

[Portainer](https://www.portainer.io) es una UI compatible con las funcionalidades de Docker y Docker Swarm, entre otros.

## Descarga
El fichero de despliegue se encuentra en [Git](https://gitlab.com/ReiIzumi/swarm-project/-/tree/master/01-Portainer).

```bash
git clone https://gitlab.com/ReiIzumi/swarm-project.git
cd swarm-project/01-Portainer
```

## Preparación
Portainer requiere una carpeta compartida, para ello se hay que crear un volumen con [GlusterFS](/docs/swarm/environment/glusterfs#volúmenes) y montarlo en ``/mnt/portainer``.

## Despliegue
Este servicio tiene pocos pasos para ser desplegado.

1. Asignar la URL final en ``traefik.http.routers.portainer.rule``.
2. Desplegar con el comando:
```bash
docker stack deploy -c portainer.yml portainer
```

La primera vez que se accede, nos preguntará para crear la contraseña del usuario admin.
