---
id: service-minio
title: MinIO
slug: minio
sidebar_position: 10
---

## Introducción
Debido a que todo se está moviendo a cloud, muchos servicios únicamente saben utilizar a estos como storage, pero no siempre es así o no es una opción útil debido al gasto tanto económico como de red que eso significa.

Para estos casos tenemos MinIO, un servidor que replica el sistema de S3, pero en local.

## Descarga
El fichero de despliegue se encuentra en [Git](https://gitlab.com/ReiIzumi/swarm-project/-/tree/master/08-MinIO).

```bash
git clone https://gitlab.com/ReiIzumi/swarm-project.git
cd swarm-project/08-MinIO
```

## Preparación
Desconocemos la cantidad de espacio que requerirá, pero debido a que es un storage, la mejor opción es crear una carpeta compartida por NFS.

```bash
mkdir /mnt/minio
```

## Despliegue
Este servicio tiene pocos pasos para ser desplegado.

1. Cada servicio de S3 dispone de un access-key y un secret-key, así que definimos los secrets `minio-access-key` y `minio-secret-key`.
2. Actualizar la URL en `traefik.http.routers.minio.rule`.
3. Desplegar en Portainer.
