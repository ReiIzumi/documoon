---
id: service-nexus3
title: Nexus3
slug: nexus3
sidebar_position: 9
---

## Introducción
Las aplicaciones creadas requieren un lugar de almacenaje, y aunque GitLab permite guardar las versiones **released**, es muy posible que, para construirlas, se requieran de librerías o contenedores que hemos creado previamente.

Esas librerías deben ser almacenadas en un repositorio especifico acorde a su procedencia:
* Java requiere Maven
* JavaScript requiere NPM
* Contenedores pueden usar el Docker registry

Y así otros tantos.

Aquí es donde necesitaremos a [Nexus Repository](https://www.sonatype.com/products/repository-oss). Este es un proyecto que unifica varios tipos de repositorios en un solo punto, fusionando la gestión y mantenimiento de todos ellos en un único lugar. Esto también significa que, algunos de estos repositorios, no podrán utilizar el 100% de su potencial, debido a que Nexus tiene su propia UI e intenta unificarlo todo en ella. Según las funcionalidades que necesitemos, es posible que nos sea mejor instalar un repositorio especifico y dejar los más genéricos en Nexus.

Sea cual sea la opción elegida, en este artículo explicaré cómo configurar Nexus conectado al LDAP (OAuth2 requiere la versión de pago), configurar su seguridad y habilitar Maven, NPM y Docker registry, no iré al punto de cómo se utiliza cada uno, ya que depende mucho de cada persona o equipo, únicamente cómo configurarlos.

Además, se desplegarán 2 Nexus totalmente separados, uno de ellos será accesible únicamente desde la intranet y otro para la extranet.

### Intranet
En la intranet necesitamos almacenar todas las versiones, tanto las de desarrollo y pruebas, como las **released**, así, los servidores tendrán acceso a ellas y podrán compilar y desplegar cualquier aplicación que tengamos.

Además de esto, debido a que muchas aplicaciones tendrán dependencias de los repositorios oficiales, crearemos un espejo de estos. Cuando una aplicación requiera descargarse una librería, en vez de descargarla del repositorio oficial, lo hará de Nexus, si este no la tiene, él la descargará y la devolverá, así reduciremos los tiempos de carga. 

Nunca deberíamos exponer un sistema espejo de repositorios oficiales a internet, ya que otros podrían utilizarlo y consumir nuestro espacio y bando de ancha de internet.

Hay que tener en cuenta que todos los sistemas están diseñados para ir siempre al repositorio oficial, así que debemos configurar cada proceso para utilizar a este y no al oficial. Para facilitar este proceso, el acceso de lectura será público, lo que evitará tener que ir creando tokens o contraseñas.

### Extranet
En este Nexus únicamente publicaremos las versiones **released** de aquellos contenedores y/o librerías que queramos exponer, no tiene sentido publicar versiones de desarrollo o que están siendo testeadas aún.

Cuando se genera una versión final, será copiada tanto en los repositorios de la intranet como de la extranet.

## Descarga
El fichero de despliegue se encuentra en [Git](https://gitlab.com/ReiIzumi/swarm-project/-/tree/master/07-Nexus3).

```bash
git clone https://gitlab.com/ReiIzumi/swarm-project.git
cd swarm-project/07-Nexus3
```

## Preparación
Nexus requiere mucho espacio para almacenar todos los paquetes y contenedores, así que se conectará mediante una carpeta compartida de tipo NFS.

Debido a que cada Nexus estará totalmente separado, se crean 2 carpetas donde se anclarán sus volúmenes y se ajustan los permisos:
```bash
mkdir -p /mnt/nexus3/intranet-data
mkdir /mnt/nexus3/extranet-data
chown -R 200 /mnt/nexus3
```

## Despliegue
1. Únicamente debemos actualizar la URL de los 5 servicios: 2 para Nexus, 2 para el public/registry de Docker en la intranet y 1 para el registry de la extranet.
* traefik.http.routers.nexus3.rule
* traefik.http.routers.nexus3-docker-registry.rule
* traefik.http.routers.nexus3-docker-public.rule
* traefik.http.routers.nexus3-extranet.rule
* traefik.http.routers.nexus3-extranet-docker-registry.rule
2. Desplegar en Portainer.
3. Al acceder a la UI nos preguntará la contraseña que se ha creado en el volumen, `admin.password` y debemos dejar habilitado el acceso anónimo.

## Configuración
Igual que otros servicios, la configuración de Nexus dependerá mucho acorde a las necesidades.

Aquí se explica cómo definir el servidor de e-mail y la configuración básica para los usuarios de Maven, NPM y Docker.

Para descargar cualquier paquete o contenedor se utilizará el usuario anónimo, únicamente se definen usuarios para poder subir a los diferentes repositorios, así que estos se almacenarán dentro de Nexus, no vale la pena conectarlo al LDAP únicamente para ello.

La configuración será la misma para la Intranet y la Extranet, la diferencia es que la Extranet únicamente tendrá los repositorios "privados" con nuestros paquetes y contenedores, ya que no tiene sentido tener los grupos o réplicas de servidores externos.

Los repositorios que tiene por defecto se deberán borrar para definir los nuevos.

### Email
En `System` - `Email Server` se configuran los datos del servicio de relay, estos son los que hay que añadir.
* **Enabled**: Seleccionar
* **Host**: tasks.relay
* **From address**: nexus@domain.cat

### Almacén de datos
Aunque no es necesario, es altamente aconsejable separar el almacén de datos para cada repositorio en vez de utilizar el de por defecto, así podremos separar las copias de seguridad y límites de cada uno de ellos si algún día los necesitamos.

Este se define en `Repository` - `Blob Stores`, únicamente hay que indicar el tipo a `File` y el nombre, la ruta automáticamente se define hacia el volumen.

### Repositorio de Maven
Vamos a separar la configuración de Maven en 3 repositorios:
* Releases
* Snapshot
* Third-party

Cada librería se dividirá según su procedencia o uso.

También añadiremos 2 repositorios que clonan a otros, uno para el repositorio oficial de Maven y otro para el de JBOSS, ya que algunas librerías suelen depender de este.

Para facilitar el uso, creamos un grupo que conecta con todos, únicamente utilizaremos este para descargar y ya se encargará de encontrar todo esté donde esté.

Esto es lo que debemos configurar para todos ellos:

#### Releases
* **Tipo**: maven2 (hosted)
* **Name/Blob store**: maven-releases

#### Snapshots
* **Tipo**: maven2 (hosted)
* **Name/Blob store**: maven-snapshots
* **Version policy**: Snapshot
* **Deployment policy**: Allow redeploy

#### Third party
* **Tipo**: maven2 (hosted)
* **Name/Blob store**: maven-third-party

#### Maven central
* **Tipo**: maven2 (proxy)
* **Name/Blob store**: maven-central
* **Remote storage**: https://repo1.maven.org/maven2/

#### JBOSS central
* **Tipo**: maven2 (proxy)
* **Name/Blob store**: maven-jboss
* **Remote storage**: https://repository.jboss.org/nexus/content/repositories/thirdparty-releases/

#### Grupo
* **Tipo**: maven2 (group)
* **Name/Blob store**: maven-group
* **Member repositories**:
```
maven-snapshots
maven-releases
maven-third-party
maven-central
maven-jboss
```

Los usuarios deberán utilizar el público para recuperar los datos:

https://nexus.domain.intranet/repository/maven-group/

### Repositorio de NPM
Similar a Maven, se crea un repositorio privado, un proxy hacia el público y un grupo que unifica.

En este caso el privado permitirá redeploy por si se utiliza la etiqueta **latest**.

#### Privado
* **Tipo**: npm (hosted)
* **Name/Blob store**: npm-releases
* **Deployment policy**: Allow redeploy

#### NPM Registry
* **Tipo**: npm (proxy)
* **Name/Blob store**: npm-registry
* **Remote storage**: https://registry.npmjs.org

#### Grupo
* **Tipo**: npm (group)
* **Name/Blob store**: npm-group
* **Member repositories**:
```
npm-private
npm-registry
```

Los usuarios deberán utilizar el público para recuperar los datos:

https://nexus.domain.intranet/repository/npm-group/

### Repositorio de Docker
Siguiendo a los anteriores, replicamos lo mismo: repositorio privado, proxy al público y un grupo que unifica.

Docker es más complejo ya que cada repositorio tiene su propia URL y puerto para acceder, por eso se han configurado todas ellas en el despliegue de swarm, por lo demás, es todo igual.

#### Privado
* **Tipo**: docker (hosted)
* **Name/Blob store**: docker-private
* **HTTP**: 8082
* **Allow anonymous docker pull**: Select

#### Docker hub
* **Tipo**: docker (proxy)
* **Name/Blob store**: docker-hub
* **Remote storage**: https://registry-1.docker.io
* **Docker Index**: Use Docker Hub

#### Grupo
* **Tipo**: docker (group)
* **Name/Blob store**: docker-group
* * **HTTP**: 8083
* **Allow anonymous docker pull**: Select
* **Member repositories**:
```
docker-private
docker-hub
```

#### Seguridad
Para poder utilizar el acceso anónimo en Docker, se debe añadir al reino de Nexus en `Security` - `Realms`.

Simplemente hay que añadir el `Docker Bearer Token Realm` y guardar.

### Usuarios y permisos
Para cada tipo de repositorio se va a crear un rol que permite hacer cualquier acción, en los de Maven y NPM también se añadirá la subida de artifacts que permite utilizar la UI, con estos se pueden crear uno o más usuarios, en estos usuarios debemos indicar el rol que deseamos.

A partir de aquí, se puede hacer tan fácil o complejo como se desee.

#### Maven
* **Role ID**: maven push
* **Role Name**: maven push
* **Role Description**: Add write and delete permissions, also to upload artifacts from the UI
* **Privileges**: 
```
nx-component-upload
nx-repository-view-maven2-*-*
```

#### NPM
* **Role ID**: npm push
* **Role Name**: npm push
* **Role Description**: Add write and delete permissions, also to upload artifacts from the UI
* **Privileges**: 
```
nx-component-upload
nx-repository-view-npm-*-*
```

#### Docker
* **Role ID**: docker push
* **Role Name**: docker push
* **Role Description**: Add write and delete permissions
* **Privileges**: 
```
nx-repository-view-docker-*-*
```
