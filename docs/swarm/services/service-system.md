---
id: service-system
title: System
slug: system
sidebar_position: 4
---

## Introducción
Los servicios de Docker requieren de un proceso que haga limpieza, entre imágenes viejas que ya no se utilizan y contenedores que se han caído y Swarm ha generado unos nuevos, podemos llegar a tener un montón de espacio ocupado en basura.

Este proceso borra todos los contenedores e imágenes que tengan más de 24h y no se estén utilizando.

## Descarga
El fichero de despliegue se encuentra en [Git](https://gitlab.com/ReiIzumi/swarm-project/-/tree/master/02-System).

```bash
git clone https://gitlab.com/ReiIzumi/swarm-project.git
cd swarm-project/02-System
```

## Despliegue
Aprovechando que tenemos Portainer, únicamente se debe crear un Stack nuevo, copiar el fichero y desplegar, este servicio no requiere ningún ajuste.
