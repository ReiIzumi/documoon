---
id: service-monitor
title: Monitor
slug: monitor
sidebar_position: 7
---

## Introducción
Con tantos servidores y servicios, es necesario tener algo a mano para comprobar el estado de todos ellos.

[Prometheus](https://prometheus.io) y [Grafana](https://grafana.com) son el conjunto ideal, uno para almacenar datos de otros lugares y hacer búsquedas sobre ellos, otro para mostrar los datos de forma más sencilla y enviar alertas si algo no está como se espera.

Además de estos, se utilizan exportadores para que Prometheus pueda recuperar las métricas.

## Descarga
El fichero de despliegue se encuentra en [Git](https://gitlab.com/ReiIzumi/swarm-project/-/tree/master/05-Monitor).

```bash
git clone https://gitlab.com/ReiIzumi/swarm-project.git
cd swarm-project/05-Monitor
```

## Preparación
Tanto Prometheus como Grafana requieren almacenar datos y configuración, así que se creará una carpeta compartida con [GlusterFS](/docs/swarm/environment/glusterfs#volúmenes) en ``/mnt/monitor``.

Ambos tienen permisos especiales:
```bash
mkdir /mnt/monitor/prometheus
mkdir /mnt/monitor/grafana
chown -R nobody:nogroup /mnt/monitor/
chown 472:0 /mnt/monitor/grafana
```

### Node exporter
Cada servidor deberá tener instalado el [node-exporter](https://github.com/prometheus/node_exporter), aunque es posible ejecutarlo como contenedor, estos suelen tener limitaciones a la hora de leer el disco u otros, además necesitamos tenerlo en todos los servidores, incluido el de base de datos.

1. Revisamos la [última versión](https://prometheus.io/download/#node_exporter) y la añadimos al enlace de descarga.
```bash
wget https://github.com/prometheus/node_exporter/releases/download/v*/node_exporter-*.*-amd64.tar.gz -O node_exporter.tar.gz
```
2. Descomprimimos en el lugar de destino.
```bash
sudo tar -xvf node_exporter.tar.gz -C /usr/local/sbin/ --strip-components=1
```
3. Creamos el servicio.
```bash
sudo vim /etc/systemd/system/nodeexporter.service
```

```vim title=/etc/systemd/system/nodeexporter.service
[Unit]
Description=NodeExporter
[Service]
TimeoutStartSec=0
ExecStart=/usr/local/sbin/node_exporter
[Install]
WantedBy=multi-user.target
```
4. Iniciamos el servicio.
```bash
sudo systemctl daemon-reload
sudo systemctl enable nodeexporter
sudo systemctl start nodeexporter
```
5. Accedemos a la URL para confirmar que funcione: http://swarm1.domain.intranet:9100/metrics

## Despliegue
1. Necesitamos los secrets de Grafana y también los de un usuario administrador en PostgreSQL para exportar las métricas.
* `grafana-admin-user`
* `grafana-admin-password`
* `postgres-admin-user`
* `postgres-admin-password`
2. Creamos un config para la configuración de Prometheus, hay que actualizarlo según la URL de todos los node agents.
* `monitor-prometheus-config_v1` contiene **prometheus-config.yml**
3. Actualizamos los dominios de Prometheus y Grafana:
* `traefik.http.routers.monitor-prometheus.rule`
* `traefik.http.routers.monitor-grafana.rule`
5. Actualizar la contraseña de Prometheus: `traefik.http.middlewares.monitor-prometheus.basicauth.users`.
6. Actualizar la URI de acceso a PostgreSQL: `DATA_SOURCE_URI`.
7. Actualizar las variables para envío de e-mail en Grafana: 
* `GF_SMTP_FROM_ADDRESS`
* `GF_SMTP_FROM_NAME`
8. Desplegar en Portainer.
9. Acceder a Grafana, configurar el acceso a Prometheus:
* Configuration > Data Sources
* Prometheus URL: http://tasks.prometheus:9090
10. Importar dashboards en Grafana, estos son unos ejemplos:
* Node Exporter
  - https://grafana.com/grafana/dashboards/1860
  - https://grafana.com/grafana/dashboards/12486
  - https://grafana.com/grafana/dashboards/11074
* Docker
  - https://grafana.com/grafana/dashboards/11939
  - https://grafana.com/grafana/dashboards/7007
* PostgreSQL
  - https://grafana.com/grafana/dashboards/9628
* Traefik
  - https://grafana.com/grafana/dashboards/12250
