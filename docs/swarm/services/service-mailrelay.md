---
id: service-mailrelay
title: Mail Relay
slug: mail-relay
sidebar_position: 6
---

## Introducción
Muchos servicios tienen que enviar e-mails para notificar, si disponemos de un sistema centralizado es más sencillo de gestionar, pero si no es así, cualquier cambio nos obliga a actualizarlo en cada servicio.

Este servicio hace de relay, recibe notificaciones de otros servicios y los envía a otro lugar, NO es un servidor de correo, simplemente hace de intermediario contra un servidor de correo real.

Acorde a la configuración de este, cualquier petición es aceptada y enviada, para evitar que aplicaciones indeseadas lo utilicen, este servicio tiene su propia red y no es accesible fuera de esta. Los servicios que requieran enviar e-mails, deberán incluir esa red, por supuesto, cualquier servicio fuera del clúster de Docker Swarm no tendrá acceso a este servicio.

## Descarga
El fichero de despliegue se encuentra en [Git](https://gitlab.com/ReiIzumi/swarm-project/-/tree/master/04-MailRelay).

```bash
git clone https://gitlab.com/ReiIzumi/swarm-project.git
cd swarm-project/04-MailRelay
```

## Preparación
Este servicio almacena los e-mails hasta que son enviados, si el servicio cae, al volver a iniciarse recupera la cola y los empieza a procesar, para ello requiere su carpeta compartida con [GlusterFS](/docs/swarm/environment/glusterfs#volúmenes) en ``/mnt/mailrelay``.

También requiere unas carpetas especificas:
```bash
mkdir -p /mnt/mailrelay/data
mkdir /mnt/mailrelay/postfix
```

## Despliegue
Este servicio tiene pocos pasos para ser desplegado.

1. Los servidores de e-mail requieren autenticación para acceder, este se guarda como un secret con el nombre ``mailrelay-config_v1`` y el siguiente formato:
```vim
<mail_server> <user_or_email>:<password>
```
2. Actualizar los campos de ``RELAY_HOST``, ``RELAY_POSTMASTER`` y ``RELAY_MYDOMAIN``.
3. Desplegar en Portainer
