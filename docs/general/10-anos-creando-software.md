---
slug: diez-anos-creando-software
title: 10 años creando software
sidebar_position: 2
---

Si alguien ve este blog que está orientado principalmente a sistemas, pensará que me dedico a ello, pero no, finalmente hace 10 años que empecé a desarrollar software en consultorías. Sistemas tenía que ser mi trabajo real (y para lo que realmente estudié), pero en su día tuve la opción de elegir y me fui hacia este lado, aun así, ser de sistemas en el mundo de la programación ha sido una gran ventaja a lo largo de mi carrera, creo que elegí bien.

A lo largo de los años he aprendido como funciona este oficio, he pasado por todos los niveles, empezando por Junior y evolucionando a Senior, Team Leader y tocando temas de arquitectura, por supuesto en todo momento he estado también en sistemas. Ha habido buenos y malos momentos, y estos hacen que aprendas a organizar tus prioridades.

## Bienvenid@ al mundo de la informática
Para aquella gente que empiece, o que esté estudiando para entrar en este mundo, tengo varios consejos. Debido a que me he enfocado toda la vida a la parte de creación de software, esto podría diferir en otras especialidades de la informática, pero la mayoría serán necesarias igualmente.

### 1- Prepárate para lo que viene

Llevo años haciendo la misma pregunta a la gente que empieza: ¿estás dispuest@ a estar al menos 10 horas delante de una pantalla al día? Cuando creas software, aquello de las 8h al día pasa a ser irreal, estarás tantas horas trabajando como sea necesario, y aunque no lo hagas, no importará, tu cabeza seguirá resolviendo ese problema que no has conseguido resolver, te irás a dormir con eso en la cabeza, soñarás con ello y te levantarás peor que cuanto te fuiste a dormir, si tienes suerte, tendrás la respuesta al problema.

Nuestro oficio se mete en tu maleta cuando te vas de vacaciones, te persigue en los sueños, no importa que lejos te vayas, seguirá allí y te perseguirá, no lo dudes.

### 2- ¿Qué esperas de tu futuro?

Esta es una pregunta que hago a la gente que entrevisto, ¿qué esperas en tu vida? ¿qué quieres ser en el futuro? ¿qué esperas de la informática y de tu trabajo? Sorprendentemente no tanta gente como suponía tiene esto claro.

Cuando te dedicas a este oficio, tienes que tener claro a donde quieres ir, ¿quieres crear software? ¿qué tipo de software? ¿en qué idioma o idiomas? ¿y por qué? ¿prefieres ser de sistemas? ¿te gusta diseñar? ¿o multimedia? ¿qué parte?

También tienes que preguntarte hasta donde quieres llegar, hay gente que es feliz siendo un Senior developer toda la vida, hay gente que aspira a lo más alto, ¿liderar equipos? ¿arquitectura? ¿quizás te va más la gestión?

¿Qué es la informática para ti? Quizás sea simplemente un oficio que te da de comer, o quizás vivas para esto, ¿algún punto intermedio?

La respuesta a estas preguntas cambia con el tiempo, y debido a ello hay gente que aspira a mucho y se "encalla" por el camino, también hay mucha gente que lo deja, muchísima gente, pregúntate cuanta gente del oficio que conozcas se ha jubilado estando en este oficio. Pero, aunque estas respuestas puedan variar, deberías saber responder a todas ellas ahora mismo. Nunca te presentes a una entrevista sin tener totalmente claras las respuestas a todo esto.

### 3- Presión, presión y más presión

La informática tiene una peculiaridad: presión. En cualquier momento todo lo que tienes delante puede caer, un volcán puede estallarte en la cara cuanto menos te lo esperes, tienes que aprender a liberar la presión, a reírte de estos problemas y seguir adelante. Forja esto en tu mente:

"Mientras lloras por aquello que no funciona o ha dejado de funcionar inexplicablemente, estás perdiendo tiempo y la locura se hará cada vez más grande hasta que no puedas con ella. ¡Supéralo!, ¡no hay nada que no puedas arreglar!, nada se va a interponer en tu camino si no se lo permites, deja de llorar, levántate, y sigue tu camino. Tienes las herramientas para saltar cualquier muro que se ponga delante de ti, así que sigue adelante".

Tienes que confiar en ti, alguien que se dedica a la informática en alma, adquiere un superpoder: puede resolver cualquier problema que se le ponga por delante, cualquiera, pero para ello requiere invertir un esfuerzo acorde al problema, normalmente superior.

Incluso la gente con más experiencia cae ante la presión, el miedo atroz de que una aplicación falle al instalarlo a producción o que estés horas y horas sin resolver un problema fantasma, te puede llevar a la locura, pondrá al límite su capacidad para aguantar, es posible que te supere, que tus límites sean destruidos y caigas ante ello. No lo permitas.

### 4- Hobby, tu otro oficio

Siguiendo la anterior, este mundo es complicado, mucho, y te va a poner a prueba más de lo que crees, más de lo que quizás puedas soportar, así que tienes que aprender a soportarlo. No existe una fórmula para ello, y con el tiempo tus sistemas de defensa caerán, tendrás que buscarte otras opciones, pero lo lograrás si sigues adelante.

Un consejo para facilitarlo: Debes tener 2 hobbies NO relacionados con la informática ni similares, esto significa que estos hobbies no pueden tener como principal herramienta PCs, ni teclados, ni ratones, ni joysticks, ni monitores, ni televisiones, ni móviles, ni tablets, a ser posible tampoco tendría que tener relación directa con lo que haces en tu trabajo. Puedes tener tantos hobbies como quieras, pero estos 2 tienen que ser importantes, la informática claramente no puede ser uno de ellos, y los videojuegos tampoco, curiosamente ver series y/o películas puede no funcionarte por utilizar una televisión, aunque puedes probar.

Puedes elegir lo que quieras: leer, escalar montañas, pintar, tocar un instrumento (físico, no con PC o digital), construir cosas, o destruirlas (aunque ojo, porque destruir e informática son mala combinación), cualquier deporte que te guste, puedes entrar en un club de algo (que no esté relacionado con pantallas), ¿te gusta la fotografía? Puede ser buena opción, aunque cuidado si tienes que utilizar PC para retocar imágenes.

Estos 2 hobbies se pueden complementar con otros, pero tienen que ser importantes y pueden ir variando. Siempre aconsejo 2 debido a que al menos 1 debería estar activo, y los hobbies van y vienen, así que mejor tener 2 por si acaso.

Si estos hobbies te permiten anular estrés (¡como subir montañas o deporte extremo!), mejor. Otros te ofrecerán otras cualidades, construir te permite aprender a organizar las tareas y a tener la tranquilidad (o fe) para llegar hasta el final del proyecto (puedes construir algún mueble, o reparar algo, o ¿has probado de montar algo con Lego? ¡también sirve!). La pintura, música o cualquiera relacionado con arte te dará capacidades imaginativas más allá, además de aumentar tu tranquilidad, incluso puede que te ayude a mejorar tus capacidades de autoaprendizaje. La lectura, escritura o cualquier similar (¿has probado a jugar a rol?) te permitirá vivir otros mundos, o crearlos, liberando tu mente, aunque sea un rato, de cualquier problema. Cuando desconectas, puedes volver a conectar viendo el mundo de otra forma.

### 5- Lo siento, no serás ric@

La riqueza y tranquilidad no van de la mano, incluso es posible que no veas ninguna de las dos. En este oficio no te va a llover dinero, si lo has elegido por ello, te has equivocado, pero tienes opciones de conseguir bastante dinero, el tema es que no será fácil. Alguien del montón no lo conseguirá, así que tienes que elegir: ¿prefieres vivir tranquilamente con lo mínimo o esperas ir a lo más alto? Existen trucos intermedios y trucos que ya irás viendo.

### 6- Alguien ya inventó la rueda

Esto te lo habrán dicho mil veces, pero lo olvidamos tantas veces que es mejor seguir repitiéndolo: ¡NO reinventes la rueda! Si necesitas algo que alguien ya ha hecho, úsalo, adáptalo a lo que necesites, no lo inventes tú de nuevo, no es necesario.

### 7- Pero aprende a utilizar la rueda

Y algo que quizás no te han dicho tanto: ¡No reinventes la rueda, pero no copies cosas de otra gente porque sí! Debes entender lo que haces, no puedes copiar lo que ha hecho otra gente, adaptarlo y considerar que todo funciona. Siempre debes entender por qué funciona aquello que haces y utilizas, solo así aprenderás, mejorarás y podrás adaptarte mejor a cualquier cambio, mejora o fallo.

### 8- No memorices, aprende a aprender

Los conocimientos en lo que haces son importantes, pero cambiará, todo cuando sabes acabará cambiando, tienes que aprender a adaptarte, a aprender en si. No tienes que saber las cosas de memoria (en serio, ¡no memorices tonterías!), tienes que aprender a buscar y a aprender por tu cuenta.

También tienes que aprender de otra gente, pregunta, pregunta y pregunta.

Algunas veces la respuesta a tu problema está más cerca de lo que piensas, debes aprender a buscar las respuestas a tus problemas, pero al mismo nivel, también tienes que aprender a preguntar a quienes saben más que tú.

No se espera que seas la genialidad en persona y consigas superarlo todo, se espera de ti que sepas sobrevivir y resolver los problemas que te pongan por delante, pero también se espera que preguntes, y que sepas preguntar.

### 9- Decide tu camino

Voy a ir cerrando de una forma peculiar: es posible que tu inicio en el oficio sea un desastre, no solo por tu falta de conocimientos (que pudiera ser), si no por encontrarte en algo más … mucho peor de lo esperado.

Este oficio no es fácil, ni bonito, quizás tu primera empresa sea un desastre, pero esto es una prueba, aprende, evoluciona, adáptate, absorbe todo lo que puedas, y nunca olvides porqué decidiste este camino.

Pero también aprende a dejar tu camino y a seguir otros caminos. Tu oficio será gran parte de tu vida, y te afectará emocionalmente, si ves que un camino no te lleva a ningún lado, párate y piensa qué deberías hacer para resolverlo.

### 10- También aprende a abandonar tu camino

Siguiendo la tónica, cuando todo caiga, cuando ya no veas por donde ir, cuando todos los caminos estén destruidos, en esos momentos en que ya nada funciona y te preguntes porqué estás donde estás, deberías dar un paso atrás, mirar con otra perspectiva lo que tienes delante, gírate y mira los caminos que has recorrido y piensa qué quieres hacer.

Nadie te va a culpar por dejar tu camino y seguir otro (y si lo hacen, no tienen derecho a ello), tu vida es tuya, debes decidir sobre ella, seguir adelante o no, es tu decisión. Quizás aun no entiendas esto, pero llegará el día en que lo entenderás.

**Si te has decidido a seguir por este camino, ¡te doy la bienvenida!**

## Si este ya es tu oficio
Los consejos anteriores son para gente nueva y estos para quienes llevan un tiempo, en cualquier caso, los dos pueden ser interesantes estés en el punto que estés.

### 1- El camino marcado, no tiene porqué ser el correcto

Mi primer consejo en esto puede no ser esperado (ni aceptado), pero: "las normas, los estándares, lo escrito, están para romperlos".

Que la comunidad diga que el mundo se dirige hacia X, no quiere decir que sea lo correcto. Dicen que cuando todos van hacia un lado, es muy improbable que se equivoquen, pero improbable no es cero, y quizás sea debido a algo que no sabes, o no has tenido en consideración.

Lo primero que tienes que entender, es que este oficio es el más grande y amplio que existe, la informática abarca tanto, incluso otros oficios, que nunca es suficiente. No importa cuanta gente se dedique a esto, siempre faltará gente. A todo, esto hay que añadir que la presión hace que mucha gente del oficio lo deje y que las futuras generaciones caigan más rápido de lo esperado, o que directamente ni lo intenten.

¿Qué significa esto? La falta de gente, y aún más de especialistas, provoca que se busquen opciones más rápidas, más fáciles, pero no mejores.

Cuanto más tiempo pasa, más aplicaciones y frameworks aparecen, estos a su vez crecen, evolucionan, siempre con un único punto de vista: simplicidad y unificación, intentan que todo funcione por sí mismo.

Tienes sistemas como docker que te permite instalar un paquete independiente que ya lleva todas las dependencias y configuraciones para funcionar, no solo la aplicación, también servidores, bases de datos, todo.

Por otro lado, en frameworks tienes Spring en caso de Java, un framework que actualmente tiene tantos módulos que es capaz de tener su propio servidor de aplicaciones, conectar a cualquier cosa, aunque no entiendas como funciona, y hasta puedes gestionar una base de datos o UI sin llegar a verla nunca.

Con estos sistemas puedes conseguir cosas que antes no eran nada fáciles y a mucha más velocidad, no requieres especialistas, incluso verás gente desarrollando software desplegando aplicaciones como si fuera algo fácil. Otra cosa es que entiendan qué hacen.

### 2- Elige tu posición dentro de las reglas

Ahora te voy a contar un secreto: la creación de estos sistemas no fue un éxito, fue la única cura que se encontró a una enfermedad que no sabíamos resolver, y esta cura nos mata.

Como ya he dicho, este oficio es tan grande que es imposible conocerlo todo, ser especialista de algo es muy complicado y requiere una cantidad enorme de tiempo, y ya no digamos formar a alguien. Las empresas esperan obtener el máximo rendimiento, formar no es una opción, y de serla, tiene que ser rápido. Para crear una aplicación requerirías aprender tanto, que tardarías meses, o años, en tener las bases, estos sistemas te hacen creer que lo tienes, ni mucho menos.

Esto se construyó para que gente mediocre fuera capaz de lograr cosas por encima de su nivel, pero yo ahora te hago esta pregunta: ¿tú qué eres? ¿eres mediocre o eres de la élite? Yo tengo clara mi respuesta. A su vez, si llevas un equipo, tendrás que preguntarte no de qué tipo son, si no qué esperas que sean, y ayudarles a ir por ese camino.

En cualquier caso, una aplicación bien hecha (que no compleja), será claramente superior en todos los aspectos a una que utilice todas estas novedades, pero requerirá mayor esfuerzo por parte de quienes la crean, y más tiempo.

### 3- Lo más importante no siempre está a la vista

El tipo de empresa, a qué se dedican y cómo lo hacen, su funcionamiento interno, la jerarquía y organización, tus cargos superiores, todo esto es importante, mucho, pero no tanto como la gente cree.

Si aún no sabes qué es lo más importante, quizás o no lo tienes, o no te has fijado en ello. Lo más importante son tus compañer@s, tu equipo.

En muchos casos vas a sobrevivir por esa gente y por nada más. No importa si están a tu lado o en otro país, si confías en tu gente y esa gente confía en ti, si el equipo trabaja unido para ayudarse en cualquier momento, sobrevivirás, y esa gente sobrevivirá también gracias al resto, y a ti.

Incluso si estás en un proyecto trabajando sin nadie más, tienes que acordarte de que eres parte de un equipo, entiendel@s, haz amistades, claramente no te llevarás perfectamente con toda la gente, pero son tu equipo, tu familia en el trabajo, y tu apoyo moral.

Si donde estás esto no existe, o peor: la gente lucha entre ella, solo te puedo aconsejar que te busques algo mejor, porque existe, y no sabes lo que te estás perdiendo.

En muchos casos conseguirás sobrevivir únicamente gracias a la gente en la que confías. Sin esa gente, yo no estaría donde estoy ahora.

### 4- Aprende y enseña

Recuerda cuando empezaste, recuerda quien fuiste, no lo olvides jamás.

Gracias a los años de dedicación habrás acumulado un montón de conocimiento, desde trucos simples a resolución de grandes problemas, comparte tu conocimiento, olvídate de esa estúpida idea de que tienes que saber cosas que nadie conoce para ser imprescindible, con eso solo lograrás desvincularte de tu equipo, odiar al resto, y crear enemistades. No se me ocurre nada más estúpido que podrías hacer.

Cuanto más conocimiento se transmita en tu equipo, menos presión tendrás tu (más gente podrá resolverlo, no dependerán siempre de ti), y también aprenderás de otra gente.

Demasiada gente cree que lo que consigues con los años es conocimiento, se equivocan. Lo mejor que tienes no es tu conocimiento, si no la capacidad que tienes para aprender (que no ha parado de crecer) y también tu adaptabilidad y resolución de problemas, forjadas a base de usarlas una y otra vez, eso es lo que compone tu verdadera experiencia, eso es lo valioso en ti, no tus conocimientos.

### 5- Cree en ti

Esto podría ser estúpido, pero la gente suele olvidarlo, y cuantos más años pasan, más lo olvidan: Cree en ti como nadie lo hará.

Los años pasan, crees que te quedas atrás, que no sabes lo suficiente, que el mundo te ha dejado atrás. Es posible que sea cierto, ¿pero crees que no puedes seguirle el ritmo al mundo? Si has llegado hasta aquí es que puedes hacerlo, quizás no te das cuenta, pero seguramente nunca has parado de seguir al mundo por instinto, todo está en tu cabeza, así como la decisión de seguir o no.

### 6- Cree en tu equipo

Igual que crees en ti, tienes que creer en tu equipo, si conoces a tu gente sabrás en qué te pueden ayudar. No puedes ver todo ante tu camino, habrá veces en que te equivoques, porque así es nuestra raza.

Y por ello tienes que confiar en tu gente, aprende a preguntar y escuchar, porque desde la persona más joven a la más experimentada te podrán dar una sorpresa, cada persona ve el mundo de forma diferente, quizás vea lo que tú no estás viendo.

### 7- Imaginación al poder

La otra fuente de nuestro potencial es la imaginación, este oficio no sigue las matemáticas tanto como la gente cree, no tenemos un único camino para llegar a un lugar, ni tampoco existe la eficiencia perfecta como en la física, incluso el camino más raro puede convertirse en el correcto.

Piensa siempre en diferentes puntos de vista, pregunta a tu gente, quizás alguien ve algo que tu no has visto, debido a que no existe un único camino, quizás estás obviando algo que pueda ser más interesante de lo que pensabas.

## Caminando hacia otros 10 más

No puedo saber qué me depara el futuro, quien sabe dónde estaré dentro de 5 años, o 10, solo se que he seguido mi camino hasta aquí y que seguiré decidiendo a dónde ir. Desconozco a dónde iré, eso es algo que averiguaré sobre la marcha.

Pero tengo claro que, si ahora mismo me presentara delante de mí yo de hace 10 años y le contara lo que he recorrido, las decisiones que he tomado, seguramente no me creería.

Sobre los consejos, podría enumerar muchos más, y aún más advertencias, pero recorriendo el camino os enfrentaréis a todas ellas, y las superaréis.

Si tuviera que decidir qué ha sido lo más importante de mi profesión durante estos 10 largos años, lo tendría claro: lo más importante y lo que ha hecho que esté aquí ahora mismo ha sido la gente, aquella gente que he considerado “mi equipo”. Confiar en tu equipo y que estos confíen en ti, eso es lo que me ha permitido seguir adelante. Sin ell@s, hace años que hubiera abandonado.
