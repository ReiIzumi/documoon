---
id: about
title: Sobre mí
sidebar_position: 1
---

:::tip
Cuando me di cuenta ya estaba en medio de una guerra sin esperanza alguna, perder era siempre el camino por seguir, pero abandonar no era una opción.
Un mundo rodeado de ordenadores, servidores y demás enemigos del ser humano.
:::

## ¿Sobre mí?
La mejor manera de describirme es uniendo `freak-geek-otaku`. Rápido, sencillo y para toda la familia ... o quizás no, ya que faltaría saber qué significa cada una de esas, e igual crees que lo sabes, pero la sociedad ha distorsionado tanto sus verdaderos significados que ya no se parecen.

Y tampoco quiero decir que utilice sus modos originales, pero vaya, sirve para hacerse una idea.

## ¿Por qué escribo esta web?
Cuando inviertes horas y horas buscando, probando, configurando, borrando, pensando en el ángulo de lanzamiento mejor para destrozar tu servidor al ser tirada por la ventana ... acabas pensando: "oye, ¿y si lo documentara todo?".
¡Y aquí estamos!

Y, por cierto, calculé parte del vector de lanzamiento, pero después pensé que si la lanzara por la ventana, seguramente saltaría detrás para salvarla, ¡un sinsentido! :confounded:

## ¿Más cosas?
:house: Nací y vivo en las cercanías de Barcelona, ciudad a la que odio y me encanta a la vez. Aunque ni de coña viviría allí.

:school: A los 12 nos hicieron rellenar qué 3 oficios nos gustaría hacer en el futuro. Como es normal la mayoría de gente miró la hoja como lo que realmente era: **un despropósito**. ¿A los 12 tienes que decidir a qué te dedicarás? Tienes problemas más importantes que conseguir lograr esa semana.

A mí me echaron la bronca por poner 3 veces informática. Lo tenía claro. Tampoco entendí por qué yo lo había hecho mal y necesitaba "ayuda", el resto ni siquiera habían elegido nada aún. ¿No serían ellos los que necesitaban ayuda?

Con eso en mente me saqué los dos módulos superiores de informática de la época (eso ha sonado a viejuno :anguished:), me lancé al ~~glorioso~~ mundo laboral donde aprendí nuevas técnicas del oficio como el "salto al precipicio mientras rezas para que esta vez no tenga lava." **Spoiler**: tenía lava :wink:

Mientras trabajaba pensé "oye, ¿ahora que cobro una mierda y apenas tengo tiempo, por qué no aprovecho para sacarme la carrera a distancia?". Sí, yo tampoco lo entiendo, pero así fue.

Me uní a la Universitat Oberta de Catalunya (UOC para amigos y enemigos) para progresar en mis estudios, la cruda realidad es que no aprendía nada. Soy una persona autodidacta, la universidad era un lastre para mi tiempo y cartera.

Y hablando de ello, nuestro sistema educativo es un error que solo va a peor. La educación no se basa en descargar la Santa Wikipedia en tu cerebro. No, en serio, la tienes en el móvil, ¿para qué dentro de tu cabeza? ¡¡ni que lo teras de espacio fueran gratuitos!!.

:::info
Lo único que necesitas para sobrevivir es aprender a desafiar a los mismos retos, aprender a superarlos y, aún más importante, a levantarte tras cada paliza recibida.
:::

Me apasiona la enseñanza, pero no dar datos ni técnicas, si no encender las llamas de la pasión por el oficio. No importa cuantos proyectos con éxito acabes en tu vida, el momento en consigues despertar la ilusión por el oficio a una persona lo supera todo. En ese momento sabes que has logrado un éxito en tu vida.

:raising_hand: Soy una persona con ideas claras, posiblemente de extremos, pero creo que todo se debe dialogar para llegar a una conclusión más correcta. No me gustan las cosas que son así porque sí, ni dejar las cosas a medias, ni dejar a la gente tirada, ni abandonar nada que hubiera empezado.

Todo se planifica, inicia, revisa y se da por concluido para ir a por la siguiente tarea. Siempre dando lo mejor de ti.

## Otras cosas
La informática es tanto mi trabajo como mi hobby (ojo, la informática, no trabajar, hay que evitar confusiones). Pero no es la única:
- La filosofía podría haber sido mi oficio, aunque concluí que sería mejor dejarla como hobby.
- La cultura japonesa es parte de mi mundo, mi ética, y toda mi ideología. Para bien y para mal.
- El rol es como una segunda vida (o tercera, según cuanto tarde ese dichoso goblin en apuñalarte por la espalda).
- La lectura (y escritura) me han acompañado toda la vida. Principalmente la fantasía y prácticamente cualquier estilo de manga.
- La pintura de miniaturas (¿conoces Warhammer? pues fue el culpable de mi inicio). Siempre me ha apasionado el arte y me gustaba dibujar, hasta que no dibujas el cuadro que estas mirando no llegas a entender todos sus detalles, su complejidad ... Aun así, nunca llegó a nada, hasta que me mostraron este juego. Y mi cartera se fue al traste :cry:.
- :guitar: Mi guitarra eléctrica, **Yui**, es un hobby peculiar. Yo intento manejarla, ella me mira mal, ¡yo le digo que lo conseguiré!, ella saca la carta magna, pierdo. Quizás algún día gane esa ardua batalla. **Spoiler**: ella sabe que no.

Contando que el trabajo se lleva una gran parte de nuestra vida (¡más que dormir si lo cuentas!), los hobbies y el trabajo acaban estando en una balanza para mantener un duro equilibrio. Nadie dijo que fuerza fácil.

:airplane: Viajar no me apasiona, pero he estado varias veces en Japón y es posible que aparezca nuevamente otras tantas. Un día me dejaron un manga para leer, había leído muchos (pero que muchos) libros, y algunos cómics, pero algo pasó allí. Desde entonces la cultura japonesa siempre ha estado a mi lado, y visitar el país del sol naciente era algo básico.

¿El resto del mundo? Siendo realista, no me llaman, no me interesan, aunque no tendría problemas en verlos.

Y sin más dilación, este punto termina aquí.

:::tip
Rei Izumi < rei.izumi arroba moon punto cat >
:::