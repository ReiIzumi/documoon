---
id: monitoring
title: Monitor
sidebar_position: 2
---

En la actualidad, uno de los puntos más importantes es poder monitorizar y entender el funcionamiento de todo el sistema. Tenemos muchas opciones para ello, aunque la combinación Prometheus+Grafana es la más común.

Existe un pack que unifica varias de las piezas más comunes. Por defecto este despliega:
* Prometheus para almacenar los datos.
* Alertmanager para detectar y avisar de alertas en el sistema.
* Grafana como visualizador de las métricas.
* Varias alertas y dashboards de Grafana para detectar y mostrar las métricas.
* Un nuevo `Custom Resources` para gestionar los Prometheus desplegados, la configuración de estos servicios y permitir su configuración a base de estos recursos y no con configuración directa.

El pack contiene más piezas y puede requerir de otras configuraciones no explicadas en este artículo (como el envío de las alertas a un sistema que nos avise activamente).

:::tip
Debido a la complejidad de Prometheus y Grafana, este artículo se limita a su instalación. Aconsejo encarecidamente dedicar unas semanas y/o comprar un curso especializado para entender su funcionamiento y aprender a configurarlos.
:::

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack).
:::

Añadimos el repositorio.
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

Definimos las variables.
```bash
vi monitoring-values.yaml
```

Los tres servicios principales requieren de un volumen para almacenar datos y configuración. También exponemos a Grafana vía Ingress.
```yaml title="monitoring-values.yaml"
prometheus:
  prometheusSpec:
    storageSpec:
      volumeClaimTemplate:
        spec:
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 40Gi
alertmanager:
  alertmanagerSpec:
    storage:
      volumeClaimTemplate:
       spec:
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 500Mi
grafana:
  adminPassword: adminadmin
  ingress:
    enabled: true
    ingressClassName: nginx-intranet
    hosts:
      - grafana.domain.intranet
    annotations:
      nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
      nginx.ingress.kubernetes.io/ssl-passthrough: "true"
  persistence:
    enabled: true
    accessModes: ["ReadWriteOnce"]
    size: 500Mi
```

:::danger
La configuración por defecto indica 2 días de alertas y 10 días de métricas. El espacio de los volúmenes tiene que adaptarse acorde a ello y nuestro volumen de datos.
:::

```bash
helm upgrade --install kube-prom prometheus-community/kube-prometheus-stack \
    -f monitoring-values.yaml \
    --namespace monitoring-system --create-namespace
```

:::caution
Al actualizar el Helm se debe revisar si existen [cambios manuales](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack#upgrading-an-existing-release-to-a-new-major-version).
:::

## Configuración
En Grafana disponemos de muchos dashboards para mostrar los datos de los nodos y los servicios detectados desde el punto de vista de Kubernetes, aun así, si un servicio dispone de sus propias métricas, es aconsejable activarlo y tener un dashboard especializado.

Con las herramientas por defecto es posible detectar la CPU, memoria y red utilizada por los servicios, pero no datos internos como cantidad de hilos activos, diferentes tipos de memoria, o incluso la memoria utilizada realmente, ya que varios lenguajes tienden a mantener reservada el máximo de memoria utilizada.

Para recolectar los datos de un servicio se requiere:
* El servicio debe exponer sus datos en el formato de Prometheus.
* Para que Prometheus se conecte a ese servicio, se debe crear un `Custom Resources` para indicarle los datos. Normalmente se utiliza el `Service Monitor`. Por defecto el sistema despliega varios que pueden utilizarse como ejemplo.
* Cada Prometheus desplegado detecta su configuración según aquellos que tienen un label con su nombre. En este caso, toda la configuración deberá tener el siguiente label: `release=kube-prom`.
* La experiencia me dice que es más sencillo mirar la sección de `Status` de Prometheus para ver si todo funciona o deducir errores antes que revisar sus logs. Este es accesible creando un `Port Forwarding` al servicio de Prometheus (`kube-prometheus-prometheus`).
* Grafana mostrará los datos si disponemos de un dashboard. Podemos crear uno o utilizar uno de la [comunidad](https://grafana.com/grafana/dashboards/) como base.
* Una vez entendemos los datos y gráficos, es aconsejable crear alertas útiles.

## Grafana dashboards
Los dashboards de Grafana se pueden crear e importar desde su UI. Debido al volumen estos datos no se deberían perder, pero en alguna actualización me han desaparecido misteriosamente. 

Para evitar estos problemas y simplificar el sistema, es más práctico configurar los dashboards como un `ConfigMap` dentro del mismo namespace. Estos son importados automáticamente por Grafana.

Ejemplo para crearlo vía `kubectl`:
```bash
kubectl create configmap grafana-dashboard-name --from-file=grafana-dashboard-name.json -n monitoring-system
kubectl label configmap grafana-dashboard-name grafana_dashboard=1 -n monitoring-system
```

Ejemplo vía yaml:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  labels:
    grafana_dashboard: "1"
  name: grafana-dashboard-name
  namespace: monitoring-system
data:
  grafana-dashboard-name.json: |-
    {
      ...
    }
```
