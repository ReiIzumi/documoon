---
id: minio
title: MinIO
sidebar_position: 5
---

Dentro del mundo bare-metal solemos encontrar un gran problema: muchas aplicaciones son capaces de conectar a un almacén de objetos para gestionar sus archivos, pero la lista de compatibilidad suele quedarse en el AWS S3 y con suerte los similares del resto de clouds.

Así que tenemos tres opciones:
* No utilizar esas capacidades, lo que puede ser un gran impacto para nuestra infraestructura.
* Pasar por caja y utilizar AWS S3 o similar.
* Desplegar un clon de S3 que nos permita "engañar" al sistema, mientras mantenemos todo en la misma red.

La tercera opción es la más asequible y la que menos afecta a la infraestructura. Para ello tenemos a [MinIO](https://min.io/), un proyecto que replica todas las funcionalidades de AWS S3 dentro de nuestro clúster.

MinIO está pensado para desplegar un clúster masivo, así que lo configuraremos sin clúster y con requisitos mínimos.

## Requisitos
MinIO requiere un volumen donde depositar los archivos. El uso que le daré no va a requerir mucha velocidad, pero es posible que requiera de espacio, así que lo montaré con `NFS`.

La carpeta compartida se debe crear previamente.

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/minio/minio).
:::

Añadimos el repositorio.
```bash
helm repo add minio-official https://charts.min.io
helm repo update
```

Creamos el namespace.
```bash
kubectl create namespace minio-mngmt
```

El volumen.
```bash
vi minio-pv.yaml
```

Lo configuramos para la carpeta `NFS`.
```yaml title="minio-pv.yaml"
apiVersion: v1
kind: PersistentVolume
metadata:
  name: minio-nfs-pv
  namespace: minio-mngmt
spec:
  storageClassName: storage-nfs
  capacity:
    storage: 50Gi
  accessModes:
    - ReadWriteMany
  nfs:
    server: nas.domain.intranet
    path: "/NFS_MinIO"
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: minio-nfs-pvc
  namespace: minio-mngmt
spec:
  storageClassName: storage-nfs
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 50Gi
```

:::note
He asignado 50 GiB. Se deberá actualizar según la necesidad.
:::

Aplicamos el volumen.
```bash
kubectl apply -f minio-pv.yaml
```

Creamos el fichero de configuración.
```bash
vi minio-values.yaml
```

Cambiamos el despliegue a modo `standalone`, con una única replica y recursos limitados. Únicamente estará disponible en la intranet.

He habilitado también la consola para facilitarme la vida y no tener que configurarlo por comandos.
```yaml title="minio-values.yaml"
clusterDomain: domain.local
mode: standalone
replicas: 1

rootUser: admin
rootPassword: adminadmin

persistence:
  existingClaim: minio-nfs-pvc

ingress:
  enabled: true
  ingressClassName: "nginx-intranet"
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: 500m
  hosts:
    - minio.domain.intranet

consoleIngress:
  enabled: true
  ingressClassName: "nginx-intranet"
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: 500m
  hosts:
    - minio-console.domain.intranet

resources:
  requests:
  limits:
    cpu: 1
    memory: 512Mi
```
:::tip
La contraseña debe tener al menos 8 caracteres.
:::

:::warning
Ya que el fichero tiene contraseña, sería recomendable borrarlo después de desplegar.
:::

:::info
He asignado al Ingress que permita subir ficheros de hasta 500 MiB debido a que el tamaño original suele ser demasiado pequeño para este tipo de servicios.
:::

Desplegamos el servicio.
```bash
helm upgrade --install minio minio-official/minio \
  -f minio-values.yaml \
  --namespace minio-mngmt
```

## Configuración
Tenemos dos URL, una es utilizada por los servicios compatibles con AWS S3 y la otra es únicamente para la gestión de MinIO, aunque también sirve como explorador de ficheros.

:::tip
S3 funciona a través de `buckets`, estos se acceden mediante paths (https://minio.domain.intranet/bucket1) o subdominios (https://bucket1.minio.domain.intranet). MinIO utiliza paths por defecto.

Si una aplicación nos obliga a utilizar el sistema de subdominio tendremos un problema. Habrá que añadir cada nombre de bucket al DNS y además generar un wildcard para el subdomino `minio.domain.intranet`. Ya que los wildcard no aceptan 2 niveles de subdominio.
:::

Sin entrar en muchos detalles, los bloques principales de la consola son los almacenes de datos llamados `Buckets` y la gestión de usuarios o cuentas de servicio encontradas en `Identity`. Debido a que según el uso que le demos la configuración y permisos variarán, habrá que ajustar MinIO según lo que necesitemos en cada momento.
