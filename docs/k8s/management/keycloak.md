---
id: keycloak
title: Keycloak
sidebar_position: 4
---

La gestión y permisos de usuarios se ha centralizado en un LDAP, a partir de usuarios y grupos, durante muchos años. Por supuesto, eso también cambió.

Con los años aparecieron diferentes tecnologías para cumplir con los nuevos requisitos, como [SAML](https://es.wikipedia.org/wiki/Security_Assertion_Markup_Language), y finalmente se asentó bajo los procesos utilizados por [OAuth2](https://oauth.net/2/). Actualmente estos procesos se utilizan acorde a la estándar de [OpenID Connect](https://openid.net/connect/) (OIDC).

Aunque muchas aplicaciones siguen sin ser compatibles con OAuth2 o OIDC, cada vez son más las que incorporan estos sistemas, e incluso que no aceptan el uso del LDAP. Debido a ello es aconsejable tener un LDAP para los sistemas más viejos, y es obligatorio tener una solución para la nueva OIDC.

[Keycloak](https://www.keycloak.org/) es un proyecto OpenSource que incorpora tanto la capacidad de utilizar SAML como OIDC. Puede hacer de puente de otros servicios vía SAML o OIDC (incluso de otros Keycloak), centralizando la gestión y aplicaciones en un único punto.

Si es necesario, se puede conectar un LDAP, aunque la experiencia me dice que solo es aconsejable utilizarlo si de verdad lo necesitamos.

También incorpora algunas funcionalidades interesantes:
* Puede utilizar la doble autenticación con varias aplicaciones referentes.
* Permite crear plantillas para modificar las pantallas según nuestros logos y diseños.
* La configuración es gigante, prácticamente puedes configurar todo. Aunque eso también significa que requiere un gran esfuerzo.

Está bajo la estela de Red Hat y tiene una gran comunidad. Aunque debo decir que la documentación y solución de errores puede ser tediosa.

## Requisitos
Keycloak requiere de una base de datos, en mi caso utilizaré un [PostgreSQL](/docs/it/databases/postgresql) en el que se ha creado tanto la base de datos como el usuario de acceso.

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/bitnami/keycloak).
:::

Añadimos el repositorio.
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

Podemos dejar la creación de los secrets al Helm o crearlos manualmente e indicarle cómo encontrarlos. Al final acaba siendo lo mismo, así que le dejaremos el trabajo al Helm.
```bash
vi keycloak-values.yaml
```

Cambiamos la configuración para usar nuestro PostgreSQL y lo publicamos en la extranet para que esté disponible para todos los servicios.

:::tip
Debemos crear el dominio en nuestra DNS de internet.
:::

```yaml title="keycloak-values.yaml"
production: true
proxy: edge
auth:
  adminUser: admin
  adminPassword: admin
resources:
  requests:
    cpu: 0.2
    memory: 384Mi
  limits:
    cpu: 1
    memory: 1Gi
ingress:
  enabled: true
  ingressClassName: nginx-extranet
  hostname: keycloak.domain.cat
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
postgresql:
  enabled: false
externalDatabase:
  host: postgresql.domain.intranet
  port: 5432
  user: keycloak_user
  password: keycloak_password
  database: keycloak_db
service:
  type: ClusterIP
```

Desplegamos el servicio.
```bash
helm upgrade --install keycloak bitnami/keycloak \
  -f keycloak-values.yaml \
  --namespace keycloak-mngmt --create-namespace
```

## Configuración
Si algo he aprendido con Keycloak, es que requiere de un artículo (o varios) solo para él. Así que en esta sección me limitaré a las secciones más enfocadas al entorno desplegado con Kubernetes.

:::tip
Siempre hay que crear un nuevo `Realm` y mantener el Master por defecto para el propio Keycloak.
:::

### E-mail
Debemos crear una excepción para usar el relay.
```bash
vi keycloak-allow-relay.yaml
```

Configuramos la excepción según el nombre de instancia y namespace.
```yaml title="keycloak-allow-relay.yaml"
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-keycloak
  namespace: email-relay-mngmt
spec:
  podSelector:
    matchLabels:
      app.kubernetes.io/instance: smtp-relay
  policyTypes:
    - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              kubernetes.io/metadata.name: keycloak-mngmt
          podSelector:
            matchLabels:
              app.kubernetes.io/instance: keycloak
      ports:
        - protocol: TCP
          port: 25
```

Aplicamos los cambios.
```bash
kubectl apply -f keycloak-allow-relay.yaml
```

En el `Realm` elegido, configuramos el e-mail en `Realm settings` > `Email`.

Ajustamos los siguientes datos, manteniendo el resto como están:

| Campo | Valor                        |
| ----- | ---------------------------- |
| From  | keycloak@domain.cat          |
| Host  | smtp-relay.email-relay-mngmt |
