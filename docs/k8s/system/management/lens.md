---
id: lens
title: Lens
sidebar_position: 2
---

[Lens](https://k8slens.dev/) no es un servicio desplegado en el clúster, es una aplicación de escritorio que te permite conectarte a múltiples sistemas Kubernetes desde tu PC y administrarlos completamente, sin tener que acceder por terminal o consola.

Personalmente me parece de los más útiles debido a la facilidad en la que puedes ver el estado de todo el sistema y explorar en él, incluso cuando no tienes del todo claro qué buscas.

Lens es gratuito para uso doméstico o pequeñas empresas, así que es ideal para la gran mayoría de casos.

## Añadir un clúster
Una vez instalado, debemos añadir nuestro clúster a su configuración.

Para darle acceso tendremos que copiar el fichero `~/.kube/config` que se encuentra en el nodo maestro y que es el que permite que nuestro usuario de Linux interactúe con el clúster.

Es posible tanto descargar este fichero como copiarlo directamente, que suele ser la opción más rápida.

:::tip
Si el fichero de configuración pertenece a un usuario limitado sin capacidad para recuperar la lista de namespaces, la aplicación nos pedirá que definamos a cuáles debe acceder.
:::