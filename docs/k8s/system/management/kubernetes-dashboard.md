---
id: kubernetes-dashboard
title: Kubernetes dashboard
sidebar_position: 1
---

Existen diversas consolas administrativas para Kubernetes, pero [Kubernetes Dashboard](https://github.com/kubernetes/dashboard) es la oficial y una de las más completas.

Personalmente no soy muy fan de esta consola, pero por muy poco espacio y requerimientos tienes una consola totalmente operativa, así que nunca está de más tenerla a mano.

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/k8s-dashboard/kubernetes-dashboard).
:::

Añadimos el repositorio.
```bash
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
helm repo update
```

Normalmente instalo los Helm indicando los campos que quiero cambiar directamente, pero las anotaciones de Ingress me parecen ilegibles, con lo que queda más fácil de entender con un fichero para ello. 

```bash
vi kubernetes-dashboard-values.yaml
```

Activamos Ingress y lo configuramos acorde al de `intranet` y redirigiendo a HTTPS por defecto
```yaml title="kubernetes-dashboard-values.yaml"
ingress:
  enabled: true
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
  className: "nginx-intranet"
  hosts:
    - dashboard.domain.intranet
```

:::caution
El `hosts` debe apuntar a un dominio aceptado por el certificado desplegado y debe existir en el DNS.
:::

Desplegamos el servicio.
```bash
helm upgrade --install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard \
	-f kubernetes-dashboard-values.yaml \
	--namespace kubernetes-dashboard-system --create-namespace
```

Si vamos a la web acorde al dominio nos mostrará la pantalla para iniciar sesión.

## Usuario
Para poder acceder vamos a crear un nuevo usuario que tendrá permisos sobre el clúster.
```bash
kubectl create serviceaccount dashboard-admin-sa -n=kubernetes-dashboard-system
kubectl create clusterrolebinding dashboard-admin-sa \
	--clusterrole=cluster-admin \
	--serviceaccount=kubernetes-dashboard-system:dashboard-admin-sa \
	-n=kubernetes-dashboard-system
```

El acceso se realiza mediante tokens que deben ser generados. Por defecto estos duran 1h
```bash
kubectl -n kubernetes-dashboard-system create token dashboard-admin-sa
```

Este nos devolverá un token en formato `jwt` que deberemos indicar en la consola para poder acceder.
