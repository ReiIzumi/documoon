---
id: longhorn
title: Longhorn
sidebar_position: 2
---

Un clúster requiere volúmenes accesibles por cada nodo. El protocolo `NFS` es muy útil para ello, pero no permite despliegue en clúster y depende de la velocidad de la red, así que no siempre es viable.

Kubernetes tiene por compatibilidad con varios sistemas que permiten la gestión y sincronización de datos entre volúmenes como [Rook](https://rook.io/), muy estable, robusto y con gran comunidad. 

Después de revisar varias opciones, he elegido [Longhorn](https://longhorn.io/) que, aunque dispone de una comunidad más pequeña, me ha parecido más fácil de manejar para una pequeña red.

:::caution
Debido a la pequeña comunidad de Longhorn, el proyecto requiere de semanas o meses para adaptarse a funcionalidades obsoletas de Kubernetes. Siempre que se decida actualizar Kubernetes, se deberá revisar si Longhorn sigue siendo compatible con la nueva versión.
:::

## Requisitos
:::info
Longhorn igual que el resto, está pensado para crear 3 réplicas, así que requiere al menos 3 nodos. Si el clúster es inferior se deberá revisar las configuraciones de `numberOfReplicas`.
:::

La carpeta por defecto utilizada por Longhorn en cada nodo es `/var/lib/longhorn/`. No me gusta utilizar el mismo disco para el sistema que para algo tan complejo como los nodos, así que en cada nodo tengo un segundo disco que será particionado y montado en esa misma carpeta.

:::warning
Estos comandos se ejecutan en los nodos y no en el maestro.
:::

Necesitamos la utilidad para crear particiones.
```bash
sudo apt update
sudo apt install parted -y
```

Creamos la partición y la carpeta donde se montará.
```bash
sudo parted /dev/sdb mklabel msdos
sudo parted -a opt /dev/sdb mkpart primary ext4 0% 100%
sudo mkfs.ext4 /dev/sdb1
sudo mkdir /var/lib/longhorn/
sudo vi /etc/fstab
```

Esta nueva partición se montará en la carpeta por defecto de Longhorn
```vim
/dev/sdb1      /var/lib/longhorn/   ext4 defaults 0 0
```

Montamos la nueva unidad.
```bash
sudo mount -a
```

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/longhorn/longhorn).
:::

Añadimos el repositorio.
```bash
helm repo add longhorn https://charts.longhorn.io
helm repo update
```

Añadimos los datos de configuración.
```bash
vi longhorn-values.yaml
```

Únicamente necesitamos configurar Ingress acorde a autenticación básica y el tamaño recomendado para subir ficheros.
```yaml title="longhorn-values.yaml"
ingress:
  enabled: true
  ingressClassName: nginx-intranet
  host: longhorn.domain.intranet
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required '
    nginx.ingress.kubernetes.io/proxy-body-size: 10000m
```

:::warning
Longhorn debe ser desplegado en el namespace `longhorn-system` para funcionar correctamente.
:::

Desplegamos el servicio.
```bash
helm upgrade --install longhorn longhorn/longhorn \
  -f longhorn-values.yaml \
  --namespace longhorn-system --create-namespace
```

Creamos el usuario que tendrá acceso a la UI.
```bash
sudo apt-get install apache2-utils -y
htpasswd -c auth admin
```

Lo añadimos como secret.
```bash
kubectl create secret generic basic-auth --from-file=auth -n longhorn-system
rm auth
```

:::tip
Longhorn tiene varios procesos así que requiere bastantes recursos. En nodos inferiores a 4 CPUs podría no ser capaz de desplegarse si estos ya tienen otros servicios.
:::

Desde la UI disponemos de un dashboard para visualizar el estado de todo longhorn y también para gestionar y configurar cualquier sección.

## Pruebas
Tenemos un nuevo storageClass llamado `longhorn` con el que crear nuevos volúmenes desde k8s, aunque también pueden ser creados mediante la UI.

Desplegamos un Nginx que tiene un nuevo volumen asociado.
```bash
kubectl create -f https://raw.githubusercontent.com/longhorn/longhorn/v1.4.0/examples/pod_with_pvc.yaml
```

Tras desplegarlo, en la sección `Volume` de la UI aparecerá el nuevo volumen, al acceder veremos toda su información.

Finalmente limpiamos el entorno.
```bash
kubectl delete -f https://raw.githubusercontent.com/longhorn/longhorn/v1.4.0/examples/pod_with_pvc.yaml
```

## Copias de seguridad
Los volúmenes gestionados por Longhorn replican un sistema similar al RAID, pero igual que en estos, se deben crear copias de seguridad. Se puede gestionar desde la UI.

Las copias de seguridad se pueden almacenar en un NFS v4 o en un S3 (como MinIO). Si se dispone de un NAS, la opción del NFS será la más sencilla.

En `Setting` > `General` buscamos la opción llamada **Backup Target** e indicamos la ruta del NFS:
```
nfs://serverName.domain.intranet:/folderName
```

Tras guardar veremos que la UI activa las opciones de `Backup`.

### Automatización de copias
El sistema permite hacer copias de seguridad y snapshots manualmente, pero lo más efectivo es automatizar el proceso, para ello se dispone del `Recurring Job`.

Las tareas se asocian a una serie de grupos y afectan a todos los volúmenes que tengan esos grupos. Si un volumen no tiene ninguna tarea asociada, entonces utilizará la tarea que esté asignada al grupo `default`. Esto permite tener una configuración genérica de copias de seguridad y poder sobrescribirla en volúmenes concretos.

Cada tarea requiere:
- Nombre.
- Elegir entre crear snapshots o copias de seguridad.
- Cantidad de snapshots o copias de seguridad a mantener (Retain).
- Debido a que una tarea puede afectar a varios volúmenes, se puede paralelizar este proceso para acelerar el tiempo de construcción, aunque también sobrecargará más al sistema (Concurrency).
- Tiempo en el que se ejecutará la tarea mediante la estándar de Cron. Tiene un asistente bastante práctico para ello.
- Grupos a los que afectará la tarea. Elegir el `default` para afectar a todos los que no tengan ya una tarea propia.

Una vez iniciado, tanto en el volumen como en la sección de `Backup` veremos que se están construyendo las nuevas copias y destruyendo las más viejas.
