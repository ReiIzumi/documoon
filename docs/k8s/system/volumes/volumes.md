# Volúmenes

Todos los clústeres tienen el mismo problema: almacenar datos en un sistema de ficheros.

Cuando los datos están en un almacén externo (como una base de datos), es sencillo. Si estos datos se almacenan en un sistema de ficheros, tenemos el problema de que cada nodo del clúster tiene el suyo propio, así que la misma aplicación desplegada en varios nodos verá datos diferentes.

Existen múltiples opciones, pero principalmente se utilizan dos sistemas:
- Utilizar una carpeta compartida o un almacén de objetos. Este es montado y accesible por todos los nodos, así que los datos no están en cada nodo, si no que están en una fuente externa. `NFS` es la solución más sencilla.
- Replicar los datos entre nodos. Esto permite a todos los nodos acceder a todos los datos, e incluso si uno de ellos se cae, se seguirá teniendo acceso debido a que están replicados o total o parcialmente.

Cada una de estas opciones se suele dividir aún más según las tecnologías utilizadas, e incluso es posible que tengamos disponible más de una opción para cada uno. Entonces, ¿cuál es la correcta? La respuesta es fácil: todas.

Al desplegar un servicio debemos conocer cómo funciona o cuáles son los requisitos, y de ahí elegir cuál es el correcto. Algunos ejemplos:
* Aplicaciones con un volumen muy grande de datos pero que no cambian frecuentemente. Puede ser buena idea utilizar NFS.
* Aplicaciones que escalan muy rápido y la velocidad es su principal fuerte. Replicar entre los nodos será la mejor opción.
* Aplicaciones que indexan datos y necesitan acceder a estos rápidamente. Según lo rápidos que sean, el modo de réplica se puede ver saturado, pero NFS le añadirá un problema de latencia, quizás no se pueda encontrar cuál es la mejor opción, si no la menos mala.

## Carpetas compartidas
Sin duda, la técnica a elegir en un bare-metal será `NFS`, el sistema nativo de Linux es muy eficiente y rápido. Además, Kubernetes tiene soporte nativo para montar unidades cuando un nodo lo necesita.

En mi caso utilizaré mi NAS que ofrece carpetas compartidas por NFS, dispone de una RAID y además realiza copias de seguridad de sus carpetas. ¡Son todo ventajas!

## Carpetas sincronizadas
Aquí la cosa se complica. Kubernetes tiene una lista de sistemas compatibles de forma nativa, y es aconsejable adaptarse a ella en vez de utilizar nuevos plugins que podrían quedar en desuso.

Una de las opciones es [GlusterFS](https://www.gluster.org/) el cual utilicé en Docker Swarm. Cuando se utiliza junto a Kubernetes se hace con [Heketi](https://github.com/heketi/heketi), que incorpora un servicio REST para autogestionar los volúmenes. El problema es que Heketi solo tiene actualizaciones por errores y GlusterFS tiene varios problemas bastante graves que preferiría evitar.

El resto de los proyectos casi siempre utilizan `Ceph`, otro sistema nativo de Kubernetes. Utilizar la versión original es bastante complejo, así que es preferible utilizar una aplicación que facilite el sistema.

La principal alternativa con `Ceph` es [Rook](https://rook.io/), que dispone de una gran comunidad, pero durante el tiempo se han hecho cambios bastante grandes, así que gran parte de la documentación no sirve. Se considera el proyecto más grande pero también es bastante complejo.

Otra alternativa más ligera es [Longhorn](https://longhorn.io/), un proyecto más joven y con una comunidad pequeña, aunque bastante fácil de desplegar y gestionar. La pega es que sus actualizaciones son muy lentas y suelen estar muy detrás de las actualizaciones de Kubernetes. Si quieres estar siempre a la última con Kubernetes, Longhorn te dejará de funcionar.

Se deberá elegir según las preferencias.
