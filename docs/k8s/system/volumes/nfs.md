---
id: nfs
title: NFS
sidebar_position: 1
---

Un sistema sencillo de utilizar volúmenes es a través de `NFS`. Para ello necesitamos que un servidor se encargue de publicar una carpeta compartida y todos los nodos del clúster deberán tener acceso hacia esa carpeta compartida.

Cuando un servicio se despliega y tiene un volumen de tipo `NFS`, Kubernetes se encarga de montarlo en ese momento, con lo que no requiere un esfuerzo extra para montar cada volumen manualmente en todos los nodos.

Este sistema también tiene sus pegas:
* NFS no está diseñado para funcionar en clúster ni replicar datos, así que, si el servidor se cae, todos los servicios que dependen de él dejarán de funcionar.
* Los datos están siempre en otro servidor, lo que puede ocasionar problemas de latencia en la red.

## Configuración
Estos volúmenes son configurados como cualquier otro `PersitentVolume` de Kubernetes.

Este ejemplo crea el volumen y el reclamo para que un servicio lo pueda utilizar. Según cada servicio se deberá ajustar el nombre del servidor NFS, el nombre de la carpeta, el límite de espacio, nombres y permisos.
```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-pv
spec:
  storageClassName: storage-nfs
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteMany
  nfs:
    server: nas.domain.intranet
    path: "/shared_folder"
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nfs-pvc
spec:
  storageClassName: storage-nfs
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 10Gi
```
