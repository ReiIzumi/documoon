---
id: metallb
title: MetalLB
sidebar_position: 3
---

Para acceder a los servicios fuera del clúster tenemos principalmente dos opciones:
- Abrir un puerto que permitirá acceder al servicio a partir del dominio del maestro. Cada servicio tendrá un puerto diferente y deben ser puertos fuera del margen de `root`.
- Utilizar un load balanced que asignará una IP y se podrá utilizar cualquier puerto.

La segunda es la opción realista, pero requiere un load balanced que ofrezca estas IPs. Las clouds suelen tener esto resuelto por defecto, en bare-metal necesitamos de [MetalLB](https://metallb.universe.tf/).

:::caution
MetalLB es un proyecto en Beta y cada cierta versión cambia radicalmente. Aun así, no tenemos otra alternativa.
:::

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/metallb/metallb).
:::

Añadimos el repositorio.
```bash
helm repo add metallb https://metallb.github.io/metallb
helm repo update
```

Desplegamos MetalLB dentro de un namespace propio.
```
helm upgrade --install metallb metallb/metallb \
  --namespace metallb-system --create-namespace
```

## Lista de IP
La configuración de IP y transporte de estos se hace mediante `Custom Resources` definidos por MetalLB.

:::caution
Los CR actualmente están en beta y el modo anterior (ConfigMaps) no está permitido. Si actualizamos el sistema habrá que revisar si esta parte ha cambiado para actualizarla manualmente.
:::

Definimos la lista de IPs que el load balanced podrá asignar, para ello creamos un fichero.
```bash
vi metallb-cs.yaml
```

:::warning
El rango asignado para MetalLB debe estar libre en la red y ser accesible por esta.
:::

Voy a asignar 2 IPs para los futuros Ingress y 8 auto asignables para futuros servicios.
```vim title="metallb-cs.yaml"
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: l2-list
  namespace: metallb-system
spec:
  ipAddressPools:
  - ingress
  - default
---
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: ingress
  namespace: metallb-system
spec:
  addresses:
  - 192.168.1.20-192.168.1.21
  autoAssign: false
---
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: default
  namespace: metallb-system
spec:
  addresses:
  - 192.168.1.22-192.168.1.29
```

:::tip
Existe otra forma de configurar las IPs mediante [Calico](https://metallb.universe.tf/configuration/calico/), pero por ahora he preferido mantener el sistema original.
:::

Aplicamos la configuración.
```bash
kubectl apply -f metallb-cs.yaml
```

## Pruebas
Siempre prefiero hacer pequeñas pruebas para confirmar que los servicios principales funcionan según lo esperado.

Desplegamos un `nginx`.
```bash
kubectl create deployment nginx --image=nginx
kubectl expose deployment nginx --type=LoadBalancer --port=80
```

Comprobamos que se le ha asignado IP.
```bash
kubectl get svc
```

Si todo ha funcionado correctamente, le habrá asignado una IP acorde al rango `default`.

Finalmente limpiamos el sistema.
```bash
kubectl delete deployment.apps/nginx service/nginx
```