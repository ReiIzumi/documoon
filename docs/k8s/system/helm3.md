---
id: helm3
title: Helm 3
sidebar_position: 2
---

Los despliegues en Kubernetes se hacen a través de ficheros `yaml` que deben modificarse acorde a nuestras variables. Debido a que es un trabajo tedioso, existe [Helm](https://helm.sh/).

Helm dispone de un sistema de plantillas que se modifican a través de variables y que construyen los ficheros finales de Kubernetes. También es capaz de interactuar con Kubernetes para desplegar los servicios de forma sencilla.

En [Artifacts Hub](https://artifacthub.io/) se puede encontrar infinidad de servicios preparados para desplegarse con Helm, aunque debo reconocer que la documentación de la gran mayoría deja que desear y requiere de una exploración manual para poder sacar provecho de los servicios.

:::tip
Hay que diferenciar entre la versión 2 y 3 de Helm. El `K8s Project` utiliza Helm 3.
:::

## Instalación
La instalación por `snap` es extremadamente sencilla.
```bash
sudo snap install helm --classic
```

Comprobamos la versión y ya podremos empezar a desplegar servicios.
```bash
helm version
```

## Actualización de servicios
El mismo comando que instala está preparado para actualizar, aunque este puede que no sea útil si no tenemos a mano las configuraciones o ficheros que fueron utilizados para el despliegue original.

En ese caso, podemos extraer los datos, por ejemplo, este comando extraerá la configuración que indicamos en su momento:
```bash
helm get values keycloak --namespace keycloak-mngmt > upgrade.yaml
```

:::caution
Debido a que este fichero puede extraer contraseñas, es recomendable borrarlo después de actualizar.
:::

Ese fichero puede ser utilizado para hacer la actualización sin perder la configuración original.
```bash
helm upgrade keycloak bitnami/keycloak \
  --values upgrade.yaml \
  --namespace keycloak-mngmt
```

:::tip
Existe la variable `--reuse-values` que teóricamente hace lo mismo, pero en todos los casos que he probado, la versión de los contenedores nunca es actualizada.
:::
