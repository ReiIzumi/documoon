---
id: history
title: Historial
sidebar_position: 99
---

Los cambios del `K8s Project` se registran en esta sección.

## Versión

### 2024/04/27
Sistema
* Cert-manager

### 2024/04/25
Sistema
* Actualización de Kubernetes/CRI-O tras 1.29

### 2023/09/11
Sistema
* Ingress con métricas

Administración
* Monitor
* Keycloak en modo producción

### 2023/08/15
Global
* Actualización de servicios
Sistema
* Actualización de Kubernetes

### 2023/04/06
Administración
* Actualizado Keycloak
### 2023/01/29
Servicios
* MediaWiki

### 2023/01/26
Servicios
* Snipe-IT

### 2023/01/22
Servicios
* WordPress

### 2023/01/18
Sistema
* Volúmenes > Longhorn

### 2022/12/25
Servicios
* Kirin

### 2022/12/21
Servicios
* Toranku

### 2022/11/17
Servicios
* Desplegar servicios

General
* GitLab Runner

### 2022/10/11
Sistema
* Kubernetes > Usuarios de sistema y conexión remota
* Helm 3 > Actualización de servicios

### 2022/09/28
Administración
* Identity Provider > Keycloak
* Object Storage > Minio

### 2022/09/25
Nuevo proyecto con Kubernetes:
* Introducción

Sistema
* Instalación de Kubernetes
* Helm 3
* Metallb
* Ingress
* Volúmenes > NFS
* Gestión > Kubernetes Dashboard
* Gestión > Lens

Administración
* E-Mail relay
* Artifactor repository > Nexus

## Roadmap
Administración
* [Percona](https://www.percona.com/software/database-tools/percona-monitoring-and-management)?
* Grafana Loki & Tempo
* [Nexus Repository Manager - alternative](https://artifacthub.io/packages/helm/sonatype/nexus-repository-manager)

