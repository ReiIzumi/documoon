---
id: mediawiki
title: MediaWiki
sidebar_position: 6
---

Una forma de documentar prácticamente cualquier cosa es utilizar una Wiki, estas son prácticamente una hoja en blanco donde podemos construir las plantillas que queramos, con un buscador de serie y capacidad para editar.

De entre todas las Wikis disponibles, [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki) es la utilizada en la Wikipedia. Esto no la convierte en la mejor, pero la gente la conoce así que es más sencillo empezar.

Además, dispone de una lista bastante ampliar de plugins para adaptar a lo que necesitemos.

## Requisitos
MediaWiki requiere de [MariaDB](/docs/it/databases/mariadb). Se utilizará el existente en vez de que proviene el Helm.

Los datos y plugins se almacenan en un volumen de Longhorn. Tanto la instalación de plugis como la configuración (que se encuentra en el fichero `LocalSettings.php`) requieren modificar los datos directamente en el volumen, pero Longhorn no tiene una forma propia para "acceder" a los ficheros. A esto se añade que los contenedores de Bitnami no permiten el uso de `debug` o `attach` para acceder al volumen. Esto nos da la opción de añadir un sidecar en el despliegue o crear un contenedor temporal con acceso al mismo volumen, siendo más sencilla la segunda opción.

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/bitnami/mediawiki).
:::

Añadimos el repositorio.
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

Creamos un volumen independiente del despliegue.
```bash
vi mediawiki-pvc.yaml
```

Este volumen tiene que permitir ser accedido desde diferentes pods.
```yaml title="mediawiki-pvc.yaml"
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mediawiki
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: longhorn
  resources:
    requests:
      storage: 2Gi
```

Definimos el fichero de propiedades.
```bash
vi mediawiki-values.yaml
```

Configuramos MediaWiki, SMTP y MariaDB externo.
```yaml title="mediawiki-values.yaml"
mediawikiUser: admin
mediawikiPassword: "admindamin"
mediawikiEmail: admin@domain.cat
mediawikiName: My Wiki
mediawikiHost: "wiki.domain.cat"
allowEmptyPassword: no

smtpHost: "smtp-relay.email-relay-mngmt"
smtpPort: "25"

persistence:
	enabled: true
	existingClaim: mediawiki

service:
  type: ClusterIP

ingress:
  enabled: true
  ingressClassName: nginx-extranet
  hostname: wiki.domain.cat
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: 256m

mariadb:
  enabled: false
externalDatabase:
  host: database.domain.intranet
  port: 3306
  user: user
  password: password
  database: wiki
```

:::caution
La contraseña debe tener 8 caracteres o más. Si no es correcta, el proceso se quedará atascado en la tarea de instalación.
:::

Si hemos configurado el SMTP, añadimos el acceso.
```bash
vi mediawiki-allow-relay.yaml
```

Abrimos acceso al nuevo servicio en su namespace.
```yaml title="mediawiki-allow-relay.yaml"
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-mediawiki
  namespace: email-relay-mngmt
spec:
  podSelector:
    matchLabels:
      app.kubernetes.io/instance: smtp-relay
  policyTypes:
    - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              kubernetes.io/metadata.name: mediawiki
          podSelector:
            matchLabels:
              app.kubernetes.io/instance: mediawiki
      ports:
        - protocol: TCP
          port: 25
```

Aplicamos todos los cambios.
```bash
kubectl create namespace mediawiki
kubectl apply -f mediawiki-pvc.yaml -n mediawiki
helm install mediawiki bitnami/mediawiki -f mediawiki-values.yaml -n mediawiki
kubectl apply -f mediawiki-allow-relay.yaml
```

## Configuración
Tanto la configuración como los plugins requieren acceso al volumen para modificar y subir nuevos ficheros. Para facilitar el proceso, se crea un pod temporal con acceso al mismo volumen, y en este se instalará cualquier herramienta que necesitemos.

Definimos el pod.
```bash
vi mediawiki-pod.yaml
```

El pod es un nginx con base Debian y acceso root. Esto nos da un pod que se mantiene activo, con capacidad para instalar y acceso al volumen en `/data/mediawiki`.
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: temp
spec:
  containers:
  - name: nginx
    image: nginx
    volumeMounts:
    - name: storage
      mountPath: /data
  volumes:
  - name: storage
    persistentVolumeClaim:
      claimName: mediawiki
```

Desplegamos el pod.
```bash
kubectl apply -f mediawiki-pod.yaml -n mediawiki
```

Una vez desplegamos, accedemos al pod e instalamos cualquier editor.
```bash
kubectl exec -it -n mediawiki temp -- /bin/bash
```

:::warning
Recordar borrarlo tras terminar la configuración.
:::

### LocalSettings.php
El fichero maestro de configuración es el `LocalSettings.php`. Según lo que queramos configurar habrá que ajustar unas partes u otras.

Esta es una lista de links hacia la documentación de los más típicos:
* [Logos](https://www.mediawiki.org/wiki/Manual:$wgLogos)
* [Favicon](https://www.mediawiki.org/wiki/Manual:$wgFavicon)
* [Cambiar extensiones soportadas para subir](https://www.mediawiki.org/wiki/Manual:$wgFileExtensions)
* [Permisos de usuarios](https://www.mediawiki.org/wiki/Manual:$wgGroupPermissions)
* [Iniciar plugins](https://www.mediawiki.org/wiki/Manual:Extension_registration)
* [Plugin: Editor](https://www.mediawiki.org/wiki/Extension:WikiEditor)

:::info
Algunos cambios requieren reiniciar el pod de MediaWiki.
:::

La lista de plugins activos se muestra en la pantalla de versión:
```
https://wiki.domain.cat/wiki/Special:Version
```

:::tip
Algunos plugins requieren actualizar el sistema, por ejemplo, para crear nuevas tablas en la base de datos. Tras activar el plugin se debe acceder al pod de MediaWiki y lanzar el siguiente comando:

`php /opt/bitnami/mediawiki/maintenance/update.php`
:::
