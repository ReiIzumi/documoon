---
id: wordpress
title: WordPress
sidebar_position: 4
---

Los blogs ya no son lo que fueron antaño, pero aun quedamos quienes los utilizamos. De todos, [WordPress](https://wordpress.com/) sigue siendo la principal opción.

He elegido la opción creada por Bitnami para el despliegue ya que WordPress es algo peculiar. Este está pensado para actualizarse a sí mismo, modificando sus datos, así que las actualizaciones desde el propio Helm pueden causar que el blog deje de funcionar. El sistema correcto es instalar un WordPress vacío y migrar los datos del existente hacia este utilizando el plugin preinstalado `All-in-One WP Migration`.

WordPress tiene un sistema propio de exportación e importación, pero únicamente mueve posts y poco más. Este plugin mueve todo.

Este plugin permite exportar todo el sistema (configuración, media, plantillas, ...), pero tiene la pega de que el fichero resultante es muy grande, normalmente superior a lo que Apache, PHP o Ingress pueden gestionar por defecto. Para evitar estos problemas, el plugin tiene infinidad de soluciones, todas de pago. Para solucionar esto se debe ampliar el tamaño de subida de datos, algo que no está documentado en ningún lugar y es lo que convierte en un infierno esta instalación. Hasta que sabes qué valores tocar, entonces es muy sencilla.

## Requisitos
WordPress requiere de [MariaDB](/docs/it/databases/mariadb). En este Helm se desactiva el despliegue de MariaDB y se utiliza un existente.

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/bitnami/wordpress).
:::

Añadimos el repositorio.
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

Definimos el fichero de propiedades.
```bash
vi wordpress-values.yaml
```

El fichero tiene varias secciones.
```yaml title="wordpress-values.yaml"
# Configuración básica WordPress para que realice la preinstalación
wordpressUsername: admin
wordpressPassword: admin
wordpressEmail: admin@domain.cat
wordpressFirstName: Nombre
wordpressLastName: Apellido
wordpressBlogName: Nombre del blog
wordpressScheme: https
allowEmptyPassword: false

# Preconfiguración del plugin WP Mail SMTP
smtpHost: smtp-relay.email-relay-mngmt
smtpPort: 25

# Configuración de PHP para soportar un tamaño superior de subida de datos
extraEnvVars:
  - name: PHP_POST_MAX_SIZE
    value: "512M"
  - name: PHP_UPLOAD_MAX_FILESIZE
    value: "512M"
  - name: PHP_MEMORY_LIMIT
    value: "512M"
  - name: PHP_MAX_EXECUTION_TIME
    value: "0"

# Cambio de recursos requeridos, en un blog pequeño es un desperdicio los requisitos originales
resources:
  requests:
    cpu: 100m
    memory: 52Mi
  limits:
    cpu: 600m
    memory: 1Gi

# Cambio del tipo de servicio ya que se utilizará Ingress
service:
  type: ClusterIP

# Habilitar Ingress hacia la extranet y soporte para subir grandes ficheros
ingress:
  enabled: true
  ingressClassName: nginx-extranet
  hostname: blog.domain.cat
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: 512m

# Volumen en Longhorn para almacenar el WordPress
persistence:
  storageClass: longhorn
  size: 5Gi
  accessMode:
  - ReadWriteMany

# Conexión al MariaDB externo
mariadb:
  enabled: false
externalDatabase:
  host: database.domain.intranet
  port: 3306
  user: user
  password: password
  database: wordpress
```

Desplegamos el servicio.
```bash
helm upgrade --install blog bitnami/wordpress \
  -f wordpress-values.yaml \
  --namespace wordpress --create-namespace
```

WordPress necesita enviar e-mails utilizando el SMTP Relay.
```bash
vi wordpress-allow-relay.yaml
```

Abrimos acceso al nuevo servicio en su namespace.
```yaml title="wordpress-allow-relay.yaml"
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-wordpress
  namespace: email-relay-mngmt
spec:
  podSelector:
    matchLabels:
      app.kubernetes.io/instance: smtp-relay
  policyTypes:
    - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              kubernetes.io/metadata.name: wordpress
          podSelector:
            matchLabels:
              app.kubernetes.io/instance: wordpress
      ports:
        - protocol: TCP
          port: 25
```

Aplicamos los cambios.
```bash
kubectl apply -f wordpress-allow-relay.yaml
```

Tras desplegar, accedemos a la consola administrativa acorde al usuario y contraseña definidos.
```
https://blog.domain.cat/wp-login.php
```

:::warning
Ojo con las actualizaciones por error, que podrían corromper WordPress, y también con borrar el Helm ya que el volumen está asociado a él y será también destruido.
:::

:::tip
Es aconsejable confirmar desde el plugin de SMTP que todo funciona correctamente. Este se quejará de que el dominio utilizado es extraño y quizás no seguro. Esto no es problema ya que es la red de Kubernetes quien se encarga del relay.
:::

## Importación
La instalación nos crea un WordPress totalmente nuevo y funcional, pero si procedemos de uno anterior, habrá que migrar los datos.

Para ello se debe generar un fichero de exportación del viejo WordPress, para ello se utiliza el plugin `All-in-One WP Migration`. En el nuevo viene preinstalado así que se activa y se importa el nuevo fichero.

:::caution
Si importamos, también se modificarán los datos de usuario, título del blog, SMTP, ... así que habrá que reajustarlos al acabar.
:::

:::tip
Si la URL de acceso ha cambiado, los posts seguramente apunten a la vieja y habrá que actualizarlos manualmente.
:::