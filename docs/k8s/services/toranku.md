---
id: toranku
title: Toranku
sidebar_position: 2
---

Muchas aplicaciones organizan sus secciones a través de categorías, para ello requieren un lugar donde gestionar y almacenar estos datos.

Debido a que es una utilidad bastante común, me decidí a construirlo mediante un microservicio separado que cualquier otra aplicación pudiera utilizar. [Toranku](https://gitlab.com/moon-toranku/service) se diseñó para ello.

Los servicios que quieran utilizar las categorías de Toranku requieren autenticarse mediante Open ID Connect. Cada servicio tiene sus propias categorías. Únicamente los servicios con los roles correctos pueden acceder a los servicios de Toranku, evitando accesos indeseados.

:::tip
Toda la información se encuentra en el repositorio de [GitLab](https://gitlab.com/moon-toranku/service) y en el [OpenAPI](https://gitlab.com/moon-toranku/openapi/-/raw/master/reference/Toranku.yaml).
:::

## Requisitos
La autenticación se realiza mediante OpenID Connect, así que se requiere de un Identity Provider.

La autorización se basa en roles que proceden del token recibido en la autenticación, en este caso se utiliza el sistema de roles utilizado por [Keycloak](/docs/k8s/management/keycloak).

:::caution
Varios Identity Provider disponen de roles, pero únicamente se han realizado pruebas con los de Keycloak, así que podría no ser compatible con el resto.
:::

Los datos se almacenan en [PostgreSQL](/docs/it/databases/postgresql).

## Keycloak
Keycloak funciona mediante reinos (Realm), cada uno es independiente del resto, pero puede tener múltiples scopes, roles, clientes, ... Así que la mejor idea suele ser tener un reino para servicios internos como Toranku, y otro para servicios a Internet donde se requiera que un usuario se autentique.

Dentro del reino interno, crearemos los roles y definiremos los clientes que quieran conectar a este u otros servicios.

En el reino, se crean los siguientes roles en `Realm roles`:

| Rol           | Descripción                                                    |
| ------------- | -------------------------------------------------------------- |
| toranku:user  | Servicio con acceso a Toranku                                  |
| toranku:admin | Servicio administrador que puede modificar los datos del resto |

Cada servicio debe tener su propio cliente, estos son gestionados en `Clients`:

| Clave                 | Valor                                                                |
| --------------------- | -------------------------------------------------------------------- |
| Client type           | OpenID Connect                                                       |
| Client ID             | Identificador del servicio utilizado para iniciar sesión en Keycloak |
| Name                  | Nombre del cliente                                                   |
| Client authentication | Enabled                                                              |
| Authentication flow   | Service account roles                                                |

Una vez creado se tienen que ajustar los `Client scopes`:

| Scope   | Assigned type |
| ------- | ------------- |
| profile | Default       |
| roles   | Default       |

:::warning
Keycloak genera un scope especial dedicado para el Client, no es necesario borrarlo.
:::

Esto permite que Toranku reciba los datos mínimos y los roles asociados a la cuenta de sistema.

En `Service account roles` se indican los roles que dispondrá esta cuenta de sistema. Puede tener tanto roles de Toranku como de otros servicios, aunque es recomendable que el nombre de esos servicios sea fácilmente distinguible entre servicios para evitar que un servicio pueda acceder a servicios a los que no debería.

El rol de administrador de Toranku no incluye el de usuario, pero una cuenta de sistema puede tener ambos.

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/toranku/toranku).
:::
Creamos el namespace.
```bash
kubectl create namespace toranku
```

Añadimos el repositorio.
```bash
helm repo add toranku https://moon-toranku.gitlab.io/helmrepo
helm repo update
```

Toranku está diseñado en Java, así que, si el Identity Provider dispone de un certificado auto firmado, necesitaremos indicarle cómo validarlo. Para ello, Java dispone del almacén de certificados `cacerts` donde indicaremos el certificado padre (CA).

Este fichero se debe crear previamente y añadirlo como un secret en el despliegue.

:::tip
El fichero cacerts se puede encontrar en cualquier instalación o imagen con Java, también en la ruta `/cacerts` de Toranku.
:::

Con la herramienta `keytool` de Java se incorporan certificados dentro de este fichero. El comando difiere ligeramente según la versión de la herramienta.

Una vez se dispone del fichero, se crea como un secret.
```
kubectl create secret generic java-cacerts --from-file=cacerts --namespace toranku
```

En este caso se publica Toranku mediante el Ingress de la Intranet, se asigna el `cacerts`, la base de datos y el reino de Keycloak.

```yaml title="toranku-values.yaml"
ingress:
  enabled: true
  className: nginx-intranet
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
  host: toranku.domain.intranet
java:
  cacerts: java-cacerts
  cacertsKey: cacerts
database:
  url: postgresql://serverName:5432/dbName
  username: dbUser
  password: dbPassword
oidc:
  url: https://keycloak.domain.cat/realms/Intranet
```

:::warning
La contraseña de la base de datos es almacenada en un secret.
:::

Desplegamos el servicio.
```bash
helm upgrade --install toranku toranku/toranku \
  -f toranku-values.yaml \
  --namespace toranku
```
