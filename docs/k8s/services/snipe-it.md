---
id: snipe-it
title: Snipe-IT
sidebar_position: 5
---

Para gestionar muchos dispositivos, accesorios, licencias, ... es necesario tener un lugar donde documentarlos. No hay una forma perfecta de organizar todo esto, así que depende que si queremos más libertad o ajustarnos a algo más específico.

[Snipe-IT](https://snipeitapp.com/) tiene cierto margen de configuración para gestionar varios componentes, aunque en mi caso echo de menos poder añadir notas, secciones específicas para cada componente o incluso poder ver un organigrama de la red acorde a sus IPs.

## Requisitos
Snipe-IT requiere de [MariaDB](/docs/it/databases/mariadb). Se utilizará el existente en vez de que proviene el Helm.

El campo de configuración `config.snipeit.key` necesita un fichero de claves especial. Para generarlo, lo más sencillo es lanzar el comando en un simple docker como el desplegado en el [GitLab Runner](/docs/k8s/gitlab-runner).
```bash
docker run -it --rm snipe/snipe-it php artisan key:generate --show
```

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/t3n/snipeit).
:::

Añadimos el repositorio.
```bash
helm repo add t3n https://storage.googleapis.com/t3n-helm-charts
helm repo update
```

Definimos el fichero de propiedades.
```bash
vi snipeit-values.yaml
```

Requiere los datos de MariaDB, SMTP, Ingress, volumen y configuración propia.
```yaml title="snipeit-values.yaml"
config:
  mysql:
    externalDatabase:
      user: "snipeit_user"
      pass: "snipeit_password"
      name: "snipeit"
      host: database.domain.intranet
      port: 3306

  snipeit:
    env: production
    url: https://snipeit.domain.cat
    key: "base64:..."
    timezone: Europe/Madrid

mysql:
  enabled: false

persistence:
  enabled: true
  accessMode: ReadWriteMany
  storageClass: "longhorn"
  size: 2Gi

ingress:
  enabled: true
  className: "nginx-extranet"
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
  hosts:
    - snipeit.domain.cat
```

Desplegamos el servicio.
```bash
helm upgrade --install snipeit t3n/snipeit \
  -f snipeit-values.yaml \
  --namespace snipeit --create-namespace
```

:::tip
En este caso no he habilitado el SMTP ya que no he considerado que aporte demasiado a no ser que se utilicen las herramientas para pedir dispositivos, algo que no es necesario en una red doméstica.
:::
