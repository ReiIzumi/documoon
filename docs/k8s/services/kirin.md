---
id: kirin
title: Kirin
sidebar_position: 3
---

Gestionar los gastos domésticos no es tarea sencilla, entre gastos fijos, compras mensuales, ahorros, ... al final es una cantidad enorme de información que se debe organizar correctamente para lograr terminar el año en positivo.

Para poder gestionar toda esta información construí a [Kirin](https://gitlab.com/moon-kirin/service), un proyecto que ha ido evolucionando para adaptarse a nuevas necesidades.

Este está construido como un servicio REST que permite almacenar datos con total libertad.

:::tip
Toda la información se encuentra en el repositorio de [GitLab](https://gitlab.com/moon-kirin/service) y en el [OpenAPI](https://gitlab.com/moon-kirin/openapi/-/raw/master/reference/Kirin.yaml).
:::

## Requisitos
La autenticación se realiza mediante OpenID Connect, cada usuario debe estar registrado en el Identity Provider y disponer del rol adecuado para acceder a Kirin.

:::caution
Varios Identity Provider disponen de roles, pero únicamente se han realizado pruebas con los de Keycloak, así que podría no ser compatible con el resto.
:::

Las diferentes secciones permiten categorizar los datos, para ello Kirin utiliza los datos de [Toranku](/docs/k8s/services/toranku).

Los datos se almacenan en [PostgreSQL](/docs/it/databases/postgresql).

## Keycloak
Para acceder a Kirin, el usuario deberá tener el rol correcto:

| Rol        | Descripción                 |
| ---------- | --------------------------- |
| kirin:user | Servicio con acceso a Kirin |

Además, requiere un cliente específico para acceder a Toranku, [explicado en su sección](/docs/k8s/services/toranku#keycloak).

## Despliegue
:::info
Más detalles en [Artifact Hub](https://artifacthub.io/packages/helm/kirin/kirin).
:::
Creamos el namespace.
```bash
kubectl create namespace kirin
```

Añadimos el repositorio.
```bash
helm repo add kirin https://moon-kirin.gitlab.io/helmrepo
helm repo update
```

Kirin está diseñado en Java, así que, si el Identity Provider o Toranku están bajo un certificado auto firmado, necesitaremos indicarle cómo validarlo. Para ello, Java dispone del almacén de certificados `cacerts` donde indicaremos el certificado padre (CA).

Este fichero se debe crear previamente y añadirlo como un secret en el despliegue.

:::tip
El fichero cacerts se puede encontrar en cualquier instalación o imagen con Java, también en la ruta `/cacerts` de Kirin.
:::

Con la herramienta `keytool` de Java se incorporan certificados dentro de este fichero. El comando difiere ligeramente según la versión de la herramienta.

Una vez se dispone del fichero, se crea como un secret.
```
kubectl create secret generic java-cacerts --from-file=cacerts --namespace kirin
```

La configuración requiere exponer el servicio en la Extranet, asignar el `cacerts`, la base de datos y los datos para conectar a Toranku.

```yaml title="kirin-values.yaml"
ingress:
  enabled: true
  className: nginx-extranet
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
  host: kirin.domain.intranet
java:
  cacerts: java-cacerts
  cacertsKey: cacerts
database:
  url: jdbc:postgresql://postgres.domain.intranet:5432/kirin
  username: dbUser
  password: dbPassword
oidc:
  url: https://keycloak.domain.cat/realms/Extranet
toranku:
  url: https://toranku.domain.intranet
  oidcUrl: https://keycloak.domain.cat/realms/Intranet
  oidcClientId: ClientId
  oidcSecret: Secret
```

:::warning
La contraseña de la base de datos y el secret de OIDC son almacenados en diferentes secrets.
:::

Desplegamos el servicio.
```bash
helm upgrade --install kirin kirin/kirin \
  -f kirin-values.yaml \
  --namespace kirin
```
