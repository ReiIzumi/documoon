---
id: introduction
title: Introducción
sidebar_position: 1
---

## K8s project
Mucho ha llovido desde el inicio del [Swarm Project](/docs/swarm/introduction), y aunque la idea era engrosar sus servicios, la cruda realidad no lo ha permitido.

Por las malas he aprendido la dificultad que tiene mantener una red con Docker Swarm y GlusterFS, llegando a un fallo masivo que la dejó inoperativa durante días de investigación. Y mejor no hablamos de ElasticSearch.

Además, en el mundo actual Docker Swarm está perdiendo terreno, con lo que en un futuro cercano podría acabar en el olvido a favor de la gran variedad de opciones que están apareciendo alrededor de Kubernetes.

Para escoger la base del nuevo clúster, esta debe funcionar acorde a:
- Gestor de contenedores auto escalables
- Ser OpenSource y gratuito
- Tener una comunidad suficiente grande como para poder resolver errores
- Funcionar sobre un clúster de entre 2 y 4 servidores
- Ser desplegado en bare-metal
- Tener un roadmap a futuro lo suficiente extenso como para no caer en el olvido en al menos 5 años
- Si es posible, que los conocimientos obtenidos sean utilizables en clústeres empresariales
- Si permite tecnología x64 y ARM en el mismo clúster, mejor

Con estas reglas tenemos [Kubernetes](https://kubernetes.io/es/) (K8s) y prácticamente todos sus hermanos pequeños como [MicroK8s](https://microk8s.io/) o [k3s](https://k3s.io/).

Después de actualizar mis conocimientos de Swarm hacia Kubernetes, de buscar diferencias y probarlos, me he decantado por la versión original de Kubernetes.

Uno de los problemas sobre K3s y otros tantos es que no son totalmente compatibles con Kubernetes, lo que no me hacía demasiada gracia. En esto, MicroK8s da muy buenos resultados, extremadamente sencillo de desplegar, incluso en clúster, y permite de serie utilizar el maestro como otro nodo, función interesante en una red doméstica. Aun así, he optado por la versión principal y luchar de tú a tú contra todo.

Y así es como empieza el `K8s Project`.

:::caution
Este proyecto no intenta ser una guía de Kubernetes, se requiere de conocimientos básicos de Kubernetes y experiencia en el manejo de Linux y redes.
:::

## Especificaciones
Este proyecto únicamente tiene en cuenta los servidores de Kubernetes y los servicios que serán desplegados en él. Los servicios básicos están en el [Rasp Project](/docs/rasp/introduction), las copias de seguridad se almacenan en un NAS mediante carpetas compartidas con `NFS` y los servicios sin contenedor están en máquinas virtuales explicadas en la sección de [IT](/docs/it/introduction).

Todos los servidores se despliegan con [Ubuntu 22.04.1 LTS](/docs/it/ubuntu22lts).

:::tip
Un clúster empresarial debe utilizar al menos 3 Maestros y 3 Nodos para mantener el clúster operativo sin peligro. En uso doméstico no es viable así que tendremos un `mini-clúster`.
:::

Debido a requerimientos en el sistema de volúmenes, el proyecto consistirá en 4 máquinas virtuales, una como Máster y tres para Nodos.

| Nombre | CPU | RAM   | Disk1  | Disk2   |
| ------ | :-: | ----: | -----: | ------: |
| Master | 4   | 6 GiB | 50 GiB |         |
| Node1  | 4   | 8 GiB | 50 GiB | 100 GiB |
| Node2  | 4   | 8 GiB | 50 GiB | 100 GiB |
| Node3  | 4   | 8 GiB | 50 GiB | 100 GiB |

El primer disco es utilizado para el sistema incluyendo Kubernetes, el segundo para datos que son sincronizados entre los nodos (volúmenes).

:::important
El nombre de cada servidor se puede resolver mediante DNS.
:::

## Arquitectura
Similar al `Swarm Project`, toda la plataforma se desplegará sobre bare-metal. Esto conlleva un problema en la asignación de IP externas y la compartición o sincronización de volúmenes en el clúster. Todo ello se irá resolviendo con cada servicio desplegado.

He dividido el proyecto en tres capas:
- **Sistema**: Servicios esenciales para el funcionamiento de la plataforma
- **Administración**: Administración y monitorización tanto de la plataforma como de sus servicios
- **Servicios**: Resto de aplicaciones

Las bases son muy similares a las que se utilizaron para Swarm, pero algunas piezas se han cambiado por otras debido a la mala experiencia obtenida (¡te miro a ti, ElasticSearch y tu habilidad para suicidarte!).

## Namespaces
Kubernetes puede separar los servicios en diferentes "cajas" llamadas `namespace`. Cada una puede tener una configuración diferente, incluso a nivel de seguridad y recursos.

Mucha gente opta por tener un gran clúster y dividir los entornos en el mismo, pero en mi caso mantendré dos entornos iguales y separados, uno para test y otro de producción. Esto me permite hacer pruebas que podrían "romper" el clúster, sin poner en peligro el entorno de producción.

Sigo las siguientes reglas:
- Los servicios del mismo tipo o zona de negocio compartirán namespace
- Los servicios de sistema tendrán el sufijo `-system`
- Los servicios de administración tendrán el sufijo `-mngmt`
- El resto de los servicios no tendrán sufijos

## Notas
Durante las pruebas para aprender y poner en marcha este proyecto, la versión de Kubernetes y de varios de los servicios utilizados por este proyecto han cambiado. Esto ha provocado que mis notas y pruebas dejaran de funcionar en la gran mayoría de casos debido a configuraciones que en pocos meses ya no eran aceptadas, y que eran las únicas documentadas o por errores de compatibilidad (normalmente por funciones obsoletas o eliminadas) entre unos y otros servicios.

En el mundo Kubernetes, una documentación sin actualizar durante más de un año seguramente ya no funcione. Incluso muchos servicios, aun sabiendo la lista de funciones obsoletas y la fecha en que serán eliminadas, hasta que esto no ocurre no empiezan a pensar en hacer la actualización.

La teoría dice que Kubernetes debe actualizarse anualmente, cada 2 años en el peor de los casos. Siendo realista, cada actualización va a requerir comprobar servicio a servicio si sigue funcionando, y si no es así, quizás requieras meses de espera o de arreglos manualmente, o incluso abandonar un servicio vital y explorar uno nuevo.

Debido a todo esto, esta serie de artículos está diseñada para Kubernetes v1.25 y podría fallar tras la aparición del a siguiente versión.

Considero esto un error en el funcionamiento de Kubernetes y también del ecosistema diseñado para ello, pero poco o nada podemos hacer contra ello.
