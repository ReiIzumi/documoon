---
id: update
title: Actualización de servicios
sidebar_position: 6
---

Es recomendable mantener todos los servicios actualizados.

Este es un resumen de los comandos para varios de los servicios explicados.

## Actualizar los repositorios
```bash
helm repo update
```

## Servicios
```bash
# MetalLB
helm upgrade metallb metallb/metallb -n metallb-system

# Ingress
helm get values ingress-nginx-intranet -n ingress-intranet-system > upgrade.yaml
helm upgrade ingress-nginx-intranet ingress-nginx/ingress-nginx --values upgrade.yaml -n ingress-intranet-system

helm get values ingress-nginx-extranet -n ingress-extranet-system > upgrade.yaml
helm upgrade ingress-nginx-extranet ingress-nginx/ingress-nginx --values upgrade.yaml -n ingress-extranet-system

# Longhorn
helm get values longhorn -n longhorn-system > upgrade.yaml
helm upgrade longhorn longhorn/longhorn --values upgrade.yaml -n longhorn-system

# Kubernetes dashboard
helm get values kubernetes-dashboard -n kubernetes-dashboard-system > upgrade.yaml
helm upgrade kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --values upgrade.yaml -n kubernetes-dashboard-system

helm get values kubernetes-dashboard -n kubernetes-dashboard-system > upgrade.yaml
helm upgrade kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --values upgrade.yaml -n kubernetes-dashboard-system

# Monitor
helm get values kube-prom -n monitoring-system > upgrade.yaml
helm upgrade kube-prom prometheus-community/kube-prometheus-stack --values upgrade.yaml -n monitoring-system
# >> Revisar cambios manuales en:
# https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack#upgrading-an-existing-release-to-a-new-major-version

# Nexus Repository
helm get values nexus-rm -n nexus-repository-mngmt > upgrade.yaml
helm upgrade nexus-rm sonatype/nexus-repository-manager --values upgrade.yaml -n nexus-repository-mngmt

# Keycloak
helm get values keycloak -n keycloak-mngmt > upgrade.yaml
helm upgrade keycloak bitnami/keycloak --values upgrade.yaml -n keycloak-mngmt
```

## Rollback
```bash
# Volver a versión 14
helm rollback nexus-rm 14 -n nexus-repository-mngmt
```
