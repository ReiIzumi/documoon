#!/bin/sh
#
# CopyLeft moon.cat
# This work is licensed under a Creative Commons Attribution 4.0 International License.
#
# Author: Rei Izumi - moon.cat - blog.moon.cat - 2017
#
#
#Identifier of the backup, is used in the filename of the compressed file and the mail
IDENTIFIER_NAME=Apache

#Only with mail enabled, used to identify the environment and server in the subject
SERVER_ENVIRONMENT=PRO
SERVER_NAME=ServerName

#Source folder to create backup
SOURCE_FOLDER=/source_folder

#Backup destination folder
BACKUP_FOLDER=/target_folder/$IDENTIFIER_NAME
#Number of old backups to save (value between 1 and 99)
BACKUP_NUMBER=5
#true: Compress each file and folder into different files
#false: Compress the folder into a single file
BACKUP_SEPARATE_FILE=false

#If MAIL_TO is empty, no mails will be sent
MAIL_TO=admin@domain.cat
#Name of the user sending the mail
MAIL_SENDER="Backup <backup@domain.cat>"
MAIL_SUBJECT_COMPLETED="Backup $SERVER_ENVIRONMENT $SERVER_NAME $IDENTIFIER_NAME completed"
MAIL_SUBJECT_ERROR="Backup $SERVER_ENVIRONMENT $SERVER_NAME $IDENTIFIER_NAME ERROR"
#Error will always be sent, this enable sending mail when everything has worked correctly
MAIL_SEND_COMPLETED=true

#Date format used in the filename
DATE=`date +_%Y-%m-%d_%H-%M-%S`

########## ########## ##########
#        INTERNAL VARS         #
########## ########## ##########

FALLBACK=false

########## ########## ##########
#          FUNCTIONS           #
########## ########## ##########
function sendMail
{
	if [[ ! -z "$MAIL_TO" ]]; then
		printf "$2" | mail -s "$1" -r "$MAIL_SENDER" $MAIL_TO
	fi
}


function errorExit
{
        echo $1
	sendMail "$MAIL_SUBJECT_ERROR" "$1"
        exit 1
}

_mask=
function createMask
{
	_mask=$1
	if [ ${#1} -eq 1 ]; then
        	_mask=0$1
	fi
	return $mask
}

function compress
{
	tar zcf $1 $2 2>/dev/null || FALLBACK=true
	if [ $FALLBACK = true ]; then
		errorExit "Failed creating backup in $BACKUP_FOLDER/01/$IDENTIFIER_NAME$DATE.tar.gz"
	fi
}
########## ########## ##########
#           PROCESS            #
########## ########## ##########

#Check exist source
if [ ! -d "$SOURCE_FOLDER" ]; then
	errorExit "Source folder not exist: $SOURCE_FOLDER"
fi

#Creata target folder
if [ ! -d "$BACKUP_FOLDER" ]; then
	mkdir $BACKUP_FOLDER || FALLBACK=true 
fi
if [ $FALLBACK = true ]; then
	errorExit "Failed creating backup folder: $BACKUP_FOLDER"
fi

#Assign name for last folder to delete
createMask $BACKUP_NUMBER
finalFolder=$_mask

#Rotate old backups
_last=
for ((x=$BACKUP_NUMBER; x>0; x--))
do
	createMask $x
	f=$_mask
	
	if [ -d "$BACKUP_FOLDER/$f" ]; then
		if [ $f -eq $finalFolder ]; then
			#Delete last folder
			rm -rf $BACKUP_FOLDER/$f
		else 
			#Move to next old folder
			mv $BACKUP_FOLDER/$f $BACKUP_FOLDER/$_last
		fi
	fi
	_last=$f
done

#Create new folder
mkdir $BACKUP_FOLDER/01

#Add backup into new folder
mailMessage=
if [ $BACKUP_SEPARATE_FILE = true ]; then
	cd $SOURCE_FOLDER
	mailMessage="Target files:\n"
	for i in *; do
		echo "Compressing $i"
		compress $BACKUP_FOLDER/01/$i$DATE.tar.gz $SOURCE_FOLDER/$i
		mailMessage="$mailMessage$BACKUP_FOLDER/01/$i$DATE.tar.gz\n"
	done
else 
	compress $BACKUP_FOLDER/01/$IDENTIFIER_NAME$DATE.tar.gz $SOURCE_FOLDER
	mailMessage="Target file: $BACKUP_FOLDER/01/$IDENTIFIER_NAME$DATE.tar.gz"
fi

#Send mail
if [ $MAIL_SEND_COMPLETED = true ]; then
	sendMail "$MAIL_SUBJECT_COMPLETED" "$mailMessage"
fi
printf "$mailMessage"
echo "Backup completed"
