// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Moon.cat',
  tagline: 'Aquí encontrarás la lista de proyectos que se han acabado con éxito.',
  url: 'https://www.moon.cat',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Moon.cat',
  projectName: 'Moon.cat',
  i18n: {
    defaultLocale: 'es',
    locales: ['es', 'ca'],
    localeConfigs: {
      es: {
        label: 'Español',
        htmlLang: 'es-ES',
        path: 'es',
      },
      ca: {
        label: 'Català',
        htmlLang: 'ca-ES',
        path: 'ca',
      }
    },
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/ReiIzumi/documoon/-/blob/master/',
        },
        blog: {
          showReadingTime: true,
          blogSidebarCount: 20,
          onUntruncatedBlogPosts: 'ignore',
          editUrl:
            'https://gitlab.com/ReiIzumi/documoon/-/blob/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      docs: {
        sidebar: {
          hideable: true,
          autoCollapseCategories: true,
        },
      },
      announcementBar: {
        id: 'cookies',
        content:
          'Esta web hace todo lo posible por no utilizar <a target="_blank" rel="noopener noreferrer" href="https://es.wikipedia.org/wiki/Cookie_(informática)">cookies</a>, pero no siempre es posible.',
        backgroundColor: '#fafbfc',
        textColor: '#091E42',
      },
      navbar: {
        title: 'Moon.cat',
        logo: {
          alt: 'Moon.cat Logo',
          src: 'img/logo.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'general/about',
            position: 'left',
            label: 'General',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            type: 'doc',
            docId: 'it/introduction',
            position: 'left',
            label: 'IT',
          },
          {
            type: 'doc',
            docId: 'sora/introduction',
            position: 'left',
            label: 'Sora Project',
          },
          {
            type: 'doc',
            docId: 'rasp/introduction',
            position: 'left',
            label: 'Rasp Project',
          },
          {
            type: 'doc',
            docId: 'k8s/introduction',
            position: 'left',
            label: 'K8s Project',
          },
          {
            type: 'doc',
            docId: 'swarm/introduction',
            position: 'left',
            label: 'Swarm Project',
          },
          {
            type: 'localeDropdown',
            position: 'right',
          },
          {
            href: 'https://gitlab.com/ReiIzumi',
            position: 'right',
            className: 'header-gitlab-link',
            'aria-label': 'GitLab',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Webs',
            items: [
              {
                label: 'Blog',
                href: '/blog'
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/ReiIzumi'
              },
              {
                label: 'Docker Hub',
                href: 'https://hub.docker.com/u/reiizumi'
              }
            ],
          },
          {
            title: 'Proyectos',
            items: [
              {
                label: 'Libro',
                to: '/docs/general/book',
              }
            ],
          },
          {
            title: 'Otros',
            items: [
              {
                label: 'Sobre mí',
                href: '/docs/general/about'
              },
              {
                label: '10 años creando software',
                href: '/docs/general/diez-anos-creando-software'
              }
            ],
          },
        ],
        copyright: `
      <ul>
        <li>
          <a rel="license" href="https://creativecommons.org/licenses/by-nc/4.0/deed.es_ES" target="_blank"><img alt="Licencia de Creative Commons" style="border-width:0" src="/img/88x31.png" /></a><br />
        </li>
        <li>
          Copyleft © ${new Date().getFullYear()} - Moon.cat - rei.izumi [arroba] moon [punto] cat<br />
          Este obra está bajo una licencia de Creative Commons (CC BY-NC 4.0).
        </li>
      </ul>`,
      },
      prism: {
        additionalLanguages: ['json', 'csv', 'docker', 'java', 'mongodb', 'vim', 'bash', 'hcl', 'yaml'],
      },
    }),
};

module.exports = config;
