# Website

Esta web ha sido creada a partir de [Docusaurus 2](https://docusaurus.io/), uno de tantos generadores de documentación, pero que permite también crear páginas web y blogs.

## Desarrollo

Para cualquier modificación, ejecutar en local con el servidor de desarrollo.

```console
npm install
npm run start
```

Al realizar cambios que requieran de multi idioma, se debe actualizar e iniciar en ese idioma.
```console
npm run write-translations -- --locale ca
npm run start -- --locale ca
```

### Build

Al terminar un cambio en desarrollo, se debe confirmar que funciona el ``build``, algunos cambios son compatibles con el servidor de desarrollo, pero no con la construcción final del proyecto estático.

Este también es el utilizado para su despliegue.

```console
npm run build
```

## Container

El servicio se despliega en un contenedor.

```console
docker build -t moon/website .
docker run -d --name website -p 80:80 moon/website
```
